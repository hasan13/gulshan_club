import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/jsonModels/app_version_response.dart';
import 'package:gulshan_club_app/jsonModels/banquet/banquet_response.dart';
import 'package:gulshan_club_app/jsonModels/login_response.dart';
import 'package:gulshan_club_app/jsonModels/message_list_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';

import 'package:http/http.dart' as http;

import '../jsonModels/current_user_info_response.dart';

class Resources {
  static LoginResponse? currentUser;
  static AppUser? appUser;
  static dynamic currentUserData;
  static List<String?> bannerImageList = [];
  static bool? loggedIn;
  static bool? isSearchOn = false;
  static int cartLength = 0;
  static var cartData;
  static String? mobileNo;

  static String? selectedRecipientID;
  static String? idForInboxDetails;

  static Messages? selectedRecipient;
  static Member? selectedMember;
  static String? selectedUserIDForPayment = "";

  static int? myUserID;

  static String? deviceId = "";

  static int notificationItemCount = 0;

  static var selectedHallName = "";

  static HallDatum? selectedHall;

  static AppVersionResponse? appVersionResponse;

  static String? localVersion;

  static Member? selectedMemberForDetails;


  static void showSheet(
      BuildContext context, {
         required Widget child,
         required VoidCallback onClicked,
      }) =>
      showCupertinoModalPopup(
        context: context,
        builder: (context) => CupertinoActionSheet(
          actions: [
            child,
          ],
          cancelButton: CupertinoActionSheetAction(
            child: Text('Done'),
            onPressed: onClicked,
          ),
        ),
      );

  static void showSnackBar(BuildContext context, String text) {
    final snackBar = SnackBar(
      content: Text(text, style: TextStyle(fontSize: 24)),
    );

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(snackBar);
  }

  static void showToast(String message,Color backgroundColor){
    Fluttertoast.showToast(
        msg: message,
        textColor: Colors.white,
        backgroundColor: backgroundColor,
        gravity: ToastGravity.SNACKBAR,
        toastLength: Toast.LENGTH_LONG);
  }

  static void showAlert(String message, BuildContext context, bool goToParent){
      // set up the button
      Widget okButton = ElevatedButton(
        child: Text("OK"),
        onPressed: () {
          Navigator.pop(context,true);
          if(goToParent == true){
            Navigator.pop(context,true);

          }

        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Center(
            child: SvgPicture.asset(
              'assets/icons/ic_logo.svg',
              width: 80.0,
              height: 65,
            )),
        content: Text(message),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
  }

  static moveTo(int index, BuildContext context) {
    switch (index) {
      case 0:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/building_directory', (route) => false);

        }
        break;

      case 1:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/profile', (route) => false);
          break;

        }
      case 2:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/members_directory', (route) => false);
          break;

        }

      case 3:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/notice', (route) => false);
        }
        break;

      case 4:
        {
          // Navigator.pushNamedAndRemoveUntil(context, '/building_directory', (route) => false);
        }
        break;
      default:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/building_directory', (route) => false);

        }
        break;
    }
  }

  static Future<void> navigateTo(BuildContext context, String routeName) async {

    //await Navigator.pushNamed(context, routeName);
    await Navigator.pushNamed(
        context, routeName);
  }
  static Widget setBottomDrawer(BuildContext context) {
    return new Theme(
      data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          canvasColor: themeBlackColor,
          // sets the active color of the `BottomNavigationBar` if `Brightness` is light
          primaryColor: Colors.red,
          textTheme: Theme.of(context).textTheme.copyWith(caption: new TextStyle(color: Colors.yellow))), // sets the inactive color of the `BottomNavigationBar`
      child: Container(
        height: 80,
        padding: EdgeInsets.all(8),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
                onTap: () {
                  Resources.moveTo(0, context);
                },
                child: Icon(
                  Icons.home,
                  color: themeGreyColor,
                  size: 40,
                )),

            // GestureDetector(
            //   onTap: () {
            //     Resources.moveTo(1, context);
            //   },
            //   child: SvgPicture.asset(
            //     "assets/icons/ic_facilities.svg",
            //     color: themeGreyColor,
            //     width: 26,
            //     height: 26,
            //   ),
            // ),
            GestureDetector(
              onTap: () {
                Resources.moveTo(1, context);
              },
              child: SvgPicture.asset(
                "assets/icons/ic_profile.svg",
                color: themeGreyColor,
                width: 26,
                height: 26,
              ),
            ),
            GestureDetector(
              onTap: () {
                Resources.moveTo(2, context);
              },
              child: SvgPicture.asset(
                "assets/icons/ic_memeber_director.svg",
                color: themeGreyColor,
                width: 26,
                height: 26,
              ),
            ),
            GestureDetector(
              onTap: () {
                Resources.moveTo(3, context);
              },
              child: SvgPicture.asset(
                "assets/icons/ic_notice_board.svg",
                color: themeGreyColor,
                width: 26,
                height: 26,
              ),
            ),
          ],
        ),
      ),
    );
  }

  // static Future<int> calculateCartItems() async{
  //
  //   final value  = await http.get(
  //       NetworkManager.baseUrl + "/api/product/card-product-info",
  //       headers: {
  //         "Authorization": "Bearer ${Resources.currentUser.data
  //             .accessToken}"
  //       }
  //   ).timeout(Duration(seconds: 30));
  //   print("status code Token:${value.statusCode}");
  //   print(value.body);
  //   if(value.statusCode==200) {
  //     final data = json.decode(value.body);
  //
  //     final cartData = cartDataFromJson(value.body);
  //     print("cartData.data.length: ");
  //     print(cartData.cart.cardItems.length);
  //
  //     return cartData.cart.cardItems.length;
  //   }else{
  //     return 0;
  //   }
  //
  //   //navigateToSignInPage(context);
  //   print('YAY! Free cute dog pics!');
  //
  //
  // }

}
