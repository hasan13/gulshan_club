class Avengers {
  String name;
  String weapon;

  Avengers({required this.name, required this.weapon});

  static List<Avengers> getAvengers() {
    return <Avengers>[
      Avengers(name: "Captain America", weapon: "Shield"),
      Avengers(name: "Thor", weapon: "Mjolnir"),
      Avengers(name: "Spiderman", weapon: "Web Shooters"),
      Avengers(name: "Doctor Strange ", weapon: "Eye Of Agamotto"),

      Avengers(name: "Captain America", weapon: "Shield"),
      Avengers(name: "Thor", weapon: "Mjolnir"),
      Avengers(name: "Spiderman", weapon: "Web Shooters"),
      Avengers(name: "Doctor Strange ", weapon: "Eye Of Agamotto"),

      Avengers(name: "Captain America", weapon: "Shield"),
      Avengers(name: "Thor", weapon: "Mjolnir"),
      Avengers(name: "Spiderman", weapon: "Web Shooters"),
      Avengers(name: "Doctor Strange ", weapon: "Eye Of Agamotto"),

    ];
  }
}