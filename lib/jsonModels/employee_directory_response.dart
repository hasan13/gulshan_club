// To parse this JSON data, do
//
//     final employeeDirectoryResponse = employeeDirectoryResponseFromJson(jsonString);

import 'dart:convert';

EmployeeDirectoryResponse employeeDirectoryResponseFromJson(String str) => EmployeeDirectoryResponse.fromJson(json.decode(str));

String employeeDirectoryResponseToJson(EmployeeDirectoryResponse data) => json.encode(data.toJson());

class EmployeeDirectoryResponse {
  EmployeeDirectoryResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  dynamic next;
  dynamic previous;
  List<Employee>? results;

  factory EmployeeDirectoryResponse.fromJson(Map<String, dynamic> json) => EmployeeDirectoryResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Employee>.from(json["results"].map((x) => Employee.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Employee {
  Employee({
    this.id,
    this.stuffName,
    this.designationGroup,
    this.designation,
    this.mobileNumberPrimary,
    this.imageMedium,
    this.imageThumbnail,
  });

  int? id;
  String? stuffName;
  String? designationGroup;
  String? designation;
  String? mobileNumberPrimary;
  String? imageMedium;
  String? imageThumbnail;

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
    id: json["id"] == null ? null : json["id"],
    stuffName: json["stuff_name"] == null ? null : json["stuff_name"],
    designationGroup: json["designation_group"] == null ? null : json["designation_group"],
    designation: json["designation"] == null ? null : json["designation"],
    mobileNumberPrimary: json["mobile_number_primary"] == null ? null : json["mobile_number_primary"],
    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
    imageThumbnail: json["image_thumbnail"] == null ? null : json["image_thumbnail"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "stuff_name": stuffName == null ? null : stuffName,
    "designation_group": designationGroup == null ? null : designationGroup,
    "designation": designation == null ? null : designation,
    "mobile_number_primary": mobileNumberPrimary == null ? null : mobileNumberPrimary,
    "image_medium": imageMedium == null ? null : imageMedium,
    "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
  };
}
