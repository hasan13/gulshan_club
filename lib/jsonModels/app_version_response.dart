// To parse this JSON data, do
//
//     final appVersionResponse = appVersionResponseFromJson(jsonString?);

import 'dart:convert';

AppVersionResponse appVersionResponseFromJson(String str) => AppVersionResponse.fromJson(json.decode(str));

String appVersionResponseToJson(AppVersionResponse data) => json.encode(data.toJson());

class AppVersionResponse {
  AppVersionResponse({
    this.status,
    this.data,
  });

  bool? status;
  Data? data;

  factory AppVersionResponse.fromJson(Map<String, dynamic> json) => AppVersionResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class Data {
  Data({
    this.android,
    this.ios,
  });

  String? android;
  String? ios;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    android: json["android"] == null ? null : json["android"],
    ios: json["ios"] == null ? null : json["ios"],
  );

  Map<String, dynamic> toJson() => {
    "android": android == null ? null : android,
    "ios": ios == null ? null : ios,
  };
}
