// To parse this JSON data, do
//
//     final productListResponse = productListResponseFromJson(jsonString?);

import 'dart:convert';

ProductListResponse productListResponseFromJson(String str) => ProductListResponse.fromJson(json.decode(str));

String productListResponseToJson(ProductListResponse data) => json.encode(data.toJson());

class ProductListResponse {
  ProductListResponse({
    this.status,
    this.data,
  });

  bool? status;
  Data? data;

  factory ProductListResponse.fromJson(Map<String, dynamic> json) => ProductListResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class Data {
  Data({
    this.next,
    this.previous,
    this.count,
    this.results,
  });

  String? next;
  String? previous;
  int? count;
  List<Products>? results;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    next: json["next"] == null ? null : json["next"],
    previous: json["previous"] == null ? null : json["previous"],
    count: json["count"] == null ? null : json["count"],
    results: json["results"] == null ? null : List<Products>.from(json["results"].map((x) => Products.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "next": next == null ? null : next,
    "previous": previous == null ? null : previous,
    "count": count == null ? null : count,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Products {
  Products({
    this.id,
    this.name,
    this.price,
    this.category,
    this.type,
    this.restaurant,
    this.unit,
    this.image,this.remarks
  });

  int? id;
  String? name;
  double? price;
  int? category;
  String? type;
  String? restaurant;
  String? unit;
  String? image;
  String? remarks;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    price: json["price"] == null ? null : json["price"],
    category: json["category"] == null ? null : json["category"],
    type: json["type"] == null ? null : json["type"],
    restaurant: json["restaurant"] == null ? null : json["restaurant"],
    unit: json["unit"] == null ? null : json["unit"],
    image: json["image"] == null ? null : json["image"],
    remarks: json["remarks"] == null ? null : json["remarks"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "price": price == null ? null : price,
    "category": category == null ? null : category,
    "type": type == null ? null : type,
    "restaurant": restaurant == null ? null : restaurant,
    "unit": unit == null ? null : unit,
    "image": image == null ? null : image,
    "remarks": remarks == null ? null : remarks,
  };
}

