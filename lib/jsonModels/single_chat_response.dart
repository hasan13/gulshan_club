// To parse this JSON data, do
//
//     final singleChatListResponse = singleChatListResponseFromJson(jsonString?);

import 'dart:convert';

import 'message_list_response.dart';

SingleChatListResponse singleChatListResponseFromJson(String str) => SingleChatListResponse.fromJson(json.decode(str));

String singleChatListResponseToJson(SingleChatListResponse data) => json.encode(data.toJson());

class SingleChatListResponse {
  SingleChatListResponse({
    this.status,
    this.data,
  });

  bool? status;
  List<Chats>? data;

  factory SingleChatListResponse.fromJson(Map<String, dynamic> json) => SingleChatListResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<Chats>.from(json["data"].map((x) => Chats.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class Chats {
  Chats({
    this.id,
    this.subject,
    this.sender,
    this.recipient,
    this.parentMsg,
    this.sentAt,
    this.readAt,
    this.repliedAt,
  });

  int? id;
  String? subject;
  Recipient? sender;
  Recipient? recipient;
  int? parentMsg;
  String? sentAt;
  String? readAt;
  String? repliedAt;

  factory Chats.fromJson(Map<String, dynamic> json) => Chats(
    id: json["id"] == null ? null : json["id"],
    subject: json["subject"] == null ? null : json["subject"],
    sender: json["sender"] == null ? null : Recipient.fromJson(json["sender"]),
    recipient: json["recipient"] == null ? null : Recipient.fromJson(json["recipient"]),
    parentMsg: json["parent_msg"] == null ? null : json["parent_msg"],
    sentAt: json["sent_at"] == null ? null : json["sent_at"],
    readAt: json["read_at"] == null ? null : json["read_at"],
    repliedAt: json["replied_at"] == null ? null : json["replied_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "subject": subject == null ? null : subject,
    "sender": sender == null ? null : sender!.toJson(),
    "recipient": recipient == null ? null : recipient!.toJson(),
    "parent_msg": parentMsg == null ? null : parentMsg,
    "sent_at": sentAt == null ? null : sentAt,
    "read_at": readAt == null ? null : readAt,
    "replied_at": repliedAt == null ? null : repliedAt,
  };
}

