// To parse this JSON data, do
//
//     final getPhoneNumberResponse = getPhoneNumberResponseFromJson(jsonString);

import 'dart:convert';

GetPhoneNumberResponse getPhoneNumberResponseFromJson(String str) => GetPhoneNumberResponse.fromJson(json.decode(str));

String getPhoneNumberResponseToJson(GetPhoneNumberResponse data) => json.encode(data.toJson());

class GetPhoneNumberResponse {
  GetPhoneNumberResponse({
    this.status,
    this.data,
    this.sms,
  });

  bool? status;
  String? data;
  Sms? sms;

  factory GetPhoneNumberResponse.fromJson(Map<String, dynamic> json) => GetPhoneNumberResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : json["data"],
    sms: json["sms"] == null ? null : Sms.fromJson(json["sms"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data,
    "sms": sms == null ? null : sms!.toJson(),
  };
}

class Sms {
  Sms({
    this.status,
    this.message,
  });

  bool? status;
  String? message;

  factory Sms.fromJson(Map<String, dynamic> json) => Sms(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
