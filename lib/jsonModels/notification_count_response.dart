// To parse this JSON data, do
//
//     final notificationCountResponse = notificationCountResponseFromJson(jsonString);

import 'dart:convert';

NotificationCountResponse notificationCountResponseFromJson(String str) => NotificationCountResponse.fromJson(json.decode(str));

String notificationCountResponseToJson(NotificationCountResponse data) => json.encode(data.toJson());

class NotificationCountResponse {
  NotificationCountResponse({
    this.status,
    this.data,
  });

  bool? status;
  int? data;

  factory NotificationCountResponse.fromJson(Map<String, dynamic> json) => NotificationCountResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : json["data"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data,
  };
}
