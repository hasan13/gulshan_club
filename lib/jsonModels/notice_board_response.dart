// To parse this JSON data, do
//
//     final noticeResponse = noticeResponseFromJson(jsonString?);

import 'dart:convert';

NoticeResponse noticeResponseFromJson(String str) => NoticeResponse.fromJson(json.decode(str));

String noticeResponseToJson(NoticeResponse data) => json.encode(data.toJson());

class NoticeResponse {
  NoticeResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  dynamic next;
  dynamic previous;
  List<Notice>? results;

  factory NoticeResponse.fromJson(Map<String?, dynamic> json) => NoticeResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Notice>.from(json["results"].map((x) => Notice.fromJson(x))),
  );

  Map<String?, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Notice {
  Notice({
    this.title,
    this.message,
    this.tag,
    this.createdAt,
    this.updatedAt,

    this.imageMedium,
    this.imageThumbnail,
  });

  String? title;
  String? message;
  String? tag;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? imageMedium;
  String? imageThumbnail;

  factory Notice.fromJson(Map<String?, dynamic> json) => Notice(
    title: json["title"] == null ? null : json["title"],
    message: json["message"] == null ? null : json["message"],
    tag: json["tag"] == null ? null : json["tag"],
    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
    imageThumbnail: json["image_thumbnail"] == null ? null : json["image_thumbnail"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String?, dynamic> toJson() => {
    "title": title == null ? null : title,
    "message": message == null ? null : message,
    "tag": tag == null ? null : tag,
    "image_medium": imageMedium == null ? null : imageMedium,
    "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
    "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
  };
}
