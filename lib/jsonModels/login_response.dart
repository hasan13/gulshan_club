// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    required this.accessToken,
    required this.expiresIn,
    required this.tokenType,
    required this.scope,
    required this.refreshToken,
  });

  String accessToken;
  int expiresIn;
  String tokenType;
  String scope;
  String refreshToken;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    accessToken: json["access_token"] == null ? null : json["access_token"],
    expiresIn: json["expires_in"] == null ? null : json["expires_in"],
    tokenType: json["token_type"] == null ? null : json["token_type"],
    scope: json["scope"] == null ? null : json["scope"],
    refreshToken: json["refresh_token"] == null ? null : json["refresh_token"],
  );

  Map<String, dynamic> toJson() => {
    "access_token": accessToken == null ? null : accessToken,
    "expires_in": expiresIn == null ? null : expiresIn,
    "token_type": tokenType == null ? null : tokenType,
    "scope": scope == null ? null : scope,
    "refresh_token": refreshToken == null ? null : refreshToken,
  };
}
