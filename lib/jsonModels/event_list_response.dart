// To parse this JSON data, do
//
//     final eventListResponse = eventListResponseFromJson(jsonString);

import 'dart:convert';

EventListResponse eventListResponseFromJson(String str) => EventListResponse.fromJson(json.decode(str));

String eventListResponseToJson(EventListResponse data) => json.encode(data.toJson());

class EventListResponse {
  EventListResponse({
    this.status,
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  bool? status;
  dynamic next;
  dynamic previous;
  List<Event>? results;

  factory EventListResponse.fromJson(Map<String, dynamic> json) => EventListResponse(
    count: json["count"] == null ? null : json["count"],
    status: json["status"] == null ? null : json["status"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Event>.from(json["results"].map((x) => Event.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "status": status == null ? null : status,
    "next": next == null? null : next,
    "previous": previous == null? null : previous,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Event {
  Event({
    this.id,
    this.name,
    this.slug,
    this.startDate,
    this.endDate,
    this.url,
    this.description,
    this.imageMedium,
    this.imageThumbnail,
    this.imageAltText,
    this.created,
  });

  int? id;
  String? name;
  String? slug;
  String? startDate;
  String? endDate;
  String? url;
  String? description;
  String? imageMedium;
  String? imageThumbnail;
  String? imageAltText;
  String? created;

  factory Event.fromJson(Map<String, dynamic> json) => Event(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    slug: json["slug"] == null ? null : json["slug"],
    startDate: json["start_date"] == null ? null : json["start_date"],
    endDate: json["end_date"] == null ? null : json["end_date"],
    url: json["url"] == null ? null : json["url"],
    description: json["description"] == null ? null : json["description"],
    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
    imageThumbnail: json["image_thumbnail"] == null ? null : json["image_thumbnail"],
    imageAltText: json["image_alt_text"] == null ? null : json["image_alt_text"],
    created: json["created"] == null ? null : json["created"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "slug": slug == null ? null : slug,
    "start_date": startDate == null ? null : startDate,
    "end_date": endDate == null ? null : endDate,
    "url": url == null ? null : url,
    "description": description == null ? null : description,
    "image_medium": imageMedium == null ? null : imageMedium,
    "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
    "image_alt_text": imageAltText == null ? null : imageAltText,
    "created": created == null ? null : created,
  };
}
