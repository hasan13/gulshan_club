// To parse this JSON data, do
//
//     final reservationListResponse = reservationListResponseFromJson(jsonString?);

import 'dart:convert';

import 'package:gulshan_club_app/jsonModels/userListResponse.dart';

import 'club_facilities_response.dart';

ReservationListResponse reservationListResponseFromJson(String str) => ReservationListResponse.fromJson(json.decode(str));

String reservationListResponseToJson(ReservationListResponse data) => json.encode(data.toJson());

class ReservationListResponse {
  ReservationListResponse({
    this.status,
    this.data,
  });

  bool? status;
  Data? data;

  factory ReservationListResponse.fromJson(Map<String?, dynamic> json) => ReservationListResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String?, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class Data {
  Data({
    this.next,
    this.previous,
    this.count,
    this.results,
  });

  String? next;
  String? previous;
  int? count;
  List<Reservation>? results;

  factory Data.fromJson(Map<String?, dynamic> json) => Data(
    next: json["next"],
    previous: json["previous"],
    count: json["count"] == null ? null : json["count"],
    results: json["results"] == null ? null : List<Reservation>.from(json["results"].map((x) => Reservation.fromJson(x))),
  );

  Map<String?, dynamic> toJson() => {
    "next": next,
    "previous": previous,
    "count": count == null ? null : count,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Reservation {
  Reservation({
    this.id,
    this.createdBy,
    this.facility,
    this.reservationDate,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  Member? createdBy;
  Facility? facility;
  String? reservationDate;
  String? status;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Reservation.fromJson(Map<String?, dynamic> json) => Reservation(
    id: json["id"] == null ? null : json["id"],
    createdBy: json["created_by"] == null ? null : Member.fromJson(json["created_by"]),
    facility: json["facility"] == null ? null : Facility.fromJson(json["facility"]),
    reservationDate: json["reservation_date"],
    status: json["status"] == null ? null : json["status"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String?, dynamic> toJson() => {
    "id": id == null ? null : id,
    "created_by": createdBy == null ? null : createdBy!.toJson(),
    "facility": facility == null ? null : facility!.toJson(),
    "reservation_date": reservationDate,
    "status": status == null ? null : status,
    "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
  };
}
