// To parse this JSON data, do
//
//     final settingsResponse = settingsResponseFromJson(jsonString);

import 'dart:convert';

SettingsResponse settingsResponseFromJson(String str) => SettingsResponse.fromJson(json.decode(str));

String settingsResponseToJson(SettingsResponse data) => json.encode(data.toJson());

class SettingsResponse {
  SettingsResponse({
    this.status,
    this.data,
  });

  bool? status;
  Data? data;

  factory SettingsResponse.fromJson(Map<String, dynamic> json) => SettingsResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class Data {
  Data({
    this.fcmNotificationShow,
    this.fontSize,
  });

  bool? fcmNotificationShow = false;
  bool? fontSize;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    fcmNotificationShow: json["fcm_notification_show"] == null ? null : json["fcm_notification_show"],
    fontSize: json["font_size"] == null ? null : json["font_size"],
  );

  Map<String, dynamic> toJson() => {
    "fcm_notification_show": fcmNotificationShow == null ? null : fcmNotificationShow,
    "font_size": fontSize == null ? null : fontSize,
  };
}
