
import 'dart:convert';

import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
// To parse this JSON data, do
//
//     final statementResponse = statementResponseFromJson(jsonString?);

StatementResponse statementResponseFromJson(String str) => StatementResponse.fromJson(json.decode(str));

String statementResponseToJson(StatementResponse data) => json.encode(data.toJson());

class StatementResponse {
  StatementResponse({
    this.status,
    this.data,
  });

  bool? status;
  Data? data;

  factory StatementResponse.fromJson(Map<String, dynamic> json) => StatementResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class Data {
  Data({
    this.next,
    this.previous,
    this.count,
    this.results,
  });

  String? next;
  String? previous;
  int? count;
  List<Statement>? results;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    next: json["next"] == null ? null : json["next"],
    previous: json["previous"] == null ? null : json["previous"],
    count: json["count"] == null ? null : json["count"],
    results: json["results"] == null ? null : List<Statement>.from(json["results"].map((x) => Statement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "next": next == null ? null : next,
    "previous": previous == null ? null : previous,
    "count": count == null ? null : count,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Statement {
  Statement({
    this.paymentBy,
    this.paymentTo,
    this.paymentId,
    this.trxId,
    this.amount,
    this.currency,
    this.merchantInvoice,
    this.transactionStatus,
    this.paymentType,
    this.createdAt,
  });

  Member? paymentBy;
  Member? paymentTo;
  String? paymentId;
  String? trxId;
  String? amount;
  String? currency;
  String? merchantInvoice;
  String? transactionStatus;
  String? paymentType;
  String? createdAt;

  factory Statement.fromJson(Map<String, dynamic> json) => Statement(
    paymentBy: json["payment_by"] == null ? null : Member.fromJson(json["payment_by"]),
    paymentTo: json["payment_to"] == null ? null : Member.fromJson(json["payment_to"]),
    paymentId: json["payment_id"] == null ? null : json["payment_id"],
    trxId: json["trx_id"] == null ? null : json["trx_id"],
    amount: json["amount"] == null ? null : json["amount"],
    currency: json["currency"] == null ? null : json["currency"],
    merchantInvoice: json["merchant_invoice"] == null ? null : json["merchant_invoice"],
    transactionStatus: json["transaction_status"] == null ? null : json["transaction_status"],
    paymentType: json["payment_type"] == null ? null : json["payment_type"],
    createdAt: json["created_at"] == null ? null : json["created_at"],
  );

  Map<String, dynamic> toJson() => {
    "payment_by": paymentBy == null ? null : paymentBy!.toJson(),
    "payment_to": paymentTo == null ? null : paymentTo!.toJson(),
    "payment_id": paymentId == null ? null : paymentId,
    "trx_id": trxId == null ? null : trxId,
    "amount": amount == null ? null : amount,
    "currency": currency == null ? null : currency,
    "merchant_invoice": merchantInvoice == null ? null : merchantInvoice,
    "transaction_status": transactionStatus == null ? null : transactionStatus,
    "payment_type": paymentType == null ? null : paymentType,
  };
}

