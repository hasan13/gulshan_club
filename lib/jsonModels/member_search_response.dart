// To parse this JSON data, do
//
//     final memberSearchResponse = memberSearchResponseFromJson(jsonString);

import 'dart:convert';

import 'package:gulshan_club_app/jsonModels/userListResponse.dart';

MemberSearchResponse memberSearchResponseFromJson(String str) => MemberSearchResponse.fromJson(json.decode(str));

String memberSearchResponseToJson(MemberSearchResponse data) => json.encode(data.toJson());

class MemberSearchResponse {
  MemberSearchResponse({
    this.status,
    this.data,
  });

  bool? status;
  List<Member>? data;

  factory MemberSearchResponse.fromJson(Map<String, dynamic> json) => MemberSearchResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<Member>.from(json["data"].map((x) => Member.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

//class Datum {
//  Datum({
//    this.id,
//    this.mobileNumberPrimary,
//    this.imageMedium,
//    this.imageThumbnail,
//  });
//
//  int id;
//  String mobileNumberPrimary;
//  String imageMedium;
//  String imageThumbnail;
//
//  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
//    id: json["id"] == null ? null : json["id"],
//    mobileNumberPrimary: json["mobile_number_primary"] == null ? null : json["mobile_number_primary"],
//    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
//    imageThumbnail: json["image_thumbnail"] == null ? null : json["image_thumbnail"],
//  );
//
//  Map<String, dynamic> toJson() => {
//    "id": id == null ? null : id,
//    "mobile_number_primary": mobileNumberPrimary == null ? null : mobileNumberPrimary,
//    "image_medium": imageMedium == null ? null : imageMedium,
//    "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
//  };
//}
