// To parse this JSON data, do
//
//     final memberDetailsResponse = memberDetailsResponseFromJson(jsonString?);

import 'dart:convert';

import 'package:gulshan_club_app/jsonModels/userListResponse.dart';

MemberDetailsResponse memberDetailsResponseFromJson(String str) => MemberDetailsResponse.fromJson(json.decode(str));

String memberDetailsResponseToJson(MemberDetailsResponse data) => json.encode(data.toJson());

class MemberDetailsResponse {
  MemberDetailsResponse({
    this.user,
    this.relation,
  });

  Member? user;
  List<Relation>? relation;

  factory MemberDetailsResponse.fromJson(Map<String, dynamic> json) => MemberDetailsResponse(
    user: json["user"] == null ? null : Member.fromJson(json["user"]),
    relation: json["relation"] == null ? null :  List<Relation>.from(json["relation"].map((x) => Relation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "user": user!.toJson(),
    "relation": List<dynamic>.from(relation!.map((x) => x.toJson())),
  };
}

class Relation {
  Relation({
    this.id,
    this.name,this.email,
    this.clubAcNumber,
    this.phonePrimary,
    this.relation,
    this.imageMedium,
    this.imageThumbnail,
    this.membershipDate,
    this.birthday,

  });

  int? id;
  String? name,email;
  String? clubAcNumber;
  String? phonePrimary;
  String? relation;
  String? imageMedium;
  String? imageThumbnail;
  String? membershipDate;
  String? birthday;


  factory Relation.fromJson(Map<String, dynamic> json) => Relation(
    id: json["id"] == null ? null :json["id"],
    email: json["email"] == null ? null :json["email"],
    name: json["name"] == null ? null : json["name"],
    clubAcNumber: json["club_ac_number"] == null ? null : json["club_ac_number"],
    phonePrimary:json["phone_primary"] == null ? null : json["phone_primary"],
    relation: json["relation"] == null ? null :json["relation"],
    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
    imageThumbnail:json["image_thumbnail"] == null ? null : json["image_thumbnail"],
    membershipDate: json["membership_date"] == null ? null : json["membership_date"],
    birthday: json["birthday"] == null ? null : json["birthday"],

  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "club_ac_number": clubAcNumber,
    "phone_primary": phonePrimary,
    "relation": relation,
    "image_medium": imageMedium,
    "image_thumbnail": imageThumbnail,
    "membership_date": membershipDate == null ? null : membershipDate,
    "birthday": birthday == null ? null : birthday,

  };
}

