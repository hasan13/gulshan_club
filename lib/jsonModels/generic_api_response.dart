// To parse this JSON data, do
//
//     final genericResponse = genericResponseFromJson(jsonString);

import 'dart:convert';

GenericResponse genericResponseFromJson(String str) => GenericResponse.fromJson(json.decode(str));

String genericResponseToJson(GenericResponse data) => json.encode(data.toJson());

class GenericResponse {
  GenericResponse({
    this.status,
    this.message,
  });

  bool? status;
  String? message;

  factory GenericResponse.fromJson(Map<String, dynamic> json) => GenericResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
  };
}
