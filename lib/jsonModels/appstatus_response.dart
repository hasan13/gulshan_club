// To parse this JSON data, do
//
//     final appStatusResponse = appStatusResponseFromJson(jsonString);

import 'dart:convert';

AppStatusResponse appStatusResponseFromJson(String str) => AppStatusResponse.fromJson(json.decode(str));

String appStatusResponseToJson(AppStatusResponse data) => json.encode(data.toJson());

class AppStatusResponse {
  AppStatusResponse({
    this.status,
    this.data,
  });

  bool? status;
  StatusData? data;

  factory AppStatusResponse.fromJson(Map<String, dynamic> json) => AppStatusResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : StatusData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class StatusData {
  StatusData({
    this.forceUpdate,
  });

  bool? forceUpdate;

  factory StatusData.fromJson(Map<String, dynamic> json) => StatusData(
    forceUpdate: json["force_update"] == null ? null : json["force_update"],
  );

  Map<String, dynamic> toJson() => {
    "force_update": forceUpdate == null ? null : forceUpdate,
  };
}
