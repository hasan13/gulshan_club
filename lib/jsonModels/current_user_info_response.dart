// To parse this JSON data, do
//
//     final currentUserInfoResponse = currentUserInfoResponseFromJson(jsonString?);

import 'dart:convert';

CurrentUserInfoResponse currentUserInfoResponseFromJson(String str) => CurrentUserInfoResponse.fromJson(json.decode(str));

String? currentUserInfoResponseToJson(CurrentUserInfoResponse data) => json.encode(data.toJson());

class CurrentUserInfoResponse {
  CurrentUserInfoResponse({
    this.status,
    this.data,
  });

  bool? status;
  AppUser? data;

  factory CurrentUserInfoResponse.fromJson(Map<String, dynamic> json) => CurrentUserInfoResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : AppUser.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class AppUser {
  AppUser({
    this.id,
    this.username,
    this.email,
    this.imageMedium,
    this.imageThumbnail,
    this.firstName,
    this.lastName,
    this.phonePrimary,
    this.phoneSecondary,
    this.clubAcNumber,
    this.categoryName,
    this.designation,
    this.membershipDate,
    this.birthday,
    this.maritalStatus,
    this.marriageAnniversary,
    this.spouse,
    this.address,
    this.nationality,
    this.bloodGroup,
    this.religion,
    this.gender,
    this.profession,
    this.education,
    this.opt,
    this.banquetBookingView,
    this.banquetBookingEdit,

  });

  int? id;
  String? username;
  String? email;
  String? imageMedium;
  String? imageThumbnail;
  String? firstName;
  String? lastName;
  String? phonePrimary;
  String? phoneSecondary;
  String? clubAcNumber;
  int? categoryName;
  String? designation;
  String? membershipDate;
  String? birthday;
  String? maritalStatus;
  String? marriageAnniversary;
  String? spouse;
  String? address;
  String? nationality;
  String? bloodGroup;
  String? religion;
  String? gender;
  String? profession;
  String? education;
  String? opt;
  bool? banquetBookingView;
  bool? banquetBookingEdit;

  factory AppUser.fromJson(Map<String, dynamic> json) => AppUser(
    id: json["id"] == null ? null : json["id"],
    username: json["username"] == null ? null : json["username"],
    email: json["email"] == null ? null : json["email"],
    imageMedium: json["image_medium"],
    imageThumbnail: json["image_thumbnail"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    phonePrimary: json["phone_primary"],
    phoneSecondary: json["phone_secondary"],
    clubAcNumber: json["club_ac_number"] == null ? null : json["club_ac_number"],
    categoryName: json["category_name"],
    designation: json["designation"] == null ? null : json["designation"],
    membershipDate: json["membership_date"],
    birthday: json["birthday"],
    maritalStatus: json["marital_status"],
    marriageAnniversary: json["marriage_anniversary"],
    spouse: json["spouse"],
    address: json["address"] == null ? null : json["address"],
    nationality: json["nationality"],
    bloodGroup: json["blood_group"],
    religion: json["religion"],
    gender: json["gender"],
    profession: json["profession"] == null ? null : json["profession"],
    education: json["education"] == null ? null : json["education"],
    opt: json["opt"],
    banquetBookingView: json["banquet_booking_view"] == null ? null : json["banquet_booking_view"],
    banquetBookingEdit: json["banquet_booking_edit"] == null ? null : json["banquet_booking_edit"],


  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "username": username == null ? null : username,
    "email": email == null ? null : email,
    "image_medium": imageMedium,
    "image_thumbnail": imageThumbnail,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "phone_primary": phonePrimary,
    "phone_secondary": phoneSecondary,
    "club_ac_number": clubAcNumber == null ? null : clubAcNumber,
    "category_name": categoryName,
    "designation": designation == null ? null : designation,
    "membership_date": membershipDate,
    "birthday": birthday,
    "marital_status": maritalStatus,
    "marriage_anniversary": marriageAnniversary,
    "spouse": spouse,
    "address": address == null ? null : address,
    "nationality": nationality,
    "blood_group": bloodGroup,
    "religion": religion,
    "gender": gender,
    "profession": profession == null ? null : profession,
    "education": education == null ? null : education,
    "opt": opt,
    "address": address == null ? null : address,
    "banquet_booking_view": banquetBookingView == null ? null : banquetBookingView,
    "banquet_booking_edit": banquetBookingEdit == null ? null : banquetBookingEdit,

  };
}
