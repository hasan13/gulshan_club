// To parse this JSON data, do
//
//     final notificationListResponse = notificationListResponseFromJson(jsonString);

import 'dart:convert';

NotificationListResponse notificationListResponseFromJson(String str) => NotificationListResponse.fromJson(json.decode(str));

String notificationListResponseToJson(NotificationListResponse data) => json.encode(data.toJson());

class NotificationListResponse {
  NotificationListResponse({
    this.status,
    this.data,
  });

  bool? status;
  NotificationResponse? data;

  factory NotificationListResponse.fromJson(Map<String, dynamic> json) => NotificationListResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : NotificationResponse.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class NotificationResponse {
  NotificationResponse({
    this.next,
    this.previous,
    this.count,
    this.results,
  });

  String? next;
  String? previous;
  int? count;
  List<Notifications>? results;

  factory NotificationResponse.fromJson(Map<String, dynamic> json) => NotificationResponse(
    next: json["next"] == null ? null : json["next"],
    previous: json["previous"] == null ? null : json["previous"],
    count: json["count"] == null ? null : json["count"],
    results: json["results"] == null ? null : List<Notifications>.from(json["results"].map((x) => Notifications.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "next": next == null ? null : next,
    "previous": previous == null ? null : previous,
    "count": count == null ? null : count,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Notifications {
  Notifications({
    this.id,
    this.message,
    this.isRead,
    this.notificationType,
    this.createdAt,
  });

  int? id;
  String? message;
  bool? isRead;
  String? notificationType;
  DateTime? createdAt;

  factory Notifications.fromJson(Map<String, dynamic> json) => Notifications(
    id: json["id"] == null ? null : json["id"],
    message: json["message"] == null ? null : json["message"],
    isRead: json["is_read"] == null ? null : json["is_read"],
    notificationType: json["notification_type"] == null ? null : json["notification_type"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "message": message == null ? null : message,
    "is_read": isRead == null ? null : isRead,
    "notification_type": notificationType == null ? null : notificationType,
    "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
  };
}
