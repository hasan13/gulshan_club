// To parse this JSON data, do
//
//     final userListResponse = userListResponseFromJson(jsonString?);

import 'dart:convert';

UserListResponse userListResponseFromJson(String str) =>
    UserListResponse.fromJson(json.decode(str));

String userListResponseToJson(UserListResponse data) =>
    json.encode(data.toJson());

class UserListResponse {
  UserListResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  dynamic next;
  dynamic previous;
  List<Member>? results;

  factory UserListResponse.fromJson(Map<String, dynamic> json) =>
      UserListResponse(
        count: json["count"] == null ? null : json["count"],
        next: json["next"],
        previous: json["previous"],
        results: json["results"] == null
            ? null
            : List<Member>.from(json["results"].map((x) => Member.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "next": next,
        "previous": previous,
        "results": results == null
            ? null
            : List<dynamic>.from(results!.map((x) => x.toJson())),
      };
}

class Member {
  Member({
    this.id,
    this.username,
    this.email,
    this.imageMedium,
    this.imageThumbnail,
    this.firstName,
    this.lastName,
    this.phonePrimary,
    this.phoneSecondary,
    this.clubAcNumber,
    this.categoryName,
    this.designation,
    this.membershipDate,
    this.birthday,
    this.deathDate,
    this.maritalStatus,
    this.marriageAnniversary,
    this.spouse,
    this.address,
    this.nationality,
    this.bloodGroup,
    this.religion,
    this.gender,
    this.profession,
    this.education,
    this.opt,
  });

  int? id;
  String? username;
  String? email;
  String? imageMedium;
  String? imageThumbnail;
  String? firstName;
  String? lastName;
  String? phonePrimary;
  String? phoneSecondary;
  String? clubAcNumber;
  int? categoryName;
  String? designation;
  String? membershipDate;
  String? birthday,deathDate;

  String? maritalStatus;
  String? marriageAnniversary;
  String? spouse;
  String? address;
  String? nationality;
  String? bloodGroup;
  String? religion;
  String? gender;
  String? profession;
  String? education;
  String? opt;

  factory Member.fromJson(Map<String, dynamic> json) => Member(
    id: json["id"] == null ? null : json["id"],
    username: json["username"] == null ? null : json["username"],
    email: json["email"] == null ? null : json["email"],
    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
    imageThumbnail: json["image_thumbnail"] == null ? null : json["image_thumbnail"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    phonePrimary: json["phone_primary"] == null ? null : json["phone_primary"],
    phoneSecondary: json["phone_secondary"] == null ? null : json["phone_secondary"],
    clubAcNumber: json["club_ac_number"] == null ? null : json["club_ac_number"],
    categoryName: json["category_name"] == null ? null : json["category_name"],
    designation: json["designation"] == null ? null : json["designation"],
    membershipDate: json["membership_date"] == null ? null : json["membership_date"],
    birthday: json["birthday"] == null ? null : json["birthday"],
    deathDate: json["death_date"] == null ? null : json["death_date"],
    maritalStatus: json["marital_status"] == null ? null : json["marital_status"],
    marriageAnniversary: json["marriage_anniversary"] == null ? null : json["marriage_anniversary"],
    spouse: json["spouse"] == null ? null : json["spouse"],
    address: json["address"] == null ? null : json["address"],
    nationality: json["nationality"] == null ? null : json["nationality"],
    bloodGroup: json["blood_group"] == null ? null : json["blood_group"],
    religion: json["religion"] == null ? null : json["religion"],
    gender: json["gender"] == null ? null : json["gender"],
    profession: json["profession"] == null ? null : json["profession"],
    education: json["education"] == null ? null : json["education"],
    opt: json["opt"] == null ? null : json["opt"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "username": username == null ? null : username,
    "email": email == null ? null : email,
    "image_medium": imageMedium == null ? null : imageMedium,
    "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "phone_primary": phonePrimary == null ? null : phonePrimary,
    "phone_secondary": phoneSecondary == null ? null : phoneSecondary,
    "club_ac_number": clubAcNumber == null ? null : clubAcNumber,
    "category_name": categoryName == null ? null : categoryName,
    "designation": designation == null ? null : designation,
    "membership_date": membershipDate == null ? null : membershipDate,
    "birthday": birthday == null ? null : birthday,
    "death_date": deathDate == null ? null : deathDate,
    "marital_status": maritalStatus == null ? null : maritalStatus,
    "marriage_anniversary": marriageAnniversary == null ? null : marriageAnniversary,
    "spouse": spouse == null ? null : spouse,
    "address": address == null ? null : address,
    "nationality": nationality == null ? null : nationality,
    "blood_group": bloodGroup == null ? null : bloodGroup,
    "religion": religion == null ? null : religion,
    "gender": gender == null ? null : gender,
    "profession": profession == null ? null : profession,
    "education": education == null ? null : education,
    "opt": opt == null ? null : opt,
  };
}
