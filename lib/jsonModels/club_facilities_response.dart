// To parse this JSON data, do
//
//     final clubFacilitiesResponse = clubFacilitiesResponseFromJson(jsonString);

import 'dart:convert';

ClubFacilitiesResponse clubFacilitiesResponseFromJson(String str) => ClubFacilitiesResponse.fromJson(json.decode(str));

String clubFacilitiesResponseToJson(ClubFacilitiesResponse data) => json.encode(data.toJson());

class ClubFacilitiesResponse {
  ClubFacilitiesResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  dynamic next;
  dynamic previous;
  List<Facility>? results;

  factory ClubFacilitiesResponse.fromJson(Map<String, dynamic> json) => ClubFacilitiesResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"],
    previous: json["previous"],
    results: json["results"] == null ? null : List<Facility>.from(json["results"].map((x) => Facility.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next,
    "previous": previous,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Facility {
  Facility({
    this.id,
    this.name,
    this.clubFacility,
    this.description,
    this.imageMedium,
    this.imageThumbnail,
  });

  int? id;
  String? name;
  int? clubFacility;
  String? description;
  String? imageMedium;
  String? imageThumbnail;

  factory Facility.fromJson(Map<String, dynamic> json) => Facility(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    clubFacility: json["club_facility"] == null ? null : json["club_facility"],
    description: json["description"] == null ? null : json["description"],
    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
    imageThumbnail: json["image_thumbnail"] == null ? null : json["image_thumbnail"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "club_facility": clubFacility == null ? null : clubFacility,
    "description": description == null ? null : description,
    "image_medium": imageMedium == null ? null : imageMedium,
    "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
  };
}
