// To parse this JSON data, do
//
//     final foodCategoryResponse = foodCategoryResponseFromJson(jsonString);

import 'dart:convert';

FoodCategoryResponse foodCategoryResponseFromJson(String str) => FoodCategoryResponse.fromJson(json.decode(str));

String foodCategoryResponseToJson(FoodCategoryResponse data) => json.encode(data.toJson());

class FoodCategoryResponse {
  FoodCategoryResponse({
    this.status,
    this.data,
  });

  bool? status;
  List<FoodCategory>? data;

  factory FoodCategoryResponse.fromJson(Map<String, dynamic> json) => FoodCategoryResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<FoodCategory>.from(json["data"].map((x) => FoodCategory.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class FoodCategory {
  FoodCategory({
    this.id,
    this.name,
    this.image,
  });

  int? id;
  String? name;
  String? image;

  factory FoodCategory.fromJson(Map<String, dynamic> json) => FoodCategory(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    image: json["image"] == null ? null : json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "image": image == null ? null : image,
  };
}
