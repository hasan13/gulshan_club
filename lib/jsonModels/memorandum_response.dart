// To parse this JSON data, do
//
//     final memorandumListResponse = memorandumListResponseFromJson(jsonString?);

import 'dart:convert';

MemorandumListResponse memorandumListResponseFromJson(String str) => MemorandumListResponse.fromJson(json.decode(str));

String memorandumListResponseToJson(MemorandumListResponse data) => json.encode(data.toJson());

class MemorandumListResponse {
  MemorandumListResponse({
    this.count,
    this.next,
    this.previous,
    this.results,
  });

  int? count;
  String? next;
  String? previous;
  List<Memorandum>? results;

  factory MemorandumListResponse.fromJson(Map<String, dynamic> json) => MemorandumListResponse(
    count: json["count"] == null ? null : json["count"],
    next: json["next"] == null ? null : json["next"],
    previous: json["previous"] == null ? null : json["previous"],
    results: json["results"] == null ? null : List<Memorandum>.from(json["results"].map((x) => Memorandum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count == null ? null : count,
    "next": next == null ? null : next,
    "previous": previous == null ? null : previous,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Memorandum {
  Memorandum({
    this.title,
    this.description,
    this.pdf,
    this.createdAt,
    this.updatedAt,
  });

  String? title;
  String? description;
  String? pdf;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Memorandum.fromJson(Map<String, dynamic> json) => Memorandum(
    title: json["title"] == null ? null : json["title"],
    description: json["description"] == null ? null : json["description"],
    pdf: json["pdf"] == null ? null : json["pdf"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "title": title == null ? null : title,
    "description": description == null ? null : description,
    "pdf": pdf == null ? null : pdf,
    "created_at": createdAt == null ? null : createdAt!.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt!.toIso8601String(),
  };
}
