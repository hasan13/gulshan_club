// To parse this JSON data, do
//
//     final messageListResponse = messageListResponseFromJson(jsonString?);

import 'dart:convert';

import 'package:gulshan_club_app/jsonModels/userListResponse.dart';

MessageListResponse messageListResponseFromJson(String str) => MessageListResponse.fromJson(json.decode(str));

String? messageListResponseToJson(MessageListResponse data) => json.encode(data.toJson());

class MessageListResponse {
  MessageListResponse({
    this.status,
    this.data,
  });

  bool? status;
  Data? data;

  factory MessageListResponse.fromJson(Map<String, dynamic> json) => MessageListResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

class Data {
  Data({
    this.next,
    this.previous,
    this.count,
    this.results,
  });

  dynamic next;
  dynamic previous;
  int? count;
  List<Messages>? results;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    next: json["next"],
    previous: json["previous"],
    count: json["count"] == null ? null : json["count"],
    results: json["results"] == null ? null : List<Messages>.from(json["results"].map((x) => Messages.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "next": next,
    "previous": previous,
    "count": count == null ? null : count,
    "results": results == null ? null : List<dynamic>.from(results!.map((x) => x.toJson())),
  };
}

class Messages {
  Messages({
    this.id,
    this.subject,
    this.sender,
    this.recipient,
    this.parentMsg,
    this.sentAt,
    this.readAt,
    this.repliedAt,
    this.count,
  });

  int? id;
  String? subject;
  Member? sender;
  Member? recipient;
  dynamic parentMsg;
  DateTime? sentAt;
  dynamic readAt;
  dynamic repliedAt;
  String? count;

  factory Messages.fromJson(Map<String, dynamic> json) => Messages(
    id: json["id"] == null ? null : json["id"],
    subject: json["subject"] == null ? null : json["subject"],
    sender: json["sender"] == null ? null : Member.fromJson(json["sender"]),
    recipient: json["recipient"] == null ? null : Member.fromJson(json["recipient"]),
    parentMsg: json["parent_msg"],
    sentAt: json["sent_at"] == null ? null : DateTime.parse(json["sent_at"]),
    readAt: json["read_at"],
    repliedAt: json["replied_at"],
    count: json["count"] == null ? null : json["count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "subject": subject == null ? null : subject,
    "sender": sender == null ? null : sender!.toJson(),
    "recipient": recipient == null ? null : recipient!.toJson(),
    "parent_msg": parentMsg,
    "sent_at": sentAt == null ? null : sentAt!.toIso8601String(),
    "read_at": readAt,
    "replied_at": repliedAt,
    "count": count == null ? null : count,
  };
}

class Recipient {
  Recipient({
    this.id,
    this.username,
    this.email,
    this.imageMedium,
    this.imageThumbnail,
    this.firstName,
    this.lastName,
    this.phonePrimary,
    this.phoneSecondary,
    this.clubAcNumber,
    this.categoryName,
    this.designation,
    this.membershipDate,
    this.birthday,
    this.maritalStatus,
    this.marriageAnniversary,
    this.spouse,
    this.address,
    this.nationality,
    this.bloodGroup,
    this.religion,
    this.gender,
    this.profession,
    this.education,
    this.opt,
  });

  int? id;
  String? username;
  String? email;
  String? imageMedium;
  String? imageThumbnail;
  String? firstName;
  String? lastName;
  String? phonePrimary;
  String? phoneSecondary;
  String? clubAcNumber;
  int? categoryName;
  String? designation;
  String? membershipDate;
  String? birthday;
  String? maritalStatus;
  String? marriageAnniversary;
  String? spouse;
  String? address;
  String? nationality;
  String? bloodGroup;
  String? religion;
  String? gender;
  String? profession;
  String? education;
  String? opt;

  factory Recipient.fromJson(Map<String, dynamic> json) => Recipient(
    id: json["id"] == null ? null : json["id"],
    username: json["username"] == null ? null : json["username"],
    email: json["email"] == null ? null : json["email"],
    imageMedium: json["image_medium"] == null ? null : json["image_medium"],
    imageThumbnail: json["image_thumbnail"] == null ? null : json["image_thumbnail"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    phonePrimary: json["phone_primary"] == null ? null : json["phone_primary"],
    phoneSecondary: json["phone_secondary"] == null ? null : json["phone_secondary"],
    clubAcNumber: json["club_ac_number"] == null ? null : json["club_ac_number"],
    categoryName: json["category_name"] == null ? null : json["category_name"],
    designation: json["designation"] == null ? null : json["designation"],
    membershipDate: json["membership_date"] == null ? null : json["membership_date"],
    birthday: json["birthday"] == null ? null : json["birthday"],
    maritalStatus: json["marital_status"] == null ? null : json["marital_status"],
    marriageAnniversary: json["marriage_anniversary"] == null ? null : json["marriage_anniversary"],
    spouse: json["spouse"] == null ? null : json["spouse"],
    address: json["address"] == null ? null : json["address"],
    nationality: json["nationality"] == null ? null : json["nationality"],
    bloodGroup: json["blood_group"] == null ? null : json["blood_group"],
    religion: json["religion"] == null ? null : json["religion"],
    gender: json["gender"] == null ? null : json["gender"],
    profession: json["profession"] == null ? null : json["profession"],
    education: json["education"] == null ? null : json["education"],
    opt: json["opt"] == null ? null : json["opt"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "username": username == null ? null : username,
    "email": email == null ? null : email,
    "image_medium": imageMedium == null ? null : imageMedium,
    "image_thumbnail": imageThumbnail == null ? null : imageThumbnail,
    "first_name": firstName == null ? null : firstName,
    "last_name": lastName == null ? null : lastName,
    "phone_primary": phonePrimary == null ? null : phonePrimary,
    "phone_secondary": phoneSecondary == null ? null : phoneSecondary,
    "club_ac_number": clubAcNumber == null ? null : clubAcNumber,
    "category_name": categoryName == null ? null : categoryName,
    "designation": designation == null ? null : designation,
    "membership_date": membershipDate == null ? null : membershipDate,
    "birthday": birthday == null ? null : birthday,
    "marital_status": maritalStatus == null ? null : maritalStatus,
    "marriage_anniversary": marriageAnniversary == null ? null : marriageAnniversary,
    "spouse": spouse == null ? null : spouse,
    "address": address == null ? null : address,
    "nationality": nationality == null ? null : nationality,
    "blood_group": bloodGroup == null ? null : bloodGroup,
    "religion": religion == null ? null : religion,
    "gender": gender == null ? null : gender,
    "profession": profession == null ? null : profession,
    "education": education == null ? null : education,
    "opt": opt == null ? null : opt,
  };
}
