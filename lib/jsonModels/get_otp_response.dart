// To parse this JSON data, do
//
//     final getOtpResponseResponse = getOtpResponseResponseFromJson(jsonString);

import 'dart:convert';

GetOtpResponse getOtpResponseResponseFromJson(String str) => GetOtpResponse.fromJson(json.decode(str));

String getOtpResponseResponseToJson(GetOtpResponse data) => json.encode(data.toJson());

class GetOtpResponse {
  GetOtpResponse({
    this.status,
    this.data,
    this.smsResponse,
    this.opt,
  });

  bool? status;
  String? data;
  SmsResponse? smsResponse;
  String? opt;

  factory GetOtpResponse.fromJson(Map<String, dynamic> json) => GetOtpResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : json["data"],
    smsResponse: json["sms_response"] == null ? null : SmsResponse.fromJson(json["sms_response"]),
    opt: json["opt"] == null ? null : json["opt"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data,
    "sms_response": smsResponse == null ? null : smsResponse!.toJson(),
    "opt": opt == null ? null : opt,
  };
}

class SmsResponse {
  SmsResponse({
    this.reply,
  });

  Reply? reply;

  factory SmsResponse.fromJson(Map<String, dynamic> json) => SmsResponse(
    reply: json["REPLY"] == null ? null : Reply.fromJson(json["REPLY"]),
  );

  Map<String, dynamic> toJson() => {
    "REPLY": reply == null ? null : reply!.toJson(),
  };
}

class Reply {
  Reply({
    this.parameter,
    this.login,
    this.pushapi,
    this.stakeholderid,
    this.permitted,
    this.smsinfo,
  });

  String? parameter;
  String? login;
  String? pushapi;
  String? stakeholderid;
  String? permitted;
  Smsinfo? smsinfo;

  factory Reply.fromJson(Map<String, dynamic> json) => Reply(
    parameter: json["PARAMETER"] == null ? null : json["PARAMETER"],
    login: json["LOGIN"] == null ? null : json["LOGIN"],
    pushapi: json["PUSHAPI"] == null ? null : json["PUSHAPI"],
    stakeholderid: json["STAKEHOLDERID"] == null ? null : json["STAKEHOLDERID"],
    permitted: json["PERMITTED"] == null ? null : json["PERMITTED"],
    smsinfo: json["SMSINFO"] == null ? null : Smsinfo.fromJson(json["SMSINFO"]),
  );

  Map<String, dynamic> toJson() => {
    "PARAMETER": parameter == null ? null : parameter,
    "LOGIN": login == null ? null : login,
    "PUSHAPI": pushapi == null ? null : pushapi,
    "STAKEHOLDERID": stakeholderid == null ? null : stakeholderid,
    "PERMITTED": permitted == null ? null : permitted,
    "SMSINFO": smsinfo == null ? null : smsinfo!.toJson(),
  };
}

class Smsinfo {
  Smsinfo({
    this.msisdn,
    this.smstext,
    this.csmsid,
    this.referenceid,
  });

  String? msisdn;
  String? smstext;
  String? csmsid;
  String? referenceid;

  factory Smsinfo.fromJson(Map<String, dynamic> json) => Smsinfo(
    msisdn: json["MSISDN"] == null ? null : json["MSISDN"],
    smstext: json["SMSTEXT"] == null ? null : json["SMSTEXT"],
    csmsid: json["CSMSID"] == null ? null : json["CSMSID"],
    referenceid: json["REFERENCEID"] == null ? null : json["REFERENCEID"],
  );

  Map<String, dynamic> toJson() => {
    "MSISDN": msisdn == null ? null : msisdn,
    "SMSTEXT": smstext == null ? null : smstext,
    "CSMSID": csmsid == null ? null : csmsid,
    "REFERENCEID": referenceid == null ? null : referenceid,
  };
}
