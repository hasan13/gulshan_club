// To parse this JSON data, do
//
//     final eventTypeResponse = eventTypeResponseFromJson(jsonString);

import 'dart:convert';

EventTypeResponse eventTypeResponseFromJson(String str) => EventTypeResponse.fromJson(json.decode(str));

String eventTypeResponseToJson(EventTypeResponse data) => json.encode(data.toJson());

class EventTypeResponse {
  EventTypeResponse({
    this.status,
    this.data,
  });

  bool? status;
  List<EventTypeDatum>? data;

  factory EventTypeResponse.fromJson(Map<String, dynamic> json) => EventTypeResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<EventTypeDatum>.from(json["data"].map((x) => EventTypeDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class EventTypeDatum {
  EventTypeDatum({
    this.name,
    this.value,
  });

  String? name;
  String? value;

  factory EventTypeDatum.fromJson(Map<String, dynamic> json) => EventTypeDatum(
    name: json["name"] == null ? null : json["name"],
    value: json["value"] == null ? null : json["value"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "value": value == null ? null : value,
  };
}
