// To parse this JSON data, do
//
//     final bookingListResponse = bookingListResponseFromJson(jsonString);

import 'dart:convert';

import '../current_user_info_response.dart';

BookingListResponse bookingListResponseFromJson(String str) => BookingListResponse.fromJson(json.decode(str));

String bookingListResponseToJson(BookingListResponse data) => json.encode(data.toJson());

class BookingListResponse {
  BookingListResponse({
    this.status,
    this.data,
  });

  bool? status;
  Map<String, List<BookingDatum>>? data;

  factory BookingListResponse.fromJson(Map<String, dynamic> json) => BookingListResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Map.from(json["data"]).map((k, v) => MapEntry<String, List<BookingDatum>>(k, List<BookingDatum>.from(v.map((x) => BookingDatum.fromJson(x))))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : Map.from(data!).map((k, v) => MapEntry<String, dynamic>(k, List<dynamic>.from(v.map((x) => x.toJson())))),
  };
}

class BookingDatum {
  BookingDatum({
    this.id,
    this.date,
    this.day,
    this.hall,
    this.eventType,
    this.accountType,
    this.clubAcNo,
    this.userInfo,
    this.remarks,
  });

  int? id;
  DateTime? date;
  String? day;
  String? hall;
  String? eventType;
  String? accountType;
  String? clubAcNo;
  AppUser? userInfo;
  String? remarks;

  factory BookingDatum.fromJson(Map<String, dynamic> json) => BookingDatum(
    id: json["id"] == null ? null : json["id"],
    date: json["date"] == null ? null : DateTime.parse(json["date"]),
    day: json["day"] == null ? null : json["day"],
    hall: json["hall"] == null ? null : json["hall"],
    eventType: json["eventType"] == null ? null : json["eventType"],
    accountType: json["accountType"] == null ? null : json["accountType"],
    clubAcNo: json["clubAcNo"] == null ? null : json["clubAcNo"],
    userInfo: json["userInfo"] == null ? null : AppUser.fromJson(json["userInfo"]),
    remarks: json["remarks"] == null ? null : json["remarks"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "date": date == null ? null :date!.toIso8601String(),// "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
    "day": day == null ? null : day,
    "hall": hall == null ? null : hall,
    "eventType": eventType == null ? null : eventType,
    "accountType": accountType == null ? null : accountType,
    "clubAcNo": clubAcNo == null ? null : clubAcNo,
    "userInfo": userInfo == null ? null : userInfo!.toJson(),
    "remarks": remarks == null ? null : remarks,
  };
}
