// To parse this JSON data, do
//
//     final bookingCreationErrorResponse = bookingCreationErrorResponseFromJson(jsonString);

import 'dart:convert';

BookingCreationErrorResponse bookingCreationErrorResponseFromJson(String str) => BookingCreationErrorResponse.fromJson(json.decode(str));

String bookingCreationErrorResponseToJson(BookingCreationErrorResponse data) => json.encode(data.toJson());

class BookingCreationErrorResponse {
  BookingCreationErrorResponse({
    this.status,
    this.data,
  });

  bool? status;
  String? data;

  factory BookingCreationErrorResponse.fromJson(Map<String, dynamic> json) => BookingCreationErrorResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : json["data"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data,
  };
}
