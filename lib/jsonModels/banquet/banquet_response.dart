// To parse this JSON data, do
//
//     final hallListResponse = hallListResponseFromJson(jsonString?);

import 'dart:convert';

HallListResponse hallListResponseFromJson(String str) => HallListResponse.fromJson(json.decode(str));

String hallListResponseToJson(HallListResponse data) => json.encode(data.toJson());

class HallListResponse {
  HallListResponse({
    this.status,
    this.data,
  });

  bool? status;
  List<HallDatum>? data;

  factory HallListResponse.fromJson(Map<String, dynamic> json) => HallListResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : List<HallDatum>.from(json["data"].map((x) => HallDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
  };
}

class HallDatum {
  HallDatum({
    this.name,
    this.value,
  });

  String? name;
  String? value;

  factory HallDatum.fromJson(Map<String, dynamic> json) =>
      HallDatum(
        name: json["name"] == null ? null : json["name"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() =>
      {
        "name": name == null ? null : name,
        "value": value == null ? null : value,
      };
}
