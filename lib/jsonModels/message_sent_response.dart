// To parse this JSON data, do
//
//     final messageSentResponse = messageSentResponseFromJson(jsonString);

import 'dart:convert';

import 'package:gulshan_club_app/jsonModels/single_chat_response.dart';

MessageSentResponse messageSentResponseFromJson(String str) => MessageSentResponse.fromJson(json.decode(str));

String messageSentResponseToJson(MessageSentResponse data) => json.encode(data.toJson());

class MessageSentResponse {
  MessageSentResponse({
    this.status,
    this.data,
  });

  bool? status;
  Chats? data;

  factory MessageSentResponse.fromJson(Map<String, dynamic> json) => MessageSentResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Chats.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toJson(),
  };
}

