import 'dart:convert';

// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flappy_search_bar/flappy_search_bar.dart';
// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/member_search_response.dart';
import 'package:gulshan_club_app/jsonModels/message_list_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

class MessagesScreen extends StatefulWidget {
  static const String route = '/messages';

  @override
  _MessagesScreenState createState() => _MessagesScreenState();
}

class _MessagesScreenState extends State<MessagesScreen> {
  // ignore: unused_field
  final SearchBarController<Member> _searchBarController =
      SearchBarController();
  final TextEditingController _typeAheadController = TextEditingController();
  bool isReplay = false;
  MessageListResponse? messageListResponse;
  bool isLoadingMore = false;
  List<Messages> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  List<Member> memberList = [];

  // var _searchResult = [];
  String queryString = "";
  bool isGoClicked = false;
  var searchText;

  List<Member> searchResult = [];

  Future<List<Member>> _getALlPosts(String text) async {
    print('get all post text length=== ${text.length}');

    if (text.length > 0) {
      return getFilteredMembers(text);
    } else {
      return memberList; //posts;
    }

    //    await Future.delayed(Duration(seconds: text.length == 2 ? 1 : 1));
    // if (isReplay) return [Post("Replaying !", "Replaying body")];
    // if (text.length == 5) throw Error();
    // if (text.length == 6) return [];
    // return  [];
    // List<Post> posts = [];
    // var random = new Random();
    // for (int i = 0; i < 10; i++) {
    //   posts.add(Post("$text $i", "body random number : ${random.nextInt(100)}"));
    // }
    //return [];//posts;
  }

  Future<List<Member>> getFilteredMembers(String text) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/user_search/?member_name=$text";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = MemberSearchResponse.fromJson(data);
              print('result....\n ${employeeResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  memberList = employeeResponse.data!;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
    return memberList;
  }

  @override
  void initState() {
    // TODO: implement initState
    getAllMessages();
    super.initState();
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllMessages();
    });
  }

  getAllMessages() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/messages/inbox/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = MessageListResponse.fromJson(data);
              print('result....\n ${employeeResponse.data?.results}');
              if (value.statusCode == 200) {
                setState(() {
                  itemList = employeeResponse.data?.results ?? [];
                  messageListResponse = employeeResponse;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  _loadMoreData() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint =
        messageListResponse?.data?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = MessageListResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        messageListResponse = moreNoticeResponse;
        List<Messages> moreData = moreNoticeResponse.data?.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  Future<List<Member>> getSuggestions(String query) async {
    print("getSuggestions $query");

    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/user_search/?member_name=$query";
    print('url---> $url');

    await http
        .get(Uri.parse(url), headers: {
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
        })
        .timeout(Duration(seconds: 30))
        .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(value.body);
          final employeeResponse = MemberSearchResponse.fromJson(data);
          print('result....\n ${employeeResponse.data}');
          setState(() {});
          if (value.statusCode == 200 || value.statusCode == 202) {
            setState(() {
              queryString = query;
              searchResult = employeeResponse.data!;
              isLoading = false;
            });

            //navigateToSignInPage(context);
            print('YAY! Free cute dog pics!');

            return employeeResponse.data;
          }
        });

    return searchResult;
  }

  getSearchData(BuildContext context, Member suggestion) async {
//    String listResponse = await getSWData();
//    var jsonData = json.decode(listResponse);
//    results = jsonData["results"];
//    print(results.length);

    // _pairList = await getSWData();

    print(suggestion.username);
    print(suggestion.id);

    setState(() {
      Resources.isSearchOn = true;
      Resources.selectedRecipientID = suggestion.id.toString();
      Resources.selectedMember = suggestion;
      _typeAheadController.clear();
    });
    Navigator.pushNamed(context, '/chat').then((value) {
      getAllMessages();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Messages',
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
            child: Container(
              // height: 100,
              width: MediaQuery.of(context).size.width * 0.89,
              decoration: BoxDecoration(
                  color: themeGreyColor,
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),

              child: TypeAheadField(
                // textFieldConfiguration: TextFieldConfiguration(
                // controller: this._typeAheadController,
                // autofocus: false,
                // style:  GoogleFonts.poppins(color: themeWhiteColor),
                // decoration: InputDecoration(

                //   icon: Padding(
                //     padding: const EdgeInsets.only(left:16.0),
                //     child: Icon(Icons.search,color: themeWhiteColor,),
                //   ),
                //   hintText: 'Search here',
                //   border: InputBorder.none,
                //   hintStyle: GoogleFonts.poppins(color: themeWhiteColor),

                // ),
                // ),
                builder: (context, controller, focusNode) {
                  return TextField(
                    controller: this._typeAheadController,
                    autofocus: false,
                    style: GoogleFonts.poppins(color: themeWhiteColor),
                    decoration: InputDecoration(
                      icon: Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: Icon(
                          Icons.search,
                          color: themeWhiteColor,
                        ),
                      ),
                      hintText: 'Search here',
                      border: InputBorder.none,
                      hintStyle: GoogleFonts.poppins(color: themeWhiteColor),
                    ),
                  );
                },
                suggestionsCallback: (pattern) async {
                  if (pattern.length > 0) {
                    setState(() {});
                    return await getSuggestions(pattern);
                  } else {
                    return [];
                  }
                },
                itemBuilder: (BuildContext context, Object? member) {
                  var itemData = member as Member;
                  return ListTile(
                    leading: Icon(Icons.search),
                    title: Text(itemData?.username ?? ' '),
                    //subtitle: Text('\$${suggestion['price']}'),
                  );
                },
                // itemBuilder: (context, Member suggestion) {
                //   return ListTile(
                //     leading: Icon(Icons.search),
                //     title: Text(suggestion.username.toString()),
                //     //subtitle: Text('\$${suggestion['price']}'),
                //   );
                // },
                onSelected: (Object? data) {
                  Member suggestion = data as Member;
                  this._typeAheadController.text =
                      suggestion.username ?? ''; //['name'];
                  print("suggestion['name']");
                  // print(suggestion['name']);
                  setState(() {
                    isLoading = true;
                    isGoClicked = true;
                  });
                  searchText = suggestion
                      .username; //['name']; // nameController.text.trim();
                  print("searchText   $searchText");
                  getSearchData(context, suggestion);
                },
              ),
            ),
          ),
          Expanded(
            child: NotificationListener(
              onNotification: (ScrollNotification scrollInfo) {
                if (scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
                  if (!isLoadingMore &&
                      messageListResponse?.data?.next != null) {
                    // start loading data
                    print("Scrolling to end...");
                    setState(() {
                      isLoadingMore = true;
                    });
                    _loadMoreData();
                  } else if (messageListResponse?.data?.next == null) {
                    print("Data end");
                    setState(() {
                      isLoadingMore = false;
                    });
                  } else {}
                }
                return true;
              },
              child: RefreshIndicator(
                onRefresh: _reloadData,
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: itemList.length,
                  itemBuilder: (context, index) {
                    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm");
                    DateTime dateTime =
                        dateFormat.parse(itemList[index].sentAt.toString());
                    String sentDateString = dateFormat.format(dateTime);
                    print(sentDateString);

                    return GestureDetector(
                      onTap: () {
                        Resources.selectedRecipientID =
                            itemList[index].id.toString();
                        Resources.idForInboxDetails =
                            itemList[index].recipient?.id.toString();
                        Resources.selectedMember = itemList[index].recipient;

                        Resources.selectedRecipient = itemList[index];
                        print(
                            'fdsjfskjfh--- ${Resources.selectedRecipient?.sender?.imageMedium}');
                        print(
                            'message details id--- ${Resources.idForInboxDetails}');
                        _typeAheadController.clear();
                        Resources.isSearchOn = false;
                        Navigator.pushNamed(context, '/chat').then((value) {
                          getAllMessages();
                        });
                      },
                      child: Container(
                        // decoration: BoxDecoration(
                        //   color: Colors.white38,
                        //   borderRadius: BorderRadius.all(Radius.circular(4)),
                        // ),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new CircleAvatar(
                                  radius: 22,
                                  backgroundColor: themeGreyColor,
                                  child: CircleAvatar(
                                    radius: 20,
                                    // backgroundImage: AssetImage('assets/images/Facilities1.jpg'),
                                    backgroundImage: (itemList[index]
                                                .recipient
                                                ?.imageMedium !=
                                            null)
                                        ? NetworkImage(NetworkManager.baseUrl +
                                            itemList[index]
                                                .recipient!
                                                .imageMedium!)
                                        : AssetImage(
                                                'assets/icons/ic_placeholder.png')
                                            as ImageProvider,
                                  ),
                                ),
                              ),
                              Flexible(
                                // child: Card(color: Colors.white38,elevation: 0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${itemList[index].recipient?.firstName} ${itemList[index].recipient?.lastName}',
                                      // itemList[index].recipient?.firstName.toString() + " " +
                                      //     itemList[index].recipient.lastName.toString(),//"Saifur Rahman",
                                      style: gcSubTitleTextStyle,
                                    ),
                                    Text(
                                      itemList[index].subject.toString(),
                                      style: gcDescriptionTextStyle,
                                    ),
                                  ],
                                ),
                              ),
                              // SizedBox(
                              //   width: 20,
                              // ),
                              // Spacer(),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    Text(
                                      sentDateString.substring(
                                          10, sentDateString.length), //"12.35",
                                      style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        color: themeGolderColor,
                                      ),
                                    ),
                                    (itemList[index].count.toString() != '0')
                                        ? new CircleAvatar(
                                            radius: 12,
                                            backgroundColor: listSubTitleColor,
                                            child: Text(
                                              itemList[index].count.toString(),
                                              style: GoogleFonts.poppins(
                                                fontSize: 8,
                                                color: themeWhiteColor,
                                              ),
                                            ),
                                          )
                                        : SizedBox(),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Post {
  final String title;
  final String body;

  Post(this.title, this.body);
}
