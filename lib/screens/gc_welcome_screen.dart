import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/appstatus_response.dart';
import 'package:gulshan_club_app/jsonModels/get_phone_number_response.dart';
import 'package:gulshan_club_app/jsonModels/login_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_font_size.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../jsonModels/app_version_response.dart';

class WelcomeScreen extends StatefulWidget {
  static const String route = '/welcome';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final TextEditingController _userIdCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();
  final GlobalKey<FormState> _globalKey = new GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  bool isLoading = false;
  bool isInitLoading = true;
  bool isFirstLogin = true;
  bool isChecking = true;

  bool storeStatus = true;

  String deviceId = '';
  String deviceType = "";

  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  Map<String, dynamic> deviceData = <String, dynamic>{};

  String deviceName = '';

  late PackageInfo _packageInfo;

  @override
  void initState() {
    // TODO: implement initState
    // checkStoreManager();
    _initPackageInfo();
    checkIfLoggedIn();
    saveFontSizeToSharedPreference();

    if (Platform.isIOS) {
      getStoreStatus();
    } else {
      checkVersion();
    }
    // getStoreStatus();
    super.initState();
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  checkVersion() async {
    await http
        .get(
          Uri.parse(NetworkManager.baseUrl + "/api/v1/mobile_app_versions"),
        )
        .timeout(Duration(seconds: 30))
        .then((value) async {
      print("checkVersion status code Token:${value.statusCode}");
      print(value.body);

      final data = json.decode(value.body);
      if (data != null) {
        if (value.statusCode == 200) {
          final statusResponse = appVersionResponseFromJson(value.body);

          print(statusResponse);

          if (this.mounted) {
            setState(() {
              // advancedStatusCheck(statusResponse);
              Resources.appVersionResponse = statusResponse;
              Resources.localVersion = _packageInfo.version;
            });
          }
          advancedStatusCheck(statusResponse);
        } else {
          setState(() {
            isLoading = false;
          });

          if (data["error_description"] != null) {
            Fluttertoast.showToast(
                msg: data["error_description"],
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        }
      } else {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
            msg: "Something went wrong!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
    // newVersion.showAlertIfNecessary(context: context);
  }

  getStoreStatus() async {
    http
        .get(
          Uri.parse(NetworkManager.baseUrl +
              "/api/v1/mobile_app_force_update/?device=ios"),
          // headers: {
          //   "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
          // }
        )
        .timeout(Duration(seconds: 30))
        .then((value) async {
      print(" getStoreStatus status code Token:${value.statusCode}");
      print(value.body);

      final data = json.decode(value.body);
      if (data != null) {
        if (value.statusCode == 200) {
          final statusResponse = appStatusResponseFromJson(value.body);

          setState(() {
            if (statusResponse.data?.forceUpdate == true) {
              // advancedStatusCheck();
              checkVersion();
            }
          });
        } else {
          setState(() {
            isLoading = false;
          });

          if (data["error_description"] != null) {
            Fluttertoast.showToast(
                msg: data["error_description"],
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.CENTER,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        }
      } else {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
            msg: "Something went wrong!",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
  }

  advancedStatusCheck(AppVersionResponse status) async {
    if (status.status == true) {
      print('android  ${status.data?.android}');
      print(' ios  ${status.data?.ios}');

      print('version ${_packageInfo.version}');
      print('buildNumber ${_packageInfo.buildNumber}');

      if (Platform.isIOS) {
        if (_packageInfo.version.toString() != status.data?.ios.toString()) {
          if (this.mounted) {
            showAlertDialog(context,
                "https://apps.apple.com/us/app/gulshan-club-limited/id1543591195");
          }
        }
      } else {
        if (_packageInfo.version.toString() != status.data?.android) {
          showAlertDialog(context,
              "https://play.google.com/store/apps/details?id=com.wsit.gulshan_club");
        }
      }
    }
  }

  // getVersionStatus() async {
  //   http
  //       .get(
  //         Uri.parse(NetworkManager.baseUrl + "/api/v1/mobile_app_versions"),
  //       )
  //       .timeout(Duration(seconds: 30))
  //       .then((value) async {
  //     print("status code Token:${value.statusCode}");
  //     print(value.body);
  //
  //     final data = json.decode(value.body);
  //     if (data != null) {
  //       if (value.statusCode == 200) {
  //         final statusResponse = appVersionResponseFromJson(value.body);
  //
  //         setState(() {
  //           if (statusResponse != null) {
  //             // advancedStatusCheck(statusResponse);
  //           }
  //         });
  //       } else {
  //         setState(() {
  //           isLoading = false;
  //         });
  //
  //         Fluttertoast.showToast(
  //             msg: "Version status error!",
  //             toastLength: Toast.LENGTH_LONG,
  //             gravity: ToastGravity.CENTER,
  //             backgroundColor: Colors.red,
  //             textColor: Colors.white,
  //             fontSize: 16.0);
  //       }
  //     }
  //   });
  // }

  showAlertDialog(BuildContext ctx, String storeLink) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("Upgrade"),
      onPressed: () async {
        if (await canLaunch(storeLink)) {
          await launch(storeLink);
        } else {
          throw 'Could not launch StoreLink';
        }
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Update Available"),
      content: Text("Please update your app for uninterrupted service!"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final platform = Theme.of(context).platform;

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child: Stack(children: [
            Image.asset(
              "assets/images/Login Background.jpg",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Form(
              key: _globalKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  Center(
                    child: Container(
                        height: 180,
                        width: 220,
                        child: IconButton(
                            icon:
                                SvgPicture.asset('assets/icons/ic_logo.svg'),
                          onPressed: () {  },)),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.65,
                    decoration: BoxDecoration(
                        color: themeWhiteColor.withOpacity(0.3),
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                          // topLeft: Radius.circular(8.0),
                          // topRight: Radius.circular(8.0),
                          // bottomLeft: Radius.circular(8.0),
                          // bottomRight: Radius.circular(8.0),
                        )),
                    child: TextFormField(
                      controller: _userIdCtrl,
                      focusNode: _userIdNode,
                      validator: (value) => MyCustomValidator.validateIsEmpty(
                          _userIdCtrl.text, 'userID', context),
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: new UnderlineInputBorder(
                          borderSide: new BorderSide(
                              //color: themeGolderColor,
                              ),
                        ),
                        enabledBorder: UnderlineInputBorder(
                            //borderSide: BorderSide(color: themeGolderColor),
                            //  when the TextFormField in unfocused
                            ),
                        focusedBorder: UnderlineInputBorder(
                            // borderSide: BorderSide(color: themeGolderColor), //HexColor("#FF7300"
                            //  when the TextFormField in focused
                            ),
                        hintText: 'Club Account',
                        hintStyle: TextStyle(
                          fontSize: kSmall,
                          fontFamily: 'Siri-Regular',
                          color: themeBlackColor,
                        ),
                      ),
                      onFieldSubmitted: (value) {},
                      style: TextStyle(
                          fontFamily: 'Siri-Regular',
                          fontSize: kMedium,
                          color: Colors.black),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  (isLoading == true)
                      ? CircularProgressIndicator()
                      : MyCustomButton(
                          height: 30,
                          // width: 200,
                          width: MediaQuery.of(context).size.width * 0.65,
                          color: Colors.white,
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text("Next")
                              // SvgPicture.asset(
                              //   'assets/icons/ic_login.svg',
                              //   height: 30,
                              // ),
                              ),
                          onPressed: () {
                            print('tapped');
                            if (_globalKey.currentState!.validate()) {
                              setState(() {
                                isLoading = true;
                              });
                              checkUserID();
                              //Navigator.pushNamed(context, '/mobileNo');
                            } else {
                              print(_userIdCtrl.text);
                              print("Error Found");
                            }
                          },
                        ),
                  SizedBox(
                    height: 40,
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }

  checkUserID() async {
    bool result = await InternetConnectionChecker().hasConnection;

    String s = "Hel-lo, 3- wor l-d! i am 'f oo'";
    print("testing--------------------------------\n$s");
    print(s.replaceAll(RegExp('[^A-Za-z0-9]'), ''));
//.replaceAll(RegExp('[^A-Za-z0-9]'), '')
    //replaceAll(new RegExp(r'[^\w\s]+'),'')

    var username =
        _userIdCtrl.text.toString().replaceAll(RegExp('[^A-Za-z0-9]'), '');
    print("testing--------------------------------\n${username.toUpperCase()}");
    var params = {
      "grant_type": "password",
      "username": username.trim().toUpperCase(),
      "client_secret": NetworkManager.clientSecret,
      "client_id": NetworkManager.clientID,
      "password": "!@#\$1234"
    };
    if (result == true) {
      http
          .post(
            Uri.parse(NetworkManager.baseUrl + "/o/token/"),
            body: params,
          )
          .timeout(Duration(seconds: 30))
          .then((value) {
        print("status code Token:${value.statusCode}");
        print(value.body);

        final data = json.decode(value.body);

        if (data != null) {
          if (value.statusCode == 200) {
            final userModel = loginResponseFromJson(value.body);
            var data = json.decode(value.body);

            setState(() {
              isLoading = false;
              isFirstLogin = true;
              Resources.currentUserData = data;
              Resources.currentUser = userModel;
              Resources.loggedIn = true;
            });

            print('access token - ${Resources.currentUser?.accessToken}');
            print("Logged In");
            // Navigator.pushNamed(context, '/mobileNo');
            if (storeStatus == true &&
                Theme.of(context).platform == TargetPlatform.iOS) {
              print("ios");
              getDeviceInfoForRegistraiton();
            } else {
              getDeviceInfoForRegistraiton();
            }
          } else {
            setState(() {
              isLoading = false;
            });

            if (data["error_description"] != null) {
              Fluttertoast.showToast(
                  msg: data["error_description"],
                  toastLength: Toast.LENGTH_LONG,
                  gravity: ToastGravity.CENTER,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          }
        } else {
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(
              msg: "Something went wrong!",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      });
    } else {
      setState(() {
        isLoading = false;
      });
      print('No internet :( Reason:');
       Fluttertoast.showToast(
          msg: "Internet is not available!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  getPhoneNumberByUserID() async {
    var username =
        _userIdCtrl.text.toString().replaceAll(RegExp('[^A-Za-z0-9]'), '');

    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/${username.trim().toUpperCase()}/phone/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final noticeResponse = GetPhoneNumberResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  Resources.mobileNo = noticeResponse.data;
                  isLoading = false;
                });
                print('${Resources.mobileNo}');
                Navigator.pushNamed(context, '/mobileNo');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  getDeviceInfoForRegistraiton() async {
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        print('Running on ${androidInfo.model}');
        setState(() {
          deviceId = androidInfo.androidId;
          deviceType = "android";
          deviceName = androidInfo.model.toString();
          getRegisterdAPI();
        });
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        print('Running on ${iosInfo.utsname.machine}');
        setState(() {
          deviceId = iosInfo.identifierForVendor;
          Resources.deviceId = deviceId;
          deviceType = "iOS";
          deviceName = iosInfo.utsname.machine.toString();
          getRegisterdAPI();
        });
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }
  }

  getRegisterdAPI() async {
    print("deviceId---- $deviceId");
    print("deviceType---- $deviceType");

    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/device-authenticate/";
    print('url---> $url');

    var username =
        _userIdCtrl.text.toString().replaceAll(RegExp('[^A-Za-z0-9]'), '');

    var params = {
      "device_id": deviceId,
      "device_name": deviceName,
      "device_type": deviceType,
      "username": username.trim().toUpperCase(),
    };

    print(params);
    if (result == true) {
      try {
        http
            .post(Uri.parse(url), body: params, headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
              "Content-Type": "application/x-www-form-urlencoded",
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("fcm status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);

              if (value.statusCode == 200) {
                setState(() {
                  isLoading = false;
                  getPhoneNumberByUserID();
                });
              } else {
                setState(() {
                  isLoading = false;
                });
                Fluttertoast.showToast(
                    msg: NetworkManager.unregisteredMessage,
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.CENTER,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
            msg: NetworkManager.errorMessage,
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } else {
      print('No internet :( Reason:');
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
          msg: "Internet is not available!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  checkIfLoggedIn() async {
    print('checking');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // bool CheckValue = prefs.containsKey('user');
    // if(CheckValue == true){
    //   print('value true');
    //   //Navigator.pushNamed(context, '/notice');
    //
    // }
    SharedPref.contain("user").then((val) async {
      print('user read--\n $val');
      if (val) {
        performRefreshToken();
      } else {
        setState(() {
          Resources.loggedIn = false;
          isInitLoading = false;
          print('not logged');
        });
        print("not Logged In");
      }
    });
  }

  performRefreshToken() async {
    String value = await SharedPref.read2('user');
    final data2 = loginResponseFromJson(value);
    bool result = await InternetConnectionChecker().hasConnection;
    //http://localhost:8000/o/revoke_token/
    var params = {
      'grant_type': 'refresh_token',
      "refresh_token": data2.refreshToken,
      "client_secret": NetworkManager.clientSecret,
      "client_id": NetworkManager.clientID,
    };
    if (result == true) {
      http
          .post(
            Uri.parse(NetworkManager.baseUrl + "/o/token/"),
            body: params,
          )
          .timeout(Duration(seconds: 30))
          .then((value) {
        print("status code Token:${value.statusCode}");
        print(value.body);

        final data = json.decode(value.body);

        if (value.statusCode == 200) {
          final userModel = loginResponseFromJson(value.body);

          setState(() {
            isLoading = false;
            isFirstLogin = true;
          });
          var data = json.decode(value.body);
          SharedPref.save("user", data);
          print(data);

          SharedPref.save("access_token", userModel.accessToken);
          print("access token---- ${userModel.accessToken}");

          SharedPref.contain("user").then((val) async {
            print('user read--\n $val');
            if (val) {
              String value = await SharedPref.read2('user');
              final data2 = loginResponseFromJson(value);
              setState(() {
                Resources.loggedIn = true;
                Resources.currentUser = data2;
              });
              print(data2.refreshToken);
              print('access token - ${Resources.currentUser?.accessToken}');
              print("Logged In");
              // Navigator.pushNamed(context, '/mobileNo');
              Navigator.pushNamedAndRemoveUntil(
                  context, '/building_directory', (route) => false);
              //await Navigator.pushNamedAndRemoveUntil(context, routeName, (route) => false);
            } else {
              SharedPref.remove('user');
              setState(() {
                Resources.loggedIn = false;
              });
              print("not Logged In");
            }
          });
        } else {
          setState(() {
            SharedPref.remove('user');
            isLoading = false;
          });
        }
      });
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );

      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      // _scaffoldKey.currentState.showSnackBar(snackBar);
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  checkStoreManager() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/store/store_factory_manager/";
    print('url---> $url');
    if (result == true) {
      try {
        await http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              print('result....\n ${data['status']}');
              if (value.statusCode == 200) {
                setState(() {
                  storeStatus = data['status'];
                  isLoading = false;
                });
                print('storeStatus=-----> $storeStatus');

                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> saveFontSizeToSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double value = (prefs.getDouble('fontScale') ?? 0.0);
    // double value = await SharedPref.read2DoubleValue('fontScale') ?? 0.0;

    print("fontScale---- $value");
    if (value != null) {
      setState(() {
        v1.fontSize = value;
        kLarge = 22.0 + v1.fontSize;
        kTitle = 18.0 + v1.fontSize;
        kMedium = 16.0 + v1.fontSize;
        kSmall = 14.0 + v1.fontSize;
      });
    } else {
      // SharedPref.save("fontScale", 0.0 );
      prefs.setDouble("fontScale", 0.0);
      setState(() {
        v1.fontSize = 0.0;
        kLarge = 22.0 + v1.fontSize;
        kTitle = 18.0 + v1.fontSize;
        kMedium = 16.0 + v1.fontSize;
        kSmall = 14.0 + v1.fontSize;
      });
    }
    print("kLarge---- ${v1.fontSize} $kLarge");
  }
}
