import 'dart:math';

// import 'package:flappy_search_bar/flappy_search_bar.dart';
// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/models/mAvengers.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';

class MembersLedgerScreen extends StatefulWidget {
  static const String route = '/members_ledger';

  @override
  _MembersLedgerScreenState createState() => _MembersLedgerScreenState();
}

class _MembersLedgerScreenState extends State<MembersLedgerScreen> {

  final SearchBarController<Post> _searchBarController = SearchBarController();
  bool isReplay = false;

  List<Avengers> avengers = [];
  List<Avengers> selectedAvengers = [];
  bool sort = false;

  Future<List<Post>> _getALlPosts(String text) async {
    await Future.delayed(Duration(seconds: text.length == 4 ? 10 : 1));
    // if (isReplay) return [Post("Replaying !", "Replaying body")];
    // if (text.length == 5) throw Error();
    // if (text.length == 6) return [];
    // return  [];
    List<Post> posts = [];

    var random = new Random();
    for (int i = 0; i < 10; i++) {
      posts.add(Post("$text $i", "body random number : ${random.nextInt(100)}"));
    }
    return [];//posts;
  }

  onSortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        avengers.sort((a, b) => a.name.compareTo(b.name));
      } else {
        avengers.sort((a, b) => b.name.compareTo(a.name));
      }
    }
  }

  @override
  void initState() {
    sort = false;
    selectedAvengers = [];
    avengers = Avengers.getAvengers();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Member Ledger',
      bottomNavigationBar: Resources.setBottomDrawer(context),
      body: Center(
        child: Padding(
            padding: const EdgeInsets.all(80.0),
            child:Text('This option is currently unavailable!',style: gcTitleTextStyle,)
        ),
      ),
      // InteractiveViewer(
      //   // scrollDirection: Axis.vertical,
      //   constrained: false,
      //   child: Center(
      //     child: Padding(
      //       padding: const EdgeInsets.only(top: 30.0),
      //       child: DataTable(
      //
      //         sortAscending: sort,
      //         sortColumnIndex: 0,
      //         columns: [
      //           DataColumn(
      //               label: Text("ID", style:GoogleFonts.poppins(
      //                 fontSize: 16, color: themeWhiteColor
      //               ),),
      //               numeric: false,
      //               onSort: (columnIndex, ascending) {
      //                 setState(() {
      //                   sort = !sort;
      //                 });
      //                 onSortColum(columnIndex, ascending);
      //               }),
      //           DataColumn(
      //             label: Text("DATE", style:GoogleFonts.poppins(
      //                 fontSize: 16, color: themeWhiteColor
      //             ),),
      //             numeric: false,
      //           ),
      //           DataColumn(
      //             label: Text("DEPT", style:GoogleFonts.poppins(
      //                 fontSize: 16, color: themeWhiteColor
      //             ),),
      //             numeric: false,
      //           ),
      //           DataColumn(
      //             label: Text("DEBIT", style:GoogleFonts.poppins(
      //                 fontSize: 16, color: themeWhiteColor
      //             ),),
      //             numeric: false,
      //           ),
      //           DataColumn(
      //             label: Text("CREDIT", style:GoogleFonts.poppins(
      //                 fontSize: 16, color: themeWhiteColor
      //             ),),
      //             numeric: false,
      //           ),
      //         ],
      //         rows: avengers
      //             .map(
      //               (avenger) => DataRow(
      //               selected: selectedAvengers.contains(avenger),
      //               cells: [
      //                 DataCell(
      //                   Text(avenger.name,style: gcDescriptionTextStyle,),
      //                   onTap: () {
      //                     print('Selected ${avenger.name}');
      //                   },
      //                 ),
      //                 DataCell(
      //                   Text(avenger.weapon,style: gcDescriptionTextStyle,),
      //                 ),
      //                 DataCell(
      //                   Text(avenger.weapon,style: gcDescriptionTextStyle,),
      //                 ),
      //                 DataCell(
      //                   Text(avenger.weapon,style: gcDescriptionTextStyle,),
      //                 ),
      //                 DataCell(
      //                   Text(avenger.weapon,style: gcDescriptionTextStyle,),
      //                 ),
      //               ]),
      //         )
      //             .toList(),
      //       ),
      //     ),
      //   ),
      // ),
    );
  }
}

class Post {
  final String title;
  final String body;

  Post(this.title, this.body);
}