import 'dart:convert';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flappy_search_bar/flappy_search_bar.dart';
// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/employee_directory_response.dart';
import 'package:gulshan_club_app/jsonModels/staff_search_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/gc_pdf_viewer_screen.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';

class EmployeeDirectoryScreen extends StatefulWidget {
  static const String route = '/employee_directory';

  @override
  _EmployeeDirectoryScreenState createState() =>
      _EmployeeDirectoryScreenState();
}

class _EmployeeDirectoryScreenState extends State<EmployeeDirectoryScreen> {
  final SearchBarController<Post> _searchBarController = SearchBarController();
  bool isReplay = false;
  List<Employee> itemList = [];
  var isLoading = false;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  EmployeeDirectoryResponse? userJson;
  bool isLoadingMore = false;

  final TextEditingController _userIdCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();

  bool isSearchOn = false;
  @override
  void initState() {
    // TODO: implement initState
    //getAllEmployes();
    super.initState();
  }

  Future<List<Post>> _getALlPosts(String text) async {
    if (text.length > 0) {
      getFilteredEmployes(text);
    } else {
      FocusScope.of(context).unfocus();

      setState(() {});
      getAllEmployes();
    }
//    await Future.delayed(Duration(seconds: text.length == 4 ? 10 : 1));
    // if (isReplay) return [Post("Replaying !", "Replaying body")];
    // if (text.length == 5) throw Error();
    // if (text.length == 6) return [];
    // return  [];
    List<Post> posts = [];

    var random = new Random();
    for (int i = 0; i < 10; i++) {
      posts
          .add(Post("$text $i", "body random number : ${random.nextInt(100)}"));
    }
    return []; //posts;
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
        pageTitle: 'Employee Directory',
        bottomNavigationBar: Resources.setBottomDrawer(context),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : itemList == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                // : (itemList.length == 0 && isSearchOn == false)
                //     ? Center(
                //         child: Text(
                //           "No Member found",
                //           style: gcPlaceholderTextStyle,
                //         ),
                //       )
                    : createListView(itemList, context));
  }

  getAllEmployes() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/stuff_users/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = EmployeeDirectoryResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${employeeResponse.results}');
              if (value.statusCode == 200) {
                setState(() {
                  itemList = employeeResponse.results ?? [];
                  userJson = employeeResponse;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
                //_scaffoldKey.currentState?.showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                  isSearchOn = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  getFilteredEmployes(String text) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/stuff_users/stuff_search/?stuff_name=$text";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse =
                  EmployeeStaffSearchResponse.fromJson(data);
              print('result....\n ${employeeResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  itemList = employeeResponse.data ?? [];
                  userJson = null;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                  isSearchOn = true;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  _loadMoreEmployee() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint =
        userJson?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = EmployeeDirectoryResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        userJson = moreNoticeResponse;
        List<Employee> moreData = moreNoticeResponse.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllEmployes();
    });
  }

  Widget createListView(List<Employee> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return Stack(
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Container(
                    height: 80,
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 120,
                      child: TextFormField(
                        controller: _userIdCtrl,
                        focusNode: _userIdNode,
                        //validator: (value) => MyCustomValidator.validateIsEmpty(_userIdCtrl.text, 'userID', context),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          border: new UnderlineInputBorder(
                            borderSide: new BorderSide(
                              color: themeGolderColor,
                            ),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: themeGolderColor),
                            //  when the TextFormField in unfocused
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: themeGolderColor), //HexColor("#FF7300"
                            //  when the TextFormField in focused
                          ),
                          hintText: 'search',
                          hintStyle: gcPlaceholderTextStyle,
                        ),
                        onFieldSubmitted: (value) {},
                        style: GoogleFonts.poppins(
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                MyCustomButton(
                  height: 30,
                  width: 80,
                  color: themeGolderColor,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Ok',
                      style: gcNormalTextStyle,
                    ),
                  ),
                  onPressed: () {
                    print('tapped');
                    print(_userIdCtrl.text);
                    if (_userIdCtrl.text.length > 0) {
                      getFilteredEmployes(_userIdCtrl.text);
                    } else {
                      //getAllMembers();
                      setState(() {
                        itemList = [];
                      });
                    }
                  },
                ),
              ],
            ),
            (results.length == 0)
                ? Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: SvgPicture.asset(
                        'assets/images/Employee directory Message.svg',
                        fit: BoxFit.contain,
                        width: MediaQuery.of(context).size.width,
                        height: 400,
                      ),
                    ),
                  )
                : SizedBox(),
            Expanded(
              child: NotificationListener(
                onNotification: (ScrollNotification scrollInfo) {
                  if (scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent) {
                    if (!isLoadingMore && userJson?.next != null) {
                      // start loading data
                      print("Scrolling to end...");
                      setState(() {
                        isLoadingMore = true;
                      });
                      _loadMoreEmployee();
                    } else if (userJson?.next == null) {
                      print("Data end");
                      setState(() {
                        isLoadingMore = false;
                      });
                    } else {}
                  }
                  return true;
                },
                child: RefreshIndicator(
                  onRefresh: _reloadData,
                  child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: itemList.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 8),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            decoration: BoxDecoration(
                                //color: Colors.white38,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                image: (index % 2 == 0)
                                    ? DecorationImage(
                                        image: AssetImage(
                                            'assets/images/ic_list_background_1.png'),
                                        fit: BoxFit.cover)
                                    : DecorationImage(
                                        image: AssetImage(
                                            'assets/images/ic_list_background_3.png'),
                                        fit: BoxFit
                                            .cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),

                                ),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    height: 120,
                                    width: 100,
                                    child: (itemList[index].imageMedium != null)
                                        ? CachedNetworkImage(
                                            imageUrl: NetworkManager.baseUrl +
                                                itemList[index].imageMedium!,
                                            imageBuilder:
                                                (context, imageProvider) =>
                                                    Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.fitHeight,
                                                    colorFilter:
                                                        ColorFilter.mode(
                                                            Colors.transparent,
                                                            BlendMode.color)),
                                              ),
                                            ),
                                            placeholder: (context, url) => Center(
                                                child:
                                                    CircularProgressIndicator()),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Icon(Icons.error),
                                          )
                                        : Image.asset(
                                            'assets/icons/ic_placeholder.png',
                                            height: 120,
                                            width: 100,
                                          ),
                                  ),
                                  // CircleAvatar(
                                  //   radius: 47,
                                  //   backgroundColor: Colors.transparent,
                                  //   backgroundImage: (itemList[index].imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium) : AssetImage('assets/icons/ic_placeholder.png'),
                                  //   //AssetImage('assets/images/Facilities1.jpg'),
                                  // ),
                                ),
                                Flexible(
                                  // child: Card(color: Colors.white38,elevation: 0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        itemList[index]
                                            .stuffName
                                            .toString(), //"Md. Mofazzal Hossain",
                                        style: gcListTitleTextStyle,
                                      ),
                                      Text(
                                        itemList[index]
                                            .designationGroup
                                            .toString(),
                                        style: GoogleFonts.poppins(
                                            fontSize: 12,
                                            color: listDescriptionColor),
                                      ),
                                      Text(
                                        itemList[index].designation.toString(),
                                        style: gcDescriptionTextStyle,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(0.0),
                                        child: Card(
                                          color: themeGreyColor,
                                          elevation: 0,
                                          child: GestureDetector(
                                            onTap: () async {
                                              print(
                                                  '${itemList[index].mobileNumberPrimary}');
                                              //launch("tel://${itemList[index].mobileNumberPrimary}");

                                              String url = "tel://" +
                                                  itemList[index]
                                                      .mobileNumberPrimary
                                                      .toString();
                                              if (await canLaunch(url)) {
                                                await launch(url);
                                              } else {
                                                throw 'Could not call ${itemList[index].mobileNumberPrimary}';
                                              }
                                            },
                                            child: Container(
                                              padding: EdgeInsets.all(4),
                                              height: 30,
                                              child: Text(
                                                itemList[index]
                                                    .mobileNumberPrimary ?? ' ', //'01819-415452',
                                                style: GoogleFonts.poppins(
                                                    // color: themeWhiteColor,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
        if (results.length == 0)
          Align(
            alignment: Alignment.bottomCenter,
            child: MyCustomButton(
                height: 30,
                width: MediaQuery.of(context).size.width / 1.5,
                color: themeGolderColor,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'See All Employee List',
                    style: gcNormalTextStyle,
                  ),
                ),
                onPressed: () {
                  print('tapped');
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => PdfViewerScreen()));
                  // const String  url =
                  //     NetworkManager.baseUrl + "/media/pdf/employee_list.pdf";
                  Navigator.push(
                      context,
                      MaterialPageRoute<dynamic>(
                        builder: (_) =>  PDFViewerFromUrl(url: NetworkManager.pdfUrl,),
                      ));
                }),
          ),
      ],
    );
  }
}

class Post {
  final String title;
  final String body;

  Post(this.title, this.body);
}
