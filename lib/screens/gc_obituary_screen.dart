import 'dart:convert';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flappy_search_bar/flappy_search_bar.dart';
// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/member_search_response.dart';
import 'package:gulshan_club_app/jsonModels/message_list_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class ObituaryScreen extends StatefulWidget {
  static const String route = '/obituary';

  @override
  _ObituaryScreenState createState() => _ObituaryScreenState();
}

class _ObituaryScreenState extends State<ObituaryScreen> {
  final SearchBarController<Post> _searchBarController = SearchBarController();
  bool isReplay = false;
  List<Member> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  bool checkingFlight = false;
  bool success = false;

  bool isSearchOn = false;

  UserListResponse? userJson;
  bool isLoadingMore = false;

  final TextEditingController _userIdCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    // getAllMembers();

    getFilteredMembers("");
    super.initState();
  }

  Future<List<Post>> _getALlPosts(String text) async {
    if (text.length > 0) {
      getFilteredMembers(text);
    } else {
      getFilteredMembers("");
    }

    List<Post> posts = [];

    var random = new Random();
    for (int i = 0; i < 10; i++) {
      posts.add(Post("$text $i", "body random number : ${random.nextInt(100)}"));
    }

    return []; //posts;
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Obituary',
      bottomNavigationBar: Resources.setBottomDrawer(context),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: TextStyle(),
                ))
              : (itemList.length == 0 && isSearchOn == false)
                  ? Center(
                      child: Text(
                        "No Member found",
                        style: gcPlaceholderTextStyle,
                      ),
                    )
                  : createListView(itemList, context),
    );
  }

  getAllMembers() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl + NetworkManager.apiString + NetworkManager.versionString + "/users/inactive-users/";
    print('url---> $url');
    if (result == true) {
      try {
        http.get(Uri.parse(url), headers: {"Authorization": "Bearer ${Resources.currentUser?.accessToken}"}).timeout(Duration(seconds: 30)).then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = UserListResponse.fromJson(data);
              print('result....\n ${employeeResponse.results}');
              if (value.statusCode == 200) {
                setState(() {
                  itemList = employeeResponse.results ?? [];
                  userJson = employeeResponse;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                  isSearchOn = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text('Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> _reloadData() async {
    setState(() {
      getFilteredMembers("");
    });
  }

  _loadMoreMember() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint = userJson?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = UserListResponse.fromJson(json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        userJson = moreNoticeResponse;
        List<Member> moreData = moreNoticeResponse.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  getFilteredMembers(String text) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl + NetworkManager.apiString + NetworkManager.versionString + "/users/inactive_user_search/?member_name=$text";
    print('url---> $url');
    if (result == true) {
      try {
        http.get(Uri.parse(url), headers: {"Authorization": "Bearer ${Resources.currentUser?.accessToken}"}).timeout(Duration(seconds: 30)).then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = MemberSearchResponse.fromJson(data);
              print('result....\n ${employeeResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  itemList = [];
                  isSearchOn = true;
                  itemList = employeeResponse.data ?? [];
                  userJson = null;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text('Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Widget createListView(List<Member> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {


    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Container(
                height: 80,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Container(
                  width: MediaQuery.of(context).size.width - 120,
                  child: TextFormField(
                    controller: _userIdCtrl,
                    focusNode: _userIdNode,
                    //validator: (value) => MyCustomValidator.validateIsEmpty(_userIdCtrl.text, 'userID', context),
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(
                          color: themeGolderColor,
                        ),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: themeGolderColor),
                        //  when the TextFormField in unfocused
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: themeGolderColor), //HexColor("#FF7300"
                        //  when the TextFormField in focused
                      ),
                      hintText: 'search',
                      hintStyle: gcPlaceholderTextStyle,
                    ),
                    onFieldSubmitted: (value) {},
                    style:  GoogleFonts.poppins(
                      fontSize: 16,color:Colors.white,
                    ),
                  ),
                ),

              ),
            ),
            MyCustomButton(
              height: 30,
              width: 80,
              color: themeGolderColor,//Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Ok',style: gcNormalTextStyle,),
              ),
              onPressed: () {
                print('tapped');
                print(_userIdCtrl.text);
                if (_userIdCtrl.text.length > 0) {
                  getFilteredMembers(_userIdCtrl.text);
                } else {
                  //getAllMembers();
                  setState(() {
                    itemList = [];
                    getFilteredMembers("");

                  });
                }
              },
            ),
          ],
        ),
        (results.length == 0)
            ? Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.asset(
              'assets/images/default_member_list_image.png',
              fit: BoxFit.contain,
              width: MediaQuery.of(context).size.width,
              height: 400,
            ),
          ),
        )
            : SizedBox(),
        Expanded(
          child: NotificationListener(
            onNotification: (ScrollNotification scrollInfo) {
              if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
                if (!isLoadingMore && userJson?.next != null) {
                  // start loading data
                  print("Scrolling to end...");
                  setState(() {
                    isLoadingMore = true;
                  });
                  _loadMoreMember();
                } else if (userJson?.next == null) {
                  print("Data end");
                  setState(() {
                    isLoadingMore = false;
                  });
                } else {}
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: _reloadData,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {


                  DateFormat dateFormat = DateFormat("yyyy-MM-dd");
                  DateTime dateTime = dateFormat.parse(itemList[index].birthday.toString());
                  String birthString = dateFormat.format(dateTime);
                  print(birthString);

                  DateTime dateTime2 = dateFormat.parse(itemList[index].deathDate.toString());
                  String deathString = dateFormat.format(dateTime2);
                  print(deathString);
                  String membershipDate = "";

                  if(itemList[index].membershipDate != null){

                    DateTime dateTime3 = dateFormat.parse(itemList[index].membershipDate.toString());
                    membershipDate = dateFormat.format(dateTime3);
                    print(membershipDate);
                  }



                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 32),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white38,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          image:   DecorationImage(image: AssetImage('assets/images/ic_list_background_1.png'), fit: BoxFit.cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),

                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top:8.0,bottom: 8,right: 8),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                    border: Border.all(color: Colors.black,width: 5,),
                                ),

                                height: 80,width: 80,
                                child: (itemList[index].imageMedium != null)
                                    ? CachedNetworkImage(
                                  imageUrl: NetworkManager.baseUrl + itemList[index].imageMedium!,
                                  imageBuilder: (context, imageProvider) => Container(

                                    decoration: BoxDecoration(
                                       image: DecorationImage(image: imageProvider, fit: BoxFit.fill, colorFilter: ColorFilter.mode(Colors.transparent, BlendMode.color)),
                                    ),
                                  ),
                                  placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                                  errorWidget: (context, url, error) => Icon(Icons.error),
                                ) //NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium)
                                    : Image.asset('assets/icons/ic_placeholder.png'),
                              ),
                            ),
                            Flexible(
                              // child: Card(color: Colors.white38,elevation: 0,
                              child:  Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 8,),
                                  (itemList[index].firstName != null)
                                      ? Text(
                                    itemList[index].firstName! + ' ' + itemList[index].lastName.toString() , //"Capt. Saifur Rahman",
                                    style: gcListTitleTextStyle,
                                  )
                                      : SizedBox(),

                                  (itemList[index].clubAcNumber != null && itemList[index].clubAcNumber != '' )
                                      ? Text(
                                    'A/C No: ' + itemList[index].clubAcNumber.toString(), //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                    style: gcDescriptionTextStyle,
                                  )
                                      : SizedBox(),
                                  (itemList[index].membershipDate != null )
                                      ? Text(
                                    'Member Since: ' +  membershipDate  , //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                    style: gcDescriptionTextStyle,textAlign: TextAlign.center,
                                  )
                                      : SizedBox(),

                                  (itemList[index].birthday != null )
                                      ? Text(
                                    'Born: ' + birthString , //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                    style: gcDescriptionTextStyle,textAlign: TextAlign.center,
                                  )
                                      : SizedBox(),

                                  (itemList[index].birthday != null && itemList[index].deathDate != '' )
                                      ? Text(
                                    'Died: ' +  deathString  , //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                    style: gcDescriptionTextStyle,textAlign: TextAlign.center,
                                  )
                                      : SizedBox(),

                                  // (itemList[index].designation != null)
                                  //     ? Container(
                                  //   decoration: BoxDecoration(color: themeGolderColor, borderRadius: BorderRadius.all(Radius.circular(8))),
                                  //   padding: EdgeInsets.all(4),
                                  //   child: Wrap(
                                  //     spacing: 8.0,
                                  //     children: [
                                  //       (itemList[index].designation.toString() == "President")
                                  //           ? SvgPicture.asset(
                                  //         'assets/icons/ic_crown.svg',
                                  //         height: 12,
                                  //         width: 12,
                                  //         color: themeWhiteColor,
                                  //       )
                                  //           : SizedBox(
                                  //         height: 1,
                                  //       ),
                                  //       (itemList[index].designation != null)
                                  //           ? Text(
                                  //         itemList[index].designation.toString(), //"Captain",
                                  //         style: gcNormalTextStyle,
                                  //       )
                                  //           : SizedBox(),
                                  //     ],
                                  //   ),
                                  // )
                                  //     : SizedBox(
                                  //   height: 1,
                                  // ),
                                  // (itemList[index].email != null)
                                  //     ? Text(
                                  //   itemList[index].email, //"captsaif@ebd-bd.com",
                                  //   style: listSubTitleTextStyle,
                                  // )
                                  //     : SizedBox(),
                                  //
                                  // (itemList[index].profession != null)
                                  //     ? Text(
                                  //   itemList[index].profession.toString(), //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                  //   style: gcDescriptionTextStyle,
                                  // )
                                  //     : SizedBox(),
                                  //
                                  // (itemList[index].phonePrimary != null && itemList[index].phonePrimary != '')? Card(
                                  //   color: themeGreyColor,
                                  //   elevation: 0,
                                  //   child: GestureDetector(
                                  //     onTap: () async {
                                  //       print('${itemList[index].phonePrimary}');
                                  //       //launch("tel://${itemList[index].mobileNumberPrimary}");
                                  //
                                  //       String url = "tel://" + itemList[index].phonePrimary.toString();
                                  //       if (await canLaunch(url)) {
                                  //         await launch(url);
                                  //       } else {
                                  //         throw 'Could not call ${itemList[index].phonePrimary}';
                                  //       }
                                  //     },
                                  //     child: Center(
                                  //       child: Container(
                                  //         padding: EdgeInsets.all(4),
                                  //         height: 30,
                                  //         child: Text(
                                  //           itemList[index].phonePrimary, //'01819-415452',
                                  //           style: listSubTitleBlackTextStyle,
                                  //         ),
                                  //       ),
                                  //     ),
                                  //   ),
                                  // ):SizedBox(),
                                ],
                              ),
                            ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                    // Container(
                    //   decoration: BoxDecoration(
                    //     color: Colors.white38,
                    //     borderRadius: BorderRadius.all(Radius.circular(8)),
                    //   ),
                    //   child: Padding(
                    //     padding: const EdgeInsets.all(8.0),
                    //     child: Row(
                    //       mainAxisAlignment: MainAxisAlignment.start,
                    //       crossAxisAlignment: CrossAxisAlignment.start,
                    //       children: [
                    //         Padding(
                    //           padding: const EdgeInsets.all(8.0),
                    //           child: CircleAvatar(
                    //             radius: 47,
                    //             backgroundColor: Colors.transparent,
                    //             backgroundImage: (itemList[index].imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium) : AssetImage('assets/icons/ic_placeholder.png'),
                    //             //AssetImage('assets/images/Facilities1.jpg'),
                    //           ),
                    //         ),
                    //         Flexible(
                    //           // child: Card(color: Colors.white38,elevation: 0,
                    //           child: Column(
                    //             mainAxisAlignment: MainAxisAlignment.start,
                    //             crossAxisAlignment: CrossAxisAlignment.start,
                    //             children: [
                    //               (itemList[index].designation != null)
                    //                   ? Container(
                    //                       decoration: BoxDecoration(color: themeGolderColor, borderRadius: BorderRadius.all(Radius.circular(8))),
                    //                       padding: EdgeInsets.all(4),
                    //                       child: Wrap(
                    //                         spacing: 8.0,
                    //                         children: [
                    //                           (itemList[index].designation.toString() == "President")
                    //                               ? SvgPicture.asset(
                    //                                   'assets/icons/ic_crown.svg',
                    //                                   height: 12,
                    //                                   width: 12,
                    //                                   color: themeWhiteColor,
                    //                                 )
                    //                               : SizedBox(
                    //                                   height: 1,
                    //                                 ),
                    //                           Text(
                    //                             itemList[index].designation.toString(), //"Captain",
                    //                             style: GoogleFonts.poppins(fontSize: 10, color: Colors.white),
                    //                           ),
                    //                         ],
                    //                       ),
                    //                     )
                    //                   : SizedBox(
                    //                       height: 1,
                    //                     ),
                    //               Text(
                    //                 itemList[index].firstName + ' ' + itemList[index].lastName, //"Capt. Saifur Rahman",
                    //                 style: gcListTitleTextStyle,
                    //               ),
                    //               Text(
                    //                 itemList[index].email, //"captsaif@ebd-bd.com",
                    //                 style: listSubTitleTextStyle,
                    //               ),
                    //               // SizedBox(
                    //               //   height: 10,
                    //               // ),
                    //               // Text(
                    //               //   itemList[index].profession.toString(), //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                    //               //   style: gcDescriptionTextStyle,
                    //               // ),
                    //               // RaisedButton(
                    //               //   color: themeGreyColor,
                    //               //   onPressed: () {
                    //               //     print('pressed');
                    //               //     setState(() {
                    //               //       Resources.selectedRecipientID = itemList[index].id.toString();
                    //               //       // Resources.selectedRecipient = Recipient(
                    //               //       //     address: itemList[index].address.toString(),
                    //               //       //     birthday: itemList[index].birthday.toString(),
                    //               //       //     id: itemList[index].id,
                    //               //       //     bloodGroup: itemList[index].bloodGroup.toString(),
                    //               //       //     clubAcNumber: itemList[index].clubAcNumber.toString(),
                    //               //       //     designation: itemList[index].designation.toString(),
                    //               //       //     education: itemList[index].education.toString(),
                    //               //       //     categoryName: itemList[index].categoryName
                    //               //       //
                    //               //       // );
                    //               //       Resources.isSearchOn = true;
                    //               //     });
                    //               //     Navigator.pushNamed(context, '/chat');
                    //               //     // showBottomSheet(
                    //               //     //   context: context,
                    //               //     //   builder: (context) => Container(
                    //               //     //     margin: const EdgeInsets.only(top: 5, left: 15, right: 15),
                    //               //     //     height: 160,
                    //               //     //     decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(15)), boxShadow: [BoxShadow(blurRadius: 10, color: Colors.grey[300], spreadRadius: 5)]),
                    //               //     //     child: Column(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.center, children: [
                    //               //     //       Container(
                    //               //     //           height: 50,
                    //               //     //           alignment: Alignment.center,
                    //               //     //           padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    //               //     //           margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    //               //     //           decoration: BoxDecoration(color: Colors.grey[300], borderRadius: BorderRadius.circular(10)),
                    //               //     //           child: TextField(
                    //               //     //             decoration: InputDecoration.collapsed(
                    //               //     //               hintText: 'Enter your reference number',
                    //               //     //             ),
                    //               //     //           )),
                    //               //     //       !checkingFlight
                    //               //     //           ? MaterialButton(
                    //               //     //               color: Colors.grey[800],
                    //               //     //               onPressed: () {
                    //               //     //
                    //               //     //
                    //               //     //               },
                    //               //     //               child: Text(
                    //               //     //                 'Check Flight',
                    //               //     //                 style: TextStyle(color: Colors.white),
                    //               //     //               ),
                    //               //     //             )
                    //               //     //           : !success
                    //               //     //               ? CircularProgressIndicator()
                    //               //     //               : Icon(
                    //               //     //                   Icons.check,
                    //               //     //                   color: Colors.green,
                    //               //     //                 )
                    //               //     //     ]),
                    //               //     //   ),
                    //               //     // );
                    //               //   },
                    //               //   child: Text(
                    //               //     'Message',
                    //               //     style: GoogleFonts.poppins(fontSize: 10, color: Colors.white),
                    //               //   ),
                    //               // )
                    //             ],
                    //           ),
                    //         ),
                    //         // ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class Post {
  final String title;
  final String body;

  Post(this.title, this.body);
}
