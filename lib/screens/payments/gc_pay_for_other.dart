import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
 import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/member_search_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';

class PayForOtherScreen extends StatefulWidget {
  @override
  _PayForOtherScreenState createState() => _PayForOtherScreenState();
}

class _PayForOtherScreenState extends State<PayForOtherScreen> {
  List<Member> itemList = [];
  var isLoading = false;
  bool isSearchOn = false;
  UserListResponse? userJson;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  bool isLoadingMore = false;

  final TextEditingController _userIdCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        title: Text(
          "Who do you want to pay?",
          style: gcTitleTextStyle,
        ),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: TextStyle(),
                ))
              // : (itemList.length == 0 && isSearchOn == false)
              //     ? Center(
              //         child: Text(
              //           "No Member found",
              //           style: gcPlaceholderTextStyle,
              //         ),
              //       )
                  : createListView(itemList, context),
    );
  }

  getAllMembers() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl + NetworkManager.apiString + NetworkManager.versionString + "/users/active-users/";
    print('url---> $url');
    if (result == true) {
      try {
        http.get(Uri.parse(url), headers: {"Authorization": "Bearer ${Resources.currentUser?.accessToken}"}).timeout(Duration(seconds: 30)).then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = UserListResponse.fromJson(data);
              print('result....\n ${employeeResponse.results}');
              if (value.statusCode == 200) {
                setState(() {
                  itemList = employeeResponse.results ?? [];
                  userJson = employeeResponse;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                  isSearchOn = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text('Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                // ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllMembers();
    });
  }

  _loadMoreMember() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint = userJson?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = UserListResponse.fromJson(json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        userJson = moreNoticeResponse;
        List<Member> moreData = moreNoticeResponse.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  getFilteredMembers(String text) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl + NetworkManager.apiString + NetworkManager.versionString + "/users/user_search/?member_name=$text";
    print('url---> $url');
    if (result == true) {
      try {
        http.get(Uri.parse(url), headers: {"Authorization": "Bearer ${Resources.currentUser?.accessToken}"}).timeout(Duration(seconds: 30)).then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = MemberSearchResponse.fromJson(data);
              print('result....\n ${employeeResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  isSearchOn = true;
                  itemList = employeeResponse.data ?? [];
                  userJson = null;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text('Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Widget createListView(List<Member> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Container(
                height: 80,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Container(
                  width: MediaQuery.of(context).size.width - 120,
                  child: TextFormField(
                    controller: _userIdCtrl,
                    focusNode: _userIdNode,
                    //validator: (value) => MyCustomValidator.validateIsEmpty(_userIdCtrl.text, 'userID', context),
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(
                          color: themeGolderColor,
                        ),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: themeGolderColor),
                        //  when the TextFormField in unfocused
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: themeGolderColor), //HexColor("#FF7300"
                        //  when the TextFormField in focused
                      ),
                      hintText: 'search',
                      hintStyle: gcPlaceholderTextStyle,
                    ),
                    onFieldSubmitted: (value) {},
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      color: Colors.white,
                    ),
                  ),
                ),

                // SearchBar<Post>(
                //   minimumChars: 0,
                //   searchBarPadding: EdgeInsets.symmetric(horizontal: 8),
                //   headerPadding: EdgeInsets.symmetric(horizontal: 8),
                //   listPadding: EdgeInsets.symmetric(horizontal: 8),
                //   onSearch: _getALlPosts,
                //   hintText: 'Search',
                //   textStyle: GoogleFonts.poppins(color: themeWhiteColor),
                //   searchBarController: _searchBarController,
                //   // placeHolder: Text("placeholder"),
                //   cancellationWidget: Text(
                //     "Cancel",
                //     style: GoogleFonts.poppins(color: themeWhiteColor),
                //   ),
                //   //emptyWidget: Text("empty"),
                //   indexedScaledTileBuilder: (int index) => ScaledTile.count(1, index.isEven ? 2 : 1),
                //
                //   onCancelled: () {
                //     print("Cancelled triggered");
                //     //getAllMembers();
                //     FocusScope.of(context).unfocus();
                //     setState(() {
                //       itemList = [];
                //     });
                //   },
                //   mainAxisSpacing: 10,
                //   crossAxisSpacing: 10,
                //   crossAxisCount: 2,
                //   onItemFound: (Post post, int index) {
                //     return Container(
                //       color: Colors.lightBlue,
                //       child: ListTile(
                //         title: Text(post.title),
                //         isThreeLine: true,
                //         subtitle: Text(post.body),
                //         onTap: () {},
                //       ),
                //     );
                //   },
                // ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top:8.0,bottom:24.0),
              child: MyCustomButton(
                height: 30,
                width: 80,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Ok',
                  ),
                ),
                onPressed: () {
                  print('tapped');
                  print(_userIdCtrl.text);
                  if (_userIdCtrl.text.length > 0) {
                    getFilteredMembers(_userIdCtrl.text);
                  } else {
                    //getAllMembers();
                    setState(() {
                      itemList = [];
                    });
                  }
                },
              ),
            ),
          ],
        ),
        (results.length == 0)
            ? Expanded(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.asset(
                    'assets/images/default_member_list_image.png',
                    fit: BoxFit.contain,
                    width: MediaQuery.of(context).size.width,
                    height: 400,
                  ),
                ),
              )
            : SizedBox(),
        Expanded(
          child: NotificationListener(
            onNotification: (ScrollNotification scrollInfo) {
              if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
                if (!isLoadingMore && userJson?.next != null) {
                  // start loading data
                  print("Scrolling to end...");
                  setState(() {
                    isLoadingMore = true;
                  });
                  _loadMoreMember();
                } else if (userJson?.next == null) {
                  print("Data end");
                  setState(() {
                    isLoadingMore = false;
                  });
                } else {}
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: _reloadData,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16),
                    child: GestureDetector(
                      onTap: () {
                        var member = itemList[index];
                        print(member.id);
                        Resources.selectedUserIDForPayment = member.id.toString();
                        print(Resources.selectedUserIDForPayment );
                        // Navigator.pushNamed(context, '/member_details', arguments: itemList[index]);
                        // Navigator.pushNamed(context, '/payment_methods',arguments: itemList[index]);
                        Navigator.pushNamed(context, '/payment_methods',arguments: true);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            // color: Colors.white38,
                            borderRadius: BorderRadius.all(Radius.circular(8)), // color: Colors.white38,
                            image: (index % 2 == 0)
                                ? DecorationImage(image: AssetImage('assets/images/ic_list_background_1.png'), fit: BoxFit.cover)
                                : DecorationImage(image: AssetImage('assets/images/ic_list_background_3.png'), fit: BoxFit.cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),
                            ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),

                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  height: 70,
                                  width: 60,
                                  child: (itemList[index].imageMedium != null)
                                      ? CachedNetworkImage(
                                    imageUrl: NetworkManager.baseUrl + itemList[index].imageMedium!,
                                    imageBuilder: (context, imageProvider) => Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        image: DecorationImage(image: imageProvider, fit: BoxFit.fitHeight, colorFilter: ColorFilter.mode(Colors.transparent, BlendMode.color)),
                                      ),
                                    ),
                                    placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                                    errorWidget: (context, url, error) => Icon(Icons.error),
                                  )
                                      : Image.asset(
                                    'assets/icons/ic_placeholder.png',
                                    height: 70,
                                    width: 60,
                                  ),
                                ), //AssetImage('assets/icons/ic_placeholder.png'),

                                //   CircleAvatar(
                                //     radius: 47,
                                //     backgroundColor: Colors.transparent,
                                //     backgroundImage: (itemList[index].imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium,) : AssetImage('assets/icons/ic_placeholder.png'),
                                //     //AssetImage('assets/images/Facilities1.jpg'),
                                //   ),
                              ),
                              Flexible(
                                // child: Card(color: Colors.white38,elevation: 0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [


                                    Text(
                                      itemList[index].firstName ?? ' ' +
                                          ' ' +
                                          itemList[index].lastName.toString()  , //"Capt. Saifur Rahman",
                                      style: gcPaymentListTitleTextStyle,
                                    ),
                                    (itemList[index].clubAcNumber != null && itemList[index].clubAcNumber != '')
                                        ? Text(
                                      'A/C No: ' + itemList[index].clubAcNumber.toString(), //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                      style: gcDescriptionTextStyle,
                                    )
                                        : SizedBox(),

                                    // (itemList[index].designation != null)
                                    //     ? Container(
                                    //         // decoration: BoxDecoration(color: themeGolderColor, borderRadius: BorderRadius.all(Radius.circular(8))),
                                    //         padding: EdgeInsets.all(4),
                                    //         child: Wrap(
                                    //           spacing: 8.0,
                                    //           children: [
                                    //             (itemList[index].designation.toString() == "President")
                                    //                 ? SvgPicture.asset(
                                    //                     'assets/icons/ic_crown.svg',
                                    //                     height: 12,
                                    //                     width: 12,
                                    //                     color: themeWhiteColor,
                                    //                   )
                                    //                 : SizedBox(
                                    //                     height: 1,
                                    //                   ),
                                    //             Text(
                                    //               itemList[index].designation.toString(), //"Captain",
                                    //               style: GoogleFonts.poppins(fontSize: 10, color: Colors.white),
                                    //             ),
                                    //           ],
                                    //         ),
                                    //       )
                                    //     : SizedBox(
                                    //         height: 1,
                                    //       ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(0.0),
                                          child: SvgPicture.asset(
                                            'assets/icons/ic_mail.svg',
                                            color: themeGolderColor,width: 10,height: 10,
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(vertical: 4),
                                            child: Text(
                                              "  " + itemList[index].email.toString(), //"captsaif@ebd-bd.com",
                                              style: gcDescriptionTextStyle,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 1,
                                    ),

                                    (itemList[index].phonePrimary != null && itemList[index].phonePrimary != '')
                                        ? GestureDetector(
                                              onTap: () async {
                                                print('${itemList[index].phonePrimary}');
                                                //launch("tel://${itemList[index].mobileNumberPrimary}");

                                                String url = "tel://" + itemList[index].phonePrimary.toString();
                                                if (await canLaunch(url)) {
                                                  await launch(url);
                                                } else {
                                                  throw 'Could not call ${itemList[index].phonePrimary}';
                                                }
                                              },
                                                child: Row(
                                                  children: [
                                                    Padding(
                                                      padding: const EdgeInsets.all(0.0),
                                                      child: SvgPicture.asset(
                                                        'assets/icons/ic_call.svg',
                                                        color: themeGolderColor,width: 10,height: 10,
                                                      ),
                                                    ),
                                                    Container(
                                                      padding: EdgeInsets.symmetric(horizontal: 4),
                                                      height: 20,
                                                      child: Text(
                                                        itemList[index].phonePrimary ?? ' ', //'01819-415452',
                                                        style: gcDescriptionTextStyle
                                                        // GoogleFonts.poppins(
                                                        //      color: themeWhiteColor,
                                                        //     fontWeight: FontWeight.bold,
                                                        //     fontSize: 12),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                            )
                                        : SizedBox(),

                                  ],
                                ),
                              ),

                              Container(
                                height: 40,width: 40,
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SvgPicture.asset(
                                        'assets/icons/Payment_Next.svg',
                                        color: themeGolderColor,width: 30,height: 30,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
