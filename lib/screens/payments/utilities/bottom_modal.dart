import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/screens/payments/utilities/restart_app.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_font_size.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';

class ModalFit extends StatefulWidget {
  const ModalFit({Key? key}) : super(key: key);

  @override
  _ModalFitState createState() => _ModalFitState();
}

class _ModalFitState extends State<ModalFit> {
  @override
  Widget build(BuildContext context) {
    return Material(
        child: SafeArea(
      top: false,
      child: Container(
        color: themeGolderColor,// listTileColor,//themeGolderColor.withOpacity(0.8),
        child: Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ListTile(
                  title: Text(
                    'Small',
                    style: gcNormalTextStyle,
                  ),
                  // leading: Icon(Icons.edit),
                  onTap: () {
                    print("small");
                    SharedPref.saveDouble("fontScale", -2.0);
                    SharedPref.saveString("fontSize", "Small" );
                    setState(() {
                      v1.fontSize = -2.0;
                      kLarge = 22.0 + v1.fontSize;
                      kTitle = 18.0 + v1.fontSize;
                      kMedium = 16.0 + v1.fontSize;
                      kSmall = 14.0 + v1.fontSize;
                      changeFontSize();
                    });
                    print(v1.fontSize);
                    print(kLarge);
                    Navigator.pop(context);
                    //RestartWidget.restartApp(context);
                  }),
              ListTile(
                  title: Text(
                    'Medium',
                    style: gcNormalTextStyle,
                  ),
                  // leading: Icon(Icons.content_copy),
                  onTap: () {
                    print("Medium");
                    SharedPref.saveString("fontSize", "Medium" );
                    SharedPref.saveDouble("fontScale", 0.0);
                    setState(() {
                      FontSizeCalc().fontSize = 0.0;
                      kLarge = 22.0 + v1.fontSize;
                      kTitle = 18.0 + v1.fontSize;
                      kMedium = 16.0 + v1.fontSize;
                      kSmall = 14.0 + v1.fontSize;
                      changeFontSize();
                    });
                     Navigator.pop(context);
                    //RestartWidget.restartApp(context);
                  }),
              ListTile(
                  title: Text(
                    'Large',
                    style: gcNormalTextStyle,
                  ),
                  // leading: Icon(Icons.content_cut),
                  onTap: () {
                    print("Large");
                    SharedPref.saveString("fontSize", "Large" );

                    SharedPref.saveDouble("fontScale", 2.0);
                    setState(() {
                      FontSizeCalc().fontSize = 2.0;
                      kLarge = 22.0 + v1.fontSize;
                      kTitle = 18.0 + v1.fontSize;
                      kMedium = 16.0 + v1.fontSize;
                      kSmall = 14.0 + v1.fontSize;
                      changeFontSize();
                    });
                    print(v1.fontSize);
                    print(kLarge);
                    Navigator.pop(context);
                  }),
            ],
          ),
        ),
      ),
    ));
  }

  void changeFontSize() {

    gcTitleTextStyle = TextStyle(
      fontSize: kTitle,fontFamily: 'Siri-Regular',
      color:themeWhiteColor,
    );
// GoogleFonts.poppins(
//   fontSize: 18,color: themeWhiteColor,
// );

    gcSubTitleTextStyle =  TextStyle(
      fontSize: kMedium,color: themeWhiteColor,fontFamily: 'Siri-Regular',
    );

    gcNormalTextStyle = TextStyle(
      fontSize: kSmall,fontFamily: 'Siri-Regular',
      color:themeWhiteColor,
    );
// GoogleFonts.poppins(
//   fontSize: 14,color: themeWhiteColor,
// );

    gcWhatsAppTextStyle = GoogleFonts.poppins(
      fontSize: 18,color: Colors.green,
    );


    gcKLargeListTitleTextStyle = TextStyle(
      fontSize: kLarge,fontFamily: 'Siri-Medium',
      color:listTileColor,
    );

    gcListTitleTextStyle = TextStyle(
      fontSize: kTitle,fontFamily: 'Siri-Medium',
      color:listTileColor,
    );
// GoogleFonts.poppins(
//   fontSize: 17,color: listTileColor,
// );

    listSubTitleTextStyle = TextStyle(
      fontSize: kMedium,fontFamily: 'Siri-Medium',
      color:listSubTitleColor,
    );
// GoogleFonts.poppins(
//   fontSize: 14,color: listSubTitleColor,
//);


    listSubTitleBlackTextStyle = TextStyle(
      fontSize: kMedium,fontFamily: 'Siri-Medium',
      color:themeBlackColor,
    );

    gcDescriptionTextStyle = TextStyle(
      fontSize: kSmall,fontFamily: 'Siri-Regular',
      color:listDescriptionColor,
    );
// GoogleFonts.poppins(
//     fontSize: 13,color: listDescriptionColor
// );


    gcGreyDescriptionTextStyle = TextStyle(
      fontSize: kSmall,fontFamily: 'Siri-Regular',
      color:themeGreyColor,
    );

    gcPaymentDescriptionTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: kSmall,
      color:listDescriptionColor,
    );
// GoogleFonts.poppins(
//     fontSize: 10,color: listDescriptionColor
// );

    gcUnderlinedTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
        fontSize: kSmall,color: themeGreyColor,decoration: TextDecoration.underline
    );

    gcPlaceholderTextStyle =  TextStyle(
      fontSize: kSmall,fontFamily: 'Siri-Regular',
      color:themeGreyColor,
    );

    gcBkashPlaceholderTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
        fontSize: kMedium,color: Colors.pink
    );

    gcInputFieldTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
        fontSize: kMedium,color:Colors.white
    );



    gcPaymentPlaceholderTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: 8,color: listDescriptionColor,
    );

    gcPaymentTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: kSmall,color: listTileColor,
    );

// GoogleFonts.poppins(
//   fontSize: 14,color: listSubTitleColor,
//);


    gcPaymentListTitleTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: kSmall,color: listTileColor,
    );
    /// scaffold > title
    ScaffoldAppBarTitle = TextStyle(
      fontSize: kLarge,fontFamily: 'Siri-Medium',
      color:themeWhiteColor,
    );

    /// scaffold > icon text
    ScaffoldAppBarIconText =  TextStyle(fontFamily: 'Siri-Regular',
      color: Color.fromRGBO(105, 105, 105, 1),
    );

    /// scaffold > drawer > navigation text
    ScaffoldAppBarDrawer =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: kSmall,
      color: Colors.white,
    );

    normalBodyTextWithGreyColor =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: kSmall,
      color: Colors.grey,
    );

    greenTitleTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: kLarge,
      fontWeight: FontWeight.bold,
      color: Colors.lightGreen,
    );

    buttonTitleStyle =  TextStyle(fontFamily: 'Siri-Regular',
      fontSize: kMedium,
      fontWeight: FontWeight.w300,
      color: Colors.white,
    );
  }
}
