// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'bkash_webview_payment.dart';


enum Intent { sale, authorization }


class BkashPaymentScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BkashPaymentScreenState();
  }

}

class _BkashPaymentScreenState extends State<BkashPaymentScreen> {

  final GlobalKey<ScaffoldMessengerState> _scaffoldKey = new GlobalKey<ScaffoldMessengerState>();
  TextEditingController _amountController = TextEditingController();

  Intent _intent = Intent.sale;
  late FocusNode focusNode ;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text("BKash Payment")),
      body: SingleChildScrollView(
        padding: const EdgeInsets.fromLTRB(80, 40, 80, 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Amount :'),
            TextField(
              focusNode: focusNode,
              controller: _amountController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                isDense: true,
                hintText: "15000",
              ),
            ),
            const SizedBox(height: 100.0),
            ListTile(
              title: const Text('Immediate'),
              leading: Radio(
                value: Intent.sale,
                groupValue: _intent,
                onChanged: (Intent? value) {
                  setState(() => _intent = value ?? Intent.sale);
                },
              ),
              dense: true,
            ),
            ListTile(
              title: const Text('Auth and Capture'),
              leading: Radio(
                value: Intent.authorization,
                groupValue: _intent,
                onChanged: (Intent? value) {
                  setState(() => _intent = value ?? Intent.sale);
                },
              ),
              dense: true,
            ),
            const SizedBox(height: 6.0),
            TextButton(
              // height: 30.0,
              // minWidth: double.infinity,
              // color: Colors.pinkAccent,
              style: ButtonStyle(
                elevation: MaterialStateProperty.all(5),
                minimumSize: MaterialStateProperty.all(Size(double.infinity,30)),
                backgroundColor: MaterialStateProperty.all(Colors.pinkAccent),
              ),
              // textColor: Colors.white,
              child: Text("Checkout",style: TextStyle(color: Colors.white),),
              onPressed: () {
                String amount = _amountController.text.trim();
                String intent = _intent == Intent.sale ? "sale" : "authorization";

                if (amount.isEmpty) {
                  _scaffoldKey.currentState!
                      .showSnackBar(SnackBar(content: Text("Amount is empty. You can't pay through bkash. Try again")));
                  return;
                }
                // remove focus from TextField to hide keyboard
                focusNode.unfocus();
                // Goto BkashPayment page & pass the params
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => BkashWebViewPayment(amount: amount, intent: intent)));
              },
            )
          ],
        ),
      ),
    );
  }

}

