// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/payments/city_bank_payment/city_bank_webview_payment.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'package:http/http.dart' as http;

class CityPaymentScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CityPaymentScreenState();
  }
}

class _CityPaymentScreenState extends State<CityPaymentScreen> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldKey = new GlobalKey<ScaffoldMessengerState>();
  TextEditingController _amountController = TextEditingController();
  TextEditingController _refController = TextEditingController();

  FocusNode amountNode = FocusNode();
  FocusNode refNode = FocusNode();
  bool isApiDataCalled = false;
  bool isLoading = false;
  bool value = false;
  @override
  void initState() {
    super.initState();

    amountNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    amountNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        title: Text("City Bank Payment"),
        backgroundColor: Colors.pink,
      ),
      body: (isApiDataCalled
          == true) ? Center(
        child: CircularProgressIndicator(),
      ) : SingleChildScrollView(
        // padding: const EdgeInsets.fromLTRB(20, 40, 80, 20),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 80,
              ),
              Text('Amount :', style: gcListTitleTextStyle),
              TextField(
                focusNode: amountNode,
                controller: _amountController,
                keyboardType: TextInputType.number,
                style: gcListTitleTextStyle,
                decoration: InputDecoration(
                  hintText: "type amount here ",
                  hintStyle: gcListTitleTextStyle,
                  border: new UnderlineInputBorder(
                    borderSide: new BorderSide(
                      color: themeGolderColor,
                    ),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.pink),
                    //  when the TextFormField in unfocused
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide:
                        BorderSide(color: Colors.pink), //HexColor("#FF7300"
                    //  when the TextFormField in focused
                  ),
                  // decoration: InputDecoration(
                  //   isDense: true,
                  //   hintText: "type amount here ",hintStyle: gcListTitleTextStyle
                ),
              ),
              // const SizedBox(height: 16.0),
              //
              // Text('Account Number :', style: gcListTitleTextStyle),
              // TextField(
              //   focusNode: refNode,
              //   controller: _refController,
              //   keyboardType: TextInputType.text,style: gcListTitleTextStyle,
              //   maxLines: 4,
              //   decoration: InputDecoration(
              //     isDense: true,
              //     hintText: "type text here ",hintStyle: gcListTitleTextStyle,
              //     border: new UnderlineInputBorder(
              //       borderSide: new BorderSide(
              //         color: themeGolderColor,
              //       ),
              //     ),
              //     enabledBorder: UnderlineInputBorder(
              //       borderSide: BorderSide(color: Colors.pink),
              //       //  when the TextFormField in unfocused
              //     ),
              //     focusedBorder: UnderlineInputBorder(
              //       borderSide: BorderSide(color: Colors.pink), //HexColor("#FF7300"
              //       //  when the TextFormField in focused
              //     ),
              //   ),
              // ),
              const SizedBox(height: 30.0),
              Container(
                    child: Text(
                      "Minimum value of transaction BDT. 500/- Maximum value of transaction BDT. 500,000/- and transaction number per day 50 and per month 400.",
                      style: gcPaymentAlertStyle,
                      textAlign: TextAlign.justify,
                    ),
              ),
              // ListTile(
              //   title: const Text('Immediate'),
              //   leading: Radio(
              //     value: Intent.sale,
              //     groupValue: _intent,
              //     onChanged: (value) {
              //       setState(() => _intent = value);
              //     },
              //   ),
              //   dense: true,
              // ),
              // ListTile(
              //   title: const Text('Auth and Capture'),
              //   leading: Radio(
              //     value: Intent.authorization,
              //     groupValue: _intent,
              //     onChanged: (value) {
              //       setState(() => _intent = value);
              //     },
              //   ),
              //   dense: true,
              // ),
              const SizedBox(height: 30.0),
              SizedBox(
                height: 40,
                child: TextButton(
                  style: ButtonStyle(
                    elevation: MaterialStateProperty.all(0),
                    backgroundColor: value == false ? MaterialStateProperty.all(Colors.grey): MaterialStateProperty.all(Colors.pinkAccent),
                  ),
                  // height: 40.0,
                  // minWidth: double.infinity,
                  //
                  // //color: Colors.pinkAccent,
                  // //textColor: Colors.white,
                  // color: value == false ? Colors.grey : Colors.pinkAccent,
                  // textColor: value == false
                  //     ? Colors.white.withOpacity(0.3)
                  //     : Colors.white,
                  child: Text("Go For Payment",style: TextStyle(color: value == false
                      ? Colors.white.withOpacity(0.3)
                      : Colors.white,),),
                  onPressed: () {
                    String amount = _amountController.text.trim();
                    String reference = _refController.text.trim();

                    if (amount.isEmpty) {
                      _scaffoldKey.currentState?.showSnackBar(SnackBar(
                          content:
                              Text("Please fill up all the fields & Try again")));
                      return;
                    }
                    // remove focus from TextField to hide keyboard
                    amountNode.unfocus();
                    if (value == false) {
                      _scaffoldKey.currentState?.showSnackBar(SnackBar(
                          content:
                              Text("Please Agree with Terms and Conditions")));
                      return;
                    } else {
                      var amountValue = int. parse(_amountController.text);
                      if(amountValue < 500 || amountValue > 500000){
                        _scaffoldKey.currentState?.showSnackBar(SnackBar(
                            content:
                            Text("Please Enter an amount between 500 to 500000")));
                        return;
                      }
                      else{
                        setState(() {
                          isApiDataCalled =true;
                        });
                        goForPayment(amount, reference);
                      }
                    }

                    // Goto BkashPayment page & pass the params
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context) => CityBankWebViewPayment(amount: amount, reference: reference)));
                  },
                ),
              ),
              const SizedBox(height: 16.0),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.all(0),
                      child: Theme(
                        data: ThemeData(unselectedWidgetColor: Colors.white),
                        child: SizedBox(
                          height: 24.0,
                          width: 24.0,
                          child: Checkbox(
                            checkColor: Colors.black,
                            activeColor: const Color(0xFFB18E4E),
                            value: this.value,
                            onChanged: (bool? value) {
                              setState(() {
                                this.value = value ?? false;
                                print(value);
                              });
                            },
                          ),
                        ), //Ch
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Text(
                          "I have agreed with the terms and conditions of Gulshan Club Payment Service",
                          style: gcPaymentAlertStyle,
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // const SizedBox(height: 26.0),
              // SizedBox(
              //   height: 100,
              //   width: 100,
              //   child: Center(
              //     child: CircularProgressIndicator(),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }

  goForPayment(String amount, String reference) async {

    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/payments/pay-city-bank/";
    print(
        'url---> $url'); //http://58.84.34.65:9090/api/v1/payments/pay-city-bank/
    print("${Resources.myUserID.toString()}#" +
        "${Resources.selectedUserIDForPayment.toString()}" +
        reference);
    Connectivity().checkConnectivity().then((connection) async {
      if (connection == ConnectivityResult.mobile || connection == ConnectivityResult.wifi) {

          try {
            var uri = Uri.parse(url);
            http.MultipartRequest request = new http.MultipartRequest('POST', uri);
            request.headers['Authorization'] =
            "Bearer ${Resources.currentUser?.accessToken}";
            request.headers["Content-Type"] = 'multipart/form-data';
            request.fields["amount"] = amount;
            request.fields["reference"] = "${Resources.myUserID.toString()}#" +
                "${Resources.selectedUserIDForPayment.toString()}"; //+ reference;
            var streamedResponse = await request.send();

            Future.delayed(
                Duration.zero,
                    () => setState(() {
                  isLoading = false;
                }));

            print(streamedResponse.statusCode);

            if (streamedResponse.statusCode == 200 ||
                streamedResponse.statusCode == 201) {
              print("Image Uploaded");
              Future.delayed(
                  Duration.zero,
                      () => setState(() {
                    isLoading = false;
                  }));
              // showAlertDialog(context, getTranslatedValue(context, 'support_alert'));
            } else {
              Future.delayed(
                  Duration.zero,
                      () => setState(() {
                    isLoading = false;
                  }));
              print("Upload Failed");
            }
            var jsonDatas = await http.Response.fromStream(streamedResponse);
            Map<String, dynamic> jsonggg = jsonDecode(jsonDatas.body);
            print(jsonggg);
            bool status = jsonggg['status'];
            String payUrl = jsonggg['pay_url'];

            print(status);
            print(payUrl);
            setState(() {
              isApiDataCalled =false;
            });
            // Navigator.pushNamedAndRemoveUntil(context, '/building_directory', (route) => false);
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => CityBankWebViewPayment(
                    amount: amount, reference: reference, payUrl: payUrl)));


          } catch (e) {
            print(e);
            setState(() {
              isLoading = false;
            });
          }

      } else {
        Fluttertoast.showToast(msg: 'Please check your internet connection!',backgroundColor: Colors.red);
      }
    });

  }
}
