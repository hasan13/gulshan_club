import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:gulshan_club_app/screens/payments/utilities/js_interface.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CityBankWebViewPayment extends StatefulWidget {
  final String amount;
  final String reference;
  final String payUrl;

  const CityBankWebViewPayment(
      {Key? key,
      required this.amount,
      required this.reference,
      required this.payUrl})
      : super(key: key);

  @override
  _CityBankWebViewPaymentState createState() => _CityBankWebViewPaymentState();
}

class _CityBankWebViewPaymentState extends State<CityBankWebViewPayment> {
  late WebViewController _controller;
  late JavaScriptInterface _javaScriptInterface;

  bool isLoading = true;
  var paymentRequest = "";

  final GlobalKey webViewKey = GlobalKey();

  // InAppWebViewController webViewController;
  // InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
  //     crossPlatform: InAppWebViewOptions(
  //       useShouldOverrideUrlLoading: true,
  //       mediaPlaybackRequiresUserGesture: false,
  //     ),
  //     android: AndroidInAppWebViewOptions(useHybridComposition: true, mixedContentMode: AndroidMixedContentMode.MIXED_CONTENT_ALWAYS_ALLOW),
  //     ios: IOSInAppWebViewOptions(
  //       allowsInlineMediaPlayback: true,
  //     ));

  @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();//SurfaceAndroidWebView();//AndroidWebView

    _javaScriptInterface = JavaScriptInterface(context);

    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      // ..addJavaScriptChannel(name, onMessageReceived: onMessageReceived)
      ..addJavaScriptChannel('approveUrl', onMessageReceived: (message) {
        _javaScriptInterface.onPaymentSuccess(message.message);
      })
      ..addJavaScriptChannel('cancelUrl', onMessageReceived: (message) {
        _javaScriptInterface = JavaScriptInterface(context);
        _javaScriptInterface.onPaymentSuccess(message.message);
      })
      ..addJavaScriptChannel('declineUrl', onMessageReceived: (message) {
        _javaScriptInterface = JavaScriptInterface(context);
        _javaScriptInterface.onPaymentSuccess(message.message);
      })
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {
            setState(() => isLoading = false);
          },
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(widget.payUrl));
    // var request = PaymentRequest(widget.amount, widget.reference);
    // paymentRequest = "{paymentRequest: ${jsonEncode(request)}}";
    // print(paymentRequest);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('City Bank Payment'),
        backgroundColor: Colors.pinkAccent,
      ),
      body: Stack(
        children: [
          // (isLoading == true)? Center(child: CircularProgressIndicator()):
          WebViewWidget(
            // initialUrl: widget.payUrl,//'assets/www/checkout_120.html', // api host link .
            // javascriptMode: JavascriptMode.unrestricted,
            //  navigationDelegate: (request) {
            //
            //   return NavigationDecision.navigate;
            // },
            // javascriptChannels: Set.from([
            //   JavascriptChannel(
            //       name: 'approveUrl',
            //       onMessageReceived: (JavascriptMessage message) {
            //         _javaScriptInterface.onPaymentSuccess(message.message);
            //       }),
            //   JavascriptChannel(
            //       name: 'cancelUrl',
            //       onMessageReceived: (JavascriptMessage message) {
            //         _javaScriptInterface = JavaScriptInterface(context);
            //         _javaScriptInterface.onPaymentSuccess(message.message);
            //       }),
            //   JavascriptChannel(
            //       name: 'declineUrl',
            //       onMessageReceived: (JavascriptMessage message) {
            //         _javaScriptInterface = JavaScriptInterface(context);
            //         _javaScriptInterface.onPaymentSuccess(message.message);
            //       })
            //
            //
            // ]),

            // onWebViewCreated: (WebViewController webViewController) {
            //   setState(() {
            //     _controller = webViewController;
            //     _controller.clearCache();
            //
            //   });
            // },
            // onPageFinished: (_) {
            //   // _controller.evaluateJavascript("javascript:callReconfigure($paymentRequest)");
            //   // _controller.evaluateJavascript("javascript:clickPayButton()");
            //   setState(() => isLoading = false);
            // },
            controller: _controller,
          ),
          // InAppWebView(
          //   key: webViewKey,
          //   // contextMenu: contextMenu,
          //   initialUrlRequest: URLRequest(url: Uri.parse(widget.payUrl)),
          //   // initialFile: "assets/index.html",
          //   // initialUserScripts: UnmodifiableListView<UserScript>([]),
          //   initialOptions: options,
          //   // pullToRefreshController: pullToRefreshController,
          //   onWebViewCreated: (controller) {
          //     webViewController = controller;
          //   },
          //   onLoadStart: (controller, url) {},
          //   androidOnPermissionRequest: (controller, origin, resources) async {
          //     return PermissionRequestResponse(resources: resources, action: PermissionRequestResponseAction.GRANT);
          //   },
          //   shouldOverrideUrlLoading: (controller, navigationAction) async {
          //     // var uri = navigationAction.request.url!;
          //     //
          //     // if (![
          //     //   "http",
          //     //   "https",
          //     //   "file",
          //     //   "chrome",
          //     //   "data",
          //     //   "javascript",
          //     //   "about"
          //     // ].contains(uri.scheme)) {
          //     //   // if (await canLaunch(url)) {
          //     //   //   // Launch the App
          //     //   //   await launch(
          //     //   //     url,
          //     //   //   );
          //     //     // and cancel the request
          //     //     return NavigationActionPolicy.CANCEL;
          //     //   }
          //     // }
          //     //
          //     return NavigationActionPolicy.ALLOW;
          //   },
          //   onLoadStop: (controller, url) async {
          //     // pullToRefreshController.endRefreshing();
          //     // setState(() {
          //     //   this.url = url.toString();
          //     //   urlController.text = this.url;
          //     // });
          //   },
          //   onLoadError: (controller, url, code, message) {
          //     // pullToRefreshController.endRefreshing();
          //   },
          //   onProgressChanged: (controller, progress) {
          //     // if (progress == 100) {
          //     //   pullToRefreshController.endRefreshing();
          //     // }
          //     // setState(() {
          //     //   this.progress = progress / 100;
          //     //   urlController.text = this.url;
          //     // });
          //   },
          //   onUpdateVisitedHistory: (controller, url, androidIsReload) {
          //     // setState(() {
          //     //   this.url = url.toString();
          //     //   urlController.text = this.url;
          //     // });
          //   },
          //   onConsoleMessage: (controller, consoleMessage) {
          //     print(consoleMessage);
          //   },
          // ),
        ],
      ),
    );
  }
}
