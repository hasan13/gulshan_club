import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/payments/models/bkash_response.dart';
import 'package:gulshan_club_app/screens/payments/models/payment_request.dart';
import 'package:gulshan_club_app/screens/payments/utilities/js_interface.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart' show rootBundle;
// #docregion platform_imports
// Import for Android features.
import 'package:webview_flutter_android/webview_flutter_android.dart';
// Import for iOS features.
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
// #enddocregion platform_imports

class BkashWebViewPayment extends StatefulWidget {
  final String amount;
  final String intent;
  final String? userID;

  const BkashWebViewPayment(
      {Key? key, required this.amount, required this.intent, this.userID})
      : super(key: key);

  @override
  _BkashWebViewPaymentState createState() => _BkashWebViewPaymentState();
}

class _BkashWebViewPaymentState extends State<BkashWebViewPayment> {
  late WebViewController _controller;
  late JavaScriptInterface _javaScriptInterface;
  late final PlatformWebViewControllerCreationParams params;
  bool isLoading = true;
  var paymentRequest = "";

  String bkashUrl = "";
  @override
  void initState() {
    super.initState();

    var request = PaymentRequest(widget.amount, widget.intent);
    paymentRequest = "{paymentRequest: ${jsonEncode(request)}}";
    print(paymentRequest);

    bkashUrl = "https://gulshanclub.com/bkash/payment.php" +
        "?amount=${widget.amount}&appToken=${Resources.currentUser?.accessToken}&payTo=${widget.userID}";
    //https://gulshanclub.com/bkash/payment.php
    //https://workspaceit.com/bkash_sandbox/payment.php?amount=13&appToken=l6DDPmD0R6SJLDe7GK2EYqQxHLtyfF
    //https://gulshanclub.com/bkash/payment.php?amount=21&appToken=emJGdyj78xld4dFwUSGotfkTD09v61&payTo=84
    print(bkashUrl);
// #docregion platform_features
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    // #enddocregion platform_features


    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..addJavaScriptChannel('GoBack', onMessageReceived: (message) {
        print("Go Back");
        print(message);
        print(message.message.toString());
        var bkashResponse = bkashResponseFromJson(message.message.toString());

        // _showAlert(context,bkashResponse);

        if (bkashResponse.paymentId == null) {
          Widget okButton = TextButton(
            child: Container(
                width: 100,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Center(child: Text("OK")))),
            onPressed: () {
              Navigator.pop(context);
              Navigator.pop(context);
              // Resources.moveTo(1, context);

              // Navigator.pop(context);
            },
          );
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    backgroundColor: Colors.pink,
                    title: Text(
                      "Payment Failed!",
                      style: gcTitleTextStyle,
                    ),
                    actions: [okButton],
                    content: Wrap(
                      alignment: WrapAlignment.center,
                      direction: Axis.vertical,
                      children: [
                        Text(
                          bkashResponse.errorMessage.toString(),
                          style: gcSubTitleTextStyle,
                        ),
                      ],
                    ),
                  ));
        } else {
          _showAlert(context, bkashResponse);
        }
      })
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {
            setState(() => isLoading = false);


          },
          onWebResourceError: (WebResourceError error) {
            debugPrint('''
                  Page resource error:
                      code: ${error.errorCode}
                               description: ${error.description}
                             errorType: ${error.errorType}
                         isForMainFrame: ${error.isForMainFrame}
          ''');
          },
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              debugPrint('blocking navigation to ${request.url}');
              return NavigationDecision.prevent;
            }
            debugPrint('allowing navigation to ${request.url}');
            return NavigationDecision.navigate;
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
        ),
      )
      ..loadRequest(Uri.parse(bkashUrl));

    // #docregion platform_features
    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }
    // #enddocregion platform_features

    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('bKash Checkout'),
        backgroundColor: Colors.pinkAccent,
      ),
      body: Stack(
        children: [
          // Here I patched the default WebView plugin to load
          // local html (along with Javascript and CSS) files from local assets.
          // ref -> https://medium.com/flutter-community/loading-local-assets-in-webview-in-flutter-b95aebd6edad
          // WebView(
          //   initialUrl:
          //       bkashUrl, //"https://workspaceit.com/bkash_sandbox/payment.php?amount=${widget.amount}", //'http://localhost:80/bkash/payment.php', // api host link .
          //   //initialUrl:'assets/www/checkout_120.html',// api host link .
          //   javascriptMode: JavascriptMode.unrestricted,
          //
          //   navigationDelegate: (NavigationRequest request) {
          //     print("WEBVIEW VALUES: $request");
          //     return NavigationDecision.navigate;
          //   },
          //   javascriptChannels: Set.from([
          //
          //     // JavascriptChannel(
          //     //     name: 'GoBack',
          //     //     onMessageReceived: (JavascriptMessage message) async {
          //     //       print("Go Back");
          //     //       print(message);
          //     //       print(message.message.toString());
          //     //       var bkashResponse =
          //     //           bkashResponseFromJson(message.message.toString());
          //     //
          //     //       // _showAlert(context,bkashResponse);
          //     //
          //     //       if (bkashResponse.paymentId == null) {
          //     //         Widget okButton = TextButton(
          //     //           child: Container(
          //     //               width: 100,
          //     //               padding: EdgeInsets.all(8),
          //     //               decoration: BoxDecoration(
          //     //                 color: Colors.white,
          //     //               ),
          //     //               child: ClipRRect(
          //     //                   borderRadius:
          //     //                       BorderRadius.all(Radius.circular(8)),
          //     //                   child: Center(child: Text("OK")))),
          //     //           onPressed: () {
          //     //             Navigator.pop(context);
          //     //             Navigator.pop(context);
          //     //             // Resources.moveTo(1, context);
          //     //
          //     //             // Navigator.pop(context);
          //     //           },
          //     //         );
          //     //         showDialog(
          //     //             context: context,
          //     //             builder: (context) => AlertDialog(
          //     //                   backgroundColor: Colors.pink,
          //     //                   title: Text(
          //     //                     "Payment Failed!",
          //     //                     style: gcTitleTextStyle,
          //     //                   ),
          //     //                   actions: [okButton],
          //     //                   content: Wrap(
          //     //                     alignment: WrapAlignment.center,
          //     //                     direction: Axis.vertical,
          //     //                     children: [
          //     //                       Text(
          //     //                         bkashResponse.errorMessage.toString(),
          //     //                         style: gcSubTitleTextStyle,
          //     //                       ),
          //     //                     ],
          //     //                   ),
          //     //                 ));
          //     //       } else {
          //     //         _showAlert(context, bkashResponse);
          //     //       }
          //     //     })
          //   ]),
          //   onWebViewCreated: (WebViewController webViewController) {
          //     _controller = webViewController;
          //     //_loadHtmlFromAssets();
          //     _controller.clearCache();
          //   },
          //
          //   onPageFinished: (_) {
          //     // _controller.evaluateJavascript(
          //     //     "javascript:callReconfigure($paymentRequest)");
          //     // _controller.evaluateJavascript("javascript:clickPayButton()");
          //
          //     setState(() => isLoading = false);
          //   },
          // ),
          WebViewWidget(controller: _controller),
          isLoading ? Center(child: CircularProgressIndicator()) : Container(),
        ],
      ),
    );
  }

  // _loadHtmlFromAssets() async {
  //   String fileText = await rootBundle.loadString('assets/bkash.html');
  //
  //   //passing create-payment response data to bkash.html file
  //   // fileText = fileText.replaceAll("9090",response.paymentID);
  //   fileText = fileText.replaceAll("9191", widget.amount);
  //   // fileText = fileText.replaceAll("9292",response.intent);
  //   // fileText = fileText.replaceAll("9393",response.orgName);
  //   // fileText = fileText.replaceAll("9494",response.orgLogo);
  //   // fileText = fileText.replaceAll("9595",response.transactionStatus);
  //   // fileText = fileText.replaceAll("9696",response.merchantInvoiceNumber);
  //   // fileText = fileText.replaceAll("9797",response.currency);
  //   // fileText = fileText.replaceAll("9898",response.createTime);
  //   var res = _controller.loadUrl(Uri.dataFromString(
  //     fileText,
  //     mimeType: 'text/html',
  //     encoding: Encoding.getByName('utf-8'),
  //   ).toString());
  // }

  sendPaymentDataToServer(String response) async {
    var bkashResponse = bkashResponseFromJson(response);
    print('bkashResponse.paymentId ${bkashResponse.paymentId}');
    print('bkashResponse.trxId ${bkashResponse.trxId}');
    print(
        'bkashResponse.merchantInvoiceNumber ${bkashResponse.merchantInvoiceNumber}');
    print("widget.userID--- ${widget.userID.toString()}");
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/payments/bkash-post-payment/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .post(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            }, body: {
              "paymentID": bkashResponse.paymentId.toString(),
              "createTime": bkashResponse.createTime,
              "updateTime": bkashResponse.updateTime,
              "trxID": bkashResponse.trxId,
              "transactionStatus": bkashResponse.transactionStatus,
              "amount": bkashResponse.amount,
              "currency": bkashResponse.currency, //"BDT",
              "intent": bkashResponse.intent, //"sale",
              "payment_type": "bkash",
              "user_id": widget.userID, //Resources.selectedUserIDForPayment,
              "merchantInvoiceNumber":
                  bkashResponse.merchantInvoiceNumber, //"46f647h7"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              // final noticeResponse = GetOtpResponse.fromJson(data); //noticeResponseFromJson(data);
              // print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  isLoading = false;
                });

                // showdi
                // YYNoticeDialog();

                // _showAlert(context,bkashResponse);
                Fluttertoast.showToast(
                    msg: "Payment Successful \n ",
                    textColor: Colors.white,
                    backgroundColor: Colors.pink,
                    gravity: ToastGravity.CENTER,
                    toastLength: Toast.LENGTH_LONG);
                // Navigator.pop(context);
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });

                Fluttertoast.showToast(
                    msg:
                        'Error-${value.statusCode}, ${NetworkManager.errorMessage}',
                    textColor: Colors.white,
                    backgroundColor: Colors.pink,
                    gravity: ToastGravity.CENTER,
                    toastLength: Toast.LENGTH_LONG);
              } else {
                setState(() {
                  isLoading = false;
                });
                Fluttertoast.showToast(
                    msg:
                        'Error-${value.statusCode}, ${bkashResponse.errorMessage.toString()}',
                    textColor: Colors.white,
                    backgroundColor: Colors.pink,
                    gravity: ToastGravity.CENTER,
                    toastLength: Toast.LENGTH_LONG);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
    }
    // Navigator.pop(context);
  }

  void _showAlert(BuildContext context, BkashResponse bkashResponse) {
    Widget okButton = TextButton(
      child: Container(
          width: 100,
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              child: Center(child: Text("DONE")))),
      onPressed: () {
        // Navigator.pop(context);
        // Navigator.pop(context);
        Resources.moveTo(1, context);

        // Navigator.pop(context);
      },
    );
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              backgroundColor: Colors.pink,
              title: Text(
                "Payment Successful!",
                style: gcTitleTextStyle,
              ),
              actions: [okButton],
              content: Wrap(
                alignment: WrapAlignment.center,
                direction: Axis.vertical,
                children: [
                  Expanded(
                      child: Text(
                    'Payment ID: \n ${bkashResponse.paymentId}',
                    style: gcSubTitleTextStyle,
                  )),
                  Text(
                    'Transaction ID: ${bkashResponse.trxId}',
                    style: gcSubTitleTextStyle,
                  ),
                  Text(
                    'Merchant No: ${bkashResponse.merchantInvoiceNumber}',
                    style: gcSubTitleTextStyle,
                  ),
                ],
              ),
            ));
  }
}
