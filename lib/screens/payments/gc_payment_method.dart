import 'dart:convert';

 import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_response.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_webview_payment.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'bkash_webview_payment.dart';

class PaymentMethodScreen extends StatefulWidget {
  @override
  _PaymentMethodScreenState createState() => _PaymentMethodScreenState();
}

class _PaymentMethodScreenState extends State<PaymentMethodScreen> {
  int selectedIndex = -1;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  TextEditingController _amountController = TextEditingController();
  final FocusNode _amountNode = FocusNode();

  bool isLoading = false;
  AppUser? userInfo;
  bool isPayForOtherSelected = false;

  bool isDataLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    if (isPayForOtherSelected == false) {
      getCurrentUserInfo();
    } else {
      print("other selected");
      getCurrentUserInfo();
    }
    super.initState();
  }

  getCurrentUserInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  userInfo = userResponse.data;
                  Resources.myUserID = userInfo?.id;
                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  // Resources.selectedUserIDForPayment = userInfo.id.toString();
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool isPayForOthersSelected =
        ModalRoute.of(context)?.settings.arguments as bool;

    setState(() {
      isPayForOtherSelected = isPayForOthersSelected;
    });
    print('isPayForOthersSelected-----> $isPayForOtherSelected');

    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        title: Text(
          "Select Payment Method",
          style: gcTitleTextStyle,
        ),
      ),
      body: SingleChildScrollView(
        child: (isLoading == true)
            ? Center(
                child: CircularProgressIndicator(
                backgroundColor: themeGolderColor,
              ))
            : ListView(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                children: [
                  // Center(
                  //   child: Padding(
                  //     padding: const EdgeInsets.all(80.0),
                  //     child:Text('This option is currently unavailable!',style: gcTitleTextStyle,)
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: ListTile(
                        tileColor: themeGreyColor,
                        trailing: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: selectedIndex == 0
                              ? SvgPicture.asset(
                                  'assets/icons/Payment_Selected_Radio_Button.svg',
                                  // color: themeGolderColor,
                                )
                              : SvgPicture.asset(
                                  'assets/icons/UnselectedRadioButton.svg',
                                  // color: themeGolderColor,
                                ),
                        ),
                        leading: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            child: Container(
                              color: themeWhiteColor,
                              height: 40,
                              width: 40,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SvgPicture.asset(
                                  'assets/icons/Payment_Bkash.svg',
                                  // color: themeGolderColor,
                                ),
                              ),
                            )),
                        title: Text(
                          'Bkash',
                          style: gcTitleTextStyle,
                        ),
                        selected: false,
                        onTap: () async {
                          setState(() {
                            selectedIndex = 0;
                          });
                          // await _navigateTo(context, '/building_directory');
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: ListTile(
                        tileColor: themeGreyColor,
                        trailing: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: selectedIndex == 1
                              ? SvgPicture.asset(
                                  'assets/icons/Payment_Selected_Radio_Button.svg',
                                  // color: themeGolderColor,
                                )
                              : SvgPicture.asset(
                                  'assets/icons/UnselectedRadioButton.svg',
                                  // color: themeGolderColor,
                                ),
                        ),
                        leading: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            child: Container(
                              color: themeWhiteColor,
                              height: 40,
                              width: 40,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SvgPicture.asset(
                                  'assets/icons/Payment_Nagad.svg',
                                  // color: themeGolderColor,
                                ),
                              ),
                            )),
                        title: Text(
                          'Nagad',
                          style: gcTitleTextStyle,
                        ),
                        selected: false,
                        onTap: () async {
                          setState(() {
                            selectedIndex = 1;
                          });
                          // await _navigateTo(context, '/pay_for_others');
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: ListTile(
                        tileColor: themeGreyColor,
                        trailing: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: selectedIndex == 2
                              ? SvgPicture.asset(
                                  'assets/icons/Payment_Selected_Radio_Button.svg',
                                  // color: themeGolderColor,
                                )
                              : SvgPicture.asset(
                                  'assets/icons/UnselectedRadioButton.svg',
                                  // color: themeGolderColor,
                                ),
                        ),
                        leading: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            child: Container(
                              color: themeWhiteColor,
                              height: 40,
                              width: 40,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SvgPicture.asset(
                                  'assets/icons/Payment_CityBank.svg',
                                  // color: themeGolderColor,
                                ),
                              ),
                            )),
                        title: Text(
                          'AMEX',
                          style: gcTitleTextStyle,
                        ),
                        selected: false,
                        onTap: () async {
                          setState(() {
                            selectedIndex = 2;
                          });
                          //await _navigateTo(context, '/pay_for_others');
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: ListTile(
                        tileColor: themeGreyColor,
                        trailing: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: selectedIndex == 3
                              ? SvgPicture.asset(
                                  'assets/icons/Payment_Selected_Radio_Button.svg',
                                  // color: themeGolderColor,
                                )
                              : SvgPicture.asset(
                                  'assets/icons/UnselectedRadioButton.svg',
                                  // color: themeGolderColor,
                                ),
                        ),
                        leading: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            child: Container(
                              color: themeWhiteColor,
                              height: 40,
                              width: 40,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SvgPicture.asset(
                                  'assets/icons/Visa.svg',
                                  // color: themeGolderColor,
                                ),
                              ),
                            )),
                        title: Text(
                          "VISA",
                          style: gcTitleTextStyle,
                        ),
                        selected: false,
                        onTap: () async {
                          setState(() {
                            selectedIndex = 3;
                          });
                          //await _navigateTo(context, '/pay_for_others');
                        },
                      ),
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: ListTile(
                        tileColor: themeGreyColor,
                        trailing: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: selectedIndex == 4
                              ? SvgPicture.asset(
                                  'assets/icons/Payment_Selected_Radio_Button.svg',
                                  // color: themeGolderColor,
                                )
                              : SvgPicture.asset(
                                  'assets/icons/UnselectedRadioButton.svg',
                                  // color: themeGolderColor,
                                ),
                        ),
                        leading: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            child: Container(
                              color: themeWhiteColor,
                              height: 40,
                              width: 40,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SvgPicture.asset(
                                  'assets/icons/MasterCard.svg',
                                  // color: themeGolderColor,
                                ),
                              ),
                            )),
                        title: Text(
                          "Master Card",
                          style: gcTitleTextStyle,
                        ),
                        selected: false,
                        onTap: () async {
                          setState(() {
                            selectedIndex = 4;
                          });
                          //await _navigateTo(context, '/pay_for_others');
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  (selectedIndex == 0 || selectedIndex == 1)
                      ? Padding(
                          padding: const EdgeInsets.only(left: 32.0, right: 32),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: TextFormField(
                              controller: _amountController,
                              focusNode: _amountNode,
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              validator: (value) =>
                                  MyCustomValidator.validateIsEmpty(
                                      _amountController.text, '', context),
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(
                                border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                    color: themeGolderColor,
                                  ),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.pink),
                                  //  when the TextFormField in unfocused
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.pink), //HexColor("#FF7300"
                                  //  when the TextFormField in focused
                                ),
                                hintText: 'amount',
                                hintStyle: gcBkashPlaceholderTextStyle,
                              ),
                              onFieldSubmitted: (value) {},
                              style: gcInputFieldTextStyle,
                            ),
                          ),
                        )
                      : SizedBox(),
                  (selectedIndex == 0 || selectedIndex == 1)
                      ? SizedBox(
                          height: 40,
                        )
                      : SizedBox(),

                  (selectedIndex != null)
                      ? Padding(
                          padding: const EdgeInsets.all(32.0),
                          child: MyCustomButton(
                            height: 30,
                            // width: 200,
                            color: themeGolderColor,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Next",
                                style: buttonTitleStyle,
                              ),
                            ),
                            onPressed: () async {
                              print('tapped');
                              // sendOTP();
                              if (selectedIndex == 0) {
                                if (_amountController.text != '') {
                                  await _navigateTo(context);
                                } else {
                                  Fluttertoast.showToast(
                                      msg: "Please insert your payment amount",
                                      textColor: Colors.white,
                                      backgroundColor: Colors.pink,
                                      gravity: ToastGravity.SNACKBAR,
                                      toastLength: Toast.LENGTH_SHORT);
                                }
                              } else if (selectedIndex == 1) {
                                if (_amountController.text != '') {
                                  await getNagadPayment();
                                  ;
                                } else {
                                  Fluttertoast.showToast(
                                      msg: "Please insert your payment amount",
                                      textColor: Colors.white,
                                      backgroundColor: Colors.pink,
                                      gravity: ToastGravity.SNACKBAR,
                                      toastLength: Toast.LENGTH_SHORT);
                                }
                              } else {
                                await _navigateTo(context);
                              }
                            },
                          ),
                        )
                      : SizedBox(
                          height: 1,
                        ),
                ],
              ),
      ),
    );
  }

  getNagadPayment() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/payments/nagad-payment/";
    print('url---> $url');

    var params = {
      "amount": _amountController.text.toString(),
      "pay_by": userInfo?.id.toString(),
      "pay_to": (isPayForOtherSelected == true)
          ? Resources.selectedUserIDForPayment.toString()
          : userInfo?.id.toString(),
    };
    print(params);
    if (result == true) {
      try {
        http
            .post(Uri.parse(url), body: params, headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 60))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = NagadResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  isDataLoading = false;
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => NagadWebViewPayment(
                          paymentUrl: userResponse.payUrl.toString())));
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isDataLoading = false;
                  Fluttertoast.showToast(msg: userResponse.data.toString());
                });
              } else {
                setState(() {
                  isDataLoading = false;
                  Fluttertoast.showToast(msg: "Something went wrong!");
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isDataLoading = false;
          Fluttertoast.showToast(
              msg: "Server encountered a problem! Try again later!");
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isDataLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> _navigateTo(BuildContext context) async {
    // await _navigateTo(context, '/pay_for_others');

    switch (selectedIndex) {
      case 0:
        // do something
        //await _navigateTo(context, '/bkash_payment');
        // await Navigator.pushNamed(context, '/bkash_payment');
        if (isPayForOtherSelected == false) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => BkashWebViewPayment(
                  amount: _amountController.text,
                  intent: "sale",
                  userID: userInfo?.id.toString())));
        } else {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => BkashWebViewPayment(
                  amount: _amountController.text,
                  intent: "sale",
                  userID: Resources.selectedUserIDForPayment)));
        }
        break;
      case 1:
        // do something else
        //   await Navigator.pushNamed(context, '/pay_for_others');

        break;

      case 2:
      // do something else
        await Navigator.pushNamed(context, '/citybank_payment');

        break;
      case 3:
      // do something else
        await Navigator.pushNamed(context, '/citybank_payment');

        break;
      case 4:
      // do something else
        await Navigator.pushNamed(context, '/citybank_payment');

        break;
      default:
        // final snackBar = SnackBar(
        //   backgroundColor: Colors.red,
        //   content: Text('Please select a payment method'),
        // );
        // _scaffoldKey.currentState.showSnackBar(snackBar);
    }
    // Navigator.pushNamed(context, '/facility_details',arguments: itemList[index]);
  }
}
