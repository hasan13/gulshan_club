// To parse this JSON data, do
//
//     final nagadResponse = nagadResponseFromJson(jsonString);

import 'dart:convert';

NagadResponse nagadResponseFromJson(String str) => NagadResponse.fromJson(json.decode(str));

String nagadResponseToJson(NagadResponse data) => json.encode(data.toJson());

class NagadResponse {
  NagadResponse({
    this.status,
    this.data,
    this.payUrl,
  });

  bool? status;
  String? data;
  String? payUrl;

  factory NagadResponse.fromJson(Map<String, dynamic> json) => NagadResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : json["data"],
    payUrl: json["pay_url"] == null ? null : json["pay_url"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data,
    "pay_url": payUrl == null ? null : payUrl,
  };
}
