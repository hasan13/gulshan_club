import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/payments/models/bkash_response.dart';
import 'package:gulshan_club_app/screens/payments/models/payment_request.dart';
import 'package:gulshan_club_app/screens/payments/utilities/js_interface.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart' show rootBundle;

class NagadWebViewPayment extends StatefulWidget {
  final String paymentUrl;

  const NagadWebViewPayment({Key? key, required this.paymentUrl}) : super(key: key);

  @override
  _NagadWebViewPaymentState createState() => _NagadWebViewPaymentState();
}

class _NagadWebViewPaymentState extends State<NagadWebViewPayment> {
  late WebViewController _controller;

  bool isLoading = true;
  late JavaScriptInterface _javaScriptInterface;

   @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();

    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))

      ..addJavaScriptChannel('GoBack', onMessageReceived: (message) {
        print("Go Back");
        print(message);
      })

      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {
            setState(() => isLoading = false);

          },
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            print("WEBVIEW VALUES: $request");
            if(request.url.startsWith(NetworkManager.baseUrl)){
              // Fluttertoast.showToast(msg: "Transaction successfull");
              _showAlert(context, request.url);
            }
            return NavigationDecision.navigate;
          },
        ),
      )
      ..loadRequest(Uri.parse(widget.paymentUrl));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nagad'),
        backgroundColor: Colors.orange,
      ),
      body: Stack(
        children: [
          //  WebViewWidget(
          //   initialUrl: widget.paymentUrl,
          //    javascriptMode: JavascriptMode.unrestricted,
          //
          //
          //   navigationDelegate: (NavigationRequest request) {
          //     print("WEBVIEW VALUES: $request");
          //     if(request.url.startsWith(NetworkManager.baseUrl)){
          //       // Fluttertoast.showToast(msg: "Transaction successfull");
          //       _showAlert(context, request.url);
          //     }
          //     return NavigationDecision.navigate;
          //
          //   },
          //   javascriptChannels: Set.from([
          //
          //     JavascriptChannel(
          //         name: 'GoBack',
          //         onMessageReceived: (JavascriptMessage message) async {
          //           print("Go Back");
          //           print(message);
          //         })
          //   ]),
          //   onWebViewCreated: (WebViewController webViewController) {
          //     _controller = webViewController;
          //     //_loadHtmlFromAssets();
          //     _controller.clearCache();
          //   },
          //
          //   onPageFinished: (_) {
          //
          //     setState(() => isLoading = false);
          //   },
          // ),
          WebViewWidget(controller: _controller),

          isLoading ? Center(child: CircularProgressIndicator()) : Container(),
        ],
      ),
    );
  }

  void _showAlert(BuildContext context, String responseString) {

    var url = Uri.parse(responseString);
    print(url.queryParameters['order_id']);
    print(url.queryParameters['status']);
    print(url.queryParameters['message']);

    Widget okButton = TextButton(
      child: Container(
          width: 100,
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            color:Colors.white,
          ),
          child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              child: Center(child: Text("DONE")))),
      onPressed: () {
        // Navigator.pop(context);
        // Navigator.pop(context);
        Resources.moveTo(1, context);

        // Navigator.pop(context);
      },
    );
    showDialog(
        context: context,
        builder: (context) => AlertDialog(backgroundColor: Colors.orange,
          title: Text(url.queryParameters['message'] ?? ' ',style: gcTitleTextStyle,),
          actions: [
            okButton
          ],
          content: Wrap(
            alignment: WrapAlignment.center,direction: Axis.vertical,
            children: [
              Expanded(child: Text('Payment ID: \n ${url.queryParameters['order_id']}',style: gcSubTitleTextStyle,)),
              // Text('Transaction ID: ${bkashResponse.trxId}',style: gcSubTitleTextStyle,),
              // Text('Merchant No: ${bkashResponse.merchantInvoiceNumber}',style: gcSubTitleTextStyle,),

            ],
          ),
        )
    );
  }
}
