// To parse this JSON data, do
//
//     final bkashResponse = bkashResponseFromJson(jsonString?);

import 'dart:convert';

BkashResponse bkashResponseFromJson(String str) => BkashResponse.fromJson(json.decode(str));

String? bkashResponseToJson(BkashResponse data) => json.encode(data.toJson());

class BkashResponse {
  BkashResponse({
    this.paymentId,
    this.createTime,
    this.updateTime,
    this.trxId,
    this.transactionStatus,
    this.amount,
    this.currency,
    this.intent,
    this.merchantInvoiceNumber,
    this.errorMessage,
  });

  String? paymentId;
  String? createTime;
  String? updateTime;
  String? trxId;
  String? transactionStatus;
  String? amount;
  String? currency;
  String? intent;
  String? merchantInvoiceNumber;
  String? errorMessage;

  factory BkashResponse.fromJson(Map<String?, dynamic> json) => BkashResponse(
    paymentId: json["paymentID"] == null ? null : json["paymentID"],
    createTime: json["createTime"] == null ? null : json["createTime"],
    updateTime: json["updateTime"] == null ? null : json["updateTime"],
    trxId: json["trxID"] == null ? null : json["trxID"],
    transactionStatus: json["transactionStatus"] == null ? null : json["transactionStatus"],
    amount: json["amount"] == null ? null : json["amount"],
    currency: json["currency"] == null ? null : json["currency"],
    intent: json["intent"] == null ? null : json["intent"],
    merchantInvoiceNumber: json["merchantInvoiceNumber"] == null ? null : json["merchantInvoiceNumber"],
      errorMessage: json["errorMessage"] == null ? null : json["errorMessage"]
  );

  Map<String?, dynamic> toJson() => {
    "paymentID": paymentId == null ? null : paymentId,
    "createTime": createTime == null ? null : createTime,
    "updateTime": updateTime == null ? null : updateTime,
    "trxID": trxId == null ? null : trxId,
    "transactionStatus": transactionStatus == null ? null : transactionStatus,
    "amount": amount == null ? null : amount,
    "currency": currency == null ? null : currency,
    "intent": intent == null ? null : intent,
    "merchantInvoiceNumber": merchantInvoiceNumber == null ? null : merchantInvoiceNumber,
    "errorMessage" : errorMessage == null ? null : errorMessage,
  };
}
