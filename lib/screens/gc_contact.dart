import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';

class ContactScreen extends StatefulWidget {
  static const String route = '/contact';

  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  int _current = 0;
  List<String> imgList = [
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
  ];

  final TextEditingController _nameCtrl = new TextEditingController();
  final TextEditingController _accountNoCtrl = new TextEditingController();
  final TextEditingController _messageCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();
  final FocusNode _accountNoNode = FocusNode();
  final FocusNode _messageNode = FocusNode();

  final GlobalKey<FormState> _globalKey = new GlobalKey<FormState>();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Contact Us',
      bottomNavigationBar: Resources.setBottomDrawer(context),

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Facilities',
                    style: gcListTitleTextStyle,
                  ),
                  Text(
                    '  Contact',
                    style: gcListTitleTextStyle,
                  ),
                  Text(
                    'Direct \nCall',
                    style: gcListTitleTextStyle,
                  ),

                  //SizedBox(width: 60,)
                ],
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Restaurant',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 \n(Ext. 117, 227, 527)  ',textAlign: TextAlign.center,
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Club Café & \nBakery ',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 130, 550)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),

              // Padding(
              //   padding: const EdgeInsets.all(8.0),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     crossAxisAlignment: CrossAxisAlignment.center,
              //     children: [
              //       Text(
              //         'Bar                ',
              //         style: gcNormalTextStyle,
              //       ),
              //       Text(
              //         '16717 (Ext. 222, 333)  ',
              //         style: gcNormalTextStyle,
              //       ),
              //       GestureDetector(
              //         onTap: () async {
              //           String url = "tel://" + '16717';
              //           if (await canLaunch(url)) {
              //             await launch(url);
              //           } else {
              //             throw 'Could not call ${16717}';
              //           }
              //         },
              //         child: SvgPicture.asset(
              //           'assets/icons/ic_call.svg',
              //           height: 20,
              //           width: 20,
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Divider(
              //   color: themeGolderColor,
              //   height: 1,
              // ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Swimming \nPool',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 221, 582)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Gym          ',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 128, 182)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Physiotherapy',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 149)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Barber \nShop',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 561,563)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Beauty \nParlor',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 562)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),


              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Security \nDesk ',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 \n(Ext. 500, 584, 100)  ',
                      textAlign: TextAlign.center,
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),



              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Club \nResidence ',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 600)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Club \nOffice ',
                      style: gcNormalTextStyle,
                    ),
                    Text(
                      '16717 (Ext. 180, 190)  ',
                      style: gcNormalTextStyle,
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: themeGolderColor,
                height: 1,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Hotline Number',
                style: gcNormalTextStyle,
              ),
              GestureDetector(
                onTap: () async {
                  String url = "tel://" + '16717';
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not call ${16717}';
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: SvgPicture.asset(
                        'assets/icons/ic_call.svg',
                        height: 16,
                        width: 16,
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        String url = "tel://" + '16717';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not call ${16717}';
                        }
                      },
                      child: Text(
                        '  16717',
                        style: gcListTitleTextStyle,
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 20,
              ),
              SvgPicture.asset(
                'assets/icons/ic_mail.svg',
                height: 20,
                width: 20,
              ),
              Text(
                'mail@gulshanclub.com',
                style: gcNormalTextStyle,
              ),
              SizedBox(
                height: 20,
              ),
              SvgPicture.asset(
                'assets/icons/ic_location.svg',
                height: 20,
                width: 20,
              ),
              // Text(
              //   'Gulshan, Dhaka',
              //   style: gcNormalTextStyle,
              // ),
              Text(
                'House NWJ-2/A, Bir Uttom Sultan Mahmud Road (Old 50) Gulshan 2, Dhaka 1212, Bangladesh',
                style: gcDescriptionTextStyle,
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 20,
              ),

              GestureDetector(
                onTap: () async {
                  String url = "tel://" + '01400223388';
                  if (await canLaunch(url)) {
                    await launch(url);
                  } else {
                    throw 'Could not call ${01400223388}';
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/icons/ic_whatsapp .svg',
                      height: 20,
                      width: 20,
                      color: Colors.green,
                    ),
                    Text(
                      '   01400223388',
                      style: gcWhatsAppTextStyle,
                    ),
                  ],
                ),
              ),
              Text(
                '( Note: Please save this WhatsApp number in your contact list to receive all notifications )',
                style: gcDescriptionTextStyle,
                textAlign: TextAlign.center,
              ),

              SizedBox(
                height: 20,
              ),

              Form(
                key: _globalKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 50,
                      padding: EdgeInsets.all(4),
                      child: TextFormField(
                        controller: _nameCtrl,
                        focusNode: _userIdNode,
                        validator: (value) =>
                            MyCustomValidator.validateMultiLine(
                                _nameCtrl.text, context),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          border: new OutlineInputBorder(
                            borderSide: new BorderSide(
                              color: themeGolderColor,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: themeGolderColor),
                            //  when the TextFormField in unfocused
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: themeGolderColor), //HexColor("#FF7300"
                            //  when the TextFormField in focused
                          ),
                          hintText: 'Name',
                          hintStyle: gcPlaceholderTextStyle,
                        ),
                        onFieldSubmitted: (value) {},
                        style: gcInputFieldTextStyle,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 50,
                      padding: EdgeInsets.all(4),
                      child: TextFormField(
                        controller: _accountNoCtrl,
                        focusNode: _accountNoNode,
                        validator: (value) =>
                            MyCustomValidator.validateMultiLine(
                                _nameCtrl.text, context),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          border: new OutlineInputBorder(
                            borderSide: new BorderSide(
                              color: themeGolderColor,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: themeGolderColor),
                            //  when the TextFormField in unfocused
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: themeGolderColor), //HexColor("#FF7300"
                            //  when the TextFormField in focused
                          ),
                          hintText: 'Account Number',
                          hintStyle: gcPlaceholderTextStyle,
                        ),
                        onFieldSubmitted: (value) {},
                        style: gcInputFieldTextStyle,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 120,
                      padding: EdgeInsets.all(4),
                      child: TextFormField(
                        controller: _messageCtrl,
                        focusNode: _messageNode,
                        validator: (value) =>
                            MyCustomValidator.validateMultiLine(
                                _nameCtrl.text, context),
                        textAlign: TextAlign.left,
                        maxLines: 8,
                        decoration: InputDecoration(
                          border: new OutlineInputBorder(
                            borderSide: new BorderSide(
                              color: themeGolderColor,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: themeGolderColor),
                            //  when the TextFormField in unfocused
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: themeGolderColor), //HexColor("#FF7300"
                            //  when the TextFormField in focused
                          ),
                          hintText: 'Type Your Message',
                          hintStyle: gcPlaceholderTextStyle,
                        ),
                        onFieldSubmitted: (value) {},
                        style: gcInputFieldTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    MyCustomButton(
                      height: 40,
                      width: MediaQuery.of(context).size.width * 0.8,
                      color: themeGolderColor, //Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'SUBMIT',
                          style: gcNormalTextStyle,
                        ),
                      ),
                      onPressed: () {
                        print('tapped');
                        print(_nameCtrl.text);
                        if (_nameCtrl.text.length > 0 &&
                            _accountNoCtrl.text.length > 0 &&
                            _messageCtrl.text.length > 0) {
                          sendFormData(_nameCtrl.text, _accountNoCtrl.text,
                              _messageCtrl.text);
                        } else {
                          Fluttertoast.showToast(
                            msg: "Check input fields!",
                            backgroundColor: Colors.red,
                          );
                        }
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  sendFormData(String name, String accountNumber, String message) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/contact-us/create-request/";
    print(
        'url---> $url'); //http://58.84.34.65:9090/api/v1/payments/pay-city-bank/
    print("$name  -------" + "$accountNumber#   ------- $message");
    if (result == true) {
      try {
        var uri = Uri.parse(url);
        http.MultipartRequest request = new http.MultipartRequest('POST', uri);
        request.headers['Authorization'] =
            "Bearer ${Resources.currentUser?.accessToken}";
        request.headers["Content-Type"] = 'multipart/form-data';
        request.fields["name"] = name;
        request.fields["account_number"] = accountNumber;
        request.fields["message"] = message;
        var streamedResponse = await request.send();

        Future.delayed(
            Duration.zero,
            () => setState(() {
                  isLoading = false;
                  _messageCtrl.text = "";
                  _accountNoCtrl.text = "";
                  _nameCtrl.text = "";
                  FocusScope.of(context).requestFocus(new FocusNode());
                }));

        print(streamedResponse.statusCode);

        if (streamedResponse.statusCode == 200 ||
            streamedResponse.statusCode == 201) {
          print("Image Uploaded");
          Future.delayed(
              Duration.zero,
              () => setState(() {
                    isLoading = false;
                  }));
          // showAlertDialog(context, getTranslatedValue(context, 'support_alert'));
        } else {
          Future.delayed(
              Duration.zero,
              () => setState(() {
                    isLoading = false;
                  }));
          print("Upload Failed");
        }
        var jsonDatas = await http.Response.fromStream(streamedResponse);
        Map<String, dynamic> jsonggg = jsonDecode(jsonDatas.body);
        print(jsonggg);
        bool status = jsonggg['status'];
        String resMessage = jsonggg['message'];
        print(status);
        print(resMessage);

        if (status == true) {
          Alert(
            context: context,
            title: "Success!",
            desc: resMessage,
            buttons: [
              DialogButton(
                child: Text(
                  "Done",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () {
                  Navigator.pop(context, true);
                },
                width: 120,
              )
            ],
            image: Image.asset(
                "packages/rflutter_alert/assets/images/icon_success.png"),
          ).show();
        } else {
          Alert(
            context: context,
            title: "Error",
            desc: resMessage,
            buttons: [
              DialogButton(
                child: Text(
                  "Done",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () {
                  Navigator.pop(context, true);
                },
                width: 120,
              )
            ],
            image: Icon(
              Icons.cancel_outlined,
              color: Colors.red,
            ),
          ).show();
        }
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(
          msg: "Invalid Request!",
          backgroundColor: Colors.red,
        );
      }
    } else {
      print('No internet :( Reason:');

      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      Fluttertoast.showToast(
        msg: "Internet is not available!",
        backgroundColor: Colors.red,
      );
    }
  }
}
