import 'dart:convert';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flappy_search_bar/flappy_search_bar.dart';
// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/member_search_response.dart';
import 'package:gulshan_club_app/jsonModels/message_list_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class MembersDirectoryScreen extends StatefulWidget {
  static const String route = '/members_directory';

  @override
  _MembersDirectoryScreenState createState() => _MembersDirectoryScreenState();
}

class _MembersDirectoryScreenState extends State<MembersDirectoryScreen> {
  final SearchBarController<Post> _searchBarController = SearchBarController();
  bool isReplay = false;
  List<Member> itemList = [];
  var isLoading = false;

  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  bool checkingFlight = false;
  bool success = false;

  bool isSearchOn = false;

  UserListResponse? userJson;
  bool isLoadingMore = false;

  var tabIndex = 2;

  final TextEditingController _userIdCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    // getAllMembers();
    super.initState();
  }

  Future<List<Post>> _getALlPosts(String text) async {
    if (text.length > 0) {
      getFilteredMembers(text);
    } else {
      //getAllMembers();
      setState(() {
        itemList = [];
      });
    }

    List<Post> posts = [];

    var random = new Random();
    for (int i = 0; i < 10; i++) {
      posts
          .add(Post("$text $i", "body random number : ${random.nextInt(100)}"));
    }

    return []; //posts;
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Members Directory',
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
            // sets the background color of the `BottomNavigationBar`
            canvasColor: themeBlackColor,
            // sets the active color of the `BottomNavigationBar` if `Brightness` is light
            primaryColor: Colors.red,
            textTheme: Theme.of(context).textTheme.copyWith(
                caption: new TextStyle(
                    color: Colors
                        .yellow))), // sets the inactive color of the `BottomNavigationBar`
        child: BottomNavigationBar(
            selectedItemColor: themeGolderColor,
            unselectedItemColor: Colors.grey[400],
            iconSize: 30,
            backgroundColor: Colors.blue,
            currentIndex: tabIndex,
            onTap: (int index) {
              setState(() {
                tabIndex = index;
                print("tabIndex----$tabIndex");
                Resources.moveTo(index, context);
              });
            },
            items: [
              BottomNavigationBarItem(
                  activeIcon: Icon(
                    Icons.home,
                    color: themeGolderColor,
                    size: 30,
                  ),
                  icon: Icon(
                    Icons.home,
                    color: themeGreyColor,
                    size: 28,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_profile.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_profile.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_memeber_director.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_memeber_director.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_notice_board.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_notice_board.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              // BottomNavigationBarItem(
              //     activeIcon: SvgPicture.asset(
              //       "assets/icons/ic_facilities.svg",
              //       color: themeGolderColor,
              //       width: 30,
              //       height: 30,
              //     ),
              //     icon: SvgPicture.asset(
              //       "assets/icons/ic_facilities.svg",
              //       color: themeGreyColor,
              //       width: 26,
              //       height: 26,
              //     ),
              //     label: ''),
            ]),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          // : itemList == null
          //     ? Center(
          //         child: Text(
          //         "Something went wrong",
          //         style: TextStyle(),
          //       ))
              // : (itemList.length == 0 && isSearchOn == false)
              //     ? Center(
              //         child: Text(
              //           "No Member found",
              //           style: gcPlaceholderTextStyle,
              //         ),
              //       )
                  : createListView(itemList, context),
    );
  }

  getAllMembers() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/active-users/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = UserListResponse.fromJson(data);
              print('result....\n ${employeeResponse.results}');
              if (value.statusCode == 200) {
                setState(() {
                  itemList = employeeResponse.results ?? [];
                  userJson = employeeResponse;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                  isSearchOn = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                // ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllMembers();
    });
  }

  _loadMoreMember() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint =
        userJson?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = UserListResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        userJson = moreNoticeResponse;
        List<Member> moreData = moreNoticeResponse.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  getFilteredMembers(String text) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/user_search/?member_name=$text";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = MemberSearchResponse.fromJson(data);
              print('result....\n ${employeeResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  isSearchOn = true;
                  itemList = employeeResponse.data ?? [];
                  userJson = null;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Widget createListView(List<Member> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Container(
                height: 80,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Container(
                  width: MediaQuery.of(context).size.width - 120,
                  child: TextFormField(
                    controller: _userIdCtrl,
                    focusNode: _userIdNode,
                    //validator: (value) => MyCustomValidator.validateIsEmpty(_userIdCtrl.text, 'userID', context),
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      border: new UnderlineInputBorder(
                        borderSide: new BorderSide(
                          color: themeGolderColor,
                        ),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: themeGolderColor),
                        //  when the TextFormField in unfocused
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: themeGolderColor), //HexColor("#FF7300"
                        //  when the TextFormField in focused
                      ),
                      hintText: 'search',
                      hintStyle: gcPlaceholderTextStyle,
                    ),
                    onFieldSubmitted: (value) {},
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            MyCustomButton(
              height: 30,
              width: 80,
              color: themeGolderColor, //Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Ok',
                  style: gcNormalTextStyle,
                ),
              ),
              onPressed: () {
                print('tapped');
                print(_userIdCtrl.text);
                if (_userIdCtrl.text.length > 0) {
                  getFilteredMembers(_userIdCtrl.text);
                } else {
                  //getAllMembers();
                  setState(() {
                    itemList = [];
                  });
                }
              },
            ),
          ],
        ),
        (results.length == 0)
            ? Expanded(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: SvgPicture.asset(
                    'assets/images/Members Directory Message.svg',
                    fit: BoxFit.contain,
                    width: MediaQuery.of(context).size.width,
                    height: 400,
                  ),
                ),
              )
            : SizedBox(),
        Expanded(
          child: NotificationListener(
            onNotification: (ScrollNotification scrollInfo) {
              if (scrollInfo.metrics.pixels ==
                  scrollInfo.metrics.maxScrollExtent) {
                if (!isLoadingMore && userJson?.next != null) {
                  // start loading data
                  print("Scrolling to end...");
                  setState(() {
                    isLoadingMore = true;
                  });
                  _loadMoreMember();
                } else if (userJson?.next == null) {
                  print("Data end");
                  setState(() {
                    isLoadingMore = false;
                  });
                } else {}
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: _reloadData,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {
                  String dateString = "";
                  if (itemList[index].membershipDate != null) {
                    DateTime parsedDate = DateTime.parse(
                            itemList[index].membershipDate.toString())
                        .toLocal();
                    dateString = DateFormat.yMd().format(parsedDate);
                  }

                  var accountNo = itemList[index].clubAcNumber;
                  var letterString =
                      accountNo?.replaceAll(new RegExp(r'[^A-Za-z]'), '');
                  var numberString =
                      accountNo?.replaceAll(new RegExp(r'[^0-9]'), '');

                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 16),
                    child: GestureDetector(
                      onTap: () {
                        var member = itemList[index];
                        // Navigator.pushNamed(context, '/member_details', arguments: itemList[index]);
                        //Navigator.pushNamed(context, '/event_details',arguments: itemList[index]);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            // color: Colors.white38,
                            borderRadius: BorderRadius.all(
                                Radius.circular(8)), // color: Colors.white38,
                            image: (index % 2 == 0)
                                ? DecorationImage(
                                    image: AssetImage(
                                        'assets/images/ic_list_background_1.png'),
                                    fit: BoxFit.cover)
                                : DecorationImage(
                                    image: AssetImage(
                                        'assets/images/ic_list_background_3.png'),
                                    fit: BoxFit
                                        .cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),
                            ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 72,
                                  width: 72,
                                  child: (itemList[index].imageMedium != null)
                                      ? ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8)),
                                          child: CachedNetworkImage(
                                            imageUrl: NetworkManager.baseUrl +
                                                itemList[index].imageMedium!,
                                            imageBuilder:
                                                (context, imageProvider) =>
                                                    Container(
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.fitHeight,
                                                    colorFilter:
                                                        ColorFilter.mode(
                                                            Colors.transparent,
                                                            BlendMode.color)),
                                              ),
                                            ),
                                            placeholder: (context, url) => Center(
                                                child:
                                                    CircularProgressIndicator()),
                                            errorWidget:
                                                (context, url, error) =>
                                                    Icon(Icons.error),
                                          ),
                                        )
                                      : Image.asset(
                                          'assets/icons/ic_placeholder.png',
                                          height: 120,
                                          width: 100,
                                        ),
                                ), //AssetImage('assets/icons/ic_placeholder.png'),

                                //   CircleAvatar(
                                //     radius: 47,
                                //     backgroundColor: Colors.transparent,
                                //     backgroundImage: (itemList[index].imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium,) : AssetImage('assets/icons/ic_placeholder.png'),
                                //     //AssetImage('assets/images/Facilities1.jpg'),
                                //   ),
                              ),
                              Flexible(
                                // child: Card(color: Colors.white38,elevation: 0,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${itemList[index].firstName} ${itemList[index].lastName}',
                                      // itemList[index].firstName +
                                      //     ' ' +
                                      //     itemList[index].lastName, //"Capt. Saifur Rahman",
                                      style: gcListTitleTextStyle,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),

                                    (itemList[index].designation != null)
                                        ? Container(
                                            decoration: BoxDecoration(
                                                color: themeGolderColor,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                            padding: EdgeInsets.all(4),
                                            child: Wrap(
                                              spacing: 8.0,
                                              children: [
                                                (itemList[index]
                                                            .designation
                                                            .toString() ==
                                                        "President")
                                                    ? SvgPicture.asset(
                                                        'assets/icons/ic_crown.svg',
                                                        height: 12,
                                                        width: 12,
                                                        color: themeWhiteColor,
                                                      )
                                                    : SizedBox(
                                                        height: 1,
                                                      ),
                                                Text(
                                                  itemList[index]
                                                      .designation
                                                      .toString(), //"Captain",
                                                  style: gcNormalTextStyle,
                                                ),
                                              ],
                                            ),
                                          )
                                        : SizedBox(
                                            height: 1,
                                          ),

                                    (itemList[index].clubAcNumber != null &&
                                            itemList[index].clubAcNumber != '')
                                        ? Text(
                                            'A/C No: ' +
                                                letterString! +
                                                "-" +
                                                numberString!, //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                            style: gcDescriptionTextStyle,
                                          )
                                        : SizedBox(),
                                    // (itemList[index].membershipDate != null &&
                                    //     itemList[index].membershipDate != '')
                                    //     ? Text(
                                    //   'Member Since: ' + dateString,
                                    //       // itemList[index]
                                    //       //     .membershipDate
                                    //       //     .toString(), //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                    //   style: gcDescriptionTextStyle,
                                    // )
                                    //     : SizedBox(),
                                    // Text(
                                    //   itemList[index]
                                    //       .email
                                    //       .toString(), //"captsaif@ebd-bd.com",
                                    //   style: listSubTitleTextStyle,
                                    // ),

                                    (itemList[index].phonePrimary != null &&
                                            itemList[index].phonePrimary != '')
                                        ? Card(
                                            color: themeGreyColor,
                                            elevation: 0,
                                            child: GestureDetector(
                                              onTap: () async {
                                                // print(
                                                //     '${itemList[index].phonePrimary}');
                                                // //launch("tel://${itemList[index].mobileNumberPrimary}");
                                                //
                                                // String url = "tel://" +
                                                //     itemList[index]
                                                //         .phonePrimary
                                                //         .toString();
                                                // if (await canLaunch(url)) {
                                                //   await launch(url);
                                                // } else {
                                                //   throw 'Could not call ${itemList[index].phonePrimary}';
                                                // }
                                                setState(() {
                                                  Resources
                                                          .selectedMemberForDetails =
                                                      itemList[index];
                                                });
                                                Navigator.pushNamed(
                                                    context, '/member_details',
                                                    arguments: itemList[index]);
                                              },
                                              child: Center(
                                                child: Container(
                                                  padding: EdgeInsets.all(4),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4),
                                                    color: themeGolderColor,
                                                  ),
                                                  width: double.infinity,
                                                  height: 30,
                                                  child: Text(
                                                    'View Details',
                                                    textAlign: TextAlign.center,
                                                    //'01819-415452',
                                                    style: GoogleFonts.poppins(
                                                        color: themeWhiteColor,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 12),
                                                  ),
                                                  // Text(
                                                  //   itemList[index]
                                                  //       .phonePrimary, //'01819-415452',
                                                  //   style: GoogleFonts.poppins(
                                                  //       // color: themeWhiteColor,
                                                  //       fontWeight:
                                                  //           FontWeight.bold,
                                                  //       fontSize: 16),
                                                  // ),
                                                ),
                                              ),
                                            ),
                                          )
                                        : SizedBox(),

                                    // (itemList[index].profession != null)?Text(
                                    //   itemList[index].profession.toString(), //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                    //   style: gcDescriptionTextStyle,
                                    // ):SizedBox(),
                                    // RaisedButton(
                                    //   color: themeGreyColor,
                                    //   onPressed: () {
                                    //     print('pressed');
                                    //     // setState(() {
                                    //     //
                                    //     // });
                                    //     Resources.selectedRecipientID = itemList[index].id.toString();
                                    //     Resources.selectedMember = itemList[index];
                                    //     Resources.isSearchOn = true;
                                    //
                                    //     print('Resources.selectedRecipientID----${Resources.selectedRecipientID}');
                                    //     Navigator.pushNamed(context, '/chat');
                                    //   },
                                    //   child: Text(
                                    //     'Message',
                                    //     style: GoogleFonts.poppins(fontSize: 10, color: Colors.white),
                                    //   ),
                                    // )
                                  ],
                                ),
                              ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class Post {
  final String title;
  final String body;

  Post(this.title, this.body);
}
