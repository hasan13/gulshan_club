// To parse this JSON data, do
//
//     final cartData = cartDataFromJson(jsonString);

import 'dart:convert';

import 'package:gulshan_club_app/jsonModels/product_list_response.dart';

CartData cartDataFromJson(String str) => CartData.fromJson(json.decode(str));

String cartDataToJson(CartData data) => json.encode(data.toJson());

class CartData {
  CartData({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  Data? data;

  factory CartData.fromJson(Map<String, dynamic> json) => CartData(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "data": data == null ? null : data!.toJson(),
  };
}

class Data {
  Data({
    this.cardItems,
    this.totalAmount,
  });

  List<CardItem>? cardItems;
  double? totalAmount;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    cardItems: json["card_items"] == null ? null : List<CardItem>.from(json["card_items"].map((x) => CardItem.fromJson(x))),
    totalAmount: json["total_amount"] == null ? null : json["total_amount"],
  );

  Map<String, dynamic> toJson() => {
    "card_items": cardItems == null ? null : List<CardItem>.from(cardItems!.map((x) => x.toJson())),
    "total_amount": totalAmount == null ? null : totalAmount,
  };
}

class CardItem {
  CardItem({
    this.cardId,
    this.product,
    this.productQuantity,
  });

  int? cardId;
  Products? product;
  int? productQuantity;

  factory CardItem.fromJson(Map<String, dynamic> json) => CardItem(
    cardId: json["card_id"] == null ? null : json["card_id"],
    product: json["product"] == null ? null : Products.fromJson(json["product"]),
    productQuantity: json["product_quantity"] == null ? null : json["product_quantity"],
  );

  Map<String, dynamic> toJson() => {
    "card_id": cardId == null ? null : cardId,
    "product": product == null ? null : product!.toJson(),
    "product_quantity": productQuantity == null ? null : productQuantity,
  };
}

// class Product {
//   Product({
//     this.id,
//     this.name,
//     this.price,
//     this.category,
//     this.type,
//     this.restaurant,
//     this.unit,
//     this.image,
//   });
//
//   int id;
//   String name;
//   double price;
//   int category;
//   String type;
//   String restaurant;
//   String unit;
//   dynamic image;
//
//   factory Product.fromJson(Map<String, dynamic> json) => Product(
//     id: json["id"] == null ? null : json["id"],
//     name: json["name"] == null ? null : json["name"],
//     price: json["price"] == null ? null : json["price"],
//     category: json["category"] == null ? null : json["category"],
//     type: json["type"] == null ? null : json["type"],
//     restaurant: json["restaurant"] == null ? null : json["restaurant"],
//     unit: json["unit"] == null ? null : json["unit"],
//     image: json["image"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id": id == null ? null : id,
//     "name": name == null ? null : name,
//     "price": price == null ? null : price,
//     "category": category == null ? null : category,
//     "type": type == null ? null : type,
//     "restaurant": restaurant == null ? null : restaurant,
//     "unit": unit == null ? null : unit,
//     "image": image,
//   };
// }
