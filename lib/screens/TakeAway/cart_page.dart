import 'dart:convert';

import 'package:badges/badges.dart' as Badge;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'bottom_modal.dart';
import 'card_data_response.dart';
import 'gc_checkout_address_screen.dart';

class CartRoute extends StatefulWidget {
  dynamic tabContainerBottomState;
  CartRoute({this.tabContainerBottomState});
  @override
  CartRouteState createState() => CartRouteState();
}

class CartRouteState extends State<CartRoute> with AutomaticKeepAliveClientMixin<CartRoute> {
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();//ScaffoldMessengerState
  var isLoading = true;
  var isLoadingDelete = false;
  var noOfReview = 25;
  var rating = 5.0;
  double totalAmount = 0.0;
  bool notLoggedIn = false;

  int itemCounter = 0;

  List<CardItem> items = [];

  @override
  void initState() {
    super.initState();
    getCartItems();
  }

  getCartItems() async {
    bool result = await InternetConnectionChecker().hasConnection;

    var url = NetworkManager.baseUrl + "/api/v1/product/card-product-info";
    if (result == true) {
      try {
        http.get(Uri.parse(url), headers: {"Authorization": "Bearer ${Resources.currentUser?.accessToken}"}).timeout(Duration(seconds: 30)).then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              if (value.statusCode == 200 && data["status"] == true) {
                final cartData = cartDataFromJson(value.body);
                // print(cartData.data.cardItems.length);
                setState(() {
                  isLoading = false;
                  itemCounter = cartData.data?.cardItems?.length ?? 0;
                  items = cartData.data?.cardItems ?? [];
                  totalAmount = cartData.data?.totalAmount ?? 0;
                });

                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data["message"] == "Please sign in" && data["status"] == false) {
                setState(() {
                  isLoading = false;
                });

                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(data["message"]),
                );
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
    }
  }

  deleteCartItem(String cartId) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +  "/api/v1/product/card-delete?ids=$cartId";

    if (result == true) {
      print("itemId");
      print(cartId);
      try {
        http.get(Uri.parse(url), headers: {"Authorization": "Bearer ${Resources.currentUser?.accessToken}"}).timeout(Duration(seconds: 30)).then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              if (value.statusCode == 200 && data["status"] == true) {
                setState(() {
                  isLoadingDelete = false;
                  isLoading = true;
                });
                getCartItems();
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else {
                setState(() {
                  isLoadingDelete = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(data["message"]),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoadingDelete = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoadingDelete = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: themeBlackColor,
        title: Text(
          'Cart',
          style: gcTitleTextStyle,
        ),
        actions: [
          Badge.Badge(
            badgeContent: Text(
              itemCounter.toString(),
              style: TextStyle(color: Colors.white),
            ),
            // position: BadgePosition.topStart(),
            position: Badge.BadgePosition.topEnd(top: 0, end: 4),
            // animationDuration: Duration(milliseconds: 300),
            // animationType: Badge.BadgeAnimationType.slide,
            child: IconButton(
              onPressed: () {
                print('clicked');
              },
              icon: SvgPicture.asset(
                "assets/icons/Cart.svg",
                fit: BoxFit.fitWidth,
              ),
              color: themeGolderColor,
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : items == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : items.length == 0
                    ? Center(
                        child: Text(
                          "No item added to cart",
                          style: TextStyle(),
                        ),
                      )
                    : Column(
                        children: <Widget>[
                          Container(
                              child:

                                  //(itemList.length == 0)? Center(child: Text("You have no recipes yet!",style: TextStyle(color: Colors.grey),)):
                                  createListView2(items, context)),
                          ClipRRect(
                            child: Container(
                              height: 100,
                              decoration: BoxDecoration(
                                color: themeGolderColor.withOpacity(0.4),
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(16.0),
                                  topLeft: Radius.circular(16.0),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(right: 8.0, left: 8.0, top: 8.0),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Total:",
                                            style: gcTitleTextStyle,
                                          ),
                                          Text(
                                            "Tk ${totalAmount.toStringAsFixed(2)} BDT",
                                            style: gcTitleTextStyle,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: MyCustomButton(
                                        width: 200,
                                        height: 30,
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => CheckoutRoute(
                                                      cartItems: items,
                                                      totalAmount: totalAmount,
                                                      preRouteCart: "cart",
                                                    )),
                                          );
                                        },
                                        color: themeGolderColor,
                                        child: Text(
                                          "Confirm",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
//                  ],
//                ),
//              ),
                          //SizedBox( height: 100,)
                        ],
                      ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Widget createListView2(items, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: items.length, //_pairList.length,
        itemBuilder: (BuildContext context, int index) {
          //return movieCard(movieList[index], context);
          //final item = data[index];

          var productItemCounter = 1;
          var previousAmount = 299;
          var amount = 200;
          return Dismissible(
              direction: DismissDirection.endToStart,
              // Each Dismissible must contain a Key. Keys allow Flutter to
              // uniquely identify widgets.
              key: UniqueKey(),
              // Provide a function that tells the app
              // what to do after an item has been swiped away.
              confirmDismiss: (DismissDirection direction) async {
                return await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: const Text("Confirm"),
                      content: const Text("Are you sure you wish to delete this item?"),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                              deleteCartItem(items[index].cardId.toString());
                            },
                            child: const Text("DELETE")),
                        TextButton(
                          onPressed: () => Navigator.of(context).pop(false),
                          child: const Text("CANCEL"),
                        ),
                      ],
                    );
                  },
                );
              },
              onDismissed: (direction) {},
              background: Container(
                color: Colors.green,
                child: Align(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[],
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ),
              secondaryBackground: Container(
                padding: EdgeInsets.only(right: 28.0),
                alignment: AlignmentDirectional.centerStart,
                color: Colors.red,
                child: Align(
                    alignment: Alignment.centerRight,
                    child: Icon(
                      Icons.delete_forever,
                      color: Colors.white,
                    )),
              ),

              // Show a red background as the item is swiped away.
              //background: Container(child: Center(child: Icon(Icons.delete,color: Colors.white,)),color: Colors.red),
              child:
                  //ListTile(title: Text('$item')),
                  GestureDetector(
                onTap: () {
                  // SnackBar mySnkBr = SnackBar(
                  //   content: Text("Clicked on a item"),
                  //   backgroundColor: Colors.lightBlueAccent,
                  // );
                  // Scaffold.of(context).showSnackBar(mySnkBr);
                },
                child: Card(
                  margin: EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
                  // color: Colors.white,
                  elevation: 0,
                  child: GestureDetector(
                    onTap: () {
                      print("GestureDetector on index ($index)");
//                        selectedRecipeID = data[index].id; //["id"];
//                        _goToDetailsRoute(context);
                    },
                    child: Column(
                      children: [
                        CartItem(
                          cart: items[index],
                          cartRouteState: this,
                        ),
                        Divider(
                          thickness: 1,
                          height: 1,
                          color: Colors.grey.shade800,
                        )
                      ],
                    ),
                  ),
                ),
              ));
        },
      ),
    );
  }
}

class Recipe {}

class CartItem extends StatefulWidget {
  CardItem cart;
  CartRouteState cartRouteState;
  CartItem({required this.cart, required this.cartRouteState});
  @override
  _CartItemState createState() => new _CartItemState();
}

class _CartItemState extends State<CartItem> {
  int _itemCount = 0;
  var amount = 299.0;
  var previousAmount = 499.0;
  var productItemCounter = 0;

  deleteItem(int cartId) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Confirm"),
          content: const Text("Are you sure you wish to delete this item?"),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                  widget.cartRouteState.deleteCartItem(cartId.toString());
                },
                child: const Text("DELETE")),
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: const Text("CANCEL"),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: themeBlackColor,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Container(
                width: 80,
                height: 80,
                color: themeGreyColor,
                child: (widget.cart.product?.image != null)
                    ? CachedNetworkImage(
                        imageUrl: widget.cart.product!.image.toString(),
                        imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                  // colorFilter:
                                  // ColorFilter.mode(Colors.red, BlendMode.colorBurn)
                                ),
                              ),
                            ),
                        placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                        errorWidget: (context, url, error) => Image.asset('assets/icons/Placeholder.png', fit: BoxFit.cover))
                    : Image.asset(
                        'assets/icons/Placeholder.png',
                        fit: BoxFit.fill,
                      ),
              ),
            ),
          ),
          if(widget.cart.product != null)Flexible(
            fit: FlexFit.loose,
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.cart.product!.name.toString() ,
                    style: gcNormalTextStyle,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(top: 7.0),
                          child: Text(
                            "Tk ${(widget.cart.product!.price! * widget.cart!.productQuantity!).toStringAsFixed(2)}",
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: gcPaymentTextStyle,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 0.0),
                          child: IconButton(
                            icon: SvgPicture.asset(
                              "assets/icons/Delete for cart screen.svg",
                              height: 20,
                              width: 20,
                            ),
                            // Icon(
                            //   Icons.delete,
                            //   color: Colors.red,
                            // ),
                            onPressed: () {
                              deleteItem(widget.cart.cardId!);
                            },
                          ),
                        )

                        // Padding(
                        //   padding: const EdgeInsets.all(8.0),
                        //   child: Container(
                        //     child: Text('\$${previousPrice * cartQuantity }',
                        //         style: TextStyles.lineThrough),
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(color: themeWhiteColor, borderRadius: BorderRadius.circular(16.0)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            print("clicked minus");
//                          setState(() {
//                            (productItemCounter > 1)
//                                ? productItemCounter =
//                                productItemCounter - 1
//                                : productItemCounter = 1;
//                          });
                            showModalBottomSheet(
                                context: context,
                                builder: (BuildContext bc) {
                                  return Container(
                                      height: 200,
                                      child: BottomModal(
                                        data: widget.cart.product!.id!,
                                        preRoute: "cart",
                                        quantityCount: widget.cart.productQuantity,
                                      ));
                                }).then((value) {
                              print("value");
                              print(value);
                              setState(() {
                                widget.cartRouteState.isLoading = true;
                                widget.cartRouteState.getCartItems();
                              });
                            });
                          },
                          child: Icon(
                            Icons.indeterminate_check_box,
                            color: themeGolderColor,
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: 50,
                            decoration: BoxDecoration(
                                // border: Border.all(color: Colors.grey, width: 1),
                                ),
                            child: Text(
                              "${widget.cart.productQuantity}",
                              textAlign: TextAlign.center,
                              //style: gcNormalTextStyle,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            print("clicked");
//                          setState(() {
//                            productItemCounter =
//                                productItemCounter + 1;
//                          });
                            showModalBottomSheet(
                                context: context,
                                builder: (BuildContext bc) {
                                  return Container(
                                      height: 200,
                                      child: BottomModal(
                                        data: widget.cart.product!.id!,
                                        preRoute: "cart",
                                        quantityCount: widget.cart.productQuantity,
                                      ));
                                }).then((value) {
                              print("value");
                              print(value);
                              setState(() {
                                widget.cartRouteState.isLoading = true;
                                widget.cartRouteState.getCartItems();
                              });
                            });
                          },
                          child: Icon(
                            Icons.add_box,
                            color: themeGolderColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
