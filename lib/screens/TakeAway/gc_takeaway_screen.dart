import 'dart:convert';
import 'dart:ui';

import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
 import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/jsonModels/category_list_response.dart';
import 'package:gulshan_club_app/jsonModels/club_facilities_response.dart';
import 'package:gulshan_club_app/jsonModels/generic_api_response.dart';
import 'package:gulshan_club_app/jsonModels/reservation_list_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

import 'order_list_response.dart';

class TakeAwayScreen extends StatefulWidget {
  static const String route = '/take_away';

  @override
  _TakeAwayScreenState createState() => _TakeAwayScreenState();
}

class _TakeAwayScreenState extends State<TakeAwayScreen>
    with TickerProviderStateMixin {
  List<String> itemList = ["dsd"];
  List<FoodCategory> allFoodCategories = [];
  List<String> namesList = [];
  var isLoading = false;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  FoodCategoryResponse? clubFacilityResponse;
  bool isLoadingMore = false;

  List<Reservation> allReservationList = [];
  ReservationListResponse? reservationsListResponse;

  late TabController _tabController;
  late TabController _ordersTabController;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  late ScrollController _scrollController;
  late bool fixedScroll;
  DateTime _chosenDateTime1 = DateTime.now();

  int _orderTabIndex = 0;
  int _tabControllerIndex = 0;

  String _selectedName = '';
  Facility? _selectedFacility;
  String startDate = "";

  final List<Tab> tabs = <Tab>[
    Tab(
      child: Text('Categories'),
    ),
    Tab(
      child: Text('Orders'),
    ),
  ];

  final List<Tab> myTabs = <Tab>[
    Tab(
      child: Text('All'),
    ),
    Tab(
      child: Text('Ordered'),
    ),
    Tab(
      child: Text('Confirmed'),
    ),
    Tab(
      child: Text('Canceled'),
    ),
  ];

  var itemCount = 0;

  List<OrdersData> allOrdersList = [];
  OrderListResponse? orderListResponse;

  bool isOrderLoading = false;

  @override
  void initState() {
    //startDate = DateFormat("yyyy-MM-dd HH:MM").format(_chosenDateTime1);
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
    _ordersTabController = TabController(length: myTabs.length, vsync: this);
    _tabController.addListener(_setActiveTab);
    _ordersTabController.addListener(_setActiveTabIndex);

    getFoodCategories();
  }

  void _setActiveTab() {
    _tabControllerIndex = _tabController.index;
    print("_tabControllerIndex $_tabControllerIndex");
    _resetData();
    if (_tabControllerIndex == 1) {
      // getAllReservation("all");
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order";
      getAllOrders(url);
    } else {
      // getAllOrders();

    }
  }

  void _setActiveTabIndex() {
    _orderTabIndex = _ordersTabController.index;
    print("_activeTabIndex $_orderTabIndex");

    setState(() {
      allOrdersList = [];
    });
    if (_ordersTabController.index == 0) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order";
      getAllOrders(url);
    } else if (_ordersTabController.index == 1) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order?status=Ordered";
      getAllOrders(url);
    } else if (_ordersTabController.index == 2) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order?status=Confirmed";
      getAllOrders(url);
    } else if (_ordersTabController.index == 3) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order?status=Canceled";
      getAllOrders(url);
    } else {
      print(_ordersTabController.index);
    }
  }

  void _resetData() {
    setState(() {
      startDate = "";
      _selectedName = 'null';
      _selectedFacility = null;
    });
  }

  getFoodCategories() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/key";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final clubResponse = FoodCategoryResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${clubResponse.data}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  allFoodCategories = clubResponse.data!;
                  clubFacilityResponse = clubResponse;
                  isLoading = false;

                  // for (var i = 0; i < allFoodCategories.length; i++) {
                  //   namesList.add(allFoodCategories[i].name);
                  // }
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  getAllOrders(String url) async {
    bool result = await InternetConnectionChecker().hasConnection;

    setState(() {
      isOrderLoading = true;
    });
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              // final data = json.decode(utf8.decode(value.bodyBytes));
              // final data = json.decode(value.body);
              // final clubResponse = orderListResponseFromJson(value.body);
              // final clubResponse = OrderListResponse.fromJson(
              //     data); //noticeResponseFromJson(data);

              final data = json.decode(value.body);
              final ordersListResponse = OrderListResponse.fromJson(data);

              print('result....\n ${ordersListResponse.data}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  allOrdersList = ordersListResponse.data ?? [];
                  orderListResponse = ordersListResponse;
                  isOrderLoading = false;
                });
              } else if (data['status'] == false) {
                setState(() {
                  isOrderLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isOrderLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isOrderLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  getAllReservation(String filterString) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/reservation/$filterString/get-reservation/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final clubResponse = ReservationListResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${clubResponse.data?.results}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  allReservationList = clubResponse.data?.results ?? [];
                  reservationsListResponse = clubResponse;
                  isLoading = false;
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                Fluttertoast.showToast(
                  msg:
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}',
                  backgroundColor: Colors.red,
                  gravity: ToastGravity.CENTER,
                );
              } else {
                setState(() {
                  isLoading = false;
                });
                Fluttertoast.showToast(
                  msg:
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}',
                  backgroundColor: Colors.red,
                  gravity: ToastGravity.CENTER,
                );
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
        msg: NetworkManager.internetErrorMessage,
        backgroundColor: Colors.red,
        gravity: ToastGravity.CENTER,
      );
    }
  }

  _loadMoreMember() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint = reservationsListResponse
        ?.data?.next ?? ''; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreReservationResponse = ReservationListResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        List<Reservation> moreData = moreReservationResponse.data?.results ?? [];
        allReservationList.addAll(moreData);
        isLoading = false;
        reservationsListResponse = moreReservationResponse;
        // List<Member> moreData = moreNoticeResponse.results;
        // itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  Future<void> _reloadOrdersData() async {
    if (_ordersTabController.index == 0) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order";
      getAllOrders(url);
    } else if (_ordersTabController.index == 1) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order?status=Ordered";
      getAllOrders(url);
    } else if (_ordersTabController.index == 2) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order?status=Confirmed";
      getAllOrders(url);
    } else if (_ordersTabController.index == 3) {
      var url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order?status=Canceled";
      getAllOrders(url);
    } else {
      print(_ordersTabController.index);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
        pageTitle: 'Take Away',
        bottomNavigationBar: Resources.setBottomDrawer(context),

        // Scaffold(
        //     backgroundColor: themeBlackColor,
        //     appBar: AppBar(
        //       backgroundColor: themeBlackColor,
        //       title: Text(
        //         "Take Away",
        //         style: gcTitleTextStyle,
        //       ),
        //       actions: [
        //         Badge(
        //           badgeContent: Text(
        //             itemCount.toString(),
        //             style: TextStyle(color: Colors.white),
        //           ),
        //           // position: BadgePosition.topStart(),
        //           position: BadgePosition.topEnd(top: 0, end: 4),
        //           animationDuration: Duration(milliseconds: 300),
        //           animationType: BadgeAnimationType.slide,
        //           child: IconButton(
        //             onPressed: () {
        //               print('clicked');
        //               Navigator.pushNamed(
        //                   context, "/notifications");
        //             },
        //             icon: SvgPicture.asset(
        //               "assets/icons/Cart.svg",
        //               fit: BoxFit.fitWidth,
        //             ),
        //             color: themeGolderColor,
        //           ),
        //         ),
        //       ],
        //     ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : itemList == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : itemList.length == 0
                    ? Center(
                        child: Text(
                          "No Data found",
                          style: gcPlaceholderTextStyle,
                        ),
                      )
                    : createListView(itemList, context));
  }

  createListView(List<String> itemList, BuildContext context) {
    return Scaffold(
        backgroundColor: themeBlackColor,
        appBar: AppBar(
          backgroundColor: themeBlackColor,
          bottom: PreferredSize(
              child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.white,
                  indicatorColor: themeGolderColor,
                  labelColor: themeGolderColor,
                  labelStyle: gcTitleTextStyle,
                  unselectedLabelStyle: gcNormalTextStyle,
                  indicatorWeight: 3,
                  onTap: (index) {
                    print("index  $index");
                    setState(() {
                      _tabControllerIndex = index;
                      _tabController.animateTo(index,
                          duration: Duration(milliseconds: 1000));
                    });
                  },
                  controller: _tabController,
                  tabs: tabs),
              preferredSize: Size.fromHeight(0.0)),
        ),
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            FoodCategoriesTabView(),
            ReservationTabView(), //OrdersTabView()
          ], //ReservationTabView()],
        ));
  }

  Widget FoodCategoriesTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : allFoodCategories == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : (allFoodCategories.length == 0)
                    ? Center(
                        child: Text(
                          "No Categories found",
                          style: gcPlaceholderTextStyle,
                        ),
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height - 230,
                        child:
                            FoodCategoriesListView(allFoodCategories, context)),
      ),
    );
  }

  Widget FoodCategoriesListView(
      List<FoodCategory> results, BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: allFoodCategories.length,
            itemBuilder: (context, index) {
              return Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16),
                child: GestureDetector(
                  onTap: () {},
                  child: Container(
                    decoration: BoxDecoration(
                      // color: Colors.white38,
                      borderRadius: BorderRadius.all(
                          Radius.circular(8)), // color: Colors.white38,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, '/products',
                              arguments: allFoodCategories[index]);
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height / 4,
                          child: Stack(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Container(
                                  height:
                                      MediaQuery.of(context).size.height / 4,
                                  width: MediaQuery.of(context).size.width,
                                  child: (allFoodCategories[index].image !=
                                          null)
                                      ? CachedNetworkImage(
                                          imageUrl:
                                              allFoodCategories[index].image!,
                                          imageBuilder:
                                              (context, imageProvider) =>
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: imageProvider,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                          placeholder: (context, url) => Center(
                                              child:
                                                  CircularProgressIndicator()),
                                          errorWidget: (context, url, error) =>
                                              Image.asset(
                                                  'assets/images/category.jpg',
                                                  fit: BoxFit.cover))
                                      : Image.asset(
                                          "assets/images/category.jpg",
                                          fit: BoxFit.fill,
                                        ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Align(
                                  alignment: Alignment.bottomLeft,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(24),
                                    child: BackdropFilter(
                                      filter: ImageFilter.blur(
                                        sigmaX: 10.0,
                                        sigmaY: 10.0,
                                      ),
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                1.5,
                                        decoration: BoxDecoration(
                                          color:
                                              themeWhiteColor.withOpacity(0.5),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                  8)), // color: Colors.white38,
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            allFoodCategories[index]
                                                .name ??' ', //"40 th E.C. Meeting",
                                            style: gcKLargeTitleTextStyle,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
      // ),
      // ),
    );
  }

  Widget OrdersTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : allOrdersList == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : (allOrdersList.length == 0)
                    ? Center(
                        child: Text(
                          "No Orders found",
                          style: gcPlaceholderTextStyle,
                        ),
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height - 150,
                        child: OrdersListView(allOrdersList, context)),
      ),
    );
  }

  Widget OrdersListView(List<OrdersData> results, BuildContext context) {
    return
        //   RefreshIndicator(
        //   onRefresh: _reloadOrdersData,
        //   key: refreshKey,
        //   child:
        // )
        Column(
      children: [
        if (allOrdersList.isEmpty == false)
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemCount: allOrdersList.length,
              itemBuilder: (context, index) {
                String dateString = "";
                if (allOrdersList.isEmpty == false) {
                  if (allOrdersList[index].created != null) {
                    DateTime parsedDate =
                        DateTime.parse(allOrdersList[index].created.toString())
                            .toLocal();
                    dateString = DateFormat.yMd().add_jm().format(parsedDate);
                  }
                }

                return Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 16),
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      decoration: BoxDecoration(
                        // color: Colors.white38,
                        borderRadius: BorderRadius.all(
                            Radius.circular(8)), // color: Colors.white38,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: GestureDetector(
                          onTap: () {},
                          child: IntrinsicHeight(
                            // child: ConstrainedBox(
                            //   constraints: BoxConstraints(minHeight: 70, maxHeight: 200),
                            child: Container(
                              // constraints:
                              //     BoxConstraints(maxHeight:80, minHeight: 80.0),
                              width: MediaQuery.of(context).size.width,
                              child: Column(
                                children: [
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              1.45,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    "Date: ",
                                                    style: gcListTitleTextStyle,
                                                  ),
                                                  Expanded(
                                                    child: Text(
                                                      dateString, //"40 th E.C. Meeting",
                                                      style: gcNormalTextStyle,
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 8,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    "Address: ",
                                                    style: gcListTitleTextStyle,
                                                  ),
                                                  if (allOrdersList.isEmpty ==
                                                      false)
                                                    Expanded(
                                                      child: Text(
                                                        allOrdersList[index]
                                                                ?.address
                                                                ?.address1 ??
                                                            " Pick Up" +
                                                                " ", //"40 th E.C. Meeting",
                                                        style:
                                                            gcNormalTextStyle,
                                                        textAlign:
                                                            TextAlign.left,
                                                        maxLines: 3,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Flexible(
                                          child: Column(
                                            children: [
                                              if (allOrdersList.isEmpty ==
                                                  false)
                                                Expanded(
                                                  child: Text(
                                                    "Tk " +
                                                        allOrdersList[index]
                                                            .amount
                                                            .toString(), //"40 th E.C. Meeting",
                                                    style: gcListTitleTextStyle,
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              if (allOrdersList.isEmpty ==
                                                  false)
                                                Container(
                                                  decoration: BoxDecoration(
                                                    color: (allOrdersList[index]
                                                                .status ==
                                                            "Ordered")
                                                        ? Colors.orangeAccent
                                                        : (allOrdersList[index]
                                                                    .status ==
                                                                "Confirmed")
                                                            ? Colors.green
                                                            : Colors.red,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            6),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            4.0),
                                                    child: Text(
                                                      allOrdersList[index]
                                                          .status ?? ' ', //"Capt. Saifur Rahman",
                                                      style: gcNormalTextStyle,
                                                    ),
                                                  ),
                                                ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Divider(
                                      thickness: 1,
                                      height: 1,
                                      color: Colors.grey.shade800,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
      ],
      // ),
    );
  }

  Widget ReservationTabView() {
    return Scaffold(
        backgroundColor: themeBlackColor,
        appBar: AppBar(
          backgroundColor: themeBlackColor,
          bottom: PreferredSize(
              child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.white,
                  indicatorColor: themeGolderColor,
                  labelColor: themeGolderColor,
                  labelStyle: gcTitleTextStyle,
                  unselectedLabelStyle: gcNormalTextStyle,
                  indicatorWeight: 3,
                  onTap: (index) {
                    print("_reservationTabController index  $index");
                  },
                  controller: _ordersTabController,
                  tabs: myTabs),
              preferredSize: Size.fromHeight(0.0)),
        ),
        body: TabBarView(
          controller: _ordersTabController,
          children: <Widget>[
            AllReservationTabView(),
            OrderedReservationTabView(),
            ConfirmedReservationTabView(),
            CanceledReservationTabView()
          ],
        ));
  }

  Widget AllReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isOrderLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : allOrdersList == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : (allOrdersList.length == 0)
                    ? Center(
                        child: Text(
                          "No Orders found",
                          style: gcPlaceholderTextStyle,
                        ),
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height - 150,
                        child: OrdersListView(allOrdersList, context)),
      ),
    );
  }

  Widget OrderedReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isOrderLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : allOrdersList == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : (allOrdersList.length == 0)
                    ? Center(
                        child: Text(
                          "No Orders found",
                          style: gcPlaceholderTextStyle,
                        ),
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height - 150,
                        child: OrdersListView(allOrdersList, context)),
      ),
    );
  }

  Widget ConfirmedReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isOrderLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : allOrdersList == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : (allOrdersList.length == 0)
                    ? Center(
                        child: Text(
                          "No Orders found",
                          style: gcPlaceholderTextStyle,
                        ),
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height - 150,
                        child: OrdersListView(allOrdersList, context)),
      ),
    );
  }

  Widget CanceledReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isOrderLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : allOrdersList == null
                ? Center(
                    child: Text(
                    "Something went wrong",
                    style: TextStyle(),
                  ))
                : (allOrdersList.length == 0)
                    ? Center(
                        child: Text(
                          "No Orders found",
                          style: gcPlaceholderTextStyle,
                        ),
                      )
                    : Container(
                        height: MediaQuery.of(context).size.height - 150,
                        child: OrdersListView(allOrdersList, context)),
      ),
    );
  }
}
