import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/TakeAway/dialogs.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

class BottomModal extends StatefulWidget {
  final int data;
  final preRoute;
  final quantityCount;
  const BottomModal(
      {Key? key, required this.data, this.preRoute, this.quantityCount})
      : super(key: key);
  @override
  _BottomModalState createState() => _BottomModalState(data);
}

class _BottomModalState extends State<BottomModal> {
  var productItemCounter = 1;
  bool isLoading = false;
  var data;

  _BottomModalState(data);

  @override
  void initState() {
    super.initState();
    if (widget.preRoute == "cart") {
      productItemCounter = widget.quantityCount;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("data");
    print(widget.data);
    print(widget.data);
    print(productItemCounter);
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            widget.preRoute == 'cart' ? "Change Quantity" : "ADD TO CART",
            style: gcKLargeListTitleTextStyle,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                print("clicked minus");
                setState(() {
                  (productItemCounter > 1)
                      ? productItemCounter = productItemCounter - 1
                      : productItemCounter = 1;
                });
              },
              child: Container(
                height: 30,
                child: Icon(
                  Icons.indeterminate_check_box,
                  color: themeGolderColor,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: 80,
                height: 30,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 1),
                ),
                child: Center(
                  child: Text(
                    "$productItemCounter",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                print("clicked");
                setState(() {
                  productItemCounter = productItemCounter + 1;
                });
              },
              child: Container(
                height: 30,
                child: Icon(
                  Icons.add_box,
                  color: themeGolderColor,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 12,
        ),
        GestureDetector(
          onTap: isLoading
              ? null
              : () {
                  setState(() {
                    isLoading = true;
                  });
                  performCartAdd();
                },
          child: Container(
              width: 200,
              height: 40,
              decoration: BoxDecoration(
                  color: themeGolderColor,
                  borderRadius: BorderRadius.circular(8)),
              child: Center(
                  child: isLoading
                      ? CircularProgressIndicator()
                      : Text(
                          "CONTINUE",
                          style: gcNormalTextStyle,
                        ))),
        ),
        SizedBox(
          height: 12,
        ),
      ],
    );
  }

  performCartAdd() async {
    print("product--${widget.data}");
    print(productItemCounter);

    var params = [
      {"product": widget.data, "product_quantity": productItemCounter}
    ];

    bool result = await InternetConnectionChecker().hasConnection;

    if (result) {
      http
          .post(
            Uri.parse(NetworkManager.baseUrl + "/api/v1/product/card-save"),
            headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
              "Content-Type": "application/json",
            },
            body: jsonEncode(params),
          )
          .timeout(Duration(seconds: 30))
          .then((value) {
        print("status code Token:${value.statusCode}");
        print(value.body);

        dynamic data = json.decode(value.body);
        if (value.statusCode == 200 && data["status"] == true) {
          print("success..");
          setState(() {
            isLoading = false;
          });

          // WCDialog.showWidgetDialog(context)
          WCDialog.showContentDialog(context,
              message: widget.preRoute == 'cart'
                  ? "Changed Quantity"
                  : "Added into Cart", onTap: () {
            Navigator.pop(context);
            Navigator.pop(context);
          });
        } else {
          setState(() {
            isLoading = false;
          });

          WCDialog.showContentDialog(context, message: data["message"],
              onTap: () {
            Navigator.pop(context);
          });
        }
      });
    } else {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
          msg: "Unstable Internet Connection!",
          textColor: Colors.white,
          backgroundColor: Colors.pink,
          gravity: ToastGravity.SNACKBAR,
          toastLength: Toast.LENGTH_SHORT);
    }
  }
}
