import 'dart:convert';

import 'package:badges/badges.dart' as Badge;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart' as fsb;

import 'package:flappy_search_bar_ns/scaled_tile.dart';
// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/category_list_response.dart';
import 'package:gulshan_club_app/jsonModels/product_list_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'bottom_modal.dart';
import 'card_data_response.dart';
import 'cart_page.dart';

class ProductsScreen extends StatefulWidget {
  static const String route = '/products';

  final FoodCategory arguments;

  ProductsScreen(this.arguments);

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  var isLoading = false;
  List<Products> itemList = [];
  ProductListResponse? noticeJson;

  bool isLoadingMore = false;
  var itemCounter = 0;

  final SearchBarController<Products> _searchBarController =
  SearchBarController();

  var isSearchOn = false;

  bool isLoadingProduct = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print(widget.arguments.name);
    print(widget.arguments.id);
    getAllProducts();
    getCartItems();
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllProducts();
    });
  }

  Future<List<Products>> _getALlPosts(String? text) async {
    if (text == null) return [];
    if (text!.length > 0) {
      getFilteredProducts(text);
    } else {
      getAllProducts();
    }

    return []; //posts;
  }

  getAllProducts() async {
    setState(() {
      isLoadingProduct = true;
      // getAllProducts();
    });

    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/product/key/${widget.arguments.id.toString()}";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');

    if (result == true) {
      try {
        await http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final noticeResponse = ProductListResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data?.results}');

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  itemList = noticeResponse.data?.results ??[];
                  noticeJson = noticeResponse;
                  isLoadingProduct = false;
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoadingProduct = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoadingProduct = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoadingProduct = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
        isLoadingProduct = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
    }
  }

  getFilteredProducts(String text) async {
    setState(() {
      isLoadingProduct = true;
      isSearchOn = true;
      // getAllProducts();
    });
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/product/key/${widget.arguments.id.toString()}?name=$text";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');

    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final noticeResponse = ProductListResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data?.results}');

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  isSearchOn = true;
                  itemList = noticeResponse.data?.results ?? [];
                  noticeJson = noticeResponse;
                  isLoadingProduct = false;
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoadingProduct = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoadingProduct = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
        isLoadingProduct = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
    }
  }

  getCartItems() async {
    bool result = await InternetConnectionChecker().hasConnection;

    var url = NetworkManager.baseUrl + "/api/v1/product/card-product-info";
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              if (value.statusCode == 200 && data["status"] == true) {
                final cartData = cartDataFromJson(value.body);
                // print(cartData.data.cardItems.length);
                setState(() {
                  isLoading = false;
                  itemCounter = cartData.data?.cardItems?.length ?? 0;
                });

                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data["message"] == "Please sign in" &&
                  data["status"] == false) {
                setState(() {
                  isLoading = false;
                  Resources.loggedIn = false;
                });

                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(data["message"]),
                );
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
    }
  }

  _loadMoreData() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String? uploadEndPoint =
        noticeJson?.data?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint ?? ''), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = ProductListResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        noticeJson = moreNoticeResponse;
        List<Products> moreData = moreNoticeResponse.data?.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        actions: [
          Badge.Badge(
            badgeContent: Text(
              itemCounter.toString(),
              style: TextStyle(color: Colors.white),
            ),
            // position: BadgePosition.topStart(),
            position: Badge.BadgePosition.topEnd(top: 0, end: 4),
            // animationDuration: Duration(milliseconds: 300),
            // animationType: Badge.BadgeAnimationType.slide,
            child: IconButton(
              onPressed: () {
                print('clicked');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CartRoute(),
                  ),
                ).then((value) => getCartItems());
              },
              icon: SvgPicture.asset(
                "assets/icons/Cart.svg",
                fit: BoxFit.fitWidth,
              ),
              color: themeGolderColor,
            ),
          ),
        ],
        title: Text(
          widget.arguments.name.toString(),
          style: gcTitleTextStyle,
        ),
      ),
      body: isLoadingProduct && isSearchOn == false
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: gcPlaceholderTextStyle,
                ))
              : itemList.length == 0 &&
                      isSearchOn == false &&
                      isLoadingProduct == false
                  ? Center(
                      child: Text(
                        "No items found",
                        style: gcPlaceholderTextStyle,
                      ),
                    )
                  : createListView(itemList, context),
    );
  }

  Widget createListView(List<Products> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Container(
            height: 80,
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: fsb.SearchBar<Products>(
              minimumChars: 0,
              searchBarPadding: EdgeInsets.symmetric(horizontal: 8),
              headerPadding: EdgeInsets.symmetric(horizontal: 8),
              listPadding: EdgeInsets.symmetric(horizontal: 8),
              onSearch: _getALlPosts,
              hintText: 'Search',
              textStyle: GoogleFonts.poppins(color: themeWhiteColor),
              searchBarController: _searchBarController,
              iconActiveColor: themeWhiteColor,

              // placeHolder: Text("placeholder"),
              cancellationWidget: Text(
                "Cancel",
                style: GoogleFonts.poppins(color: themeWhiteColor),
              ),
              //emptyWidget: Text("empty"),
              indexedScaledTileBuilder: (int index) =>
                  ScaledTile.count(1, index.isEven ? 2 : 1),
              onCancelled: () {
                print("Cancelled triggered");
                getAllProducts();
                FocusScope.of(context).unfocus();
              },
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              crossAxisCount: 2,
              onItemFound: (Products? post, int index) {
                return Container(
                  color: Colors.lightBlue,
                  child: ListTile(
                    title: Text(post?.name.toString() ?? ' '),
                    isThreeLine: true,
                    subtitle: Text(post?.price.toString() ?? ' '),
                    onTap: () {},
                  ),
                );
              },
            ),
          ),
        ),
        if (itemList.length == 0)
          Center(
            child: Text(
              "No items found",
              style: gcPlaceholderTextStyle,
            ),
          ),
        Expanded(
          child: NotificationListener(
            onNotification: (ScrollNotification scrollInfo) {
              if (scrollInfo.metrics.pixels ==
                  scrollInfo.metrics.maxScrollExtent) {
                if (!isLoadingMore && noticeJson?.data?.next != null) {
                  // start loading data
                  print("Scrolling to end...");
                  setState(() {
                    isLoadingMore = true;
                  });
                  _loadMoreData();
                } else if (noticeJson?.data?.next == null) {
                  print("Data end");
                  setState(() {
                    isLoadingMore = false;
                  });
                } else {}
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: _reloadData,
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int position) {
                        return IntrinsicHeight(
                          child: GestureDetector(
                            onTap: () {},
                            child: Padding(
                              padding: const EdgeInsets.all(8),
                              child: Container(
                                // height: 190,
                                decoration: BoxDecoration(
                                  color: themeBlackColor.withOpacity(0.5),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                itemList[position].name!,
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                style: gcListTitleTextStyle,
                                              ),

                                              Text(
                                                itemList[position].name!,
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                // maxLines: 2,
                                                style: gcPlaceholderTextStyle,
                                              ),

// --------------------------product price and discount percentage------------------
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 0, left: 0, top: 0),
                                                child: Container(
                                                  child: Text(
                                                    "Tk " +
                                                        itemList[position]
                                                            .price
                                                            .toString(), //+ "  (${itemList[position].unit.toString() })",
                                                    textAlign: TextAlign.left,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 1,
                                                    style:
                                                        gcPaymentDescriptionTextStyle,
                                                  ),
                                                ),
                                              ),
                                              if (itemList[position].remarks !=
                                                  null)
                                                Text(
                                                  itemList[position]
                                                      .remarks
                                                      .toString(),
                                                  textAlign: TextAlign.left,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  style:
                                                      gcBkashPlaceholderTextStyle,
                                                ),
                                            ],
                                          ),
                                        ),
                                        IconButton(
                                          color: themeWhiteColor,
                                          icon: Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          12)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: SvgPicture.asset(
                                                  'assets/icons/Add.svg',
                                                  height: 16,
                                                  width: 16,
                                                ),
                                              )),
                                          onPressed: isLoading
                                              ? null
                                              : () {
                                                  print(
                                                      "clicked ${itemList[position].id}");
//                          setState(() {
//                            productItemCounter =
//                                productItemCounter + 1;
//                          });
                                                  showModalBottomSheet(
                                                      context: context,
                                                      builder:
                                                          (BuildContext bc) {
                                                        return Container(
                                                            height: 200,
                                                            child: BottomModal(
                                                              data: itemList[
                                                                      position]
                                                                  .id!,
                                                              preRoute:
                                                                  "product",
                                                              quantityCount: 1,
                                                            ));
                                                      }).then((value) {
                                                    print("value");
                                                    print(value);
                                                    setState(() {
                                                      getCartItems();
                                                    });
                                                  });
                                                },
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 12),
                                    Divider(
                                      color: themeGreyColor,
                                      thickness: 1,
                                      height: 1,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                      childCount: results.length, // 1000 list items
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: isLoadingMore ? 50.0 : 0,
                            color: Colors.transparent,
                            child: Center(
                              child: new CircularProgressIndicator(),
                            ),
                          ),
                        ],
                      ),
//              ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
