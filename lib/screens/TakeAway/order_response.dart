// To parse this JSON data, do
//
//     final orderResponse = orderResponseFromMap(jsonString?);

import 'dart:convert';

OrderResponse orderResponseFromMap(String str) => OrderResponse.fromMap(json.decode(str));

String orderResponseToMap(OrderResponse data) => json.encode(data.toMap());

class OrderResponse {
  OrderResponse({
    this.status,
    this.data,
  });

  bool? status;
  Data? data;

  factory OrderResponse.fromMap(Map<String, dynamic> json) => OrderResponse(
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromMap(json["data"]),
  );

  Map<String, dynamic> toMap() => {
    "status": status == null ? null : status,
    "data": data == null ? null : data!.toMap(),
  };
}

class Data {
  Data({
    this.id,
    this.amount,
    this.status,
    this.address,
    this.updated,
    this.created,
  });

  int? id;
  double? amount;
  String? status;
  Address? address;
  // String? address;
  DateTime? updated;
  DateTime? created;

  factory Data.fromMap(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    amount: json["amount"] == null ? null : json["amount"],
    status: json["status"] == null ? null : json["status"],
    address: json["address"] == null ? null : Address.fromMap(json["address"]),
    // address:json["address"] == null ? null : json["address"],
    updated: json["updated"] == null ? null : DateTime.parse(json["updated"]),
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
  );

  Map<String, dynamic> toMap() => {
    "id": id == null ? null : id,
    "amount": amount == null ? null : amount,
    "status": status == null ? null : status,
    "address": address == null ? null : address!.toMap(),
    // "address": address == null ? null : address ,
    "updated": updated == null ? null : updated!.toIso8601String(),
    "created": created == null ? null : created!.toIso8601String(),
  };
}

class Address {
  Address({
    this.address1,
    this.address2,
    this.phone,
  });

  String? address1;
  String? address2;
  String? phone;

  factory Address.fromMap(Map<String, dynamic> json) => Address(
    address1: json["address1"] == null ? null : json["address1"],
    address2: json["address2"] == null ? null : json["address2"],
    phone: json["phone"] == null ? null : json["phone"],
  );

  Map<String, dynamic> toMap() => {
    "address1": address1 == null ? null : address1,
    "address2": address2 == null ? null : address2,
    "phone": phone == null ? null : phone,
  };
}
