import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';

class WCAuthField extends StatelessWidget {
  final TextEditingController controller;
  final double? width;
  final EdgeInsets? padding;
  final VoidCallback? onComplete;
  final FocusNode focusNode;
  final TextInputAction? inputAction;
  final IconData? icon;
  final String? hint;
  final bool? obscure;
  final int? maxLength;
  final TextInputType? type;
  final FormFieldValidator? validator;
  final FormFieldSetter? onSaved;
  final bool rounded ;

  WCAuthField(
      {required this.controller,
      this.width,
      this.padding,
      this.onComplete,
      required this.focusNode,
      this.inputAction,
      this.icon,
      this.hint,
      this.obscure,
      this.maxLength,
      this.type,
      this.validator,
      this.onSaved,
      this.rounded = true});

  final InputBorder inputBorder = UnderlineInputBorder(
     borderSide: BorderSide(color: themeGolderColor, width: 1),
  );

  final InputBorder inputBorderEdge = UnderlineInputBorder(
     borderSide: BorderSide(color: themeGolderColor, width: 1),
  );

  final InputBorder errorBorderEdge = OutlineInputBorder(
    borderSide: BorderSide(color:themeGolderColor , width: 1),
  );

  final TextStyle fieldStyle = gcNormalTextStyle;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      padding: padding,
      decoration: !rounded
          ? BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  color: Colors.grey,
                  blurRadius: 0.8,
                  offset: Offset(0.5, 1.0),
                  spreadRadius: 1)
            ])
          : null,
      child: TextFormField(
        controller: controller,
        onEditingComplete: onComplete,
        focusNode: focusNode,
        textInputAction: inputAction,
        decoration: InputDecoration(
          focusColor: themeGolderColor,
          prefixIcon: Padding(
            padding: EdgeInsets.only(
              left: 8,top: 0
            ),
            child: Icon(icon, color: themeGolderColor, size: 25),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: themeGolderColor, width: 1),
          ),//;rounded ? inputBorder : inputBorderEdge,
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: themeGolderColor, width: 1),
          ),//rounded ? inputBorder : inputBorderEdge,
          hintText: hint,
          hintStyle: fieldStyle,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: themeGolderColor, width: 1),
          ),//rounded ? inputBorder : inputBorderEdge,
          counterText: '',
//          contentPadding: EdgeInsets.symmetric(
//              vertical: 16, horizontal: 5
//          ),
          /*errorStyle: TextStyle(
            color: Colors.red,
            fontSize: 10.0,
          ),*/
          //errorBorder: rounded ? null : errorBorderEdge
        ),
        style: fieldStyle.copyWith(color: Colors.white),
        cursorColor: themeGolderColor,
        cursorWidth: 3,
        cursorRadius: Radius.circular(2),
        obscureText: obscure ?? false,
        maxLength: maxLength,
        keyboardType: type,
        validator: validator,
        onSaved: onSaved,
      ),
    );
  }
}
