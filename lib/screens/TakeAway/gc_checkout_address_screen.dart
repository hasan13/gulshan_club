import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/gc_otp_screen.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'auth_field.dart';
import 'card_data_response.dart';
import 'dialogs.dart';
import 'order_response.dart';

class CheckoutRoute extends StatefulWidget {
  String preRouteCart;
  List<CardItem> cartItems;
  double totalAmount;

  CheckoutRoute({required this.preRouteCart, required this.cartItems, required this.totalAmount});
  @override
  _CheckoutRouteState createState() => _CheckoutRouteState();
}

class _CheckoutRouteState extends State<CheckoutRoute> {


  final GlobalKey<FormState> _globalKey = new GlobalKey<FormState>();
  final _scaffoldKeySign = GlobalKey<ScaffoldMessengerState>();

  final TextEditingController _addressController = new TextEditingController();
  final FocusNode _addressNode = FocusNode();

  final TextEditingController _additionalAddressController =
      new TextEditingController();
  final FocusNode _additionalAddressNode = FocusNode();

  bool isLoading = false;
  bool isLoadingData = false;

  List<dynamic> _cartList = [];
  AppUser? userInfo;

  OrderResponse? orderResponse;

  var _site = 1;

  @override
  void initState() {
    super.initState();
    getCurrentUserInfo();
  }

  getCurrentUserInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  userInfo = userResponse.data;
                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  isLoading = false;
                  _addressController.text = userInfo?.address.toString() ?? '';
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  performCheckout() async {
    if (_globalKey.currentState!.validate()) {
      var productItem;
      if (widget.preRouteCart == null) {
      } else {
        for (int i = 0; i < widget.cartItems.length; i++) {
          _cartList.add({
            "id": widget.cartItems[i].product?.id,
            "quantity": widget.cartItems[i].productQuantity,
          });
        }
        print(productItem);
      }

      var params;
      if (widget.preRouteCart == null) {
      } else {
        if (_site == 3) {
          params = {
            "products": _cartList,
            "address": {
              "phone": userInfo?.phonePrimary.toString()
            },
            "type":  "pickup"
          };
        } else {
          params = {
            "products": _cartList,
            "address": {
              "address1": (_site == 1)
                  ? _addressController.text.toString()
                  : _additionalAddressController.text.toString(),
              "phone": userInfo?.phonePrimary.toString()
            },
            "type":   "delivery"
          };
        }
      }
      print("params----$params");
      print("${Resources.currentUser?.accessToken}");

      bool result = await InternetConnectionChecker().hasConnection;
      var url = NetworkManager.baseUrl +
          "/api/v1/order"; //http://58.84.34.65:9090/api/v1/order
      print("url----$url");

      if (result == true) {
        http
            .post(Uri.parse(url), body: jsonEncode(params), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
              "Content-Type": "application/json"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);

              dynamic data = json.decode(value.body);

              if (value.statusCode == 200 && data["status"] == true) {
                //Clearing cart when order is placed
                final data = json.decode(utf8.decode(value.bodyBytes));
                var orderResponsee =
                    OrderResponse.fromMap(data); //noticeResponseFromJson(data);
                print('result....\n ${orderResponsee.data?.id}');

                setState(() {
                  isLoading = false;
                  orderResponse = orderResponsee;
                });

                if(orderResponse != null){
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        fullscreenDialog: true,
                        builder: (context) =>
                            OTPScreen(preRoute: "checkout", data: orderResponse!),
                      ));

                }
              } else {
                setState(() {
                  isLoading = false;
                });

                print(data["data"]);
                Fluttertoast.showToast(
                    msg: data["data"].toString(),
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.CENTER,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
              }
            });
      }
    } else {
      setState(() {
        isLoading = false;
      });

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text("No internet connection"),
      );

      _scaffoldKeySign.currentState?.showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKeySign, resizeToAvoidBottomInset: false, //new line
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: themeBlackColor,
        title: Text(
          "Checkout",
          style: gcTitleTextStyle,
        ),
      ),
      body: isLoadingData
          ? Center(
              child: CircularProgressIndicator(),
            )
          : GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SafeArea(
                // child: SingleChildScrollView(
                child: Container(
                  constraints: BoxConstraints(
                      maxHeight: MediaQuery.of(context).size.height),
                  child: Theme(
                    data: ThemeData(unselectedWidgetColor: Colors.white),
                    child: Form(
                      key: _globalKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 16.0, right: 16.0),
                            child: Row(
                              children: [
                                Radio(
                                  activeColor: themeGolderColor,
                                  value: 1,
                                  groupValue: _site,
                                  onChanged: (int? value) {
                                    setState(() {
                                      _site = value ?? 1;
                                    });
                                  },
                                ),
                                Text(
                                  "Delivery Address",
                                  style: gcPaymentListTitleTextStyle,
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 16.0, right: 16),
                            child: IntrinsicHeight(
                              child: TextField(
                                controller: _addressController,
                                //enabled: _site == 1 ? true : false,
                                enabled: false,
                                focusNode: _addressNode,
                                keyboardType: TextInputType.text,
                                style: gcNormalTextStyle,
                                maxLines: 3,
                                decoration: InputDecoration(
                                  isDense: true,
                                  hintText: "no default address added",
                                  hintStyle: gcNormalTextStyle,
                                  border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                      color: themeGolderColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: themeGolderColor),
                                    //  when the TextFormField in unfocused
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            themeGolderColor), //HexColor("#FF7300"
                                    //  when the TextFormField in focused
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 16.0, right: 16),
                            child: Row(
                              children: [
                                Radio(
                                  activeColor: themeGolderColor,
                                  value: 2,
                                  groupValue: _site,
                                  onChanged: (int? value) {
                                    setState(() {
                                      _site = value  ?? 2;
                                    });
                                  },
                                ),
                                Text(
                                  "Additional Address",
                                  style: gcPaymentListTitleTextStyle,
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 16.0, right: 16),
                            child: IntrinsicHeight(
                              child: TextField(
                                controller: _additionalAddressController,
                                focusNode: _additionalAddressNode,
                                enabled: _site == 2 ? true : false,
                                keyboardType: TextInputType.text,
                                style: gcNormalTextStyle,
                                maxLines: 4,
                                decoration: InputDecoration(
                                  isDense: true,
                                  hintText: "type address here ",
                                  hintStyle: gcNormalTextStyle,
                                  border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                      color: themeGolderColor,
                                    ),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: themeGolderColor),
                                    //  when the TextFormField in unfocused
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color:
                                            themeGolderColor), //HexColor("#FF7300"
                                    //  when the TextFormField in focused
                                  ),
                                ),
                              ),
                            ),
                          ),

                          Padding(
                            padding:
                                const EdgeInsets.only(left: 16.0, right: 16.0),
                            child: Row(
                              children: [
                                Radio(
                                  activeColor: themeGolderColor,
                                  value: 3,
                                  groupValue: _site,
                                  onChanged: (int? value) {
                                    setState(() {
                                      _site = value ?? 3;
                                    });
                                  },
                                ),
                                Text(
                                  "Pick Up",
                                  style: gcPaymentListTitleTextStyle,
                                ),
                              ],
                            ),
                          ),

                          // Spacer(),
                          Expanded(
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: ClipRRect(
                                child: Container(
                                  height: 100,
                                  decoration: BoxDecoration(
                                    color: themeGolderColor.withOpacity(0.4),
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(16.0),
                                      topLeft: Radius.circular(16.0),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 8.0, left: 8.0, top: 8.0),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.all(0.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Total:",
                                                style: gcTitleTextStyle,
                                              ),
                                              Text(
                                                "Tk ${widget.totalAmount.toStringAsFixed(2)} BDT",
                                                style: gcTitleTextStyle,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: isLoading == true
                                              ? CircularProgressIndicator(
                                            backgroundColor: themeGolderColor,
                                                )
                                              : MyCustomButton(
                                                  width: 200,
                                                  height: 30,
                                                  onPressed: () {
                                                    setState(() {
                                                      isLoading = true;
                                                    });
                                                    performCheckout();
                                                  },
                                                  color: themeGolderColor,
                                                  child: Text(
                                                    "CONFIRM",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              // ),
            ),
    );
  }
}
