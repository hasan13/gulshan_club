// To parse this JSON data, do
//
//     final orderListResponse = orderListResponseFromJson(jsonString?);

import 'dart:convert';
// To parse this JSON data, do
//
//     final orderListResponse = orderListResponseFromJson(jsonString?);

OrderListResponse orderListResponseFromJson(String str) =>
    OrderListResponse.fromJson(json.decode(str));

String orderListResponseToJson(OrderListResponse data) =>
    json.encode(data.toJson());

class OrderListResponse {
  OrderListResponse({
    this.status,
    this.data,
  });

  bool? status;
  List<OrdersData>? data;

  factory OrderListResponse.fromJson(Map<String, dynamic> json) =>
      OrderListResponse(
        status: json["status"] == null ? null : json["status"],
        data: json["data"] == null
            ? null
            : List<OrdersData>.from(
                json["data"].map((x) => OrdersData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "data": data == null
            ? null
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class OrdersData {
  OrdersData({
    this.id,
    this.amount,
    this.status,
    this.address,
    this.updated,
    this.created,
  });

  int? id;
  double? amount;
  String? status;
  Address? address;
  DateTime? updated;
  DateTime? created;

  factory OrdersData.fromJson(Map<String, dynamic> json) => OrdersData(
        id: json["id"] == null ? null : json["id"],
        amount: json["amount"] == null ? null : json["amount"],
        status: json["status"] == null ? null : json["status"],
        address:
            json["address"] == null ? null : Address.fromJson(json["address"]),
        updated:
            json["updated"] == null ? null : DateTime.parse(json["updated"]),
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "amount": amount == null ? null : amount,
        "status": status == null ? null : status,
        "address": address == null ? null : address?.toJson(),
        "updated": updated == null ? null : updated?.toIso8601String(),
        "created": created == null ? null : created?.toIso8601String(),
      };
}

class Address {
  Address({
    this.address1,
    this.address2,
    this.phone,
  });

  String? address1;
  String? address2;
  String? phone;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        address1: json["address1"] == null ? null : json["address1"],
        address2: json["address2"] == null ? null : json["address2"],
        phone: json["phone"] == null ? null : json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "address1": address1 == null ? null : address1,
        "address2": address2 == null ? null : address2,
        "phone": phone == null ? null : phone,
      };
}
