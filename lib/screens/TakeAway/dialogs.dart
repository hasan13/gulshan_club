import 'dart:io';

import 'package:flutter/material.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';

class WCDialog {
  static final TextStyle _titleStyle = TextStyle(
      color: Colors.black,// AppColors.primaryText,
      fontWeight: FontWeight.bold,
      fontSize: 17,
      shadows: [
        BoxShadow(
            color: Colors.black26, blurRadius: 1, offset: Offset(1.0, 1.0))
      ]);

  static Future showPasswordInputDialog(BuildContext context,
      {VoidCallback? onConfirm,
        TextEditingController? controller,
        GlobalKey<FormState>? formKey}) {
    TextEditingController passwordController =
        controller ?? TextEditingController();
    GlobalKey<FormState> globalKey = formKey ?? GlobalKey<FormState>();

    return showDialog(
      barrierDismissible: false,
      context: context,
      builder:(BuildContext context) {
        return AlertDialog(
          title: Text(
            "Password",
            textAlign: TextAlign.center,
            style: WCDialog._titleStyle,
          ),
          content: Container(
              child: Form(
                key: globalKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      controller: passwordController,
                      // validator: (value) =>
                      //     WCValidators.validatePassword(value, context),
                      obscureText: true,
                      decoration: InputDecoration(
                          labelText: "Password",
                          hintText: "Password"),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(themeGolderColor),
                          ),
                          child: Text("Confirm",
                              style: TextStyle(color: Colors.white, fontSize: 17)),
                          onPressed: () {
                            if (globalKey.currentState!.validate()) {
                              onConfirm!();
                            }
                          },
                        ))
                  ],
                ),
              )),
        );
      },);
  }


//  static Future showCardDialog(BuildContext context, CardModel card) {
//    final TextStyle titleItemsStyle = TextStyle(fontWeight: FontWeight.w700);
//
//    return showDialog(
//        context: context,
//        child: AlertDialog(
//            title: Text(
//              S.of(context).cardInformation,
//              style: WCDialog._titleStyle,
//              textAlign: TextAlign.center,
//            ),
//            content: Container(
//              //height: 100,
//              child: Column(
//                mainAxisSize: MainAxisSize.min,
//                crossAxisAlignment: CrossAxisAlignment.center,
//                children: <Widget>[
//                  Row(
//                    children: <Widget>[
//                      Expanded(
//                        child: Text("Nº:", style: titleItemsStyle),
//                      ),
//                      Expanded(
//                        child: Text(
//                          '**** **** **** ${card.lastDigits}',
//                          textAlign: TextAlign.end,
//                        ),
//                      )
//                    ],
//                  ),
//                  Row(
//                    children: <Widget>[
//                      Expanded(
//                        child: Text(
//                          "${S.of(context).expirateDateLabel}:",
//                          style: titleItemsStyle,
//                        ),
//                      ),
//                      Expanded(
//                        child: Text(
//                          '**/${card.expirateYear}',
//                          textAlign: TextAlign.end,
//                        ),
//                      )
//                    ],
//                  ),
//                  Row(
//                    children: <Widget>[
//                      Expanded(
//                        child: Text(
//                          "${S.of(context).typeLabel}:",
//                          style: titleItemsStyle,
//                        ),
//                      ),
//                      Expanded(
//                        child: Text(
//                          '${card.type.toUpperCase()}',
//                          textAlign: TextAlign.end,
//                        ),
//                      )
//                    ],
//                  ),
//                ],
//              ),
//            )));
//  }

  static Future showContentDialog(BuildContext context,
      {required String message,
        required VoidCallback onTap,
        bool actions = true,
        bool dismissible = false}) {
    return WCDialog.showWidgetDialog(context,title: message,
        dismissible: dismissible,
        onTap: onTap ,
        actions: actions,
        widget: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            // Text(
            //   message,
            //   textAlign: TextAlign.center,
            // )
          ],
        ));
  }

  static Future showWidgetDialog(BuildContext context,
      {Widget? widget,
        VoidCallback? onTap,
        String? title,
        bool actions = true,
        bool dismissible = false}) {
    return showDialog(
      context: context,
      barrierDismissible: dismissible,
      builder: (BuildContext context){
        return Center(
          child:AlertDialog(
              title: Text(
                title ?? "Failed!",
                style: _titleStyle,
                textAlign: TextAlign.center,
              ),
              actions: (actions)
                  ? <Widget>[
                TextButton(
                    child: Text("Ok",style: TextStyle(color: Colors.white),),
                    style: ButtonStyle(
                      elevation: MaterialStateProperty.all(0),
                      backgroundColor: MaterialStateProperty.all(themeGolderColor),
                    ),
                    // color: themeGolderColor,
                    // textColor: Colors.white,
                    onPressed:
                        (){
                      //onTap ??
                          Navigator.pop(context);
                          Navigator.pop(context);
                    }
                    //onTap ==?? () => Navigator.pop(context)
        )
              ]
                  : null,
              content: widget),
        );
      },
    );
  }


  static Future showContentDialogForCheckout(BuildContext context,
      {required String message,
        required VoidCallback onTap,
        bool actions = true,
        bool dismissible = false}) {
    return WCDialog.showWidgetDialogForCheckout(context,title: message,
        dismissible: dismissible,
        onTap: onTap ,
        actions: actions,
        widget: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            // Text(
            //   message,
            //   textAlign: TextAlign.center,
            // )
          ],
        ));
  }

  static Future showWidgetDialogForCheckout(BuildContext context,
      {Widget? widget,
        VoidCallback? onTap,
        String? title,
        bool actions = true,
        bool dismissible = false}) {
    return showDialog(
      context: context,
      barrierDismissible: dismissible,
      builder: (BuildContext context){
        return Center(
          child:AlertDialog(
              title: Text(
                title ?? "Failed!",
                style: _titleStyle,
                textAlign: TextAlign.center,
              ),
              actions: (actions)
                  ? <Widget>[
                TextButton(
                    child: Text("Ok",style: TextStyle(color: Colors.white),),
                    style: ButtonStyle(
                      elevation: MaterialStateProperty.all(0),
                      backgroundColor: MaterialStateProperty.all(themeGolderColor),
                    ),
                    // color: themeGolderColor,
                    // textColor: Colors.white,
                    onPressed:
                        (){
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/take_away', (route) => false);
                    }
                  //onTap ==?? () => Navigator.pop(context)
                )
              ]
                  : null,
              content: widget),
        );
      },
    );
  }

//  static Future showImagePickerBottomSheet(
//    BuildContext context, {
//    Function onSelected,
//  }) {
//    return showModalBottomSheet(
//        context: context,
//        builder: (_) {
//          return Container(
//            padding: EdgeInsets.symmetric(horizontal: 100, vertical: 20),
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              mainAxisSize: MainAxisSize.min,
//              children: <Widget>[
//                Text(
//                  "CHOOSE",
//                  style: WCDialog._titleStyle,
//                ),
//                ListTile(
//                  title: Text(
//                    "Camera",
//                    style: TextStyle(fontSize: 17, letterSpacing: 1.5),
//                  ),
//                  trailing: Icon(
//                    Icons.camera_alt,
//                    color: AppColors.opaqueBlue,
//                  ),
//                  dense: true,
//                  onTap: () async {
//                    File file =
//                        await ImagePicker.pickImage(source: ImageSource.camera);
//                    if (file != null) {
//                      onSelected(file);
//                    }
//                    Navigator.pop(context);
//                  },
//                ),
//                ListTile(
//                  title: Text(
//                    S.of(context).gallery,
//                    style: TextStyle(fontSize: 17, letterSpacing: 1.5),
//                  ),
//                  trailing: Icon(Icons.image, color: WCColors.opaqueBlue),
//                  dense: true,
//                  onTap: () async {
//                    File file = await ImagePicker.pickImage(
//                        source: ImageSource.gallery);
//                    if (file != null) {
//                      onSelected(file);
//                    }
//                    Navigator.pop(context);
//                  },
//                ),
//              ],
//            ),
//          );
//        });
//  }
}
