import 'dart:convert';

 import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/get_otp_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

class MobileNumberScreen extends StatefulWidget {
  static const String route = '/mobileNo';

  @override
  _MobileNumberScreenState createState() => _MobileNumberScreenState();
}

class _MobileNumberScreenState extends State<MobileNumberScreen> {
  final TextEditingController _userIdCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    _userIdCtrl.text = Resources.mobileNo.toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(children: [
          Image.asset(
            "assets/images/Login Background.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0, left: 16),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                      height: 30,
                      width: 30,
                      child: IconButton(
                          icon: SvgPicture.asset('assets/icons/ic_back.svg'),
                        onPressed: () {
                          Navigator.pop(context);

                        },))),
            ),
          ),
          Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Center(
                child: Container(
                    height: 180,
                    width: 220,
                    child: IconButton(
                        icon: SvgPicture.asset('assets/icons/ic_logo.svg'), onPressed: () {
                      Navigator.pop(context);

                    },)),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                // width: MediaQuery.of(context).size.width * 0.65,
                width: MediaQuery.of(context).size.width * 0.65,
                decoration: BoxDecoration(
                    color: themeWhiteColor.withOpacity(0.3),
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                      // topLeft: Radius.circular(8.0),
                      // topRight: Radius.circular(8.0),
                      // bottomLeft: Radius.circular(8.0),
                      // bottomRight: Radius.circular(8.0),
                    )),
                child: TextFormField(
                  controller: _userIdCtrl,
                  readOnly: true,
                  // initialValue: Resources.mobileNo,
                  focusNode: _userIdNode,
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: false, signed: false),
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    border: new UnderlineInputBorder(
                      borderSide: new BorderSide(
                          //color: themeGolderColor,
                          ),
                    ),
                    enabledBorder: UnderlineInputBorder(
                        //borderSide: BorderSide(color: themeGolderColor),
                        //  when the TextFormField in unfocused
                        ),
                    focusedBorder: UnderlineInputBorder(
                        //borderSide: BorderSide(color: themeGolderColor), //HexColor("#FF7300"
                        //  when the TextFormField in focused
                        ),
                    hintText: 'Mobile Number',
                    hintStyle: gcPlaceholderTextStyle,
                  ),
                  onFieldSubmitted: (value) {},
                  style: TextStyle(
                      fontFamily: 'Siri-Regular',
                      fontSize: kMedium,
                      color: Colors.black),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              (isLoading == true)?CircularProgressIndicator()  : MyCustomButton(
                height: 30,
                // width: 200,
                width: MediaQuery.of(context).size.width * 0.65,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                    child: Text("Next")
                  // SvgPicture.asset(
                  //   'assets/icons/ic_login.svg',
                  //   height: 30,
                  // ),
                ),
                onPressed: () {
                  print('tapped');
                  setState(() {
                    isLoading = true;
                  });
                  getOTPByMobileNumber();
                },
              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
        ]),
      ),
    );
  }

  getOTPByMobileNumber() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/${_userIdCtrl.text.toString()}/get_opt/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final noticeResponse =
                  GetOtpResponse.fromJson(data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  isLoading = false;
                });
                Navigator.pushNamed(context, '/otp');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                // ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                // ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');


      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }
}
