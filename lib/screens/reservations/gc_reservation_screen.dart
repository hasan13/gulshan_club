import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
 import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/jsonModels/club_facilities_response.dart';
import 'package:gulshan_club_app/jsonModels/generic_api_response.dart';
import 'package:gulshan_club_app/jsonModels/reservation_list_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class ReservationScreen extends StatefulWidget {
  static const String route = '/reservation';

  @override
  _ReservationScreenState createState() => _ReservationScreenState();
}

class _ReservationScreenState extends State<ReservationScreen>
    with TickerProviderStateMixin {
  List<String> itemList = ["dsd"];
  List<Facility> allFacilityList = [];
  List<String> namesList = [];
  var isLoading = false;
  var isLoadingCreation = false;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  ClubFacilitiesResponse? clubFacilityResponse;
  bool isLoadingMore = false;

  List<Reservation> allReservationList = [];
  ReservationListResponse? reservationsListResponse;

  late TabController _tabController;
  late TabController _reservationTabController;
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  // ScrollController _scrollController = ScrollController();
  DateTime _chosenDateTime1 = DateTime.now();

  int _reservationTabIndex = 0;
  int _tabControllerIndex = 0;

  String? _selectedName;
  Facility? _selectedFacility;
  String startDate = "";

  final List<Tab> tabs = <Tab>[
    Tab(
      child: Text('New Reservation'),
    ),
    Tab(
      child: Text('Reservation History'),
    ),
  ];

  final List<Tab> myTabs = <Tab>[
    Tab(
      child: Text('All'),
    ),
    Tab(
      child: Text('Pending'),
    ),
    Tab(
      child: Text('Confirmed'),
    ),
    Tab(
      child: Text('Canceled'),
    ),
  ];

  @override
  void initState() {
    //startDate = DateFormat("yyyy-MM-dd HH:MM").format(_chosenDateTime1);
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
    _reservationTabController =
        TabController(length: myTabs.length, vsync: this);
    _reservationTabController.addListener(_setActiveTabIndex);
    _tabController.addListener(_setActiveTab);
    getClubFacilities();
  }

  void _setActiveTab() {
    _tabControllerIndex = _tabController.index;
    print("_tabControllerIndex $_tabControllerIndex");
    _resetData();
    if (_tabControllerIndex == 1) {
      getAllReservation("all");
    }
  }

  void _setActiveTabIndex() {
    _reservationTabIndex = _reservationTabController.index;
    print("_activeTabIndex $_reservationTabIndex");

    if (_reservationTabController.index == 0) {
      getAllReservation("all");
    } else if (_reservationTabController.index == 1) {
      getAllReservation("pending");
    } else if (_reservationTabController.index == 2) {
      getAllReservation("confirmed");
    } else if (_reservationTabController.index == 3) {
      getAllReservation("canceled");
    } else {
      print(_reservationTabController.index);
    }
  }

  void _resetData() {
    setState(() {
      startDate = "";
      _selectedName = null;
      _selectedFacility = null;
    });
  }

  getClubFacilities() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/club_facility_detail_details/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
        })
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(utf8.decode(value.bodyBytes));
          final clubResponse = ClubFacilitiesResponse.fromJson(
              data); //noticeResponseFromJson(data);
          print('result....\n ${clubResponse.results}');

          if (value.statusCode == 401) {
            Navigator.pushNamedAndRemoveUntil(
                context, '/welcome', (route) => false);
            return;
          }
          if (value.statusCode == 200) {
            setState(() {
              if( clubResponse.results != null){
                allFacilityList = clubResponse.results!;
                clubFacilityResponse = clubResponse;
                isLoading = false;

                for (var i = 0; i < allFacilityList.length; i++) {
                  namesList.add(allFacilityList[i].name.toString());
                }
              }

            });
          } else if (data['status'] == false) {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  getAllReservation(String filterString) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/reservation/$filterString/get-reservation/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
        })
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(utf8.decode(value.bodyBytes));
          final clubResponse = ReservationListResponse.fromJson(
              data); //noticeResponseFromJson(data);
          print('result....\n ${clubResponse.data?.results}');

          if (value.statusCode == 401) {
            Navigator.pushNamedAndRemoveUntil(
                context, '/welcome', (route) => false);
            return;
          }
          if (value.statusCode == 200) {
            setState(() {
              allReservationList = clubResponse.data?.results ?? [];
              reservationsListResponse = clubResponse;
              isLoading = false;
            });
          } else if (data['status'] == false) {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(
              msg:
              'Error-${value.statusCode}, ${NetworkManager.errorMessage}',
              backgroundColor: Colors.red,
              gravity: ToastGravity.CENTER,
            );
          } else {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(
              msg:
              'Error-${value.statusCode}, ${NetworkManager.errorMessage}',
              backgroundColor: Colors.red,
              gravity: ToastGravity.CENTER,
            );
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(
        msg: NetworkManager.internetErrorMessage,
        backgroundColor: Colors.red,
        gravity: ToastGravity.CENTER,
      );
    }
  }

  _loadMoreMember() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint = reservationsListResponse?.data?.next ?? ''; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreReservationResponse = ReservationListResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        List<Reservation> moreData = moreReservationResponse.data?.results ?? [];
        allReservationList.addAll(moreData);
        isLoading = false;
        reservationsListResponse = moreReservationResponse;
        // List<Member> moreData = moreNoticeResponse.results;
        // itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  createReservation() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/reservation/create-reservation/";
    print('url---> $url');

    Map params = {
      "facility_id": _selectedFacility?.id.toString(),
      "observation_date": startDate
    };
    print(params);
    if (result == true) {
      try {
        http
            .post(Uri.parse(url), body: jsonEncode(params), headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
        })
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(utf8.decode(value.bodyBytes));
          final noticeResponse = GenericResponse.fromJson(
              data); //noticeResponseFromJson(data);
          print('result....\n ${noticeResponse.message}');

          if (value.statusCode == 401) {
            SharedPref.remove('user');

            Navigator.pushNamedAndRemoveUntil(
                context, '/welcome', (route) => false);
            return;
          }
          if (value.statusCode == 200) {
            setState(() {
              isLoadingCreation = false;
              Fluttertoast.showToast(
                  msg: noticeResponse.message!,
                  backgroundColor: themeGolderColor,
                  gravity: ToastGravity.BOTTOM,
                  toastLength: Toast.LENGTH_LONG);

              _resetData();
              // _tabController.animateTo(1);
            });
          } else {
            setState(() {
              isLoadingCreation = false;
            });
            // final snackBar = SnackBar(
            //   backgroundColor: Colors.red,
            //   content: Text(
            //       'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            // );
            // _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
            Fluttertoast.showToast(
              msg:
              'Error-${value.statusCode}, ${NetworkManager.errorMessage}',
              backgroundColor: Colors.red,
              gravity: ToastGravity.CENTER,
            );
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoadingCreation = false;
        });
        Fluttertoast.showToast(
          msg: 'Error-${e.toString()} }',
          backgroundColor: Colors.red,
          gravity: ToastGravity.CENTER,
        );
      }
    } else {
      print('No internet :( Reason:');
      Fluttertoast.showToast(
        msg: ' ${NetworkManager.internetErrorMessage}',
        backgroundColor: Colors.red,
        gravity: ToastGravity.CENTER,
      );
      //
      // final snackBar = SnackBar(
      //   backgroundColor: Colors.red,
      //   content: Text('Internet is not available!'),
      // );
      // setState(() {
      //   isLoading = false;
      // });
      // // Find the Scaffold in the widget tree and use
      // // it to show a SnackBar.
      // _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  Future<void> _reloadData() async {
    // setState(() {
    //   // getAllReservation("all");
    // });
    print("_activeTabIndex $_reservationTabIndex");

    if (_reservationTabController.index == 0) {
      getAllReservation("all");
    } else if (_reservationTabController.index == 1) {
      getAllReservation("pending");
    } else if (_reservationTabController.index == 2) {
      getAllReservation("confirmed");
    } else if (_reservationTabController.index == 3) {
      getAllReservation("canceled");
    } else {
      print(_reservationTabController.index);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
        pageTitle: 'Reservations',
        bottomNavigationBar: Resources.setBottomDrawer(context),

        body: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : itemList == null
            ? Center(
            child: Text(
              "Something went wrong",
              style: TextStyle(),
            ))
            : itemList.length == 0
            ? Center(
          child: Text(
            "No Reservations found",
            style: gcPlaceholderTextStyle,
          ),
        )
            : createListView(itemList, context));
  }

  createListView(List<String> itemList, BuildContext context) {
    return Scaffold(
        backgroundColor: themeBlackColor,
        appBar: AppBar(
          backgroundColor: themeBlackColor,
          bottom: PreferredSize(
              child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.white,
                  indicatorColor: themeGolderColor,
                  labelColor: themeGolderColor,
                  labelStyle: gcTitleTextStyle,
                  unselectedLabelStyle: gcNormalTextStyle,
                  indicatorWeight: 3,
                  onTap: (index) {
                    print("index  $index");
                    setState(() {
                      _tabControllerIndex = index;
                      _tabController.animateTo(index,
                          duration: Duration(milliseconds: 1000));
                    });
                  },
                  controller: _tabController,
                  tabs: tabs),
              preferredSize: Size.fromHeight(0.0)),
        ),
        body: TabBarView(
          controller: _tabController,
          children: <Widget>[FacilitiesTabView(), ReservationTabView()],
        ));
  }

  void _showDatePicker(ctx, tag) {
    // showCupertinoModalPopup is a built-in function of the cupertino library
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
          height: 400,
          color: Color.fromARGB(255, 255, 255, 255),
          child: Column(
            children: [
              Container(
                height: 300,
                child: CupertinoDatePicker(
                    initialDateTime: DateTime.now(),
                    // maximumDate: DateTime.now()  ,
                    minimumYear: DateTime.now().year,
                    use24hFormat: false,
                    mode: CupertinoDatePickerMode.dateAndTime,
                    onDateTimeChanged: (val) {
                      setState(() {
                        if (tag == 1) {
                          _chosenDateTime1 = val;
                          startDate = DateFormat("yyyy-MM-dd HH:mm").format(_chosenDateTime1);
                          // var time = DateFormat.Hm().format(_chosenDateTime1);

                          // DateFormat("yyyy-MM-dd HH:MM")
                          // .format(_chosenDateTime1);
                          print(startDate);
                          // print(time);
                        } else {
                          print("none");
                        }
                      });
                    }),
              ),

              // Close the modal
              Container(
                height: 50,
                width: 100,
                color: themeGolderColor,
                child: CupertinoButton(
                  child: Text(
                    'OK',
                    style: gcTitleTextStyle,
                  ),
                  onPressed: () => Navigator.of(ctx).pop(),
                ),
              )
            ],
          ),
        ));
  }

  Widget FacilitiesTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 40,
              ),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1.0, color: themeGolderColor),
                  ),
                ),
                child: DropdownButton(
                  dropdownColor: themeGolderColor,
                  hint: Text(
                    'Please choose a facility',
                    style: gcTitleTextStyle,
                  ),
                  underline: SizedBox(),
                  value: _selectedName,
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: Colors.white,
                  ),
                  items: namesList.map((facility) {
                    return new DropdownMenuItem<String>(
                      value: facility,
                      child: new Text(
                        facility,
                        style: gcTitleTextStyle,
                        textAlign: TextAlign.center,
                      ),
                    );
                  }).toList(),
                  isExpanded: true,
                  onChanged: (String? val) {
                    setState(() {
                      _selectedName = val;
                    });
                    print("val----$val");

                    if(_selectedName != null){
                      print("_selectedFacility----$_selectedName");
                      var index = namesList.indexOf(_selectedName!);
                      print(index);

                      _selectedFacility = allFacilityList[index];
                      print(
                          "_selectedFacility----${_selectedFacility!.name}---- ${_selectedFacility!.id}");
                    }

                  },
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                height: 40,
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1.0, color: themeGolderColor),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CupertinoButton(
                      padding: EdgeInsetsDirectional.zero,
                      child: Text(
                        'Pick a Date: ',
                        style: gcTitleTextStyle,
                      ),
                      onPressed: () => _showDatePicker(context, 1),
                    ),
                    CupertinoButton(
                      padding: EdgeInsetsDirectional.zero,
                      child: (startDate == "")
                          ? Icon(
                        Icons.calendar_today,
                        color: themeGolderColor,
                      )
                          : Text(startDate.toString(),
                          style: gcListTitleTextStyle),
                      onPressed: () => _showDatePicker(context, 1),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 40,
              ),
              (isLoadingCreation == true)? CircularProgressIndicator(backgroundColor: themeGolderColor,):MyCustomButton(
                height: 40,
                width: MediaQuery.of(context).size.width * 0.8,
                color: themeGolderColor, //Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'CREATE',
                    style: gcNormalTextStyle,
                  ),
                ),
                onPressed: () {
                  print('tapped');
                  if (startDate != "" && _selectedName != null) {

                    setState(() {
                      isLoadingCreation = true;

                    });
                    createReservation();
                  } else {
                    Fluttertoast.showToast(
                      msg: "Check input fields!",
                      backgroundColor: Colors.red,
                    );
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget ReservationTabView() {
    return Scaffold(
        backgroundColor: themeBlackColor,
        appBar: AppBar(
          backgroundColor: themeBlackColor,
          bottom: PreferredSize(
              child: TabBar(
                  isScrollable: true,
                  unselectedLabelColor: Colors.white,
                  indicatorColor: themeGolderColor,
                  labelColor: themeGolderColor,
                  labelStyle: gcTitleTextStyle,
                  unselectedLabelStyle: gcNormalTextStyle,
                  indicatorWeight: 3,
                  onTap: (index) {
                    print("_reservationTabController index  $index");
                  },
                  controller: _reservationTabController,
                  tabs: myTabs),
              preferredSize: Size.fromHeight(0.0)),
        ),
        body: TabBarView(
          controller: _reservationTabController,
          children: <Widget>[
            AllReservationTabView(),
            PendingReservationTabView(),
            ConfirmedReservationTabView(),
            CanceledReservationTabView()
          ],
        ));
  }

  Widget AllReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : allReservationList == null
            ? Center(
            child: Text(
              "Something went wrong",
              style: TextStyle(),
            ))
            : (allReservationList.length == 0)
            ? Center(
          child: Text(
            "No Reservation found",
            style: gcPlaceholderTextStyle,
          ),
        )
            : Container(
            height: MediaQuery.of(context).size.height - 280,
            child: createReservationListView(
                allReservationList, context)),
      ),
    );
  }

  Widget PendingReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : allReservationList == null
            ? Center(
            child: Text(
              "Something went wrong",
              style: TextStyle(),
            ))
            : (allReservationList.length == 0)
            ? Center(
          child: Text(
            "No Reservation found",
            style: gcPlaceholderTextStyle,
          ),
        )
            : Container(
            height: MediaQuery.of(context).size.height - 280,
            child: createReservationListView(
                allReservationList, context)),
      ),
    );
  }

  Widget ConfirmedReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : allReservationList == null
            ? Center(
            child: Text(
              "Something went wrong",
              style: TextStyle(),
            ))
            : (allReservationList.length == 0)
            ? Center(
          child: Text(
            "No Reservation found",
            style: gcPlaceholderTextStyle,
          ),
        )
            : Container(
            height: MediaQuery.of(context).size.height - 280,
            child: createReservationListView(
                allReservationList, context)),
      ),
    );
  }

  Widget CanceledReservationTabView() {
    return SingleChildScrollView(
      child: Container(
        color: themeBlackColor,
        child: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : allReservationList == null
            ? Center(
            child: Text(
              "Something went wrong",
              style: TextStyle(),
            ))
            : (allReservationList.length == 0)
            ? Center(
          child: Text(
            "No Reservation found",
            style: gcPlaceholderTextStyle,
          ),
        )
            : Container(
            height: MediaQuery.of(context).size.height - 280,
            child: createReservationListView(
                allReservationList, context)),
      ),
    );
  }

  Widget createReservationListView(
      List<Reservation> results, BuildContext context) {
    return NotificationListener(
      onNotification: (ScrollNotification scrollInfo) {
        if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          if (isLoadingMore == false && reservationsListResponse?.data?.next != null ) {
            // start loading data
            print("Scrolling to end...");
            setState(() {
              isLoadingMore = true;
            });
            _loadMoreMember();
          } else if (reservationsListResponse?.data?.next == null) {
            print("Data end");
            setState(() {
              isLoadingMore = false;
            });
          } else {}
        }
        return true;
      },
      child: RefreshIndicator(
        onRefresh: _reloadData,
        // key: refreshKey,
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemCount: allReservationList.length,
                itemBuilder: (context, index) {
                  String dateString = "";
                  if (allReservationList[index].reservationDate != null) {
                    DateTime parsedDate = DateTime.parse(
                        allReservationList[index].reservationDate.toString())
                        .toLocal();
                    dateString =  DateFormat.yMd().add_jm().format(parsedDate);
                  }

                  return Padding(
                    padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16),
                    child: GestureDetector(
                      onTap: () {
                        // var member = allReservationList[index];
                        // Navigator.pushNamed(context, '/member_details', arguments: itemList[index]);
                        //Navigator.pushNamed(context, '/event_details',arguments: itemList[index]);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          // color: Colors.white38,
                            borderRadius: BorderRadius.all(
                                Radius.circular(8)), // color: Colors.white38,
                            image: (index % 2 == 0)
                                ? DecorationImage(
                                image: AssetImage(
                                    'assets/images/ic_list_background_1.png'),
                                fit: BoxFit.cover)
                                : DecorationImage(
                                image: AssetImage(
                                    'assets/images/ic_list_background_3.png'),
                                fit: BoxFit
                                    .cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 80,
                                  width: 80,
                                  child: (allReservationList[index]
                                      .facility
                                      ?.imageMedium !=
                                      null)
                                      ? CachedNetworkImage(
                                    imageUrl: NetworkManager.baseUrl +
                                        allReservationList[index]
                                            .facility!
                                            .imageMedium!,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.fill,
                                                colorFilter: ColorFilter.mode(
                                                    Colors.transparent,
                                                    BlendMode.color)),
                                          ),
                                        ),
                                    placeholder: (context, url) => Center(
                                        child: CircularProgressIndicator()),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  )
                                      : Image.asset(
                                    'assets/icons/ic_placeholder.png',
                                    height: 120,
                                    width: 100,
                                  ),
                                ), //AssetImage('assets/icons/ic_placeholder.png'),

                                //   CircleAvatar(
                                //     radius: 47,
                                //     backgroundColor: Colors.transparent,
                                //     backgroundImage: (itemList[index].imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium,) : AssetImage('assets/icons/ic_placeholder.png'),
                                //     //AssetImage('assets/images/Facilities1.jpg'),
                                //   ),
                              ),
                              Flexible(
                                // child: Card(color: Colors.white38,elevation: 0,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          color: (allReservationList[index].status ==
                                              "Pending")
                                              ? Colors.orangeAccent
                                              : (allReservationList[index].status ==
                                              "Confirmed")
                                              ? Colors.green
                                              : Colors.red,
                                          borderRadius: BorderRadius.circular(6),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Text(
                                            allReservationList[index].status ?? ' ', //"Capt. Saifur Rahman",
                                            style: gcNormalTextStyle,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        allReservationList[index]
                                            .facility?.name ?? ' ', //"Capt. Saifur Rahman",
                                        style: gcKLargeListTitleTextStyle,
                                      ),
                                      if (allReservationList[index].reservationDate !=
                                          null)
                                        Text(
                                          "Date: " +
                                              dateString, //"Capt. Saifur Rahman",
                                          style: gcListTitleTextStyle,
                                        ),
                                    ],
                                  ),
                                ),
                              ),
                              // ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
