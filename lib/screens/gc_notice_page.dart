import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/jsonModels/notice_board_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

class NoticeScreen extends StatefulWidget {
  static const String route = '/notice';

  @override
  _NoticeScreenState createState() => _NoticeScreenState();
}

class _NoticeScreenState extends State<NoticeScreen> {
  List<Notice> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  NoticeResponse? noticeJson;
  bool isLoadingMore = false;

  int tabIndex = 3;
  List<Widget> listScreens = [];

  int notificationCount = 3;

  @override
  void initState() {
    // TODO: implement initState
    if (tabIndex == null) {
      tabIndex = 0;
    } else {
      tabIndex = tabIndex;
    }
    getAllNotices();
    getCurrentUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Notice',
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
            // sets the background color of the `BottomNavigationBar`
            canvasColor: themeBlackColor,
            // sets the active color of the `BottomNavigationBar` if `Brightness` is light
            primaryColor: Colors.red,
            textTheme: Theme.of(context).textTheme.copyWith(
                caption: new TextStyle(
                    color: Colors
                        .yellow))), // sets the inactive color of the `BottomNavigationBar`
        child: BottomNavigationBar(
            selectedItemColor: themeGolderColor,
            unselectedItemColor: Colors.grey[400],
            iconSize: 30,
            backgroundColor: Colors.blue,
            currentIndex: tabIndex,
            onTap: (int index) {
              setState(() {
                tabIndex = index;
                print("tabIndex----$tabIndex");
                Resources.moveTo(index, context);
              });
            },
            items: [
              BottomNavigationBarItem(
                  activeIcon: Icon(
                    Icons.home,
                    color: themeGolderColor,
                    size: 30,
                  ),
                  icon: Icon(
                    Icons.home,
                    color: themeGreyColor,
                    size: 28,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_profile.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_profile.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),
              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_memeber_director.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_memeber_director.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_notice_board.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_notice_board.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              // BottomNavigationBarItem(
              //     activeIcon: SvgPicture.asset(
              //       "assets/icons/ic_facilities.svg",
              //       color: themeGolderColor,
              //       width: 30,
              //       height: 30,
              //     ),
              //     icon: SvgPicture.asset(
              //       "assets/icons/ic_facilities.svg",
              //       color: themeGreyColor,
              //       width: 26,
              //       height: 26,
              //     ),
              //     label: ''),
            ]),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: TextStyle(),
                ))
              : itemList.length == 0
                  ? Center(
                      child: Text(
                        "No notices found",
                        style: gcPlaceholderTextStyle,
                      ),
                    )
                  : createListView(itemList, context),
    );
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllNotices();
    });
  }

  getAllNotices() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/notice_boards/";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');

    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final noticeResponse =
                  NoticeResponse.fromJson(data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.results}');

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  itemList = noticeResponse.results ?? [];
                  noticeJson = noticeResponse;
                  isLoading = false;
                  notificationCount = 9;
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  _loadMoreData() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint =
        noticeJson?.next ?? ''; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = NoticeResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        noticeJson = moreNoticeResponse;
        List<Notice> moreData = moreNoticeResponse.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  Widget createListView(List<Notice> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return NotificationListener(
      onNotification: (ScrollNotification scrollInfo) {
        if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          if (!isLoadingMore && noticeJson?.next != null) {
            // start loading data
            print("Scrolling to end...");
            setState(() {
              isLoadingMore = true;
            });
            _loadMoreData();
          } else if (noticeJson?.next == null) {
            print("Data end");
            setState(() {
              isLoadingMore = false;
            });
          } else {}
        }
        return true;
      },
      child: RefreshIndicator(
        onRefresh: _reloadData,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: itemList.length,
          itemBuilder: (context, index) {
            DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
            DateTime dateTime =
                dateFormat.parse(itemList[index].updatedAt.toString());
            String string = dateFormat.format(dateTime);
            print(string);

            return Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 32),
              child: Container(
                decoration: BoxDecoration(
                    // color: Colors.white38,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: (index % 2 == 0)
                        ? DecorationImage(
                            image: AssetImage('assets/images/1.jpg'),
                            fit: BoxFit.cover)
                        : DecorationImage(
                            image: AssetImage('assets/images/1.jpg'),
                            fit: BoxFit
                                .cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),
                    ),
                child: Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/notice_details',
                          arguments: itemList[index]);
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height / 5,

                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              child: (itemList[index].imageMedium != null)
                                  ? CachedNetworkImage(
                                      imageUrl: NetworkManager.baseUrl +
                                          itemList[index].imageMedium!,
                                      imageBuilder: (context, imageProvider) =>
                                          Container(
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover,
                                            // colorFilter:
                                            // ColorFilter.mode(Colors.red, BlendMode.colorBurn)
                                          ),
                                        ),
                                      ),
                                      placeholder: (context, url) => Center(
                                          child: CircularProgressIndicator()),
                                      errorWidget: (context, url, error) =>
                                          Column(
                                        children: [
                                          Icon(Icons.error),
                                          // Text(itemList[index].imageAltText),
                                        ],
                                      ),
                                    )
                                  : Image.asset(
                                      "assets/images/1.jpg",
                                      fit: BoxFit.fill,
                                    ),
//                          Image.asset(
//                            imgList[index],
//                            fit: BoxFit.fill,
//                          ),
                            ),
                          ),
                          Center(
                            child: Container(
                              color: themeGolderColor,
                              width: MediaQuery.of(context).size.width,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  itemList[index]
                                      .title
                                      .toString(), //"40 th E.C. Meeting",
                                  style: gcNormalTextStyle,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      // Column(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   crossAxisAlignment: CrossAxisAlignment.center,
                      //   children: [
                      //     Container(
                      //       color: themeGolderColor,
                      //       width: MediaQuery.of(context).size.width,
                      //
                      //       child: Text(
                      //         itemList[index].title, //"40 th E.C. Meeting",
                      //         style: gcNormalTextStyle,textAlign: TextAlign.center,
                      //       ),
                      //     ),
                      //     // Text(
                      //     //   string, //itemList[index].updatedAt.toString(),//"Dated: 06-03-2018",
                      //     //   style: listSubTitleTextStyle,
                      //     // ),
                      //     // SizedBox(
                      //     //   height: 10,
                      //     // ),
                      //     // Text(
                      //     //   itemList[index]
                      //     //       .message
                      //     //       .toString(), //"Noticed is hereby called for E.C Meeting to be held on 10th March,2018 at the club Board Room to discuss the following agenda.",
                      //     //   style: gcDescriptionTextStyle,
                      //     // ),
                      //   ],
                      // ),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  getCurrentUserInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  // userInfo = userResponse.data;
                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
