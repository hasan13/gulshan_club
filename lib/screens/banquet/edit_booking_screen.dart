import 'dart:convert';

import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gulshan_club_app/jsonModels/banquet/booking_creation_error_response.dart';
import 'package:gulshan_club_app/jsonModels/banquet/event_type_response.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_response.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_webview_payment.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

import '../../jsonModels/banquet/booking_list_response.dart';
import '../../jsonModels/member_search_response.dart';
import '../../theme/debounce.dart';
import '../../utils/shared_preference.dart';

class EditBookingScreen extends StatefulWidget {
  static const String route = '/edit_booking';
  final BookingDatum selectedEvent;

  const EditBookingScreen({Key? key, required this.selectedEvent}) : super(key: key);

  @override
  _EditBookingScreenState createState() => _EditBookingScreenState();
}

class _EditBookingScreenState extends State<EditBookingScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  TextEditingController _amountController = TextEditingController();
  final FocusNode _amountNode = FocusNode();

  bool isLoading = false;
  AppUser? userInfo;
  bool isPayForOtherSelected = false;

  bool isDataLoading = false;

  String startDate = '';

  DateTime _chosenDateTime1 = DateTime.now();

  String? _selectedName;
  List<String> namesList = [];

  EventTypeDatum ? _selectedFacility;

  List<EventTypeDatum> items = [];

  int selectedIndex = -1;

  final TextEditingController _accountNumberCtrl = new TextEditingController();
  final FocusNode _userIdNode = FocusNode();
  final TextEditingController _accountNameCtrl = new TextEditingController();
  final FocusNode _accountNameNode = FocusNode();

  final TextEditingController _remarksController = new TextEditingController();

  Member? selectedMember;

  List<Member> memberList = [];
  List<String> memberNameList = [];

  String? searchValue;

  bool isMemberSelected = false;

  bool isLoadingCreation = false;

  final _debouncer = Debouncer(milliseconds: 500);


  @override
  void initState() {
    // TODO: implement initState
    startDate = widget.selectedEvent.date.toString().substring(0, 10);
    if(widget.selectedEvent.clubAcNo != null){
      _accountNumberCtrl.text = widget.selectedEvent.clubAcNo.toString();
    }
    if(widget.selectedEvent.remarks != null){
      _remarksController.text = widget.selectedEvent.remarks.toString();

    }
    if(widget.selectedEvent.userInfo != null){
      _accountNameCtrl.text = widget.selectedEvent.userInfo!.firstName.toString() + " " +
          widget.selectedEvent.userInfo!.lastName.toString();
    }

    if(widget.selectedEvent.userInfo != null){
      selectedMember = Member(
          firstName: widget.selectedEvent.userInfo!.firstName,
          lastName: widget.selectedEvent.userInfo!.lastName,
          clubAcNumber: widget.selectedEvent.userInfo!.clubAcNumber
      );
    }

    if(widget.selectedEvent.accountType == "account"){
      selectedIndex = 0;
    }else   if(widget.selectedEvent.accountType == "event"){
      selectedIndex = 1;
    }else{
      print(selectedIndex);
    }
    getEventTypeList();
    super.initState();
  }

  getEventTypeList() async {
    print(Resources.currentUser?.accessToken);
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/banquet-bookings/event-types";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
        })
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(value.body);
          final userResponse = EventTypeResponse.fromJson(data);
          print('result....\n ${userResponse.data}');
          if (value.statusCode == 200 || value.statusCode == 202) {
            setState(() {
              items = userResponse.data ?? [];
              isLoading = false;

              for (var i = 0; i < items.length; i++) {
                namesList.add(items[i].value.toString());
                if(items[i].name == widget.selectedEvent.eventType){
                  _selectedFacility = items[i];
                  _selectedName = items[i].value;
                }
              }

            });
            //navigateToSignInPage(context);
            print('YAY! Free cute dog pics!');
          } else if (data['status'] == false) {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            setState(() {
              isLoading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      Resources.showToast("Internet not available", Colors.red);
    }
  }

  getFilteredMembers(String text) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/user_search_by_club_no/?club_ac_no=$text";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
        })
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(value.body);
          final employeeResponse = MemberSearchResponse.fromJson(data);
          print('result....\n ${employeeResponse.data}');
          if (value.statusCode == 200 || value.statusCode == 202) {
            setState(() {
              memberList = employeeResponse.data ?? [];
              isLoading = false;
              memberList.forEach((member) {
                memberNameList.add(member.clubAcNumber.toString());
              });
            });
            //navigateToSignInPage(context);
            print('YAY! Free cute dog pics!');
          } else if (data['status'] == false) {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            setState(() {
              isLoading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        centerTitle: true,
        leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: themeGolderColor,
            )),
        title: Text(
          "Update Schedule",
          style: gcTitleTextStyle,
        ),
      ),
      body: SingleChildScrollView(
        child: (isLoading == true)
            ? Center(
            child: CircularProgressIndicator(
              backgroundColor: themeGolderColor,
            ))
            : Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            // shrinkWrap: true,
            // padding: EdgeInsets.all(16),
            // scrollDirection: Axis.vertical,
            children: [
              ///date selection label
              Text(
                "Pick a date",
                style: gcDescriptionTextStyle,
              ),

              ///date selection widget

              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Container(
                  // height: 46,
                    decoration: BoxDecoration(
                      color: themeGrayBackColor,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: ListTile(
                      dense: true,
                      leading: CupertinoButton(
                        padding: EdgeInsetsDirectional.zero,
                        child: Text(startDate.toString(),
                            style: gcTitleTextStyle),
                        onPressed: () => _showDatePicker(context, 1),
                      ),
                      trailing: InkWell(
                          onTap: () => _showDatePicker(context, 1),
                          child: Icon(
                            Icons.calendar_month,
                            color: themeGolderColor,
                          )),
                    )),
              ),

              ///event type selection label
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  "Event Type",
                  style: gcDescriptionTextStyle,
                ),
              ),
              ///event type selection widget
              Container(
                decoration: BoxDecoration(
                  color: themeGrayBackColor,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: DropdownButton(
                  dropdownColor: themeGolderColor,
                  hint: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Text(
                      'Please choose a event type',
                      style: gcTitleTextStyle,
                    ),
                  ),
                  underline: SizedBox(),
                  value: _selectedName,
                  icon: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.keyboard_arrow_down_rounded,
                      color: Colors.white,
                    ),
                  ),
                  items: namesList.map((facility) {
                    return new DropdownMenuItem<String>(
                      value: facility,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        child: new Text(
                          facility,
                          style: gcTitleTextStyle,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  }).toList(),
                  isExpanded: true,
                  onChanged: (val) {
                    setState(() {
                      _selectedName = val as String?;
                    });
                    print("val----$val");

                    print("_selectedFacility----$_selectedName");
                    var index = namesList.indexOf(_selectedName!);
                    print(index);

                    _selectedFacility = items[index];
                    print(
                        "_selectedFacility----${_selectedFacility?.name}---- ${_selectedFacility?.value}");
                  },
                ),
              ),
              ///account type selection label
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  "Account Type",
                  style: gcDescriptionTextStyle,
                ),
              ),
              ///account type selection label
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2.3,
                    decoration: BoxDecoration(
                      border: Border.all(color: themeGolderColor),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: ListTile(
                      // tileColor: themeGreyColor,
                      leading: selectedIndex == 0
                          ? SvgPicture.asset(
                        'assets/icons/Payment_Selected_Radio_Button.svg',
                        // color: themeGolderColor,
                      )
                          : SvgPicture.asset(
                        'assets/icons/UnselectedRadioButton.svg',
                        // color: themeGolderColor,
                      ),
                      title: Text(
                        'Club A/C',
                        style: gcTitleTextStyle,
                      ),
                      selected: false,
                      onTap: () async {
                        setState(() {
                          selectedIndex = 0;
                          _accountNumberCtrl.text = '';
                          _remarksController.text = '';
                          _accountNameCtrl.text = '';
                          isMemberSelected = false;
                          memberList = [];
                          memberNameList = [];
                          selectedMember = null;
                        });
                        // await _navigateTo(context, '/building_directory');
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2.3,
                    decoration: BoxDecoration(
                      border: Border.all(color: themeGolderColor),
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: ListTile(
                      // tileColor: themeGreyColor,
                      leading: selectedIndex == 1
                          ? SvgPicture.asset(
                        'assets/icons/Payment_Selected_Radio_Button.svg',
                        // color: themeGolderColor,
                      )
                          : SvgPicture.asset(
                        'assets/icons/UnselectedRadioButton.svg',
                        // color: themeGolderColor,
                      ),
                      title: Text(
                        'Club Event',
                        style: gcTitleTextStyle,
                      ),
                      selected: false,
                      onTap: () async {
                        setState(() {
                          selectedIndex = 1;
                          _accountNumberCtrl.text = '';
                          _remarksController.text = '';
                          _accountNameCtrl.text = '';
                          isMemberSelected = false;
                          memberList = [];
                          memberNameList = [];
                          selectedMember = null;
                        });
                        // await _navigateTo(context, '/building_directory');
                      },
                    ),
                  )
                ],
              ),
              if (selectedIndex == 0)
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Text(
                    "Account Number",
                    style: gcDescriptionTextStyle,
                  ),
                ),
              ///account id field
              if (selectedIndex == 0)
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Container(
                    // height: 46,
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      decoration: BoxDecoration(
                        color: themeGrayBackColor,
                        borderRadius:
                        BorderRadius.all(Radius.circular(8)),
                      ),
                      child: TextFormField(
                        enabled: true,
                        controller: _accountNumberCtrl,
                        focusNode: _userIdNode,

                        // keyboardType: TextInputType.numberWithOptions(
                        //     decimal: false, signed: false),
                        textAlign: TextAlign.left,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.cancel_outlined,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                _accountNumberCtrl.text = '';
                                _accountNameCtrl.text = '';
                                isMemberSelected = false;
                                memberList = [];
                                memberNameList = [];
                                FocusScope.of(context).unfocus();
                              });
                            },
                          ),
                          border: new UnderlineInputBorder(
                            borderSide: new BorderSide(
                              //color: themeGolderColor,
                            ),
                          ),
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintText: 'Enter account number',
                          hintStyle: gcPlaceholderTextStyle,
                        ),
                        onChanged: (value) {

                          _debouncer.run(() {
                            if (value.length > 1) {
                              setState(() {
                                isMemberSelected = false;
                                getFilteredMembers(value);

                              });
                            }                                  //perform search here
                          });


                        },
                        onFieldSubmitted: (value) {
                          print("onFieldSubmitted ");

                          if (selectedMember != null) {
                          } else if (value.length > 1) {
                            setState(() {
                              isMemberSelected = false;
                            });
                            getFilteredMembers(value);
                          }
                        },
                        onEditingComplete: () {
                          print("onEditingComplete ");
                          FocusScope.of(context).unfocus();

                        },

                        style: TextStyle(
                            fontFamily: 'Siri-Regular',
                            fontSize: kMedium,
                            color: Colors.white),
                      )),
                ),
              ///account listview
              if (selectedIndex == 0 && memberList.isEmpty == false)
                Container(
                  child: ListView.builder(
                      shrinkWrap: true,physics: NeverScrollableScrollPhysics(),
                      itemCount: memberList.length,
                      itemBuilder: (context, index) {
                        Member message = memberList[index];

                        return ListTile(
                          tileColor: themeGrayBackColor,
                          title: Text(
                            message.clubAcNumber.toString(),
                            style: gcTitleTextStyle,
                          ),
                          onTap: () {
                            setState(() {
                              selectedMember = memberList[index];
                              _accountNumberCtrl.text =
                                  memberList[index].clubAcNumber.toString();
                              isMemberSelected = true;
                              _accountNameCtrl.text =
                                  selectedMember!.firstName.toString() +
                                      " " +
                                      selectedMember!.lastName.toString() ;
                              memberList = [];
                              memberNameList = [];
                            });
                          },
                        );
                      }),
                ),


              ///                          "Account Name",
              if (selectedMember != null && selectedIndex == 0)
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Text(
                    "Account Name",
                    style: gcDescriptionTextStyle,
                  ),
                ),
              if (selectedMember != null && selectedIndex == 0)
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Container(
                    // height: 46,
                    padding: EdgeInsets.symmetric(horizontal: 8),

                    decoration: BoxDecoration(
                      color: themeGrayBackColor,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: Container(
                        // height: 46,
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          decoration: BoxDecoration(
                            color: themeGrayBackColor,
                            borderRadius:
                            BorderRadius.all(Radius.circular(8)),
                          ),
                          child: TextFormField(
                            enabled: false,
                            controller: _accountNameCtrl,
                            focusNode: _accountNameNode,
                            // keyboardType: TextInputType.numberWithOptions(
                            //     decimal: false, signed: false),
                            textAlign: TextAlign.left,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              hintStyle: gcPlaceholderTextStyle,
                            ),

                            style: TextStyle(
                                fontFamily: 'Siri-Regular',
                                fontSize: kMedium,
                                color: Colors.white),
                          )),
                    ),
                  ),
                ),


              // ///event type selection label
              // Padding(
              //   padding: EdgeInsets.symmetric(vertical: 8),
              //   child: Text(
              //     "Event Type",
              //     style: gcDescriptionTextStyle,
              //   ),
              // ),
              // ///event type selection widget
              // Container(
              //   decoration: BoxDecoration(
              //     color: themeGrayBackColor,
              //     borderRadius: BorderRadius.all(Radius.circular(8)),
              //   ),
              //   child: DropdownButton(
              //     dropdownColor: themeGolderColor,
              //     hint: Padding(
              //       padding: EdgeInsets.symmetric(horizontal: 8),
              //       child: Text(
              //         'Please choose a event type',
              //         style: gcTitleTextStyle,
              //       ),
              //     ),
              //     underline: SizedBox(),
              //     value: _selectedName,
              //     icon: Padding(
              //       padding: const EdgeInsets.all(8.0),
              //       child: Icon(
              //         Icons.keyboard_arrow_down_rounded,
              //         color: Colors.white,
              //       ),
              //     ),
              //     items: namesList.map((facility) {
              //       return new DropdownMenuItem<String>(
              //         value: facility,
              //         child: Padding(
              //           padding: EdgeInsets.symmetric(horizontal: 8),
              //           child: new Text(
              //             facility,
              //             style: gcTitleTextStyle,
              //             textAlign: TextAlign.center,
              //           ),
              //         ),
              //       );
              //     }).toList(),
              //     isExpanded: true,
              //     onChanged: (val) {
              //       setState(() {
              //         _selectedName = val;
              //       });
              //       print("val----$val");
              //
              //       print("_selectedFacility----$_selectedName");
              //       var index = namesList.indexOf(_selectedName);
              //       print(index);
              //
              //       _selectedFacility = items[index];
              //       print(
              //           "_selectedFacility----${_selectedFacility.name}---- ${_selectedFacility.value}");
              //     },
              //   ),
              // ),
              // ///account type selection label
              // Padding(
              //   padding: EdgeInsets.symmetric(vertical: 8),
              //   child: Text(
              //     "Account Type",
              //     style: gcDescriptionTextStyle,
              //   ),
              // ),
              // ///account type selection label
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   children: [
              //     Container(
              //       width: MediaQuery.of(context).size.width / 2.3,
              //       decoration: BoxDecoration(
              //         border: Border.all(color: themeGolderColor),
              //         borderRadius: BorderRadius.all(Radius.circular(8)),
              //       ),
              //       child: ListTile(
              //         // tileColor: themeGreyColor,
              //         leading: selectedIndex == 0
              //             ? SvgPicture.asset(
              //           'assets/icons/Payment_Selected_Radio_Button.svg',
              //           // color: themeGolderColor,
              //         )
              //             : SvgPicture.asset(
              //           'assets/icons/UnselectedRadioButton.svg',
              //           // color: themeGolderColor,
              //         ),
              //         title: Text(
              //           'Club A/C',
              //           style: gcTitleTextStyle,
              //         ),
              //         selected: false,
              //         onTap: () async {
              //           setState(() {
              //             selectedIndex = 0;
              //           });
              //           // await _navigateTo(context, '/building_directory');
              //         },
              //       ),
              //     ),
              //     Container(
              //       width: MediaQuery.of(context).size.width / 2.3,
              //       decoration: BoxDecoration(
              //         border: Border.all(color: themeGolderColor),
              //         borderRadius: BorderRadius.all(Radius.circular(8)),
              //       ),
              //       child: ListTile(
              //         // tileColor: themeGreyColor,
              //         leading: selectedIndex == 1
              //             ? SvgPicture.asset(
              //           'assets/icons/Payment_Selected_Radio_Button.svg',
              //           // color: themeGolderColor,
              //         )
              //             : SvgPicture.asset(
              //           'assets/icons/UnselectedRadioButton.svg',
              //           // color: themeGolderColor,
              //         ),
              //         title: Text(
              //           'Club Event',
              //           style: gcTitleTextStyle,
              //         ),
              //         selected: false,
              //         onTap: () async {
              //           setState(() {
              //             selectedIndex = 1;
              //           });
              //           // await _navigateTo(context, '/building_directory');
              //         },
              //       ),
              //     )
              //   ],
              // ),
              // if (selectedIndex == 0)
              //   Padding(
              //     padding: EdgeInsets.symmetric(vertical: 8),
              //     child: Text(
              //       "Account Number",
              //       style: gcDescriptionTextStyle,
              //     ),
              //   ),
              // if (selectedIndex == 0)
              //   Padding(
              //     padding: EdgeInsets.symmetric(vertical: 8),
              //     child: Container(
              //       // height: 46,
              //         padding: EdgeInsets.symmetric(horizontal: 8),
              //         decoration: BoxDecoration(
              //           color: themeGrayBackColor,
              //           borderRadius:
              //           BorderRadius.all(Radius.circular(8)),
              //         ),
              //         child: TextFormField(
              //           enabled: true,
              //           controller: _accountNumberCtrl,
              //           focusNode: _userIdNode,
              //           // keyboardType: TextInputType.numberWithOptions(
              //           //     decimal: false, signed: false),
              //           textAlign: TextAlign.left,
              //           decoration: InputDecoration(
              //             border: new UnderlineInputBorder(
              //               borderSide: new BorderSide(
              //                 //color: themeGolderColor,
              //               ),
              //             ),
              //             enabledBorder: InputBorder.none,
              //             focusedBorder: InputBorder.none,
              //             hintText: 'Enter account number',
              //             hintStyle: gcPlaceholderTextStyle,
              //           ),
              //           onChanged: (value) {
              //             if (value != null) {
              //               setState(() {
              //                 isMemberSelected = false;
              //               });
              //             }
              //             getFilteredMembers(value);
              //           },
              //
              //           style: TextStyle(
              //               fontFamily: 'Siri-Regular',
              //               fontSize: kMedium,
              //               color: Colors.white),
              //         )),
              //   ),
              // if (isMemberSelected == false)
              //   Container(
              //     height: 400,
              //     child: ListView.builder(
              //         shrinkWrap: true,
              //         itemCount: memberList.length,
              //         itemBuilder: (context, index) {
              //           Member message = memberList[index];
              //
              //           return ListTile(
              //             title: Text(
              //               message.clubAcNumber,
              //               style: gcTitleTextStyle,
              //             ),
              //             onTap: () {
              //               setState(() {
              //                 selectedMember = memberList[index];
              //                 _accountNumberCtrl.text =
              //                     memberList[index].clubAcNumber;
              //                 isMemberSelected = true;
              //                 _accountNameCtrl.text =
              //                     selectedMember.firstName.toString() +
              //                         " " +
              //                         selectedMember.lastName.toString();
              //               });
              //             },
              //           );
              //         }),
              //   ),
              // if (selectedMember != null && selectedIndex == 0)
              //   Padding(
              //     padding: EdgeInsets.symmetric(vertical: 8),
              //     child: Text(
              //       "Account Name",
              //       style: gcDescriptionTextStyle,
              //     ),
              //   ),
              // if (selectedMember != null && selectedIndex == 0)
              //   Padding(
              //     padding: EdgeInsets.symmetric(vertical: 8),
              //     child: Container(
              //       // height: 46,
              //       padding: EdgeInsets.symmetric(horizontal: 8),
              //
              //       decoration: BoxDecoration(
              //         color: themeGrayBackColor,
              //         borderRadius: BorderRadius.all(Radius.circular(8)),
              //       ),
              //       child: Padding(
              //         padding: EdgeInsets.symmetric(vertical: 8),
              //         child: Container(
              //           // height: 46,
              //             padding: EdgeInsets.symmetric(horizontal: 8),
              //             decoration: BoxDecoration(
              //               color: themeGrayBackColor,
              //               borderRadius:
              //               BorderRadius.all(Radius.circular(8)),
              //             ),
              //             child: TextFormField(
              //               enabled: false,
              //               controller: _accountNameCtrl,
              //               focusNode: _accountNameNode,
              //               // keyboardType: TextInputType.numberWithOptions(
              //               //     decimal: false, signed: false),
              //               textAlign: TextAlign.left,
              //               decoration: InputDecoration(
              //                 border: InputBorder.none,
              //                 enabledBorder: InputBorder.none,
              //                 focusedBorder: InputBorder.none,
              //                 hintStyle: gcPlaceholderTextStyle,
              //               ),
              //
              //               style: TextStyle(
              //                   fontFamily: 'Siri-Regular',
              //                   fontSize: kMedium,
              //                   color: Colors.white),
              //             )),
              //       ),
              //     ),
              //   ),
              //
              //
              //
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  "Remarks",
                  style: gcDescriptionTextStyle,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Container(
                  // height: 46,
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    decoration: BoxDecoration(
                      color: themeGrayBackColor,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                    ),
                    child: TextField(
                      controller: _remarksController,
                      keyboardType: TextInputType.multiline,
                      style: TextStyle(
                          fontFamily: 'Siri-Regular',
                          fontSize: kMedium,
                          color: Colors.white),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        hintStyle: gcPlaceholderTextStyle,
                      ),
                      minLines:
                      1, //Normal textInputField will be displayed
                      maxLines:
                      5, // when user presses enter it will adapt to it
                    )),
              ),

              (isLoadingCreation == true)
                  ? Center(
                child: CircularProgressIndicator(
                  color: themeGolderColor,
                ),
              )
                  : Padding(
                padding: EdgeInsets.symmetric(
                    vertical: 24, horizontal: 0),
                child: ElevatedButton(
                  onPressed: () async {
                    if(selectedIndex == 0){
                      if (selectedMember == null ||
                          _selectedFacility == null) {
                        Resources.showToast(
                            "Please input all data", Colors.red);
                      } else {
                        setState(() {
                          isLoadingCreation = true;
                        });
                        updateBooking();
                      }
                    }else{
                      if(_selectedFacility == null){
                        Resources.showToast(
                            "Please input all data", Colors.red);
                      } else {
                        setState(() {
                          isLoadingCreation = true;
                        });
                        updateBooking();
                      }
                    }

                  },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: themeGolderColor,
                      textStyle: gcTitleTextStyleBlack),
                  child: Container(
                      height: 42,
                      child: Center(
                          child: Text(
                            "Update",
                            style: gcTitleTextStyleBlack,
                          ))),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _showDatePicker(ctx, tag) {
    // showCupertinoModalPopup is a built-in function of the cupertino library
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
          height: 400,
          color: Color.fromARGB(255, 255, 255, 255),
          child: Column(
            children: [
              Container(
                height: 300,
                child: CupertinoDatePicker(
                    initialDateTime: DateTime.now(),
                    // maximumDate: DateTime.now()  ,
                    minimumYear: DateTime.now().year,
                    use24hFormat: false,
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (val) {
                      setState(() {
                        if (tag == 1) {
                          _chosenDateTime1 = val;
                          startDate = DateFormat("yyyy-MM-dd")
                              .format(_chosenDateTime1);
                          // var time = DateFormat.Hm().format(_chosenDateTime1);

                          // .format(_chosenDateTime1);
                          print(startDate);
                          // print(time);
                        } else {
                          print("none");
                        }
                      });
                    }),
              ),

              // Close the modal
              Container(
                height: 50,
                width: 100,
                color: themeGolderColor,
                child: CupertinoButton(
                  child: Text(
                    'OK',
                    style: gcTitleTextStyle,
                  ),
                  onPressed: () => Navigator.of(ctx).pop(),
                ),
              )
            ],
          ),
        ));
  }

  updateBooking() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/banquet-bookings/${widget.selectedEvent.id.toString()}/update";
    print('url---> $url');

    Map params = {
      "id": widget.selectedEvent.id,
      "date": startDate,
      "hall": Resources.selectedHall?.name ?? '',
      "eventType": _selectedFacility?.name,
      "accountType": selectedIndex == 0 ? "account" : "event",
      "clubAcNo": selectedMember == null ? "" : selectedMember?.clubAcNumber.toString(),
      "remarks": _remarksController.text
    };
    print(params);
    if (result == true) {
      try {
        http
            .put(Uri.parse(url), body: jsonEncode(params), headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
        })
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(utf8.decode(value.bodyBytes));

          if (value.statusCode == 401) {
            SharedPref.remove('user');

            Navigator.pushNamedAndRemoveUntil(
                context, '/welcome', (route) => false);
            return;
          } else if (value.statusCode == 200) {
            setState(() {
              isLoadingCreation = false;
            });

            final userResponse =
            BookingCreationErrorResponse.fromJson(data);
            Resources.showAlert(userResponse.data.toString(),context,true);
          } else {
            setState(() {
              isLoadingCreation = false;
            });
            final userResponse =
            BookingCreationErrorResponse.fromJson(data);
            Resources.showToast(userResponse.data.toString(), Colors.red);
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoadingCreation = false;
        });
        Fluttertoast.showToast(
          msg: 'Error-${e.toString()} }',
          backgroundColor: Colors.red,
          gravity: ToastGravity.CENTER,
        );
      }
    } else {
      print('No internet :( Reason:');
      Fluttertoast.showToast(
        msg: ' ${NetworkManager.internetErrorMessage}',
        backgroundColor: Colors.red,
        gravity: ToastGravity.CENTER,
      );

    }
  }
}
