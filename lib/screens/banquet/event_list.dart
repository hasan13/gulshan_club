import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/banquet/booking_list_response.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/banquet/edit_booking_screen.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_response.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_webview_payment.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';

import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

import '../../jsonModels/banquet/booking_creation_error_response.dart';
import '../../utils/shared_preference.dart';

class BanquetEventListScreen extends StatefulWidget {
  static const String route = '/banquet_events';

  @override
  _BanquetEventListScreenState createState() => _BanquetEventListScreenState();
}

class _BanquetEventListScreenState extends State<BanquetEventListScreen> {
  int selectedIndex = -1;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  TextEditingController _amountController = TextEditingController();
  final FocusNode _amountNode = FocusNode();

  bool isLoading = false;
  AppUser? userInfo;
  bool isPayForOtherSelected = false;

  bool isDataLoading = false;

  Map<String, List<BookingDatum>>? bookingListResponse;
  List<String> keys = [];

  DateTimeRange? _selectedDateRange;

  bool isLoadingCreation = false;

  // This function will be triggered when the floating button is pressed

  @override
  void initState() {
    // TODO: implement initState
    getData();
    getBookingList();
    super.initState();
  }

  getData() async {
    await getCurrentUserInfo();
  }

  getCurrentUserInfo() async {
    print(Resources.currentUser?.accessToken);
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  userInfo = userResponse.data;
                  Resources.appUser = userResponse.data;
                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  isLoading = false;

                  var accountNo = userResponse.data?.clubAcNumber;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  void _show() async {
    final DateTimeRange? result = await showDateRangePicker(
      context: context,
      firstDate: DateTime(2020, 1, 1),
      lastDate: DateTime(2050, 12, 31),
      currentDate: DateTime.now(),
      saveText: 'Done',
    );

    if (result != null) {
      // Rebuild the UI
      print(result.start.toString());
      setState(() {
        _selectedDateRange = result;
        getBookingList();
      });
    }
  }

  getBookingList() async {
    print(Resources.currentUser?.accessToken);
    bool result = await InternetConnectionChecker().hasConnection;
    String url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/banquet-bookings/all/${Resources.selectedHall?.name.toString()}/";
    if (_selectedDateRange != null) {
      url = url +
          "?start_date=${_selectedDateRange?.start.toString().split(' ')[0]}&end_date=${_selectedDateRange?.end.toString().split(' ')[0]}";
    }
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = BookingListResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  if (userResponse.data != null) {
                    bookingListResponse = userResponse.data!;
                    keys = userResponse.data!.keys.toList();
                  }
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        centerTitle: true,
        leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: themeGolderColor,
            )),
        title: Text(
          "Banquet Booking",
          style: gcTitleTextStyle,
        ),
      ),
      bottomNavigationBar: (Resources.appUser?.banquetBookingEdit == true)
          ? Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 40.0, vertical: 24),
              child: ElevatedButton(
                onPressed: () async {
                  if (Resources.appUser?.banquetBookingEdit == true) {
                    Resources.navigateTo(context, '/book_schedule')
                        .then((value) {
                      getBookingList();
                    });
                  } else {
                    Resources.showToast(
                        "You don't have permission for add new booking",
                        Colors.red);
                  }
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: themeGolderColor,
                    textStyle: gcTitleTextStyleBlack),
                child: Container(
                    height: 42,
                    child: Center(
                        child: Text(
                      "Add Schedule",
                      style: gcTitleTextStyleBlack,
                    ))),
              ),
            )
          : SizedBox(),
      body: (isLoading == true)
          ? Center(
              child: CircularProgressIndicator(
              backgroundColor: themeGolderColor,
            ))
          : Padding(
              padding: const EdgeInsets.only(top: 18.0, left: 16, right: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          width: 120,
                          child: RichText(
                            text: TextSpan(
                              text: 'Hall: ',
                              style: GoogleFonts.poppins(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  color: listDescriptionColor),
                              children: <TextSpan>[
                                TextSpan(
                                    text:
                                        Resources.selectedHall?.value.toString(),
                                    style: GoogleFonts.poppins(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                        color: Colors.white)),
                              ],
                            ),
                          )),
                      InkWell(
                        onTap: _show,
                        child: Container(
                          width: MediaQuery.of(context).size.width - 180,
                          height: 32,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: themeGolderColor)),
                          child: FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                InkWell(
                                  onTap: _show,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: Icon(
                                      Icons.calendar_month,
                                      color: themeGolderColor,
                                    ),
                                  ),
                                ),
                                // SizedBox(width: 16,),
                                InkWell(
                                  onTap: _show,
                                  child: (_selectedDateRange == null)
                                      ? Text(
                                          'Select date',
                                          style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12,
                                              color: themeGolderColor),
                                        )
                                      : Text(
                                          "${_selectedDateRange?.start.toString().split(' ')[0]} to"
                                              " ${_selectedDateRange?.end.toString().split(' ')[0]}",
                                          style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12,
                                              color: themeGolderColor),
                                        ),
                                ),
                                //
                                // ListTile(
                                //   leading: Icon(Icons.calendar_month,color: themeGolderColor,),
                                //   horizontalTitleGap: 0,
                                //   dense: true,contentPadding: EdgeInsets.all(0),
                                //   title: Text('Select date',style: GoogleFonts.poppins(
                                //       fontWeight: FontWeight.w400,fontSize: 12,color: listDescriptionColor
                                //   ),),
                                //   onTap: (){
                                //
                                //   },
                                //
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  // Text("Hall: ${Resources.selectedHallName}",style: gcNormalTextStyle,),
                  if (bookingListResponse != null)
                    Expanded(
                      child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: keys.length,
                        itemBuilder: (context, index) {
                          // DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
                          // DateTime dateTime =
                          // dateFormat.parse(itemList[index].updatedAt.toString());
                          // String string = dateFormat.format(dateTime);
                          // print(string);

                          return Padding(
                            padding: EdgeInsets.only(top: 8, bottom: 8),
                            child: Container(
                              decoration: BoxDecoration(
                                color: themeGrayIconBackColor,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8.0, left: 16),
                                    child: Text(
                                      keys[index].toString(),
                                      style: GoogleFonts.poppins(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                          color: themeGolderColor),
                                    ),
                                  ),
                                  if(bookingListResponse != null)Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: setUpItemList(context, 0,
                                        bookingListResponse![keys[index]]!),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                ],
              ),
            ),
    );
  }

  setUpItemList(BuildContext context, int position, List<BookingDatum> items) {
    List<Widget> commentWidgets = [];

    for (var i = 0; i < items.length; i++) {
      String name = '';
      if (items[i].userInfo != null) {
        name = name + items[i].userInfo!.firstName.toString();
      }
      if (items[i].userInfo != null) {
        name = name + ' ' + items[i].userInfo!.lastName.toString();
      }

      commentWidgets.add(Padding(
        padding:
            const EdgeInsets.only(left: 16.0, right: 16, top: 8, bottom: 8),
        child: GestureDetector(
          onTap: () {},
          child: Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: themeBlackColor,
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Stack(
                    // alignment: Alignment.topRight,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (items[i].accountType.toString() == "event")
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: RichText(
                                text: TextSpan(
                                  text: 'A/C           :  ',
                                  style: GoogleFonts.poppins(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w300,
                                      color: listDescriptionColor),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text:
                                            (items[i].accountType.toString() ==
                                                    "account")
                                                ? items[i].clubAcNo.toString()
                                                : "Club Event",
                                        style: GoogleFonts.poppins(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                            color: Colors.white)),
                                  ],
                                ),
                              ),
                            ),
                          if (items[i].accountType.toString() == "account")
                            Row(
                              children: [
                                CircleAvatar(
                                  radius: 32,
                                  backgroundColor: themeGolderBackColor,
                                  backgroundImage: (items[i].userInfo?.imageMedium !=
                                          null)
                                      ? NetworkImage(items[i]
                                          .userInfo
                                          !.imageMedium
                                          .toString())
                                      : AssetImage(
                                      'assets/icons/ic_placeholder.png') as ImageProvider,
                                  //AssetImage('assets/images/Facilities1.jpg'),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        if (name != '')
                                          Text(
                                            name,
                                            style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: listDescriptionColor),
                                          ),
                                        RichText(
                                          text: TextSpan(
                                            text: 'A/C  ',
                                            style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w300,
                                                color: listDescriptionColor),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: (items[i]
                                                              .accountType
                                                              .toString() ==
                                                          "account")
                                                      ? items[i]
                                                          .clubAcNo
                                                          .toString()
                                                      : "Club Event",
                                                  style: GoogleFonts.poppins(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      color: Colors.white)),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          // if(items[i].accountType.toString() == "account") RichText(
                          //    text: TextSpan(
                          //      text: 'Name      :  ',
                          //      style: GoogleFonts.poppins(
                          //          fontSize: 12,
                          //          fontWeight: FontWeight.w300,
                          //          color: listDescriptionColor),
                          //      children: <TextSpan>[
                          //        TextSpan(
                          //            text: (items[i].accountType == "account")
                          //                ? items[i].userInfo.firstName.toString() +
                          //                    " " +
                          //                    items[i].userInfo.lastName.toString()
                          //                : "Club Event",
                          //            style: GoogleFonts.poppins(
                          //                fontSize: 12,
                          //                fontWeight: FontWeight.w400,
                          //                color: listDescriptionColor)),
                          //      ],
                          //    ),
                          //  ),
                         if(items[i]
                             .eventType != null) Padding(
                            padding: const EdgeInsets.only(
                                left: 8.0, top: 8, bottom: 8),
                            child: RichText(
                              text: TextSpan(
                                text: 'Event       :  ',
                                style: GoogleFonts.poppins(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300,
                                    color: listDescriptionColor),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: items[i]
                                          .eventType!
                                          .toUpperCase()
                                          .replaceAll(
                                              RegExp('[^A-Za-z0-9]'), ' '),
                                      style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600,
                                          color: themeGolderColor)),
                                ],
                              ),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: RichText(
                              text: TextSpan(
                                text: 'Remarks : ',
                                style: GoogleFonts.poppins(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w300,
                                    color: listDescriptionColor),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: items[i]
                                          .remarks
                                          .toString(), //"items[i]. remarks. toString() items[i]. remarks. toString() items[i]. remarks. toString()",
                                      style: GoogleFonts.poppins(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: listDescriptionColor)),
                                ],
                              ),
                            ),
                          ),
                          // Text("Remarks", style:GoogleFonts.poppins(
                          //     fontSize: 12,
                          //     fontWeight: FontWeight.w300,
                          //     color: listDescriptionColor)),
                          // Text(items[i].remarks.toString(), style:GoogleFonts.poppins(
                          //     fontSize: 12,
                          //     fontWeight: FontWeight.w300,
                          //     color: listDescriptionColor)),

                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.start,
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   children: [
                          //     Text(
                          //       'Remarks   :',
                          //       style: GoogleFonts.poppins(
                          //           fontSize: 12,
                          //           fontWeight: FontWeight.w300,
                          //           color: listDescriptionColor),
                          //     ),
                          //     Expanded(
                          //       child: Text(items[i].remarks.toString(),
                          //           style: GoogleFonts.poppins(
                          //               fontSize: 12,
                          //               fontWeight: FontWeight.w400,
                          //               color: listDescriptionColor)),
                          //     ),
                          //
                          //
                          //   ],
                          // ),
                        ],
                      ),
                      // if (Resources.appUser.banquetBookingEdit == true)
                      //   Align(
                      //     alignment: Alignment.centerRight,
                      //     child: PopupMenuButton(
                      //       onSelected: (value) {
                      //         if (value == 1) {
                      //           print("edit clickd");
                      //
                      //           Navigator.push(
                      //             context,
                      //             MaterialPageRoute(
                      //               builder: (context) => EditBookingScreen(
                      //                 selectedEvent: items[i],
                      //               ),
                      //             ),
                      //           ).then((value) {
                      //             getBookingList();
                      //           });
                      //         } else if (value == 2) {
                      //            showDialog(
                      //               context: context,
                      //               builder: (context) => new AlertDialog(
                      //             title: new Text('Are you sure?'),
                      //             content: new Text('Do you want to delete this booking?'),
                      //             actions: <Widget>[
                      //               new TextButton(
                      //                 onPressed: () => Navigator.of(context).pop(false),
                      //                 child: new Text('No'),
                      //               ),
                      //               new TextButton(
                      //                 onPressed: () => deleteBooking(bookingInfo: items[i]),
                      //                 child: new Text('Yes'),
                      //               ),
                      //             ],
                      //           ),
                      //         );
                      //         } else {}
                      //       },
                      //       icon: Icon(
                      //         Icons.more_vert,
                      //         color: Colors.white,
                      //       ),
                      //       itemBuilder: (ctx) => [
                      //         // _buildPopupMenuItem(
                      //         //   'Edit',
                      //         //   Icons.edit,
                      //         //   (){
                      //         //     print("edit clickd");
                      //         //     Resources.navigateTo(ctx, '/edit_booking');
                      //         //   }
                      //         // ),
                      //         PopupMenuItem(
                      //           value: 1,
                      //           child: Center(
                      //               child: Text(
                      //             "Edit",
                      //             textAlign: TextAlign.center,
                      //           )),
                      //         ),
                      //         PopupMenuItem(
                      //           value: 2,
                      //           child: Center(
                      //               child: Text(
                      //             "Delete",
                      //             textAlign: TextAlign.center,
                      //           )),
                      //         ),
                      //       ],
                      //     ),
                      //   ),
                    ],
                  ),
                ),
              ),
              if (Resources.appUser?.banquetBookingEdit == true)
                Align(
                  alignment: Alignment.centerRight,
                  child: PopupMenuButton(
                    iconSize: 16,
                    onSelected: (value) {
                      if (value == 1) {
                        print("edit clickd");

                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => EditBookingScreen(
                              selectedEvent: items[i],
                            ),
                          ),
                        ).then((value) {
                          getBookingList();
                        });
                      } else if (value == 2) {
                        showDialog(
                          context: context,
                          builder: (context) => new AlertDialog(
                            title: new Text('Are you sure?'),
                            content:
                                new Text('Do you want to delete this booking?'),
                            actions: <Widget>[
                              new TextButton(
                                onPressed: () =>
                                    Navigator.of(context).pop(false),
                                child: new Text('No'),
                              ),
                              new TextButton(
                                onPressed: () =>
                                    deleteBooking(bookingInfo: items[i]),
                                child: new Text('Yes'),
                              ),
                            ],
                          ),
                        );
                      } else {}
                    },
                    icon: Icon(
                      Icons.more_vert,
                      color: Colors.white,
                    ),
                    itemBuilder: (ctx) => [
                      // _buildPopupMenuItem(
                      //   'Edit',
                      //   Icons.edit,
                      //   (){
                      //     print("edit clickd");
                      //     Resources.navigateTo(ctx, '/edit_booking');
                      //   }
                      // ),
                      PopupMenuItem(
                        value: 1,
                        child: Center(
                            child: Text(
                          "Edit",
                          textAlign: TextAlign.center,
                        )),
                      ),
                      PopupMenuItem(
                        value: 2,
                        child: Center(
                            child: Text(
                          "Delete",
                          textAlign: TextAlign.center,
                        )),
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ),
      ));
    }

    return commentWidgets;
  }

  PopupMenuItem _buildPopupMenuItem(
      String title, IconData iconData, VoidCallback onTap, int value) {
    return PopupMenuItem(
      onTap: onTap,
      value: value,
      child: Center(
          child: Text(
        title,
        textAlign: TextAlign.center,
      )),
    );
  }

  deleteBooking({required BookingDatum bookingInfo}) async {
    Navigator.of(context).pop(true);

    print("delete booking");
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/banquet-bookings/${bookingInfo.id.toString()}/delete";
    print('url---> $url');

    if (result == true) {
      try {
        http
            .delete(Uri.parse(url), headers: {
              "Content-Type": "application/json",
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              } else if (value.statusCode == 200) {
                setState(() {
                  isLoadingCreation = false;
                });
                final userResponse =
                    BookingCreationErrorResponse.fromJson(data);
                // Resources.showAlert(userResponse.data.toString(),context);
                // set up the button
                Widget okButton = ElevatedButton(
                  child: Text("OK"),
                  onPressed: () {
                    Navigator.pop(context, true);
                    getBookingList();
                  },
                );

                // set up the AlertDialog
                AlertDialog alert = AlertDialog(
                  title: Center(
                      child: SvgPicture.asset(
                    'assets/icons/ic_logo.svg',
                    width: 80.0,
                    height: 65,
                  )),
                  content: Text("Deleted Successfully!"),
                  actions: [
                    okButton,
                  ],
                );

                // show the dialog
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return alert;
                  },
                );
              } else {
                setState(() {
                  isLoadingCreation = false;
                });
                final userResponse =
                    BookingCreationErrorResponse.fromJson(data);
                Resources.showToast(userResponse.data.toString(), Colors.red);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoadingCreation = false;
        });
        Fluttertoast.showToast(
          msg: 'Error-${e.toString()} }',
          backgroundColor: Colors.red,
          gravity: ToastGravity.CENTER,
        );
      }
    } else {
      print('No internet :( Reason:');
      Fluttertoast.showToast(
        msg: ' ${NetworkManager.internetErrorMessage}',
        backgroundColor: Colors.red,
        gravity: ToastGravity.CENTER,
      );
    }
  }
}
