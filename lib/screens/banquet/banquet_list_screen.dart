import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_response.dart';
import 'package:gulshan_club_app/screens/payments/nagad_payment/nagad_webview_payment.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../../jsonModels/banquet/banquet_response.dart';

class BanquetListScreen extends StatefulWidget {
  static const String route = '/halls';

  @override
  _BanquetListScreenState createState() => _BanquetListScreenState();
}

class _BanquetListScreenState extends State<BanquetListScreen> {

  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  TextEditingController _amountController = TextEditingController();
  final FocusNode _amountNode = FocusNode();

  bool isLoading = false;
  AppUser? userInfo;

  bool isDataLoading = false;

  List<HallDatum> halls = [];
  // List<String> hallNames = [];
  int selectedIndex = -1;
  HallDatum? selectedHall ;


  @override
  void initState() {
    // TODO: implement initState

    getCurrentUserInfo();
    getBanquetList();

    super.initState();
  }
  getCurrentUserInfo() async {
    print(Resources.currentUser?.accessToken);
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
          "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
        })
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(value.body);
          final userResponse = CurrentUserInfoResponse.fromJson(data);
          print('result....\n ${userResponse.data}');
          if (value.statusCode == 200 || value.statusCode == 202) {
            setState(() {
              userInfo = userResponse.data;
              Resources.mobileNo = userResponse.data?.phonePrimary;
              isLoading = false;

              var accountNo = userResponse.data?.clubAcNumber;
              // letterString =
              //     accountNo.replaceAll(new RegExp(r'[^A-Za-z]'), '');
              // numberString =
              //     accountNo.replaceAll(new RegExp(r'[^0-9]'), '');
              //
              // print(letterString);
              // print(numberString);
            });
            //navigateToSignInPage(context);
            print('YAY! Free cute dog pics!');
          } else if (data['status'] == false) {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            setState(() {
              isLoading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  getBanquetList() async {
    print(Resources.currentUser?.accessToken);
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/banquet-bookings/halls";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = HallListResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  halls = userResponse.data ?? [];
                  // Resources.selectedUserIDForPayment = userInfo.id.toString();
                  isLoading = false;

                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    // final bool isPayForOthersSelected =
    //     ModalRoute.of(context).settings.arguments;
    //
    // setState(() {
    //   isPayForOtherSelected = isPayForOthersSelected;
    // });
    // print('isPayForOthersSelected-----> $isPayForOtherSelected');

    return ThemeScaffold(
      pageTitle: 'Banquet Booking',
      bottomNavigationBar: Container(
        height: 132,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 0),
              child: ElevatedButton(
                onPressed: () async {

                  if(selectedHall != null){
                    await Resources.navigateTo(context, '/banquet_events');
                  }else{
                    Resources.showToast("Select a Hall please", Colors.red);
                  }

                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: themeGolderColor,
                    textStyle: gcTitleTextStyleBlack),
                child: Container(
                    height: 42,
                    child: Center(
                        child: Text(
                      "Next",
                      style: gcTitleTextStyleBlack,
                    ))),
              ),
            ),
            Resources.setBottomDrawer(context)

          ],
        ),
      ),
      body: (isLoading == true )
            ? Center(
                child: CircularProgressIndicator(
                backgroundColor: themeGolderColor,
              ))
            : ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: halls.length ?? 0,
        itemBuilder: (context, index) {
          // DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
          // DateTime dateTime =
          // dateFormat.parse(itemList[index].updatedAt.toString());
          // String string = dateFormat.format(dateTime);
          // print(string);

          return  Padding(
            padding: const EdgeInsets.symmetric(
                vertical: 8, horizontal: 20.0),
            child: ListTile(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              tileColor: themeGrayIconBackColor, //themeGrayBackColor,
              trailing: Padding(
                padding: const EdgeInsets.all(8.0),
                child: selectedIndex == index
                    ? SvgPicture.asset(
                  'assets/icons/Payment_Selected_Radio_Button.svg',
                  // color: themeGolderColor,
                )
                    : SvgPicture.asset(
                  'assets/icons/UnselectedRadioButton.svg',
                  // color: themeGolderColor,
                ),
              ),
              leading: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: Container(
                    color: themeGrayBackColor,
                    height: 40,
                    width: 40,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SvgPicture.asset(
                        'assets/icons/hotel 1.svg',
                        // color: themeGolderColor,
                      ),
                    ),
                  )),
              title: Text(
                halls[index].value.toString(),
                style: gcTitleTextStyle,
              ),
              selected: false,
              onTap: () async {
                setState(() {
                  selectedIndex = index;
                  selectedHall = halls[index];
                  Resources.selectedHall =  halls[index];
                  print(selectedHall?.value);
                });
                // await _navigateTo(context, '/building_directory');
              },
            ),
          );
        },
      ),
    );
  }
}
