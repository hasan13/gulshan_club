

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
 import 'package:flutter_svg/flutter_svg.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;


class PDFViewerFromUrl extends StatelessWidget {
  final String url;

  const PDFViewerFromUrl({Key? key,  required this.url}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        title: Text("Employee List",style: ScaffoldAppBarTitle,),
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
                height: 10,
                width: 10,
                child: IconButton(
                    color: themeGolderColor,
                    icon: SvgPicture.asset('assets/icons/ic_back.svg',color: themeGolderColor,height: 20,width: 20,),
                  onPressed: () {
                    Navigator.pop(context);

                  },))),
        centerTitle: true,
      ),
      body: const PDF(
        fitEachPage: true,
        fitPolicy: FitPolicy.BOTH,

      ).fromUrl(
        NetworkManager.pdfUrl,
        placeholder: (double progress) => Center(child: Text('$progress %',style: buttonTitleStyle,)),
        errorWidget: (dynamic error) => Center(child: Text(error.toString())),
      ),
    );
  }
}
