// import 'dart:convert';
//
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/painting.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
// import 'package:gulshan_club_app/jsonModels/member_details_response.dart';
// import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
// import 'package:gulshan_club_app/models/resource.dart';
// import 'package:gulshan_club_app/networking/network_manager.dart';
// import 'package:gulshan_club_app/theme/theme_colors.dart';
// import 'package:gulshan_club_app/theme/theme_scafold.dart';
// import 'package:gulshan_club_app/theme/theme_style.dart';
// import 'package:http/http.dart' as http;
// import 'package:rflutter_alert/rflutter_alert.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// class MemberDetailsScreen extends StatefulWidget {
//   static const String route = '/member_details';
//
//   @override
//   _MemberDetailsScreenState createState() => _MemberDetailsScreenState();
// }
//
// class _MemberDetailsScreenState extends State<MemberDetailsScreen> {
//   var isLoading = true;
//   final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
//
//   Member userInfo;
//
//   int tabIndex = 1;
//   var letterString = "";
//   var numberString = "";
//
//   List<Relation> relations = [];
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     getMemberDetails();
//
//     super.initState();
//   }
//
//   getMemberDetails() async {
//     print(Resources.currentUser.accessToken);
//     bool result = await DataConnectionChecker().hasConnection;
//     var url = NetworkManager.baseUrl +
//         NetworkManager.apiString +
//         NetworkManager.versionString +
//         "get-user-relation-details/${Resources.selectedMemberForDetails.id.toString()}";
//     print('url---> $url');
//     if (result == true) {
//       try {
//         http
//             .get(Uri.parse(url), headers: {
//               "Authorization": "Bearer ${Resources.currentUser.accessToken}"
//             })
//             .timeout(Duration(seconds: 30))
//             .then((value) {
//               print("status code Token:${value.statusCode}");
//               print(value.body);
//               final data = json.decode(value.body);
//               final userResponse = MemberDetailsResponse.fromJson(data);
//               print('result....\n ${userResponse.user}');
//               if (value.statusCode == 200 || value.statusCode == 202) {
//                 setState(() {
//                   userInfo = userResponse.user;
//                   relations = userResponse.relation;
//                 });
//                 //navigateToSignInPage(context);
//                 print('YAY! Free cute dog pics!');
//               } else if (data['status'] == false) {
//                 setState(() {
//                   isLoading = false;
//                 });
//                 final snackBar = SnackBar(
//                   backgroundColor: Colors.red,
//                   content: Text(
//                       'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
//                 );
//                 _scaffoldKey.currentState.showSnackBar(snackBar);
//                 //ScaffoldMessenger.of(context).showSnackBar(snackBar);
//               } else {
//                 setState(() {
//                   isLoading = false;
//                 });
//               }
//             });
//       } catch (e) {
//         print(e);
//         setState(() {
//           isLoading = false;
//         });
//       }
//     } else {
//       print('No internet :( Reason:');
//       print(DataConnectionChecker().lastTryResults);
//
//       final snackBar = SnackBar(
//         backgroundColor: Colors.red,
//         content: Text('Internet is not available!'),
//       );
//       setState(() {
//         isLoading = false;
//       });
//       _scaffoldKey.currentState.showSnackBar(snackBar);
//       //ScaffoldMessenger.of(context).showSnackBar(snackBar);
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return ThemeScaffold(
//       pageTitle: 'Member Details',
//       // bottomNavigationBar: Resources.setBottomDrawer(context),
//       bottomNavigationBar: new Theme(
//         data: Theme.of(context).copyWith(
//             // sets the background color of the `BottomNavigationBar`
//             canvasColor: themeBlackColor,
//             // sets the active color of the `BottomNavigationBar` if `Brightness` is light
//             primaryColor: Colors.red,
//             textTheme: Theme.of(context).textTheme.copyWith(
//                 caption: new TextStyle(
//                     color: Colors
//                         .yellow))), // sets the inactive color of the `BottomNavigationBar`
//         child: BottomNavigationBar(
//             selectedItemColor: themeGolderColor,
//             unselectedItemColor: Colors.grey[400],
//             iconSize: 30,
//             backgroundColor: Colors.blue,
//             currentIndex: tabIndex,
//             onTap: (int index) {
//               setState(() {
//                 tabIndex = index;
//                 print("tabIndex----$tabIndex");
//                 Resources.moveTo(index, context);
//               });
//             },
//             items: [
//               BottomNavigationBarItem(
//                   activeIcon: Icon(
//                     Icons.home,
//                     color: themeGolderColor,
//                     size: 30,
//                   ),
//                   icon: Icon(
//                     Icons.home,
//                     color: themeGreyColor,
//                     size: 28,
//                   ),
//                   label: ''),
//
//               BottomNavigationBarItem(
//                   activeIcon: SvgPicture.asset(
//                     "assets/icons/ic_profile.svg",
//                     color: themeGolderColor,
//                     width: 30,
//                     height: 30,
//                   ),
//                   icon: SvgPicture.asset(
//                     "assets/icons/ic_profile.svg",
//                     color: themeGreyColor,
//                     width: 26,
//                     height: 26,
//                   ),
//                   label: ''),
//               BottomNavigationBarItem(
//                   activeIcon: SvgPicture.asset(
//                     "assets/icons/ic_memeber_director.svg",
//                     color: themeGolderColor,
//                     width: 30,
//                     height: 30,
//                   ),
//                   icon: SvgPicture.asset(
//                     "assets/icons/ic_memeber_director.svg",
//                     color: themeGreyColor,
//                     width: 26,
//                     height: 26,
//                   ),
//                   label: ''),
//
//               BottomNavigationBarItem(
//                   activeIcon: SvgPicture.asset(
//                     "assets/icons/ic_notice_board.svg",
//                     color: themeGolderColor,
//                     width: 30,
//                     height: 30,
//                   ),
//                   icon: SvgPicture.asset(
//                     "assets/icons/ic_notice_board.svg",
//                     color: themeGreyColor,
//                     width: 26,
//                     height: 26,
//                   ),
//                   label: ''),
//
//               // BottomNavigationBarItem(
//               //     activeIcon: SvgPicture.asset(
//               //       "assets/icons/ic_facilities.svg",
//               //       color: themeGolderColor,
//               //       width: 30,
//               //       height: 30,
//               //     ),
//               //     icon: SvgPicture.asset(
//               //       "assets/icons/ic_facilities.svg",
//               //       color: themeGreyColor,
//               //       width: 26,
//               //       height: 26,
//               //     ),
//               //     label: ''),
//             ]),
//       ),
//       body: isLoading
//           ? Center(
//               child: CircularProgressIndicator(),
//             )
//           : userInfo == null
//               ? Center(
//                   child: Text(
//                   "Something went wrong",
//                   style: GoogleFonts.poppins(color: themeGreyColor),
//                 ))
//               : createListView(userInfo, context),
//     );
//   }
//
//   Widget createListView(Member result, BuildContext context) {
//     //Widget createListView2(List<Recipe> data, BuildContext context) {
//
//     return SingleChildScrollView(
//       child: Column(
//         children: [
//           Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Center(
//               child: Container(
//                 decoration: BoxDecoration(
//                   color: Colors.transparent,
//                   borderRadius: BorderRadius.circular(10),
//                 ),
//                 height: 72,
//                 width: 72,
//                 // child: (result.imageMedium != null)
//                 //     ? ClipRRect(
//                 //         borderRadius: BorderRadius.all(Radius.circular(8)),
//                 //         child: CachedNetworkImage(
//                 //           imageUrl: NetworkManager.baseUrl + result.imageMedium,
//                 //           imageBuilder: (context, imageProvider) => Container(
//                 //             decoration: BoxDecoration(
//                 //               borderRadius: BorderRadius.circular(10),
//                 //               image: DecorationImage(
//                 //                   image: imageProvider,
//                 //                   fit: BoxFit.fitHeight,
//                 //                   colorFilter: ColorFilter.mode(
//                 //                       Colors.transparent, BlendMode.color)),
//                 //             ),
//                 //           ),
//                 //           placeholder: (context, url) =>
//                 //               Center(child: CircularProgressIndicator()),
//                 //           errorWidget: (context, url, error) =>
//                 //               Icon(Icons.error),
//                 //         ),
//                 //       )
//                 //     : Image.asset(
//                 //         'assets/icons/ic_placeholder.png',
//                 //         height: 120,
//                 //         width: 100,
//                 //       ),
//               ),
//               // CircleAvatar(
//               //   radius: 49,
//               //   backgroundColor: themeGreyColor,
//               //   child: CircleAvatar(
//               //     radius: 47,
//               //     backgroundColor: Colors.transparent,
//               //     backgroundImage: (result.imageMedium != null)
//               //         ? NetworkImage(result.imageMedium)
//               //         : AssetImage('assets/icons/ic_placeholder.png'),
//               //     //AssetImage('assets/images/Facilities1.jpg'),
//               //   ),
//               // ),
//             ),
//           ),
//           Text(
//             result.firstName.toString() +
//                 ' ' +
//                 result.lastName.toString(), //"Rubel Aziz",
//             style: gcKLargeListTitleTextStyle, softWrap: true,
//             overflow: TextOverflow.ellipsis, textAlign: TextAlign.center,
//           ),
//           if (result.designation != null)
//             Text(
//               result.designation.toString(), // "Director",
//               style: gcDescriptionTextStyle,
//             ),
//           SizedBox(
//             height: 8,
//           ),
//           Divider(
//             height: 1,
//             color: themeGreyColor,
//           ),
//           SizedBox(
//             height: 8,
//           ),
//           Padding(
//             padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
//             child: Row(
//               children: [
//                 Text(
//                   'Club A/C #',
//                   style: gcGreyDescriptionTextStyle,
//                 ),
//                 SizedBox(
//                   width: 8,
//                 ),
//                 (result.clubAcNumber != null)
//                     ? Text(
//                         letterString + "-" + numberString, //'DR-7',
//                         style: gcNormalTextStyle,
//                       )
//                     : SizedBox(),
//               ],
//             ),
//           ),
//
//           Padding(
//             padding: const EdgeInsets.only(left: 20.0, top: 8),
//             child: Row(
//               children: [
//                 SvgPicture.asset(
//                   'assets/icons/ic_call.svg',
//                   color: themeWhiteColor,
//                   height: 16,
//                   width: 16,
//                 ),
//                 SizedBox(
//                   width: 16,
//                 ),
//                 GestureDetector(
//                   onTap: () async {
//                     print('${result.phonePrimary.toString()}');
//                     //launch("tel://${itemList[index].mobileNumberPrimary}");
//
//                     String url = "tel://" + result.phonePrimary.toString();
//                     if (await canLaunch(url)) {
//                       await launch(url);
//                     } else {
//                       throw 'Could not call ${result.phonePrimary.toString()}';
//                     }
//                   },
//                   child: Text(
//                     result.phonePrimary.toString(), //'01730084093',
//                     style: gcNormalTextStyle,
//                   ),
//                 )
//               ],
//             ),
//           ),
//
//           Padding(
//             padding: const EdgeInsets.only(left: 20.0, top: 10),
//             child: Row(
//               children: [
//                 SvgPicture.asset(
//                   'assets/icons/ic_mail.svg',
//                   color: themeWhiteColor,
//                   height: 16,
//                   width: 16,
//                 ),
//                 SizedBox(
//                   width: 16,
//                 ),
//                 Expanded(
//                   child: Text(
//                     result.email.toString(), //'rubelaziz@partyex.net',
//                     style: gcNormalTextStyle,
//                   ),
//                 ),
//               ],
//             ),
//           ),
//
//           Padding(
//             padding: const EdgeInsets.only(left: 20.0, top: 8, bottom: 8),
//             child: Row(
//               children: [
//                 Text(
//                   'Member Since',
//                   style:
//                       GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//                 ),
//                 SizedBox(
//                   width: 16,
//                 ),
//                 (result.membershipDate != null)
//                     ? Expanded(
//                         child: Text(
//                           result.membershipDate.toString(), //'Donor',
//                           style: gcNormalTextStyle,
//                         ),
//                       )
//                     : Text(
//                         "data not available",
//                         style: gcNormalTextStyle,
//                       )
//               ],
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(left: 20.0, top: 8, bottom: 8),
//             child: Row(
//               children: [
//                 Text(
//                   'Address ',
//                   style:
//                       GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//                 ),
//                 SizedBox(
//                   width: 16,
//                 ),
//                 (result.address != null)
//                     ? Expanded(
//                         child: Text(
//                           result.address.toString(), //'Donor',
//                           style: gcNormalTextStyle,
//                         ),
//                       )
//                     : Text(
//                         " not available",
//                         style: gcNormalTextStyle,
//                       )
//               ],
//             ),
//           ),
//
//           SizedBox(
//             height: 8,
//           ),
//           Divider(
//             height: 1,
//             color: themeGreyColor,
//           ),
//           // new ListTile(
//           //   dense: true,
//           //   contentPadding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 16, right: 16),
//           //   hoverColor: themeGreyColor,
//           //   trailing: Container(
//           //       height: 20,
//           //       width: 20,
//           //       child: Icon(
//           //         Icons.chevron_right,
//           //         color: Colors.white,
//           //       )),
//           //   title: Text('Account Statements', style: gcNormalTextStyle),
//           //   selected: false,
//           //   onTap: () async {
//           //     setState(() {});
//           //     //await _navigateTo(context, '/notice');
//           //     Navigator.pushNamed(context, '/statements');
//           //   },
//           // ),
//           // Divider(
//           //   height: 1,
//           //   color: themeGreyColor,
//           // ),
//           new ListTile(
//             contentPadding:
//                 EdgeInsets.only(top: 0.0, bottom: 0.0, left: 16, right: 16),
//             dense: true,
//             hoverColor: themeGreyColor,
//             trailing: Container(
//                 height: 20,
//                 width: 20,
//                 child: Icon(Icons.chevron_right, color: Colors.white)),
//             title: Text(
//               'Payment',
//               style: gcNormalTextStyle,
//             ),
//             selected: false,
//             onTap: () async {
//               //await _navigateTo(context, '/payment');
//               Navigator.pushNamed(context, '/payment');
//             },
//           ),
//           Divider(
//             height: 1,
//             color: themeGreyColor,
//           ),
//           // Padding(
//           //   padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
//           //   child: Row(
//           //     children: [
//           //       Text(
//           //         'Membership since',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//           //       ),
//           //       SizedBox(
//           //         width: 16,
//           //       ),
//           //       (result.membershipDate != null)?Text(
//           //         result.membershipDate.toString(), //'06-03-1998',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeWhiteColor),
//           //       ):SizedBox(),
//           //     ],
//           //   ),
//           // ),
//           // Padding(
//           //   padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
//           //   child: Row(
//           //     children: [
//           //       Text(
//           //         'Birth Date',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//           //       ),
//           //       SizedBox(
//           //         width: 16,
//           //       ),
//           //       (result.birthday != null)? Text(
//           //         result.birthday.toString(), //'16-11-1970',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeWhiteColor),
//           //       ):SizedBox(),
//           //     ],
//           //   ),
//           // ),
//           //
//           // Padding(
//           //   padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
//           //   child: Row(
//           //     children: [
//           //       Text(
//           //         'Marital Status',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//           //       ),
//           //       SizedBox(
//           //         width: 16,
//           //       ),
//           //       (result.maritalStatus != null)? Text(
//           //         result.maritalStatus.toString(), //'Married',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeWhiteColor),
//           //       ):SizedBox(),
//           //     ],
//           //   ),
//           // ),
//           // Padding(
//           //   padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
//           //   child: Row(
//           //     children: [
//           //       Text(
//           //         'Marriage anniversary',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//           //       ),
//           //       SizedBox(
//           //         width: 16,
//           //       ),
//           //       (result.marriageAnniversary != null)?Text(
//           //           result.marriageAnniversary.toString(), //'27-10-1997',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeWhiteColor),
//           //       ):SizedBox(),
//           //     ],
//           //   ),
//           // ),
//           // Padding(
//           //   padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
//           //   child: Row(
//           //     children: [
//           //       Text(
//           //         'Father’s Name',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//           //       ),
//           //       SizedBox(
//           //         width: 16,
//           //       ),
//           //       Text(
//           //         result.username.toString(), //'M.A. Hasem',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeWhiteColor),
//           //       ),
//           //     ],
//           //   ),
//           // ),
//           // Padding(
//           //   padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
//           //   child: Row(
//           //     children: [
//           //       Text(
//           //         'Mother’s Name',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeGreyColor),
//           //       ),
//           //       SizedBox(
//           //         width: 16,
//           //       ),
//           //       Text(
//           //         result.username.toString(), //'Sultana Hashem',
//           //         style: GoogleFonts.poppins(fontSize: 14, color: themeWhiteColor),
//           //       ),
//           //     ],
//           //   ),
//           // ),
//
//           Center(
//             child: Column(
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.symmetric(
//                       horizontal: 40.0, vertical: 24),
//                   child: ElevatedButton(
//                     onPressed: () async {
//                       //http://www.gulshanclub.com/member-login/
//                       const url = 'http://118.179.81.84/ERP/Account/';
//                       if (await canLaunch(url)) {
//                         await launch(url);
//                       } else {
//                         throw 'Could not launch $url';
//                       }
//                     },
//                     style: ElevatedButton.styleFrom(
//                         primary: Colors.white,
//                         textStyle: gcTitleTextStyleBlack),
//                     child: Container(
//                         height: 40,
//                         child: Center(
//                             child: Text(
//                           "Member Login",
//                           style: gcTitleTextStyleBlack,
//                         ))),
//                   ),
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(
//                       "**** ",
//                       style: GoogleFonts.poppins(
//                           fontSize: 14, color: themeWhiteColor),
//                       textAlign: TextAlign.justify,
//                     ),
//                     Expanded(
//                       child: Text(
//                         "Please use \'Member Login\' option to check the detail ledger of your Club account.",
//                         style: GoogleFonts.poppins(
//                             fontSize: 14, color: themeWhiteColor),
//                         textAlign: TextAlign.justify,
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           ),
//
//           SizedBox(
//             height: 60,
//           )
//         ],
//       ),
//     );
//   }
//
//   _navigateTo(BuildContext context, String s) {}
// }
