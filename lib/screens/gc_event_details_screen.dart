import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/event_list_response.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:intl/intl.dart';

class EventDetailsScreen extends StatefulWidget {
  static const String route = '/event_details';

  @override
  _EventDetailsScreenState createState() => _EventDetailsScreenState();
}

class _EventDetailsScreenState extends State<EventDetailsScreen> {
  late Event selectedEvent;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final Event sEvent = ModalRoute.of(context)?.settings.arguments as Event;
    print('${sEvent.created.toString()}');
    DateFormat dateFormat = DateFormat("yyyy-MM-dd");
    DateTime dateTime = dateFormat.parse(sEvent.created.toString());
    String string = dateFormat.format(dateTime) + " ${sEvent.created?.substring(11,16)}";
    print(string);

    return Scaffold(
      backgroundColor: themeBlackColor,
      body: SingleChildScrollView(
        child: Stack(children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 300,
                child:
                (sEvent.imageMedium != null)? CachedNetworkImage(
                  imageUrl: NetworkManager.baseUrl +  sEvent.imageMedium!,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                          colorFilter: ColorFilter.mode(Colors.transparent, BlendMode.clear)
                          // colorFilter:
                          // ColorFilter.mode(Colors.transparent, BlendMode.clear)
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                  errorWidget: (context, url, error) => Column(
                    children: [
                      Icon(Icons.error),
                      Text(sEvent.imageAltText ?? 'No Image'),
                    ],
                  ),
                ):Image.asset(
                  'assets/images/events1.jpg',
                  fit: BoxFit.fill,
                ),
//                Image.asset(
//                  'assets/images/events1.jpg',
//                  fit: BoxFit.cover,
//                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  sEvent.name ??  ' ',
                  style: gcListTitleTextStyle,
                  textAlign: TextAlign.left,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(string,
                    style: listSubTitleTextStyle, textAlign: TextAlign.left),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(sEvent.description ??  ' ',
                    //'Mr. Showkat Aziz Russell has been elected as the President of Gulshan Club for the year 2017-2018 in the Election held on 15thDecember 2017 at the Club premises following the 39th Annual General Meeting. He secured 334of the votes cast.',
                    style: gcDescriptionTextStyle,
                    textAlign: TextAlign.left),
              ),
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Text(
//                    'Mr. Minhaz Kamal Khan, Mrs. Rita Husain, Mr. Chowdhury Moazzem Hossain (Arif), Mr. Rafiqul Alam (Helal), Mr. Matiur Rahman Chowdhury (Dulal), Mrs. Tahmina Rehman, Dr. Md. Wahiduzzaman (Tamal), Ms. Shirin Shila, Dr. Misbahul Alam Khan and Mr. Naushad Shahriar Sayeed were elected as the Members of the Executive Committee of the Gulshan Club Limited for the year 2017-2018.',
//                    style: gcDescriptionTextStyle,
//                    textAlign: TextAlign.left),
//              ),
              SizedBox(
                height: 40,
              ),
            ],
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 32.0, left: 16),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                      height: 40,
                      width: 40,
                      child: IconButton(
                          icon: SvgPicture.asset('assets/icons/ic_back.svg'), onPressed: () {
                        Navigator.pop(context);

                      },))),
            ),
          ),
        ]),
      ),
    );
  }
}
