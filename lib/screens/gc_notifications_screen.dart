import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/jsonModels/notice_board_response.dart';
import 'package:gulshan_club_app/jsonModels/notification_list_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

class NotificationsScreen extends StatefulWidget {
  static const String route = '/notifications';

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  List<Notifications> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  NotificationResponse? noticeJson;
  bool isLoadingMore = false;

  int tabIndex = 3;
  List<Widget> listScreens = [];

  int notificationCount = 3;

  @override
  void initState() {
    // TODO: implement initState
    if (tabIndex == null) {
      tabIndex = 0;
    } else {
      tabIndex = tabIndex;
    }
    getAllNotifications();
    getCurrentUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: themeBlackColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.pushNamedAndRemoveUntil(
              context, '/building_directory', (route) => false),
        ),
        title: Text(
          "Notifications",
          style: gcTitleTextStyle,
        ),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: TextStyle(),
                ))
              : itemList.length == 0
                  ? Center(
                      child: Text(
                        "No notifications found",
                        style: gcPlaceholderTextStyle,
                      ),
                    )
                  : createListView(itemList, context),
    );
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllNotifications();
    });
  }

  getAllNotifications() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/messages/notifications";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');

    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final noticeResponse = NotificationListResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data?.results}');

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  itemList = noticeResponse.data?.results ?? [];
                  noticeJson = noticeResponse.data;
                  isLoading = false;
                  // notificationCount = 9;

                  readAllNotifications();
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  readAllNotifications() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/messages/notifications/0";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');

    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  isLoading = false;
                  // notificationCount = 9;
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  _loadMoreData() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint =
        noticeJson?.next ?? ''; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = NotificationListResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        noticeJson = moreNoticeResponse.data;
        List<Notifications> moreData = moreNoticeResponse.data?.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  Widget createListView(List<Notifications> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return NotificationListener(
      onNotification: (ScrollNotification scrollInfo) {
        if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          if (!isLoadingMore && noticeJson?.next != null) {
            // start loading data
            print("Scrolling to end...");
            setState(() {
              isLoadingMore = true;
            });
            _loadMoreData();
          } else if (noticeJson?.next == null) {
            print("Data end");
            setState(() {
              isLoadingMore = false;
            });
          } else {}
        }
        return true;
      },
      child: RefreshIndicator(
        onRefresh: _reloadData,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          // physics: NeverScrollableScrollPhysics(),
          itemCount: itemList.length,
          itemBuilder: (context, index) {
            String dateString = "";
            if (itemList[index].createdAt != null) {
              DateTime parsedDate =
                  DateTime.parse(itemList[index].createdAt.toString())
                      .toLocal();
              dateString = DateFormat.yMd().add_jm().format(parsedDate);
              print(dateString);
            }

            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: GestureDetector(
                  onTap: () {
                    // Navigator.pushNamed(context, '/notice_details',
                    //     arguments: itemList[index]);
                  },
                  // child: ConstrainedBox(
                  //   constraints: BoxConstraints(maxHeight: 120, minHeight: 60.0),
                  child: IntrinsicHeight(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(0.0)),
                                    height: 40,
                                    width: 40,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: SvgPicture.asset(
                                        "assets/images/notification_image.svg",
                                        fit: BoxFit.fitWidth,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        dateString, //"40 th E.C. Meeting",
                                        style: gcPlaceholderTextStyle,
                                        textAlign: TextAlign.left,
                                      ),
                                      Expanded(
                                        child: Text(
                                          itemList[index].message.toString(), //"40 th E.C. Meeting",
                                          style: gcSubTitleTextStyle,
                                          textAlign: TextAlign.left,
                                          // overflow: TextOverflow.ellipsis,
                                          // maxLines: 4,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Divider(
                            height: 1,
                            color: themeGreyColor.withOpacity(0.5),
                            thickness: 1.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  // ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  getCurrentUserInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  // userInfo = userResponse.data;
                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
