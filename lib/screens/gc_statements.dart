import 'dart:convert';

 import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:gulshan_club_app/jsonModels/statement_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

class StatementsScreen extends StatefulWidget {
  static const String route = '/statements';

  @override
  _StatementsScreenState createState() => _StatementsScreenState();
}

class _StatementsScreenState extends State<StatementsScreen> {
  List<Statement> itemList = [];
  var isLoading = false;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  StatementResponse? noticeJson;
  bool isLoadingMore = false;

  List<Widget> listScreens = [];

  DateTime _chosenDateTime1 = DateTime.now();
  DateTime _chosenDateTime2 = DateTime.now();
  DateTime dateTime = DateTime.now();

  String startDate = "";
  String endDate = "";
  bool isSearchOn = false;

  @override
  void initState() {
    // TODO: implement initState

    // getAllStatements();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: themeBlackColor,
        title: Text(
          "Statements",
          style: gcTitleTextStyle,
        ),
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: TextStyle(),
                ))
              // : (itemList.length == 0  && isSearchOn == false)
              //     ? Center(
              //         child: Text(
              //           "No Statement found",
              //           style: gcPlaceholderTextStyle,
              //         ),
              //       )
                  : createListView(itemList, context),
    );
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllStatements();
    });
  }

  getAllStatements() async {
    print(startDate);
    print(endDate);
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/payments/user-payment-statement/?start_date=${startDate}&end_date=${endDate}";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final noticeResponse = StatementResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {

                  itemList = noticeResponse.data?.results ?? [];
                  noticeJson = noticeResponse;
                  isLoading = false;
                  if(itemList.length == 0){
                    Fluttertoast.showToast(
                      msg: "No data found!",
                      backgroundColor: Colors.red,
                      gravity: ToastGravity.CENTER,
                    );
                  }
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  _loadMoreData() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint =
        noticeJson?.data?.next ?? ''; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreStatementResponse = StatementResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        noticeJson = moreStatementResponse;
        List<Statement> moreData = moreStatementResponse.data?.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  // Show the modal that contains the CupertinoDatePicker
  void _showDatePicker(ctx, tag) {
    // showCupertinoModalPopup is a built-in function of the cupertino library
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 500,
              color: Color.fromARGB(255, 255, 255, 255),
              child: Column(
                children: [
                  Container(
                    height: 400,
                    child: CupertinoDatePicker(
                        initialDateTime: DateTime.now()..year,
                        maximumDate: DateTime.now()..year,
                        minimumYear: 1971,
                        mode: CupertinoDatePickerMode.date,
                        onDateTimeChanged: (val) {
                          setState(() {
                            if (tag == 1) {
                              _chosenDateTime1 = val;
                              startDate = DateFormat("yyyy-MM-dd")
                                  .format(_chosenDateTime1);
                            } else if (tag == 2) {
                              _chosenDateTime2 = val;
                              endDate = DateFormat("yyyy-MM-dd")
                                  .format(_chosenDateTime2);
                            } else {
                              print("none");
                            }
                          });
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: Text('OK'),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  Widget createListView(List<Statement> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            color: themeGreyColor,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(
            children: [
              Container(
                height: 40,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Container(
                  width: MediaQuery.of(context).size.width / 3 - 8,
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1.0, color: themeGolderColor),
                    ),
                  ),
                  child: CupertinoButton(
                    padding: EdgeInsetsDirectional.zero,
                    child: (startDate == "")
                        ? Text(
                            'Start Date',
                            style: gcNormalTextStyle,
                          )
                        : Text(startDate.toString(), style: gcNormalTextStyle),
                    onPressed: () => _showDatePicker(context, 1),
                  ),
                ),
              ),
              Text(
                "to",
                style: gcSubTitleTextStyle,
              ),
              Container(
                height: 40,
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Container(
                  width: MediaQuery.of(context).size.width / 3 - 8,
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1.0, color: themeGolderColor),
                    ),
                  ),
                  child: CupertinoButton(
                    padding: EdgeInsetsDirectional.zero,
                    child: (endDate == "")
                        ? Text('End Date', style: gcNormalTextStyle)
                        : Text(endDate.toString(), style: gcNormalTextStyle),
                    onPressed: () => _showDatePicker(context, 2),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: MyCustomButton(
                  height: 30,
                  width: MediaQuery.of(context).size.width / 3 - 60,
                  color: themeGolderColor, //Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Ok',
                      style: gcNormalTextStyle,
                    ),
                  ),
                  onPressed: () {
                    print('tapped');

                    if (_chosenDateTime1.isAfter(_chosenDateTime2)) {
                      print("invalid date");
                      Fluttertoast.showToast(
                        msg: "Start Date should be less than End Date",
                        backgroundColor: Colors.red,
                        gravity: ToastGravity.CENTER,
                      );
                    } else {
                      print("valid date");
                      getAllStatements();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
        Divider(
          color: themeGolderColor,
          height: 1,
        ),
        Expanded(
          child: NotificationListener(
            onNotification: (ScrollNotification scrollInfo) {
              if (scrollInfo.metrics.pixels ==
                  scrollInfo.metrics.maxScrollExtent) {
                if (!isLoadingMore && noticeJson?.data?.next != null) {
                  // start loading data
                  print("Scrolling to end...");
                  setState(() {
                    isLoadingMore = true;
                  });
                  _loadMoreData();
                } else if (noticeJson?.data?.next == null) {
                  print("Data end");
                  setState(() {
                    isLoadingMore = false;
                  });
                } else {}
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: _reloadData,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {
                  String dateString = itemList[index]
                      .createdAt!
                      .trim()
                      .substring(0, itemList[index].createdAt!.indexOf('T'));
                  // DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
                  // DateTime dateTime = dateFormat.parse(string);
                  // String dateString = dateFormat.format(dateTime);
                  // print(dateString);

                  // String dateString = string.split('').reversed.join('');
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 8),
                    child: Container(
                      decoration: BoxDecoration(
                          // color: Colors.white38,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          image: (index % 2 == 0)
                              ? DecorationImage(
                                  image: AssetImage(
                                      'assets/images/ic_list_background_1.png'),
                                  fit: BoxFit.cover)
                              : DecorationImage(
                                  image: AssetImage(
                                      'assets/images/ic_list_background_3.png'),
                                  fit: BoxFit
                                      .cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),
                          ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width / 2 -
                                      16,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      CircleAvatar(
                                        radius: 20,
                                        backgroundColor: themeGreyColor,
                                        child: CircleAvatar(
                                          radius: 18,
                                          // backgroundImage: AssetImage('assets/images/Facilities1.jpg'),
                                          backgroundImage: (itemList[index]
                                                      .paymentBy
                                                      ?.imageMedium !=
                                                  null)
                                              ? NetworkImage(
                                                  NetworkManager.baseUrl +
                                                      itemList[index]
                                                          .paymentBy!
                                                          .imageMedium!)
                                              : AssetImage(
                                                  'assets/icons/ic_placeholder.png') as ImageProvider,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Payment by",
                                              style:
                                                  gcPaymentPlaceholderTextStyle,
                                            ),
                                            if(itemList[index]
                                                .paymentBy != null)Text(
                                              itemList[index]
                                                  .paymentBy!
                                                  .username
                                                  .toString(), //firstName.toString() + " " + itemList[index].paymentBy.lastName.toString(),//"Saifur Rahman",
                                              style: gcPaymentTextStyle,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width / 2 -
                                      16,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      CircleAvatar(
                                        radius: 20,
                                        backgroundColor: themeGreyColor,
                                        child: CircleAvatar(
                                          radius: 18,
                                          // backgroundImage: AssetImage('assets/images/Facilities1.jpg'),
                                          backgroundImage: (itemList[index]
                                                      .paymentTo
                                                      ?.imageMedium !=
                                                  null)
                                              ? NetworkImage(
                                                  NetworkManager.baseUrl +
                                                      itemList[index]
                                                          .paymentTo!
                                                          .imageMedium!)
                                              : AssetImage(
                                                  'assets/icons/ic_placeholder.png') as ImageProvider,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Payment to",
                                              style:
                                                  gcPaymentPlaceholderTextStyle,
                                            ),
                                            if(itemList[index]
                                                .paymentTo != null)Text(
                                              itemList[index]
                                                  .paymentTo!
                                                  .username
                                                  .toString(), // + " " + itemList[index].paymentTo.lastName.toString(),//"Saifur Rahman",
                                              style: gcPaymentTextStyle,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Divider(
                                height: 1,
                                color: themeGreyColor,
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                               if(itemList[index].paymentType != null) Text(
                                  "Payment Type: " +
                                      itemList[index].paymentType! , //"40 th E.C. Meeting",
                                  style: gcPaymentDescriptionTextStyle,
                                ),
                                Text(
                                  dateString, //itemList[index].createdAt,//dateString, //"40 th E.C. Meeting",
                                  style: gcPaymentDescriptionTextStyle,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Text(
                              "Payment Status: " +
                                  itemList[index]
                                      .transactionStatus!, //"40 th E.C. Meeting",
                              style: gcPaymentDescriptionTextStyle,
                            ),
                            (itemList[index].paymentType == "NAGAD")? Text(
                              "Order ID: " +
                                  itemList[index]
                                      .merchantInvoice
                                      .toString(), //"Noticed is hereby called for E.C Meeting to be held on 10th March,2018 at the club Board Room to discuss the following agenda.",
                              style: gcPaymentDescriptionTextStyle,
                            ):
                            Text(
                              "Payment ID: " +
                                  itemList[index]
                                      .paymentId
                                      .toString(), //"Noticed is hereby called for E.C Meeting to be held on 10th March,2018 at the club Board Room to discuss the following agenda.",
                              style: gcPaymentDescriptionTextStyle,
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                               if(itemList[index].paymentType == "NAGAD") Expanded(
                                  child: Text(
                                    "Transaction ID: " +
                                        itemList[index]
                                            .trxId
                                            .toString(), //"Noticed is hereby called for E.C Meeting to be held on 10th March,2018 at the club Board Room to discuss the following agenda.",
                                    style: gcPaymentDescriptionTextStyle,
                                  ),
                                ),
                                Text(
                                  itemList[index].amount! +
                                      " " +
                                      itemList[index].currency!, //dateString, //"40 th E.C. Meeting",
                                  style: gcListTitleTextStyle,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }


}
