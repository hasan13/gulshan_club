import 'dart:async';
import 'dart:convert';
import 'dart:math';

// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flappy_search_bar/flappy_search_bar.dart';
// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/message_sent_response.dart';
import 'package:gulshan_club_app/jsonModels/single_chat_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class SingleChatScreen extends StatefulWidget {
  static const String route = '/chat';

  @override
  _SingleChatScreenState createState() => _SingleChatScreenState();
}

class _SingleChatScreenState extends State<SingleChatScreen> {
  final TextEditingController textEditingController =
      new TextEditingController();
  SingleChatListResponse? messageListResponse;
  bool isLoadingMore = false;
  List<Chats> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  final _controller = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    print('init called');
    getAllChats();
    super.initState();
  }

  getAllChats() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString;
    // "/messages/inbox-details/";
    print('fklsjflksj ${Resources.isSearchOn}');
    if (Resources.isSearchOn == true) {
      url += "/messages/select-inbox/";
    } else {
      url += "/messages/inbox-details/";
    }
    print('url---> $url');
    print('parent_id ${Resources.selectedRecipientID.toString()}');
    // print('image url----> ${NetworkManager.baseUrl + Resources.selectedRecipient.sender.imageMedium.toString()}');
    // print('image url2----> ${ Resources.selectedRecipient.sender.imageMedium.toString()}');

    var params;

    if (Resources.isSearchOn == true) {
      params = {"recipient": Resources.selectedRecipientID.toString()};
    } else {
      params = {"parent_id": Resources.selectedRecipientID.toString()};
      // Resources.idForInboxDetails = itemList[index].recipient.toString();
    }
    if (result == true) {
      try {
        http
            .post(Uri.parse(url),
                headers: {
                  "Authorization":
                      "Bearer ${Resources.currentUser?.accessToken}"
                },
                body: params)
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(value.body);
          final employeeResponse = SingleChatListResponse.fromJson(data);
          print('result....\n ${employeeResponse.data}');
          if (value.statusCode == 200 || value.statusCode == 202) {
            setState(() {
              itemList = employeeResponse.data?.reversed.toList() ?? [];
              messageListResponse = employeeResponse;
              isLoading = false;
            });
            //navigateToSignInPage(context);
            print('YAY! Free cute dog pics!');
          } else if (data['status'] == false) {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            setState(() {
              isLoading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    Timer(
      Duration(seconds: 1),
      () => _controller.jumpTo(_controller.position.maxScrollExtent),
    );

    return SafeArea(
      child: Scaffold(
        backgroundColor: themeBlackColor,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 24.0, left: 16),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                          height: 30,
                          width: 30,
                          child: IconButton(
                            icon: SvgPicture.asset('assets/icons/ic_back.svg'),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ))),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0, left: 16),
                  child: CircleAvatar(
                    radius: 20,
                    // backgroundImage: AssetImage('assets/images/Facilities1.jpg'),
                    backgroundImage: (Resources.isSearchOn == true)
                        ? (Resources.selectedMember?.imageMedium != null)
                            ? NetworkImage(NetworkManager.baseUrl +
                                Resources.selectedMember!.imageMedium!)
                            : AssetImage('assets/icons/ic_placeholder.png')
                                as ImageProvider
                        : (Resources.selectedMember?.imageMedium != null)
                            ? NetworkImage(NetworkManager.baseUrl +
                                Resources.selectedMember!.imageMedium!)
                            : AssetImage('assets/icons/ic_placeholder.png')
                                as ImageProvider
                    //(Resources.selectedRecipient.sender.imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + Resources.selectedRecipient.sender.imageMedium) : AssetImage('assets/icons/ic_placeholder.png')
                    ,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 24.0, left: 16),
                    child: Text(
                      (Resources.isSearchOn == true)
                          ? '${Resources.selectedMember?.firstName.toString()} ${Resources.selectedMember?.lastName.toString()}'
                          : '${Resources.selectedRecipient?.recipient?.firstName.toString()} ${Resources.selectedRecipient?.recipient?.lastName.toString()}',
                      // Resources.selectedRecipient?.recipient?.firstName.toString()
                      // + ' ' + Resources.selectedRecipient.recipient.lastName.toString()', //"Saifur Rahman",
                      style: gcTitleTextStyle,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Divider(
              height: 1,
              color: themeGreyColor,
            ),
            SizedBox(
              height: 16,
            ),
            Expanded(
              child: ListView.builder(
                controller: _controller,
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {
                  // print('bbb-${itemList[index].sender.phonePrimary}');
                  // print('${Resources.mobileNo}');
                  if (itemList[index].sender?.phonePrimary ==
                      Resources.mobileNo) {
                    //This is the sent message. We'll later use data from firebase instead of index to determine the message is sent or received.
                    return Container(
                        child: Column(children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            child: Text(
                              itemList[index]
                                  .subject
                                  .toString(), //'This is a sent message This is a sent message This is a sent message This is a sent message This is a sent message This is a sent message This is a sent message ',
                              style: TextStyle(color: Colors.white),
                            ),
                            padding:
                                EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                            width: MediaQuery.of(context).size.width * 0.8,
                            decoration: BoxDecoration(
                              color: themeGolderColor,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20.0),
                                topRight: Radius.circular(20.0),
                                bottomLeft: Radius.circular(20.0),
                                bottomRight: Radius.zero,
                              ),
                            ),
                            margin: EdgeInsets.only(right: 10.0),
                          )
                        ],
                        mainAxisAlignment: MainAxisAlignment
                            .end, // aligns the chatitem to right end
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Container(
                              child: Text(
                                '12.35pm',
                                style: TextStyle(
                                    color: themeGreyColor,
                                    fontSize: 12.0,
                                    fontStyle: FontStyle.normal),
                              ),
                              margin: EdgeInsets.only(
                                  left: 5.0, top: 5.0, bottom: 5.0),
                            )
                          ])
                    ]));
                  } else {
                    // This is a received message
                    return Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                child: Text(
                                  itemList[index]
                                      .subject
                                      .toString(), //'This is a received message This is a received message This is a received message This is a received message This is a received message',
                                  style: TextStyle(color: Colors.white),
                                ),
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                                width: MediaQuery.of(context).size.width * 0.8,
                                decoration: BoxDecoration(
                                  color: themeGreyColor,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20.0),
                                    topRight: Radius.circular(20.0),
                                    bottomLeft: Radius.zero,
                                    bottomRight: Radius.circular(20.0),
                                  ),
                                ),
                                margin: EdgeInsets.only(left: 10.0),
                              )
                            ],
                          ),
                          Container(
                            child: Text(
                              '12.35pm',
                              style: gcDescriptionTextStyle,
                            ),
                            margin: EdgeInsets.only(
                                left: 5.0, top: 5.0, bottom: 5.0),
                          )
                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),
                    );
                  }
                },
              ),
            ),
            SizedBox(
              height: 60,
            ),
          ],
        ),
        bottomSheet: Container(
          child: Row(
            children: <Widget>[
              Material(
                child: new Container(
                  margin: new EdgeInsets.symmetric(horizontal: 1.0),
                  child: new IconButton(
                    icon: new Icon(Icons.face),
                    color: themeGolderColor,
                    onPressed: () {},
                  ),
                ),
                color: Colors.white,
              ),

              // Text input
              Flexible(
                child: Container(
                  child: TextField(
                    style: TextStyle(color: themeBlackColor, fontSize: 15.0),
                    controller: textEditingController,
                    decoration: InputDecoration.collapsed(
                      hintText: 'Type a message',
                      hintStyle: TextStyle(color: themeGreyColor),
                    ),
                  ),
                ),
              ),

              // Send Message Button
              Material(
                child: new Container(
                  margin: new EdgeInsets.symmetric(horizontal: 8.0),
                  child: new IconButton(
                    icon: new Icon(Icons.send),
                    onPressed: () => {
                      if (textEditingController.text != '') {sendMessage()}
                    },
                    color: themeGolderColor,
                  ),
                ),
                color: Colors.white,
              ),
            ],
          ),
          width: double.infinity,
          height: 50.0,
          decoration: new BoxDecoration(
              border: new Border(
                  top: new BorderSide(color: themeGreyColor, width: 0.5)),
              color: Colors.white),
        ),
      ),
    );
  }

  sendMessage() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/messages/compose/";
    print('url---> $url');
    print('url---> ${Resources.selectedRecipientID.toString()}');
    print('url---> ${textEditingController.text}');

    var params;

    if (Resources.isSearchOn == true) {
      params = {
        "recipient": Resources.selectedRecipientID.toString(),
        "subject": textEditingController.text
      };
    } else {
      params = {
        "recipient": Resources.idForInboxDetails.toString(),
        "subject": textEditingController.text
      };
    }
    print(" params------:${params}");

    //var params = {"recipient": Resources.selectedRecipient.recipient.id.toString(), "subject": textEditingController.text};
    if (result == true) {
      try {
        http
            .post(Uri.parse(url),
                headers: {
                  "Authorization":
                      "Bearer ${Resources.currentUser?.accessToken}"
                },
                body: params)
            .timeout(Duration(seconds: 30))
            .then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(value.body);

          if (value.statusCode == 200 || value.statusCode == 202) {
            setState(() {
              isLoading = false;
              textEditingController.clear();
              getAllChats();
            });
            //navigateToSignInPage(context);
            print('YAY! Free cute dog pics!');
          } else if (data['status'] == false) {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text(
                  'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            setState(() {
              isLoading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}

class Post {
  final String title;
  final String body;

  Post(this.title, this.body);
}
