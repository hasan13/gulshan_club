import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/club_facilities_response.dart';
import 'package:gulshan_club_app/jsonModels/notification_count_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';

import '../jsonModels/current_user_info_response.dart';

class BuildingDirectoryScreen extends StatefulWidget {
  static const String route = '/building_directory';

  @override
  _BuildingDirectoryScreenState createState() =>
      _BuildingDirectoryScreenState();
}

class _BuildingDirectoryScreenState extends State<BuildingDirectoryScreen> {
  int _current = 0;
  List<String> imgList = [
    'assets/images/1.png',
    'assets/images/2.png',
    'assets/images/3.png',
    // 'assets/images/4.jpg',
    // 'assets/images/6.jpg',
    // 'assets/images/5.jpg',
    // 'assets/images/7.jpg',
    // 'assets/images/8.jpg',
  ];

  // List<Facility> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  ClubFacilitiesResponse? clubFacilityResponse;
  bool isLoadingMore = false;
  int tabIndex = 0;
  String fcmToken = '';
  String deviceId = '';
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  Map<String, dynamic> deviceData = <String, dynamic>{};

  String deviceType = "";

  int itemCount = 0;

  AppUser? userInfo;

  @override
  void initState() {
    // TODO: implement initState
    // if(Resources.appVersionResponse.data.ios == Resources.localVersion){
    //   if (Platform.isIOS) {
    //     // if(_packageInfo.version.toString() != status.data.ios.toString()){
    //     //   if (this.mounted) {
    //         showAlertDialog(context,"https://apps.apple.com/us/app/gulshan-club-limited/id1543591195");
    //     //   }
    //     // }
    //   }
    // }
    getData();
    getDeviceInfo();
    getNotificationBadgeCount();
    // Resources.appVersionResponse = statusResponse;
    // Resources.localVersion = _packageInfo.version;
    super.initState();
  }

  showAlertDialog(BuildContext ctx, String storeLink) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("Upgrade"),
      onPressed: () async {
        if (await canLaunch(storeLink)) {
          await launch(storeLink);
        } else {
          throw 'Could not launch StoreLink';
        }
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Update Available"),
      content: Text("Please update your app for uninterrupted service!"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  getData() async {
    await getCurrentUserInfo();
  }

  getCurrentUserInfo() async {
    print(Resources.currentUser?.accessToken);
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  userInfo = userResponse.data;
                  Resources.appUser = userResponse.data;
                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  isLoading = false;

                  var accountNo = userResponse.data?.clubAcNumber;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  getNotificationBadgeCount() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/messages/notifications/count";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');

    if (result == true) {
      try {
        await http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final noticeResponse = NotificationCountResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  itemCount = noticeResponse.data ?? 0;
                  Resources.notificationItemCount = itemCount;
                  // notificationCount = 9;
                });
              } else if (data['status'] == false) {
                setState(() {});
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {});
              }
            });
      } catch (e) {
        print(e);
        setState(() {});
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {});
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  Future<void> getDeviceInfo() async {
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        print('Running on ${androidInfo.model}');
        setState(() {
          deviceId = androidInfo.androidId;
          deviceType = "android";
        });
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        print('Running on ${iosInfo.utsname.machine}');
        setState(() {
          deviceId = iosInfo.identifierForVendor;
          Resources.deviceId = deviceId;
          deviceType = "iOS";
        });
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    FirebaseMessaging.instance.getToken().then((token) {
      print("fcm token---- $token");
      setState(() {
        fcmToken = token ?? '';
      });
      registerForNotificationOnServer();
    });
  }

  void registerForNotificationOnServer() async {
    print(deviceId);
    print(fcmToken);

    var params = {
      "device_id": deviceId,
      "device_type": deviceType,
      "fcm_token": fcmToken,
    };
    print(params);

    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl + "/api/v1/settings/fcm/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .post(Uri.parse(url), body: params, headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
              "Content-Type": "application/x-www-form-urlencoded",
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("fcm status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              // final noticeResponse = GetOtpResponse.fromJson(data); //noticeResponseFromJson(data);
              // print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  isLoading = false;
                });
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
        pageTitle: 'Home',
        parentNotificationCount: Resources.notificationItemCount,
        // bottomNavigationBar: Resources.setBottomDrawer(context),
        bottomNavigationBar: new Theme(
          data: Theme.of(context).copyWith(
              // sets the background color of the `BottomNavigationBar`
              canvasColor: themeBlackColor,
              // sets the active color of the `BottomNavigationBar` if `Brightness` is light
              primaryColor: Colors.red,
              textTheme: Theme.of(context).textTheme.copyWith(
                  caption: new TextStyle(
                      color: Colors
                          .yellow))), // sets the inactive color of the `BottomNavigationBar`
          child: BottomNavigationBar(
              selectedItemColor: themeGolderColor,
              unselectedItemColor: Colors.grey[400],
              iconSize: 30,
              backgroundColor: Colors.blue,
              currentIndex: tabIndex,
              onTap: (int index) {
                setState(() {
                  tabIndex = index;
                  print("tabIndex----$tabIndex");
                  Resources.moveTo(index, context);
                });
              },
              items: [
                BottomNavigationBarItem(
                    activeIcon: Icon(
                      Icons.home,
                      color: themeGolderColor,
                      size: 30,
                    ),
                    icon: Icon(
                      Icons.home,
                      color: themeGreyColor,
                      size: 28,
                    ),
                    label: ''),

                BottomNavigationBarItem(
                    activeIcon: SvgPicture.asset(
                      "assets/icons/ic_profile.svg",
                      color: themeGolderColor,
                      width: 30,
                      height: 30,
                    ),
                    icon: SvgPicture.asset(
                      "assets/icons/ic_profile.svg",
                      color: themeGreyColor,
                      width: 26,
                      height: 26,
                    ),
                    label: ''),
                BottomNavigationBarItem(
                    activeIcon: SvgPicture.asset(
                      "assets/icons/ic_memeber_director.svg",
                      color: themeGolderColor,
                      width: 30,
                      height: 30,
                    ),
                    icon: SvgPicture.asset(
                      "assets/icons/ic_memeber_director.svg",
                      color: themeGreyColor,
                      width: 26,
                      height: 26,
                    ),
                    label: ''),

                BottomNavigationBarItem(
                    activeIcon: SvgPicture.asset(
                      "assets/icons/ic_notice_board.svg",
                      color: themeGreyColor,
                      width: 30,
                      height: 30,
                    ),
                    icon: SvgPicture.asset(
                      "assets/icons/ic_notice_board.svg",
                      color: themeGreyColor,
                      width: 26,
                      height: 26,
                    ),
                    label: ''),

                // BottomNavigationBarItem(
                //     activeIcon: SvgPicture.asset(
                //       "assets/icons/ic_facilities.svg",
                //       color: themeGolderColor,
                //       width: 30,
                //       height: 30,
                //     ),
                //     icon: SvgPicture.asset(
                //       "assets/icons/ic_facilities.svg",
                //       color: themeGreyColor,
                //       width: 26,
                //       height: 26,
                //     ),
                //     label: ''),
              ]),
        ),
        body: Stack(
          children: [
            CarouselSlider(
              items: List.generate(
                  imgList.length,
                  (i) => InkWell(
                        onTap: () {
                          if (imgList != null) {
                            if (imgList.length != 0) {
                              switch (i) {
                                //darbar
                                case 0:
                                  {
                                    // Navigator.pushNamed(context, '/facility_details',arguments: imgList[7]);
                                    Navigator.pushNamed(
                                        context, '/club_facilities');
                                  }
                                  break;
                                //cafe bistro
                                case 1:
                                  {
                                    Navigator.pushNamed(context, '/take_away');
                                  }
                                  break;
                                //pan asia
                                case 2:
                                  {
                                    Navigator.pushNamed(context,
                                        '/members_directory'); //,arguments: imgList[11]);
                                  }
                                  break;
                                //club tavern
                                //   case 3: {
                                //     Navigator.pushNamed(context, '/facility_details',arguments: imgList[6]);
                                //
                                //   }
                                //   break;
                                //
                                //   // garden
                                //   case 4: {
                                //     Navigator.pushNamed(context, '/facility_details',arguments: imgList[8]);
                                //
                                //   }
                                //   break;
                                //   //rajanighanda
                                //   case 5: {
                                //     Navigator.pushNamed(context, '/facility_details',arguments: imgList[15]);
                                //
                                //   }
                                //   break;
                                //   //darbar
                                //   case 6: {
                                //     Navigator.pushNamed(context, '/facility_details',arguments: imgList[7]);
                                //
                                //   }
                                //   break;
                                //
                                // //club tavern
                                //   case 7: {
                                //     Navigator.pushNamed(context, '/facility_details',arguments: imgList[6]);
                                //
                                //   }
                                //   break;
                                default:
                                  {
                                    print("Invalid choice");
                                  }
                                  break;
                              }
                            }
                            // Navigator.pushNamed(context, '/facility_details',arguments: itemList[i]);
                          }
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8.0),
                            child: Image.asset(
                              imgList[i],
                              fit: (i == 1) ? BoxFit.fill : BoxFit.fill,
                            ),
//                     CachedNetworkImage(
//                       imageUrl: NetworkManager.baseUrl + itemList[i].imageMedium,
//                       imageBuilder: (context, imageProvider) => Container(
//                         decoration: BoxDecoration(
//                           image: DecorationImage(
//                             image: imageProvider,
//                             fit: BoxFit.cover,
//                           ),
//                         ),
//                       ),
//                       placeholder: (context, url) => Container(height: MediaQuery.of(context).size.height * 0.3, width: 50, alignment: Alignment.center, child: CircularProgressIndicator()),
// //                              progressIndicatorBuilder: (context, url, downloadProgress) =>
// //                                  Container(
// //                                      height: MediaQuery.of(context).size.height * 0.3, width: 50,
// //                                      alignment: Alignment.center,
// //                                      child: CircularProgressIndicator(value: downloadProgress.progress,)),
//                       errorWidget: (context, url, error) => Icon(Icons.error),
//                     ),
                          ),
                        ),
                      )).toList(),
              //imageSliders,
              options: CarouselOptions(
                  height: MediaQuery.of(context).size.height,
                  autoPlay: false,
                  enableInfiniteScroll: false,
                  enlargeCenterPage: true,
                  // aspectRatio: 2.0,
                  viewportFraction: 1.0,
                  initialPage: 0,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }),
            ),
            // Align(
            //   alignment: Alignment.center,
            //   child: Text(
            //     'Your Second Home',
            //     textAlign: TextAlign.center,
            //     style: GoogleFonts.poppins(
            //       fontSize: 40,
            //       color: themeWhiteColor,
            //     ),
            //   ),
            // ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: imgList.map((url) {
                  int index = imgList.indexOf(url);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _current == index
                            ? themeWhiteColor
                            : themeGolderColor),
                  );
                }).toList(),
              ),
            ),
          ],
        ));
  }
}
