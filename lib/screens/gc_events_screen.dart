import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:gulshan_club_app/jsonModels/event_list_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

class EventsScreen extends StatefulWidget {
  static const String route = '/events';

  @override
  _EventsScreenState createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {
  int _current = 0;
  List<String> imgList = [
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
    'assets/images/events1.jpg',
    'assets/images/events2.jpg',
  ];
  List<Event> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  late String _chosenDateTime;
  List<String> yearList = [];

  List<DropdownMenuItem<ListItem>> _dropdownMenuItems = [];
  late ListItem _selectedItem;
  List<ListItem> _dropdownItems = [];

  @override
  void initState() {
    // TODO: implement initState
    _chosenDateTime = DateTime.now().year.toString();

    for (var i = DateTime.now().year; i > 1970; i--) {
      // yearList.add(i.toString());
      ListItem item = ListItem(i, i.toString());
      _dropdownItems.add(item);
    }

    _dropdownMenuItems = buildDropDownMenuItems(_dropdownItems);
    _selectedItem = _dropdownMenuItems[0].value!;

    getAllEvents(_selectedItem.name);
    super.initState();
  }

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = [];
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }

  // Show the modal that contains the CupertinoDatePicker

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Events',
      bottomNavigationBar: Resources.setBottomDrawer(context),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: TextStyle(),
                ))
              : itemList.length == 0
                  ? Column(
                      children: [
                        SizedBox(
                          height: 60,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  'Showing you events from ',
                                  style: gcListTitleTextStyle,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    padding: const EdgeInsets.only(
                                        left: 10.0, right: 10.0),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        color: themeGolderColor,
                                        border: Border.all()),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                          dropdownColor: themeGolderColor,
                                          style: gcNormalTextStyle,
                                          iconEnabledColor: Colors.white,
                                          value: _selectedItem,
                                          items: _dropdownMenuItems,
                                          onChanged: (value) {
                                            setState(() {
                                              _selectedItem = value as ListItem;
                                              getAllEvents(_selectedItem.value
                                                  .toString());
                                            });
                                          }),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Center(
                          child: Text(
                            "No Events found",
                            style: gcPlaceholderTextStyle,
                          ),
                        ),
                      ],
                    )
                  : //createListView(itemList, context),
                  Scaffold(
                      backgroundColor: themeBlackColor,
                      key: _scaffoldKey,
                      body: CustomScrollView(
                        slivers: <Widget>[
                          SliverToBoxAdapter(
                            child: SizedBox(
                              height: 60,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      'Showing you events from ',
                                      style: gcListTitleTextStyle,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            left: 10.0, right: 10.0),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                            color: themeGolderColor,
                                            border: Border.all()),
                                        child: DropdownButtonHideUnderline(
                                          child: DropdownButton(
                                              dropdownColor: themeGolderColor,
                                              style: gcNormalTextStyle,
                                              iconEnabledColor: Colors.white,
                                              value: _selectedItem,
                                              items: _dropdownMenuItems,
                                              onChanged: (value) {
                                                setState(() {
                                                  _selectedItem = value as ListItem;
                                                  getAllEvents(_selectedItem
                                                      .value
                                                      .toString());
                                                });
                                              }),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SliverFillRemaining(
                            child: createListView(itemList, context),
                          )
                        ],
                      ),
                    ),
    );
  }

  getAllEvents(String year) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/events/?year=$year";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final employeeResponse = EventListResponse.fromJson(data);
              print('result....\n ${employeeResponse.results}');
              if (value.statusCode == 200) {
                setState(() {
                  itemList = employeeResponse.results ?? [];
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Widget createListView(List<Event> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: itemList.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16),
          child: GestureDetector(
            onTap: () {
              // Navigator.pushNamed(context, '/event_details', arguments: itemList[index]);
            },
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: Container(
                        height: 200,
                        width: MediaQuery.of(context).size.width,
                        child: (itemList[index].imageMedium != null)
                            ? CachedNetworkImage(
                                imageUrl: NetworkManager.baseUrl +
                                    itemList[index].imageMedium!,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover,
                                      // colorFilter:
                                      // ColorFilter.mode(Colors.red, BlendMode.colorBurn)
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    Center(child: CircularProgressIndicator()),
                                errorWidget: (context, url, error) => Column(
                                  children: [
                                    Icon(Icons.error),
                                    if(itemList[index].imageAltText != null)Text(itemList[index].imageAltText!),
                                  ],
                                ),
                              )
                            : Image.asset(
                                imgList[0],
                                fit: BoxFit.fill,
                              ),
//                          Image.asset(
//                            imgList[index],
//                            fit: BoxFit.fill,
//                          ),
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        color: Colors.black.withOpacity(0.4),
                        child: Text(
                          itemList[index].name ?? ' ',
                            textAlign: TextAlign.center,
                          style: gcNormalTextStyle,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void onChanged(DateTime value) {
    print(value);
  }
}

class ListItem {
  int value;
  String name;

  ListItem(this.value, this.name);
}
