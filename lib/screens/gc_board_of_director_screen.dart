import 'dart:convert';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart' as fsb;

// import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flappy_search_bar_ns/scaled_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/member_search_response.dart';
import 'package:gulshan_club_app/jsonModels/message_list_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';

class BoardDirectoryScreen extends StatefulWidget {
  static const String route = '/board_directory';

  @override
  _BoardDirectoryScreenState createState() => _BoardDirectoryScreenState();
}

class _BoardDirectoryScreenState extends State<BoardDirectoryScreen> {
  final SearchBarController<Post> _searchBarController =
       SearchBarController();
  bool isReplay = false;
  List<Member> itemList = [];
  var isLoading = true;

  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  bool checkingFlight = false;
  bool success = false;

  bool isSearchOn = false;

  UserListResponse? userJson;
  bool isLoadingMore = false;

  var tabIndex = 3;

  @override
  void initState() {
    // TODO: implement initState
    getAllMembers();
    super.initState();
  }

  Future<List<Post>> _getALlPosts(String? text) async {
    if(text != null){
      if (text.length > 0) {
        getFilteredMembers(text);
      } else {
        getAllMembers();
      }

      List<Post> posts = [];

      var random = new Random();
      for (int i = 0; i < 10; i++) {
        posts
            .add(Post("$text $i", "body random number : ${random.nextInt(100)}"));
      }
    }


    return []; //posts;
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Board Directory',
      bottomNavigationBar: Resources.setBottomDrawer(context),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : itemList == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: TextStyle(),
                ))
              : (itemList.length == 0 && isSearchOn == false)
                  ? Center(
                      child: Text(
                        "No Member found",
                        style: gcPlaceholderTextStyle,
                      ),
                    )
                  : createListView(itemList, context),
    );
  }

  getAllMembers() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/active-bod-users/";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = UserListResponse.fromJson(data);
              print('result....\n ${employeeResponse.results}');
              if (value.statusCode == 200) {
                setState(() {
                  itemList = employeeResponse.results ?? [];
                  userJson = employeeResponse;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                  isSearchOn = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Future<void> _reloadData() async {
    setState(() {
      getAllMembers();
    });
  }

  _loadMoreMember() async {
    print("load more");
    // update data and loading status

    print('${Resources.currentUser?.accessToken}');
    String uploadEndPoint =
        userJson?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = UserListResponse.fromJson(
          json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        userJson = moreNoticeResponse;
        List<Member> moreData = moreNoticeResponse.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  getFilteredMembers(String text) async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/active-bod-users-search/?member_name=$text";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final employeeResponse = MemberSearchResponse.fromJson(data);
              print('result....\n ${employeeResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  isSearchOn = true;
                  itemList = employeeResponse.data ?? [];
                  userJson = null;

                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  Widget createListView(List<Member> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 80,
          padding: EdgeInsets.symmetric(horizontal: 8),
          child: fsb.SearchBar<Post>(
            minimumChars: 0,
            searchBarPadding: EdgeInsets.symmetric(horizontal: 8),
            headerPadding: EdgeInsets.symmetric(horizontal: 8),
            listPadding: EdgeInsets.symmetric(horizontal: 8),
            onSearch: _getALlPosts,
            hintText: 'Search',
            textStyle: GoogleFonts.poppins(color: themeWhiteColor),
            searchBarController: _searchBarController,
            // placeHolder: Text("placeholder"),
            cancellationWidget: Text(
              "Cancel",
              style: GoogleFonts.poppins(color: themeWhiteColor),
            ),
            //emptyWidget: Text("empty"),
            indexedScaledTileBuilder: (int index) =>
                ScaledTile.count(1, index.isEven ? 2 : 1),
            onCancelled: () {
              print("Cancelled triggered");
              getAllMembers();
              FocusScope.of(context).unfocus();
            },
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            crossAxisCount: 2,
            onItemFound: (Post? post, int index) {
              return Container(
                color: Colors.lightBlue,
                child: ListTile(
                  title: Text(post?.title ?? ' '),
                  isThreeLine: true,
                  subtitle: Text(post?.body ?? ' '),
                  onTap: () {},
                ),
              );
            },
          ),
        ),
        Expanded(
          child: NotificationListener(
            onNotification: (ScrollNotification scrollInfo) {
              if (scrollInfo.metrics.pixels ==
                  scrollInfo.metrics.maxScrollExtent) {
                if (!isLoadingMore && userJson?.next != null) {
                  // start loading data
                  print("Scrolling to end...");
                  setState(() {
                    isLoadingMore = true;
                  });
                  _loadMoreMember();
                } else if (userJson?.next == null) {
                  print("Data end");
                  setState(() {
                    isLoadingMore = false;
                  });
                } else {}
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: _reloadData,
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: itemList.length,
                itemBuilder: (context, index) {
                  var accountNo = itemList[index].clubAcNumber;
                  var letterString =
                      accountNo?.replaceAll(new RegExp(r'[^A-Za-z]'), '');
                  var numberString =
                      accountNo?.replaceAll(new RegExp(r'[^0-9]'), '');

                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 16),
                    child: Container(
                      decoration: BoxDecoration(
                          //color: Colors.white38,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          image: (index % 2 == 0)
                              ? DecorationImage(
                                  image: AssetImage(
                                      'assets/images/ic_list_background_1.png'),
                                  fit: BoxFit.cover)
                              : DecorationImage(
                                  image: AssetImage(
                                      'assets/images/ic_list_background_3.png'),
                                  fit: BoxFit
                                      .cover) //AssetImage('assets/images/ic_list_background_1.png'):AssetImage('assets/images/ic_list_background_2.png'),

                          ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Padding(
                            //   padding: const EdgeInsets.all(8.0),
                            //   child: CircleAvatar(
                            //     radius: 47,
                            //     backgroundColor: Colors.transparent,
                            //     backgroundImage: (itemList[index].imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium) : AssetImage('assets/icons/ic_placeholder.png'),
                            //     //AssetImage('assets/images/Facilities1.jpg'),
                            //   ),
                            // ),

                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: 120,
                                width: 100,
                                child: (itemList[index].imageMedium != null)
                                    ? CachedNetworkImage(
                                        imageUrl: NetworkManager.baseUrl +
                                            itemList[index].imageMedium!,
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.fitHeight,
                                                colorFilter: ColorFilter.mode(
                                                    Colors.transparent,
                                                    BlendMode.color)),
                                          ),
                                        ),
                                        placeholder: (context, url) => Center(
                                            child: CircularProgressIndicator()),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                      )
                                     : Image.asset(
                                        'assets/icons/ic_placeholder.png')   ,
                              ),
                            ),
                            Flexible(
                              // child: Card(color: Colors.white38,elevation: 0,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  (itemList[index].firstName != null)
                                      ? Text(
                                          itemList[index].firstName! +
                                              ' ' +
                                             '${itemList[index].lastName}', //"Capt. Saifur Rahman",
                                          style: gcListTitleTextStyle,
                                        )
                                      : SizedBox(),
                                  (itemList[index].designation != null)
                                      ? Container(
                                          decoration: BoxDecoration(
                                              color: themeGolderColor,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(8))),
                                          padding: EdgeInsets.all(4),
                                          child: Wrap(
                                            spacing: 8.0,
                                            children: [
                                              (itemList[index]
                                                          .designation
                                                          .toString() ==
                                                      "President")
                                                  ? SvgPicture.asset(
                                                      'assets/icons/ic_crown.svg',
                                                      height: 12,
                                                      width: 12,
                                                      color: themeWhiteColor,
                                                    )
                                                  : SizedBox(
                                                      height: 1,
                                                    ),
                                              (itemList[index].designation !=
                                                      null)
                                                  ? Text(
                                                      itemList[index]
                                                          .designation
                                                          .toString(), //"Captain",
                                                      style: gcNormalTextStyle,
                                                    )
                                                  : SizedBox(),
                                            ],
                                          ),
                                        )
                                      : SizedBox(
                                          height: 1,
                                        ),
                                  (itemList[index].email != null)
                                      ? Text(
                                          itemList[index]
                                              .email!, //"captsaif@ebd-bd.com",
                                          style: listSubTitleTextStyle,
                                        )
                                      : SizedBox(),
                                  (itemList[index].profession != null)
                                      ? Text(
                                          itemList[index]
                                              .profession
                                              .toString(), //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                          style: gcDescriptionTextStyle,
                                        )
                                      : SizedBox(),
                                  (itemList[index].clubAcNumber != null &&
                                          itemList[index].clubAcNumber != '')
                                      ? Text(
                                          'A/C No: ' +
                                              letterString! +
                                              "-" +
                                              numberString!, //"Development & Master Plan, Security, Membership, Constitution, Discipline, Entertainment & CSR",
                                          style: gcDescriptionTextStyle,
                                        )
                                      : SizedBox(),
                                  (itemList[index].phonePrimary != null &&
                                          itemList[index].phonePrimary != '')
                                      ? Card(
                                          color: themeGreyColor,
                                          elevation: 0,
                                          child: GestureDetector(
                                            onTap: () async {
                                              print(
                                                  '${itemList[index].phonePrimary}');
                                              //launch("tel://${itemList[index].mobileNumberPrimary}");

                                              String url = "tel://" +
                                                  itemList[index]
                                                      .phonePrimary
                                                      .toString();
                                              if (await canLaunch(url)) {
                                                await launch(url);
                                              } else {
                                                throw 'Could not call ${itemList[index].phonePrimary}';
                                              }
                                            },
                                            child: Center(
                                              child: Container(
                                                padding: EdgeInsets.all(4),
                                                height: 30,
                                                child: Text(
                                                  itemList[index]
                                                      .phonePrimary ?? ' ', //'01819-415452',
                                                  style:
                                                      listSubTitleBlackTextStyle,
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      : SizedBox(),
                                ],
                              ),
                            ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class Post {
  final String title;
  final String body;

  Post(this.title, this.body);
}
