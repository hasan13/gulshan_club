import 'dart:convert';

 import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/jsonModels/settings_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/payments/utilities/bottom_modal.dart';
import 'package:gulshan_club_app/screens/payments/utilities/restart_app.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class SettingsScreen extends StatefulWidget {
  static const String route = '/settings';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  int _current = 0;

  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  AppUser? userInfo;

  int tabIndex = 1;
  bool isSwitched = false;

  String fontSize = "Medium";

  Data? settingInfo;

  @override
  void initState() {
    // TODO: implement initState
    checkFontSize();
    getSettingInfo();
    super.initState();
  }

  getCurrentUserInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  userInfo = userResponse.data;
                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  getSettingInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/settings/all/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final settingResponse = SettingsResponse.fromJson(data);
              print('result....\n ${settingResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  settingInfo = settingResponse.data;
                  isLoading = false;
                  isSwitched = settingInfo?.fcmNotificationShow ??  false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                  isSwitched = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                  isSwitched = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
          isSwitched = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
      pageTitle: 'Settings',
      bottomNavigationBar: Resources.setBottomDrawer(context),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ListTile(
              dense: true,
              contentPadding:
                  EdgeInsets.only(top: 0.0, bottom: 0.0, left: 16, right: 16),
              hoverColor: themeGreyColor,
              trailing: Container(
                // height: 20,
                // width: 20,
                child: isLoading == true
                    ? CircularProgressIndicator(
                  backgroundColor: themeGolderColor,
                      )
                    : Switch(
                        value: isSwitched,
                        onChanged: (value) {
                          setState(() {
                            isSwitched = value;
                            isLoading = true;
                            print(isSwitched);
                            changeNotificationSetting();
                          });
                        },
                        activeTrackColor: themeGolderColor,
                        inactiveTrackColor: themeWhiteColor,
                        activeColor: themeGolderColor,
                      ),
              ),
              title: Text('Receive Notifications', style: gcNormalTextStyle),
              selected: false,
              onTap: () async {
                setState(() {});
                //await _navigateTo(context, '/notice');
                // Navigator.pushNamed(context, '/statements');
              },
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Divider(
                height: 1,
                color: themeGreyColor,
              ),
            ),
            ListTile(
              dense: true,
              contentPadding:
                  EdgeInsets.only(top: 0.0, bottom: 0.0, left: 16, right: 16),
              hoverColor: themeGreyColor,
              trailing: Wrap(
                children: [
                  Text(
                    fontSize,
                    style: gcListTitleTextStyle,
                  ),
                  Container(
                      height: 20,
                      width: 20,
                      child: Icon(
                        Icons.chevron_right,
                        color: Colors.white,
                      )),
                ],
              ),
              title: Text('Change Font', style: gcNormalTextStyle),
              selected: false,
              onTap: () async {
                setState(() {});
                // showCupertinoModalBottomSheet(
                //   context: context,
                //   builder: (context) => ModalFit(),
                // ).then((value) {
                //   checkFontSize();
                // });
                showModalBottomSheet(context: context, builder: (context) => ModalFit(),).then((value) =>{
                checkFontSize()
                });
              },
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Divider(
                height: 1,
                color: themeGreyColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _navigateTo(BuildContext context, String s) {}

  Future<void> checkFontSize() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value = (prefs.get('fontSize') as String ?? "Medium");
    print(value);
    setState(() {
      fontSize = value.toString();
    });
    //getCurrentUserInfo();
  }

  void changeNotificationSetting() async {
    var params = {
      "title": "FCM Notifications Show",
      "name": "fcm_notification_show",
      "value": isSwitched.toString(),
    };
    print(params);

    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +  "/api/v1/settings/save/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .post(Uri.parse(url), body: params, headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
              "Content-Type": "application/x-www-form-urlencoded",
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("fcm status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              // final noticeResponse = GetOtpResponse.fromJson(data); //noticeResponseFromJson(data);
              // print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  isLoading = false;
                });
                if (isSwitched == true) {
                  Fluttertoast.showToast(
                      msg: "Notification Subscription is turned ON!",
                      backgroundColor: themeGolderColor);
                } else {
                  Fluttertoast.showToast(
                      msg: "Notification Subscription is turned OFF!",
                      backgroundColor: Colors.red);
                }
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                  isSwitched = !isSwitched;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                  isSwitched = !isSwitched;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
          isSwitched = !isSwitched;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }
}
