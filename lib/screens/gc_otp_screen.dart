import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/login_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/screens/TakeAway/dialogs.dart';
import 'package:gulshan_club_app/screens/TakeAway/order_response.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

class OTPScreen extends StatefulWidget {
  static const String route = '/otp';
  String? preRoute;
  OrderResponse? data;
  OTPScreen({this.preRoute, this.data});
  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  bool _onEditing = false;
  String _code = '';
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      body: SingleChildScrollView(
        child: Stack(children: [
          // Image.asset("assets/images/background.png",
          //   height: MediaQuery.of(context).size.height,
          //   width: MediaQuery.of(context).size.width,
          //   fit: BoxFit.cover,
          // ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0, left: 16),
              child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                      height: 30,
                      width: 30,
                      child: IconButton(
                        icon: SvgPicture.asset('assets/icons/ic_back.svg'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ))),
            ),
          ),
          Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Text(
                'Verification Code',
                style: gcTitleTextStyle,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Please type the verification code sent to ${Resources.mobileNo.toString()}',
                  style: gcDescriptionTextStyle,
                ),
              ),

              SizedBox(
                height: 30,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.65,
                child: VerificationCode(
                  autofocus: true,
                  textStyle: gcNormalTextStyle,
                  underlineColor: Colors.amber,
                  keyboardType: TextInputType.number,
                  length: 4,
                  // clearAll is NOT required, you can delete it
                  // takes any widget, so you can implement your design
                  // clearAll: Padding(
                  //   padding: const EdgeInsets.all(8.0),
                  //   child: Text(
                  //     'clear all',
                  //     style: TextStyle(
                  //         fontSize: 14.0,
                  //         decoration: TextDecoration.underline,
                  //         color: Colors.blue[700]),
                  //   ),
                  // ),
                  onCompleted: (String value) {
                    setState(() {
                      _code = value;
                    });
                  },
                  onEditing: (bool value) {
                    setState(() {
                      _onEditing = value;
                    });
                    if (!_onEditing) FocusScope.of(context).unfocus();
                  },
                ),
              ),

              // Padding(
              //   padding: const EdgeInsets.all(16.0),
              //   child: GestureDetector(
              //     onTap: (){
              //
              //     },
              //       child: Text('Resend OTP',style: gcUnderlinedTextStyle,)),
              // ),

              Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(
                  child: _onEditing
                      ? Text(
                          'Please enter full code',
                          style: TextStyle(color: Colors.red),
                        )
                      : SizedBox(
                          height: 1,
                        ),
                ),
              ),

              SizedBox(
                height: 40,
              ),
              (isLoading == true)
                  ? CircularProgressIndicator(
                      backgroundColor: themeGolderColor,
                    )
                  : MyCustomButton(
                      height: 30,
                      width: 200,
                      color: Colors.white,
                      child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Next")
                          // SvgPicture.asset(
                          //   'assets/icons/ic_login.svg',
                          //   height: 30,
                          // ),
                          ),
                      onPressed: () {
                        print('tapped');
                        setState(() {
                          isLoading = true;
                        });
                        sendOTP();
                      },
                    ),

              SizedBox(
                height: 40,
              ),
            ],
          ),
        ]),
      ),
    );
  }

  sendOTP() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = "";
    // print("data-----${widget.data.data.id.toString()}");
    if (widget.preRoute == "checkout") {
      url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/order/${widget.data?.data?.id.toString()}/code?code=$_code";
    } else {
      url = NetworkManager.baseUrl +
          NetworkManager.apiString +
          NetworkManager.versionString +
          "/users/$_code/opt_validate/";
    }
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              // final noticeResponse = GetOtpResponse.fromJson(data); //noticeResponseFromJson(data);
              // print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                setState(() {
                  isLoading = false;
                });

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200 && data["status"] == true) {
                setState(() {
                  isLoading = false;
                });
                if (widget.preRoute == "checkout") {
                  WCDialog.showContentDialogForCheckout(context,
                      message: "Order Placed Successfully", onTap: () {
                    // Navigator.pop(context);
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/take_away', (route) => false);
                  });
                } else {
                  setState(() {
                    SharedPref.save("user", Resources.currentUserData);
                    // print(data);

                    SharedPref.contain("user").then((val) async {
                      print('user read--\n $val');
                      if (val) {
                        String value = await SharedPref.read2('user');
                        final data2 = loginResponseFromJson(value);
                        setState(() {
                          Resources.loggedIn = true;
                          isLoading = false;
                          Resources.currentUser = data2;
                        });
                        print(data2.refreshToken);
                        print(
                            'access token - ${Resources.currentUser?.accessToken}');
                        print("Logged In");
                        // Navigator.pushNamed(context, '/mobileNo');
                      } else {
                        setState(() {
                          Resources.loggedIn = false;
                          isLoading = false;
                        });
                        Fluttertoast.showToast(
                            msg: NetworkManager.errorMessage,
                            toastLength: Toast.LENGTH_LONG,
                            gravity: ToastGravity.CENTER,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);

                        print("not Logged In");
                      }
                      // });
                    });
                    //
                    // SharedPref.save("access_token", Resources.currentUserData.accessToken);
                    // print("access token---- ${Resources.currentUserData.accessToken}");
                  });
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/building_directory', (route) => false);
                }
              } else if (value.statusCode == 400) {
                setState(() {
                  isLoading = false;
                });
                Fluttertoast.showToast(
                  msg: data["data"].toString(),
                  backgroundColor: Colors.red,
                  gravity: ToastGravity.CENTER,
                );
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });

                if (widget.preRoute == "checkout") {
                  Fluttertoast.showToast(
                    msg: data["data"].toString(),
                    backgroundColor: Colors.red,
                    gravity: ToastGravity.CENTER,
                  );
                } else {
                  Fluttertoast.showToast(
                    msg: data["data"].toString(),
                    backgroundColor: Colors.red,
                    gravity: ToastGravity.CENTER,
                  );
                }
              } else {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                // Fluttertoast.showToast(
                //   msg:
                //   'Error-${value.statusCode}, ${NetworkManager.errorMessage}',
                //   backgroundColor: Colors.red,
                //   gravity: ToastGravity.CENTER,
                // );
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }
}
