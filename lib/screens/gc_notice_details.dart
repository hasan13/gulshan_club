import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gulshan_club_app/jsonModels/notice_board_response.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';

class NoticeDetailsScreen extends StatefulWidget {
  static const String route = '/notice_details';

  @override
  _NoticeDetailsScreenState createState() => _NoticeDetailsScreenState();
}

class _NoticeDetailsScreenState extends State<NoticeDetailsScreen> {
  late Notice selectedEvent;

  @override
  void initState() {
    // TODO: implement initState


    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    final Notice sEvent = ModalRoute.of(context)?.settings.arguments as Notice;

    print(sEvent.title);
    return Scaffold(
        backgroundColor: themeBlackColor,
        appBar: AppBar(
          backgroundColor: themeBlackColor,
          title: Text("Notice",style: ScaffoldAppBarTitle,),
          leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                  height: 10,
                  width: 10,
                  child: IconButton(
                      color: themeGolderColor,
                      icon: SvgPicture.asset('assets/icons/ic_back.svg',color: themeGolderColor,height: 20,width: 20,),
                    onPressed: () {
                      Navigator.pop(context);

                    },))),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Center(
              child: InteractiveViewer(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.8,
                  width: MediaQuery.of(context).size.width,
                  child: (sEvent.imageMedium != null)
                      ? CachedNetworkImage(
                    imageUrl:  NetworkManager.baseUrl + sEvent.imageMedium!,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.fitHeight,
                          // colorFilter:
                          // ColorFilter.mode(Colors.red, BlendMode.colorBurn)
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                    errorWidget: (context, url, error) => Column(
                      children: [
                        Icon(Icons.error),
                        // Text(itemList[index].imageAltText),
                      ],
                    ),
                  )
                      : Image.asset(
                    'assets/images/events1.jpg',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
        //     ListView(
        //       shrinkWrap: true,scrollDirection: Axis.vertical ,
        //         children: [
        //   Align(
        //     alignment: Alignment.topLeft,
        //     child: Padding(
        //       padding: const EdgeInsets.only(top: 32.0, left: 16),
        //       child: GestureDetector(
        //           onTap: () {
        //             Navigator.pop(context);
        //           },
        //           child: Container(color: themeGolderColor, height: 40, width: 40, child: IconButton(icon: SvgPicture.asset('assets/icons/ic_back.svg')))),
        //     ),
        //   ),
        //   Align(
        //     alignment: Alignment.center,
        //     child: Container(
        //       height: MediaQuery.of(context).size.height * 0.8,
        //       width: MediaQuery.of(context).size.width,
        //       child: (sEvent.imageMedium != null)
        //           ? CachedNetworkImage(
        //               imageUrl: sEvent.imageMedium,
        //               imageBuilder: (context, imageProvider) => Container(
        //                 decoration: BoxDecoration(
        //                   image: DecorationImage(
        //                     image: imageProvider,
        //                     fit: BoxFit.fitHeight,
        //                     // colorFilter:
        //                     // ColorFilter.mode(Colors.red, BlendMode.colorBurn)
        //                   ),
        //                 ),
        //               ),
        //               placeholder: (context, url) => Center(child: CircularProgressIndicator()),
        //               errorWidget: (context, url, error) => Column(
        //                 children: [
        //                   Icon(Icons.error),
        //                   // Text(itemList[index].imageAltText),
        //                 ],
        //               ),
        //             )
        //           : Image.asset(
        //               'assets/images/events1.jpg',
        //               fit: BoxFit.fill,
        //             ),
        //     ),
        //   ),
        // ])
        )
    );
  }
}
