import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/jsonModels/userListResponse.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_drawer.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../jsonModels/member_details_response.dart';
import '../utils/shared_preference.dart';

class MemberDetailsScreen extends StatefulWidget {
  static const String route = '/member_details';

  @override
  _MemberDetailsScreenState createState() => _MemberDetailsScreenState();
}

class _MemberDetailsScreenState extends State<MemberDetailsScreen> {
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  Member? userInfo;

  int tabIndex = 1;
  var letterString = "";
  var numberString = "";

  List<Relation> relations = [];

  @override
  void initState() {
    // TODO: implement initState
    getMemberDetails();

    super.initState();
  }

  getMemberDetails() async {
    print(Resources.currentUser?.accessToken);
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/get-user-relation-details/${Resources.selectedMemberForDetails?.id.toString()}";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) async {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = MemberDetailsResponse.fromJson(data);
              print('result....\n ${userResponse.user}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  isLoading = false;
                  userInfo = userResponse.user;
                  relations = userResponse.relation ?? [];
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (value.statusCode == 404) {
                SharedPref.remove('user');
                await _navigateTo(context, '/welcome');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        title: Text(
          "Member Details",
          style: ScaffoldAppBarTitle,
        ),
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
                height: 10,
                width: 10,
                child: IconButton(
                    color: themeGolderColor,
                    icon: SvgPicture.asset(
                      'assets/icons/ic_back.svg',
                      color: themeGolderColor,
                      height: 20,
                      width: 20,
                    ), onPressed: () {
                  Navigator.pop(context);

                },))),
        centerTitle: true,
      ), // bottomNavigationBar: Resources.setBottomDrawer(context),
      // bottomNavigationBar: new Theme(
      //   data: Theme.of(context).copyWith(
      //     // sets the background color of the `BottomNavigationBar`
      //       canvasColor: themeBlackColor,
      //       // sets the active color of the `BottomNavigationBar` if `Brightness` is light
      //       primaryColor: Colors.red,
      //       textTheme: Theme.of(context).textTheme.copyWith(
      //           caption: new TextStyle(
      //               color: Colors
      //                   .yellow))), // sets the inactive color of the `BottomNavigationBar`
      //   child: BottomNavigationBar(
      //       selectedItemColor: themeGolderColor,
      //       unselectedItemColor: Colors.grey[400],
      //       iconSize: 30,
      //       backgroundColor: Colors.blue,
      //       currentIndex: tabIndex,
      //       onTap: (int index) {
      //         setState(() {
      //           tabIndex = index;
      //           print("tabIndex----$tabIndex");
      //           Resources.moveTo(index, context);
      //         });
      //       },
      //       items: [
      //         BottomNavigationBarItem(
      //             activeIcon: Icon(
      //               Icons.home,
      //               color: themeGolderColor,
      //               size: 30,
      //             ),
      //             icon: Icon(
      //               Icons.home,
      //               color: themeGreyColor,
      //               size: 28,
      //             ),
      //             label: ''),
      //
      //         BottomNavigationBarItem(
      //             activeIcon: SvgPicture.asset(
      //               "assets/icons/ic_profile.svg",
      //               color: themeGolderColor,
      //               width: 30,
      //               height: 30,
      //             ),
      //             icon: SvgPicture.asset(
      //               "assets/icons/ic_profile.svg",
      //               color: themeGreyColor,
      //               width: 26,
      //               height: 26,
      //             ),
      //             label: ''),
      //         BottomNavigationBarItem(
      //             activeIcon: SvgPicture.asset(
      //               "assets/icons/ic_memeber_director.svg",
      //               color: themeGolderColor,
      //               width: 30,
      //               height: 30,
      //             ),
      //             icon: SvgPicture.asset(
      //               "assets/icons/ic_memeber_director.svg",
      //               color: themeGreyColor,
      //               width: 26,
      //               height: 26,
      //             ),
      //             label: ''),
      //
      //         BottomNavigationBarItem(
      //             activeIcon: SvgPicture.asset(
      //               "assets/icons/ic_notice_board.svg",
      //               color: themeGolderColor,
      //               width: 30,
      //               height: 30,
      //             ),
      //             icon: SvgPicture.asset(
      //               "assets/icons/ic_notice_board.svg",
      //               color: themeGreyColor,
      //               width: 26,
      //               height: 26,
      //             ),
      //             label: ''),
      //
      //         // BottomNavigationBarItem(
      //         //     activeIcon: SvgPicture.asset(
      //         //       "assets/icons/ic_facilities.svg",
      //         //       color: themeGolderColor,
      //         //       width: 30,
      //         //       height: 30,
      //         //     ),
      //         //     icon: SvgPicture.asset(
      //         //       "assets/icons/ic_facilities.svg",
      //         //       color: themeGreyColor,
      //         //       width: 26,
      //         //       height: 26,
      //         //     ),
      //         //     label: ''),
      //       ]),
      // ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : userInfo == null
              ? Center(
                  child: Text(
                  "Something went wrong",
                  style: GoogleFonts.poppins(color: themeGreyColor),
                ))
              : createListView(userInfo!, context),
    );
  }

  Widget createListView(Member result, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {
    String dateString = "";
    if (result.membershipDate != null) {
      DateTime parsedDate =
          DateTime.parse(result.membershipDate.toString()).toLocal();
      dateString = DateFormat.yMd().format(parsedDate);
    }

    var accountNo = result.clubAcNumber;
    var letterString = accountNo?.replaceAll(new RegExp(r'[^A-Za-z]'), '');
    var numberString = accountNo?.replaceAll(new RegExp(r'[^0-9]'), '');

    List<Widget> commentsWidgets = [];

    for (var data in relations) {

      var accountNo2 = data.clubAcNumber ?? '';
      var letterString2 = accountNo2.replaceAll(new RegExp(r'[^A-Za-z]'), '');
      var numberString2 = accountNo2.replaceAll(new RegExp(r'[^0-9]'), '');

      String tmp = "";
      if (data.membershipDate != null) {
        DateTime parsedDate =
        DateTime.parse(data.membershipDate.toString()).toLocal();
        tmp = DateFormat.yMd().format(parsedDate);
      }

      String birthString = "";
      if (data.birthday != null) {
        DateTime parsedDate =
        DateTime.parse(data.birthday.toString()).toLocal();
        birthString = DateFormat.yMd().format(parsedDate);
      }

      commentsWidgets.add(Padding(
        padding: const EdgeInsets.all(12.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius:
                BorderRadius.all(Radius.circular(8)), // color: Colors.white38,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        height: 48,
                        width: 48,
                        child: (data.imageMedium != null)
                            ? ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                child: CachedNetworkImage(
                                  imageUrl:
                                      NetworkManager.baseUrl + data.imageMedium!,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.fitHeight,
                                          colorFilter: ColorFilter.mode(
                                              Colors.transparent,
                                              BlendMode.color)),
                                    ),
                                  ),
                                  placeholder: (context, url) => Center(
                                      child: CircularProgressIndicator()),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              )
                            : Image.asset(
                                'assets/icons/ic_placeholder.png',
                                height: 48,
                                width: 48,
                              ),
                      ), //AssetImage('assets/icons/ic_placeholder.png'),

                      //   CircleAvatar(
                      //     radius: 47,
                      //     backgroundColor: Colors.transparent,
                      //     backgroundImage: (itemList[index].imageMedium != null) ? NetworkImage(NetworkManager.baseUrl + itemList[index].imageMedium,) : AssetImage('assets/icons/ic_placeholder.png'),
                      //     //AssetImage('assets/images/Facilities1.jpg'),
                      //   ),
                    ),
                    Flexible(
                      // child: Card(color: Colors.white38,elevation: 0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data.name ?? ' ', //"Capt. Saifur Rahman",
                            style: gcListTitleTextStyle,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: detailsBackground,
                            ),
                            width: 120,
                            height: 30,
                            child: Text(
                              data.relation ?? ' ',
                              textAlign: TextAlign.center,
                              //'01819-415452',
                              style: GoogleFonts.poppins(
                                  color: themeBlackColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                            // Text(
                            //   itemList[index]
                            //       .phonePrimary, //'01819-415452',
                            //   style: GoogleFonts.poppins(
                            //       // color: themeWhiteColor,
                            //       fontWeight:
                            //           FontWeight.bold,
                            //       fontSize: 16),
                            // ),
                          ),
                        ],
                      ),
                    ),
                    // ),
                  ],
                ),
                if (data.clubAcNumber != null)
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10.0, top: 10, bottom: 0),
                    child: Row(
                      children: [
                        Icon(
                          Icons.person,
                          color: listDescriptionColor,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          'A/C No: ',
                          style: gcGreyDescriptionTextStyle,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        (data.clubAcNumber != null)
                            ? Text(
                                letterString2 + "-" + numberString2, //'DR-7',
                                style: gcGreyDescriptionTextStyle,
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                (data.membershipDate != null && data.membershipDate != '')
                    ? Padding(
                  padding: const EdgeInsets.only(left: 10.0, top: 10),
                  child: Row(
                    children: [
                      Text(
                        'Member Since: ' ,
                        style: gcGreyDescriptionTextStyle,
                      ),
                      Text(tmp,
                        style: gcGreyDescriptionTextStyle,
                      ),
                    ],
                  ),
                )
                    : SizedBox(),
                if (data.email != null)
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/icons/ic_mail.svg',
                          color: listDescriptionColor,
                          height: 16,
                          width: 16,
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          child: Text(
                            data.email.toString(), //'rubelaziz@partyex.net',
                            style: gcGreyDescriptionTextStyle,
                          ),
                        ),
                      ],
                    ),
                  ),

                if (data.birthday != null)
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: Row(
                      children: [
                        Text(
                          'DOB: ' ,
                          style: gcGreyDescriptionTextStyle,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Text(
                            birthString, //'rubelaziz@partyex.net',
                            style: gcGreyDescriptionTextStyle,
                          ),
                        ),
                      ],
                    ),
                  ),
                if (data.phonePrimary != null)
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/icons/ic_call.svg',
                          color: listDescriptionColor,
                          height: 16,
                          width: 16,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Expanded(
                          child: Text(
                            data.phonePrimary
                                .toString(), //'rubelaziz@partyex.net',
                            style: gcGreyDescriptionTextStyle,
                          ),
                        ),
                        if(data.phonePrimary != null)GestureDetector(
                          onTap: () async {
                            print('${data.phonePrimary}');
                            //launch("tel://${itemList[index].mobileNumberPrimary}");

                            String url =
                                "tel://" + data.phonePrimary.toString();
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {}
                          },
                          child: Center(
                            child: Container(
                              padding: EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                // color: themeGolderColor.withOpacity(0.3),
                              ),
                              // width: 120,
                              height: 30,
                              child: Text(
                                'Call Now',
                                textAlign: TextAlign.center,
                                //'01819-415452',
                                style: GoogleFonts.poppins(
                                    color: themeWhiteColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12),
                              ),
                              // Text(
                              //   itemList[index]
                              //       .phonePrimary, //'01819-415452',
                              //   style: GoogleFonts.poppins(
                              //       // color: themeWhiteColor,
                              //       fontWeight:
                              //           FontWeight.bold,
                              //       fontSize: 16),
                              // ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),


              ],
            ),
          ),
        ),
      )); // TODO: Whatever layout you need for each widget.
    }

    return SingleChildScrollView(
      child: Column(
         mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(10),
                ),
                height: 72,
                width: 70,
                child: (result.imageMedium != null)
                    ? ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        child: CachedNetworkImage(
                          imageUrl: NetworkManager.baseUrl + result.imageMedium! ,
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              // borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fitHeight,
                                  colorFilter: ColorFilter.mode(
                                      Colors.transparent, BlendMode.color)),
                            ),
                          ),
                          placeholder: (context, url) =>
                              Center(child: CircularProgressIndicator()),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      )
                    : Image.asset(
                        'assets/icons/ic_placeholder.png',
                        height: 72,
                        width: 70,
                      ),
              ),
              // CircleAvatar(
              //   radius: 49,
              //   backgroundColor: themeGreyColor,
              //   child: CircleAvatar(
              //     radius: 47,
              //     backgroundColor: Colors.transparent,
              //     backgroundImage: (result.imageMedium != null)
              //         ? NetworkImage(result.imageMedium)
              //         : AssetImage('assets/icons/ic_placeholder.png'),
              //     //AssetImage('assets/images/Facilities1.jpg'),
              //   ),
              // ),
            ),
          ),
          Text(
            result.firstName.toString() +
                ' ' +
                result.lastName.toString(), //"Rubel Aziz",
            style: gcKLargeListTitleTextStyle, softWrap: true,
            overflow: TextOverflow.ellipsis, textAlign: TextAlign.center,
          ),
          // if (result.designation != null)
          //   Text(
          //     result.designation.toString(), // "Director",
          //     style: gcDescriptionTextStyle,
          //   ),
          (result.designation != null)
              ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28.0,vertical: 8),
                child: Container(
            decoration: BoxDecoration(
                  color: themeGolderColor.withOpacity(0.8 ),
                  borderRadius: BorderRadius.all(
                      Radius.circular(8))),
            padding: EdgeInsets.all(16),
            child: Wrap(
                // spacing: 8.0,
                children: [
                  (result
                      .designation
                      .toString() ==
                      "President")
                      ? SvgPicture.asset(
                    'assets/icons/ic_crown.svg',
                    height: 12,
                    width: 12,
                    color: themeWhiteColor,
                  )
                      : SizedBox(
                    height: 1,
                  ),
                  Text(
                    result
                        .designation
                        .toString(), //"Captain",
                    style: gcNormalTextStyle,
                  ),
                ],
            ),
          ),
              )
              : SizedBox(
            height: 1,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 10, bottom: 10),
            child: Row(
              children: [
                Icon(
                  Icons.person,
                  color: themeGolderBackColor,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  'A/C No: ',
                  style: gcDescriptionTextStyle,
                ),
                SizedBox(
                  width: 8,
                ),
                (result.clubAcNumber != null)
                    ? Text(
                        letterString! + "-" + numberString!, //'DR-7',
                        style: gcDescriptionTextStyle,
                      )
                    : SizedBox(),
              ],
            ),
          ),
          (result.membershipDate != null && result.membershipDate != '')
              ? Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 10),
            child: Row(
              children: [
                Text(
                  'Member Since: ' + dateString,
                  style: gcDescriptionTextStyle,
                ),
              ],
            ),
          )
              : SizedBox(),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 10),
            child: Row(
              children: [
                SvgPicture.asset(
                  'assets/icons/ic_mail.svg',
                  color: listDescriptionColor,
                  height: 16,
                  width: 16,
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Text(
                    result.email.toString(), //'rubelaziz@partyex.net',
                    style: gcDescriptionTextStyle,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 10),
            child: Row(
              children: [
                SvgPicture.asset(
                  'assets/icons/ic_call.svg',
                  color: listDescriptionColor,
                  height: 16,
                  width: 16,
                ),
                SizedBox(
                  width: 8,
                ),
                Expanded(
                  child: Text(
                    result.phonePrimary.toString(), //'rubelaziz@partyex.net',
                    style: gcDescriptionTextStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Card(
                    // color: themeGreyColor,
                    elevation: 0,
                    child: GestureDetector(
                      onTap: () async {
                        print('${result.phonePrimary}');
                        //launch("tel://${itemList[index].mobileNumberPrimary}");

                        String url = "tel://" + result.phonePrimary.toString();
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {}
                      },
                      child: Center(
                        child: Container(
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: callBackground,
                          ),
                          width: 120,
                          height: 30,
                          child: Text(
                            'Call Now',
                            textAlign: TextAlign.center,
                            //'01819-415452',
                            style: GoogleFonts.poppins(
                                color: themeGolderColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                          // Text(
                          //   itemList[index]
                          //       .phonePrimary, //'01819-415452',
                          //   style: GoogleFonts.poppins(
                          //       // color: themeWhiteColor,
                          //       fontWeight:
                          //           FontWeight.bold,
                          //       fontSize: 16),
                          // ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Divider(
            height: 1,
            color: themeGreyColor,
          ),
          Column(
            children: commentsWidgets,
          )
        ],
      ),
    );
  }

  _navigateTo(BuildContext context, String s) {}
}
