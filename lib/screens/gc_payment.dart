import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
 import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/current_user_info_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

class PaymentScreen extends StatefulWidget {
  static const String route = '/payment';

  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  int tabIndex = 2;

  bool isLoading = false;
  AppUser? userInfo;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    // TODO: implement initState
    Resources.selectedUserIDForPayment = "";
    getCurrentUserInfo();
    super.initState();
  }

  getCurrentUserInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/users/basic_info/";
    print('url---> $url');
    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(value.body);
              final userResponse = CurrentUserInfoResponse.fromJson(data);
              print('result....\n ${userResponse.data}');
              if (value.statusCode == 200 || value.statusCode == 202) {
                setState(() {
                  userInfo = userResponse.data;
                  Resources.myUserID = userInfo?.id;

                  Resources.mobileNo = userResponse.data?.phonePrimary;
                  // Resources.selectedUserIDForPayment = userInfo.id.toString();
                  isLoading = false;
                });
                //navigateToSignInPage(context);
                print('YAY! Free cute dog pics!');
              } else if (data['status'] == false) {
                setState(() {
                  isLoading = false;
                });
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                // _scaffoldKey.currentState.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      _scaffoldKey.currentState?.showSnackBar(snackBar);
      //ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: themeBlackColor,
        title: Text(
          "Payment",
          style: gcTitleTextStyle,
        ),
      ),
      bottomNavigationBar: new Theme(
        data: Theme.of(context).copyWith(
            // sets the background color of the `BottomNavigationBar`
            canvasColor: themeBlackColor,
            // sets the active color of the `BottomNavigationBar` if `Brightness` is light
            primaryColor: Colors.red,
            textTheme: Theme.of(context).textTheme.copyWith(
                caption: new TextStyle(
                    color: Colors
                        .yellow))), // sets the inactive color of the `BottomNavigationBar`
        child: BottomNavigationBar(
            selectedItemColor: themeGolderColor,
            unselectedItemColor: Colors.grey[400],
            iconSize: 30,
            backgroundColor: Colors.blue,
            currentIndex: tabIndex,
            onTap: (int index) {
              setState(() {
                tabIndex = index;
                print("tabIndex----$tabIndex");
                moveTo(index, context);
              });
            },
            items: [
              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_notice_board.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_notice_board.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_facilities.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_facilities.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_payment.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_payment.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: SvgPicture.asset(
                    "assets/icons/ic_memeber_director.svg",
                    color: themeGolderColor,
                    width: 30,
                    height: 30,
                  ),
                  icon: SvgPicture.asset(
                    "assets/icons/ic_memeber_director.svg",
                    color: themeGreyColor,
                    width: 26,
                    height: 26,
                  ),
                  label: ''),

              BottomNavigationBarItem(
                  activeIcon: Icon(
                    Icons.home,
                    color: themeGolderColor,
                    size: 30,
                  ),
                  icon: Icon(
                    Icons.home,
                    color: themeGreyColor,
                    size: 28,
                  ),
                  label: ''),
            ]),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // Center(
            //   child: Padding(
            //     padding: const EdgeInsets.all(80.0),
            //     child:Text('This option is currently unavailable!',style: gcTitleTextStyle,)
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: Container(
                    child: ListTile(
                      tileColor: themeGreyColor,
                      leading: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          child: Container(
                            color: themeWhiteColor,
                            height: 40,
                            width: 40,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SvgPicture.asset(
                                'assets/icons/Payment_Payforme.svg',
                                color: themeGolderColor,
                              ),
                            ),
                          )),
                      title: Text(
                        'Pay For Me',
                        style: gcTitleTextStyle,
                      ),
                      selected: false,
                      onTap: () async {
                        // await _navigateTo(context, '/building_directory');
                        Resources.selectedUserIDForPayment =
                            userInfo?.id.toString();

                        Navigator.pushNamed(context, '/payment_methods',
                            arguments: false);
                      },
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: ListTile(
                  tileColor: themeGreyColor,
                  leading: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: Container(
                        color: themeWhiteColor,
                        height: 40,
                        width: 40,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SvgPicture.asset(
                            'assets/icons/Payment_Payforothers.svg',
                            color: themeGolderColor,
                          ),
                        ),
                      )),
                  title: Text(
                    'Pay For Others',
                    style: gcTitleTextStyle,
                  ),
                  selected: false,
                  onTap: () async {
                    await _navigateTo(context, '/pay_for_others');
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: ListTile(
                  tileColor: themeGreyColor,
                  leading: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      child: Container(
                        color: themeWhiteColor,
                        height: 40,
                        width: 40,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SvgPicture.asset(
                            'assets/icons/Payment Statement.svg',
                            color: themeGolderColor,
                          ),
                        ),
                      )),
                  title: Text(
                    'Payment Statements',
                    style: gcTitleTextStyle,
                  ),
                  selected: false,
                  onTap: () async {
                    Navigator.pushNamed(context, '/statements');
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 22, right: 22),
              child: Container(
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: Container(
                        child: SvgPicture.asset(
                          'assets/icons/payment_alert.svg',
                          height: 11,
                          width: 8,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 50.0),
                        child: Text(
                          "We have no refund policy, if you made any wrong payment and need to refund then please kindly contact with Gulshan Club office with an application.",
                          style: gcPaymentAlertStyle,
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 22, right: 22),
              child: Container(
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: Container(
                        child: SvgPicture.asset(
                          'assets/icons/payment_alert.svg',
                          height: 11,
                          width: 8,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Text(
                          "Honorable Member please check the Gulshan Club E-Tin (836549204290) for your convenient.",
                          style: gcPaymentAlertStyle,
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static moveTo(int index, BuildContext context) {
    switch (index) {
      case 0:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/notice', (route) => false);

        }
        break;

      case 1:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/club_facilities', (route) => false);
          break;

        }
      case 2:
        {
          // Navigator.pushNamedAndRemoveUntil(context, '/members_directory', (route) => false);
          break;

        }

      case 3:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/members_directory', (route) => false);
        }
        break;

      case 4:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/building_directory', (route) => false);
        }
        break;
      default:
        {
          Navigator.pushNamedAndRemoveUntil(context, '/building_directory', (route) => false);

        }
        break;
    }
  }


  Future<void> _navigateTo(BuildContext context, String routeName) async {
    await Navigator.pushNamed(context, routeName);
    // Navigator.pushNamed(context, '/facility_details',arguments: itemList[index]);
  }
}
