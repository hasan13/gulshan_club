import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/club_facilities_response.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_custom_button.dart';
import 'package:gulshan_club_app/theme/theme_custom_input_field.dart';
import 'package:gulshan_club_app/theme/theme_custom_validator.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';

class FacilityDetails extends StatefulWidget {
  static const String route = '/facility_details';

  @override
  _FacilityDetailsState createState() => _FacilityDetailsState();
}

class _FacilityDetailsState extends State<FacilityDetails> {
  Facility? selectedEvent;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final Facility sEvent = ModalRoute.of(context)?.settings.arguments as Facility;

    return Scaffold(
      backgroundColor: themeBlackColor,
      appBar: AppBar(
        backgroundColor: themeBlackColor,
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
                height: 10,
                width: 10,
                child: IconButton(
                  color: themeGolderColor,
                    icon: SvgPicture.asset('assets/icons/ic_back.svg',color: themeGolderColor,height: 20,width: 20,), onPressed: () {
                  Navigator.pop(context);

                },))),
         centerTitle: true,
         title:Text(
           sEvent.name ?? ' ',
           style: ScaffoldAppBarTitle,
           textAlign: TextAlign.left,
         ),

      ),
      body: SingleChildScrollView(

        child: Stack(children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 300,
                child:
                (sEvent.imageMedium != null)? CachedNetworkImage(
                  imageUrl: NetworkManager.baseUrl + sEvent.imageMedium!,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                          colorFilter:
                          ColorFilter.mode(Colors.transparent, BlendMode.color)),
                    ),
                  ),
                  placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ):Image.asset(
                  'assets/images/events1.jpg',
                  fit: BoxFit.fill,
                ),
//                Image.asset(
//                  'assets/images/events1.jpg',
//                  fit: BoxFit.cover,
//                ),
              ),
              SizedBox(
                height: 24,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(sEvent.description ?? ' ',
                     style: gcDescriptionTextStyle,
                    textAlign: TextAlign.justify),
              ),
              SizedBox(
                height: 24,
              ),
            ],
          ),
//          Align(
//            alignment: Alignment.topLeft,
//            child: Padding(
//              padding: const EdgeInsets.only(top: 24.0, left: 16),
//              child: GestureDetector(
//                  onTap: () {
//                    Navigator.pop(context);
//                  },
//                  child: Container(
//                      height: 30,
//                      width: 30,
//                      child: IconButton(
//                          icon: SvgPicture.asset('assets/icons/ic_back.svg')))),
//            ),
//          ),
//          Align(
//            alignment: Alignment.topCenter,
//              child: Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Text(
//                  sEvent.name,
//                  style: gcListTitleTextStyle,
//                  textAlign: TextAlign.left,
//                ),
//              ),
//          ),

        ]),
      ),
    );
  }
}
