import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
 import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/jsonModels/club_facilities_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_scafold.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

class ClubFacilitiesScreen extends StatefulWidget {
  static const String route = '/club_facilities';

  @override
  _ClubFacilitiesScreenState createState() => _ClubFacilitiesScreenState();
}

class _ClubFacilitiesScreenState extends State<ClubFacilitiesScreen> {
  int _current = 0;
  List<String> imgList = [
    'assets/images/Facilities1.jpg',
    'assets/images/Facilities2.jpg',
    'assets/images/Facilities1.jpg',
    'assets/images/Facilities2.jpg',
    'assets/images/Facilities1.jpg',
    'assets/images/Facilities2.jpg',
    'assets/images/Facilities1.jpg',
    'assets/images/Facilities2.jpg',
  ];
  
  List<Facility> itemList = [];
  var isLoading = true;
  final _scaffoldKey = GlobalKey<ScaffoldMessengerState>();

  ClubFacilitiesResponse? clubFacilityResponse;
  bool isLoadingMore = false;

  int tabIndex = 1;

  @override
  void initState() {
    // TODO: implement initState
    getClubFacilities();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ThemeScaffold(
        pageTitle: 'Facilities',
        bottomNavigationBar: Resources.setBottomDrawer(context),
        body: isLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : itemList == null
            ? Center(
            child: Text(
              "Something went wrong",
              style: TextStyle(),
            ))
            : itemList.length == 0
            ? Center(
          child: Text(
            "No facilities found",
            style: gcPlaceholderTextStyle,
          ),
        )
            : createListView(itemList, context)
    );
  }

  Future<void> _reloadData() async {
    setState(() {
      getClubFacilities();
    });
  }

  getClubFacilities() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl + NetworkManager.apiString + NetworkManager.versionString + "/club_facility_detail_details/";
    print('url---> $url');
    if (result == true) {
      try {
        http.get(Uri.parse(url), headers: {"Authorization": "Bearer ${Resources.currentUser?.accessToken}"}).timeout(Duration(seconds: 30)).then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          final data = json.decode(utf8.decode(value.bodyBytes));
          final clubResponse = ClubFacilitiesResponse.fromJson(data);//noticeResponseFromJson(data);
          print('result....\n ${clubResponse.results}');

          if (value.statusCode == 401) {
            Navigator.pushNamedAndRemoveUntil(context, '/welcome', (route) => false);
            return;
          }
          if (value.statusCode == 200) {
            setState(() {
              itemList  = clubResponse.results!;
              clubFacilityResponse = clubResponse;
              isLoading = false;
            });
          } else if (data['status'] == false) {

            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text('Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            setState(() {
              isLoading = false;
            });
            final snackBar = SnackBar(
              backgroundColor: Colors.red,
              content: Text('Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
            );
            _scaffoldKey.currentState?.showSnackBar(snackBar);
            //ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {
        isLoading = false;
      });
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }
  _loadMoreData() async {
    print("load more");
    // update data and loading status

    String uploadEndPoint = clubFacilityResponse?.next; //NetworkManager.baseUrl + apiEndPoint;
    print(uploadEndPoint);
    final response = await http.get(Uri.parse(uploadEndPoint), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      "Authorization": "Bearer ${Resources.currentUser?.accessToken}",
    });
    print(response.body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      print("inside condition");
      setState(() {
        isLoadingMore = false;
      });

      final moreNoticeResponse = ClubFacilitiesResponse.fromJson(json.decode(response.body)); //allProductItemFromJson(response.body);
      setState(() {
        isLoadingMore = false;
        clubFacilityResponse = moreNoticeResponse;
        List<Facility> moreData = moreNoticeResponse.results ?? [];
        itemList.addAll(moreData);
        // allData = moreNoticeResponse.data;
      });
    } else {
      //different status code
      print("inside different status code");
      setState(() {
        isLoadingMore = false;
        // allData = null;
      });

      return [];
    }
  }

  Widget createListView(List<Facility> results, BuildContext context) {
    //Widget createListView2(List<Recipe> data, BuildContext context) {

    return  NotificationListener(
      onNotification: (ScrollNotification scrollInfo){
        if (scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
          if (!isLoadingMore && clubFacilityResponse?.next != null) {
            // start loading data
            print("Scrolling to end...");
            setState(() {
              isLoadingMore = true;
            });
            _loadMoreData();
          } else if (clubFacilityResponse?.next == null) {
            print("Data end");
            setState(() {
              isLoadingMore = false;
            });
          } else {}
        }
        return true;
      },
      child: RefreshIndicator(
        onRefresh: _reloadData,
        child: GridView.count(
          // Create a grid with 2 columns. If you change the scrollDirection to
          // horizontal, this produces 2 rows.nn
          crossAxisCount: 2,
          crossAxisSpacing: 8,mainAxisSpacing: 4,
          childAspectRatio: (MediaQuery.of(context).size.width/1) /
              (MediaQuery.of(context).size.height / 3),//3 / 2,
          // Generate 100 widgets that display their index in the List.
          children: List.generate(results.length, (index) {
            return GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/facility_details',arguments: itemList[index]);
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                child: Stack(children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: (results[index].imageMedium != null)? CachedNetworkImage(
                      imageUrl: NetworkManager.baseUrl + results[index].imageMedium!,
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                              colorFilter:
                              ColorFilter.mode(Colors.transparent, BlendMode.color)
                          ),
                        ),
                      ),
                      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) =>Container(color:themeGolderColor.withOpacity(0.5),child: SvgPicture.asset("assets/icons/ic_logo.svg",fit: BoxFit.contain,)),
                    ):Image.asset(
                      imgList[0],
                      fit: BoxFit.fill,
                    ),
                  ),
//            (index % 2 == 0)
//                ? Align(
//              alignment: Alignment.center,
//              child: Text(
//                'LOBBY',
//                style: gcTitleTextStyle,
//              ),
//            )
//                :
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                         decoration: BoxDecoration(
                           borderRadius: BorderRadius.circular(4),
                           color: Colors.black.withOpacity(0.4),
                         ),
                         child: Text(
                          itemList[index].name ?? ' ',textAlign: TextAlign.center,//'GYM',
                          style: gcSubTitleTextStyle,
                        ),
                      ),
                    ),
                  ),
                ]),
              ),
            );
          }),
        ),
      ),
    );
  }
}
