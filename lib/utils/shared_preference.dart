import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class SharedPref {


  static read2(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return (prefs.getString(key));
  }


  static read2DoubleValue(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return (prefs.getDouble(key));
  }
  static getFirebaseToken(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key)??"";
  }

  static save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }
  static saveString(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }
  static saveDouble(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble(key, value);
  }
  static remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  static contain(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }
}