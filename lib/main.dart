import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gulshan_club_app/jsonModels/category_list_response.dart';
import 'package:gulshan_club_app/networking/local_notifications_service.dart';
import 'package:gulshan_club_app/screens/TakeAway/gc_products_screen.dart';
import 'package:gulshan_club_app/screens/TakeAway/gc_takeaway_screen.dart';
import 'package:gulshan_club_app/screens/banquet/banquet_list_screen.dart';
import 'package:gulshan_club_app/screens/banquet/book_schedule_screen.dart';
import 'package:gulshan_club_app/screens/banquet/edit_booking_screen.dart';
import 'package:gulshan_club_app/screens/banquet/event_list.dart';
import 'package:gulshan_club_app/screens/gc_board_of_director_screen.dart';
import 'package:gulshan_club_app/screens/gc_building_directory_screen.dart';
import 'package:gulshan_club_app/screens/gc_club_facilities_screen.dart';
import 'package:gulshan_club_app/screens/gc_contact.dart';
import 'package:gulshan_club_app/screens/gc_employee_directory.dart';
import 'package:gulshan_club_app/screens/gc_event_details_screen.dart';
import 'package:gulshan_club_app/screens/gc_events_screen.dart';
import 'package:gulshan_club_app/screens/gc_facilities_details.dart';
import 'package:gulshan_club_app/screens/gc_member_details.dart';
import 'package:gulshan_club_app/screens/gc_member_ledger_screen.dart';
import 'package:gulshan_club_app/screens/gc_members_directory_screen.dart';
import 'package:gulshan_club_app/screens/gc_messages_screen.dart';
import 'package:gulshan_club_app/screens/gc_notice_details.dart';
import 'package:gulshan_club_app/screens/gc_notice_page.dart';
import 'package:gulshan_club_app/screens/gc_mobile_number_screen.dart';
import 'package:gulshan_club_app/screens/gc_notifications_screen.dart';
import 'package:gulshan_club_app/screens/gc_obituary_screen.dart';
import 'package:gulshan_club_app/screens/gc_otp_screen.dart';
import 'package:gulshan_club_app/screens/gc_payment.dart';
import 'package:gulshan_club_app/screens/gc_profile_screen.dart';
import 'package:gulshan_club_app/screens/gc_settings.dart';
import 'package:gulshan_club_app/screens/gc_single_chat_screen.dart';
import 'package:gulshan_club_app/screens/gc_statements.dart';
import 'package:gulshan_club_app/screens/gc_welcome_screen.dart';
import 'package:gulshan_club_app/screens/memorandum_screen.dart';
import 'package:gulshan_club_app/screens/payments/bkash_payment_screen.dart';
import 'package:gulshan_club_app/screens/payments/city_bank_payment/city_bank_payment.dart';
import 'package:gulshan_club_app/screens/payments/gc_pay_for_other.dart';
import 'package:gulshan_club_app/screens/payments/gc_payment_method.dart';
import 'package:gulshan_club_app/screens/reservations/gc_reservation_screen.dart';
import 'package:gulshan_club_app/screens/splash_screen.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_font_size.dart';


import 'jsonModels/banquet/booking_list_response.dart';
import 'models/resource.dart';
import 'networking/network_manager.dart';

//receive message in background
Future <void> backgroundHandler(RemoteMessage message) async{
  print(message.data.toString());
  print(message.notification?.title);

}
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(backgroundHandler);


   // await Permission.camera.request();
  // await Permission.microphone.request();
  // await Permission.storage.request();

  // if (Platform.isAndroid) {
  //   await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  //
  //   var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
  //       AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
  //   var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
  //       AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);
  //
  //   if (swAvailable && swInterceptAvailable) {
  //     AndroidServiceWorkerController serviceWorkerController =
  //     AndroidServiceWorkerController.instance();
  //
  //     serviceWorkerController.serviceWorkerClient = AndroidServiceWorkerClient(
  //       shouldInterceptRequest: (request) async {
  //         print(request);
  //         return null;
  //       },
  //     );
  //   }
  // }

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _message = '';
  String fcmToken = '';
  String deviceId = '';
   DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
   Map<String, dynamic> deviceData = <String, dynamic>{};

  _registerOnFirebase() async {
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        print('Running on ${androidInfo.model}');
        setState(() {
          deviceId = androidInfo.androidId;
          Resources.deviceId = deviceId;
        });
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        print('Running on ${iosInfo.utsname.machine}');
        setState(() {
          deviceId = iosInfo.identifierForVendor;
          Resources.deviceId = deviceId;
        });
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    FirebaseMessaging.instance.getToken().then((token) {

      print("fcm token---- $token");
      setState(() {
        fcmToken = token ?? '';
      });

     });


  }
  @override
  void initState() {



    super.initState();
    LocalNotificationService.intialize(context);
    _registerOnFirebase();
    //gives the message
    //open app from terminated state
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if(message?.notification != null) {
        if(message?.notification != null){

          final routeFromMessage = message?.data["data"];
          print("Data--- $routeFromMessage");
          final routeFromMessageBody = message?.data["body"];
          print("routeFromMessageBody----$routeFromMessageBody");
        }
      }


    });

    //foreground state
    FirebaseMessaging.onMessage.listen((message) {
      if(message.notification != null){
       print(message.notification!.body);

       final routeFromMessage = message.data["data"];
       print("Data--- $routeFromMessage");
       final routeFromMessageBody = message.data["body"];
       print("routeFromMessageBody----$routeFromMessageBody");
      }
      LocalNotificationService.displayNotification(message);
    });

    //app is in background but opened and user taps
    //on the notification
    FirebaseMessaging.onMessageOpenedApp.listen((message) {

      final routeFromMessage = message.data["data"];
      print("Data--- $routeFromMessage");
      final routeFromMessageBody = message.data["body"];
      print("routeFromMessageBody----$routeFromMessageBody");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gulshan App',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: RouteConfiguration.onGenerateRoute,
      initialRoute: (Platform.isAndroid)? SplashScreen.route: WelcomeScreen.route,
      theme: ThemeData(
        fontFamily: 'Siri',
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
        hintColor: themeGolderColor,
        primaryColor: Colors.blue,
      ),
    );
  }
}


class RouteConfiguration {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {

      case '/splash':
        return MaterialPageRoute<void>(
          builder: (context) => SplashScreen(),
          settings: settings,
        );

      case '/welcome':
        return MaterialPageRoute<void>(
          builder: (context) => WelcomeScreen(),
          settings: settings,
        );

      case '/notifications':
        return MaterialPageRoute<void>(
          builder: (context) => NotificationsScreen(),
          settings: settings,
        );

      case '/take_away':
        return MaterialPageRoute<void>(
          builder: (context) => TakeAwayScreen(),
          settings: settings,
        );

      case '/products':
        return MaterialPageRoute<void>(
          builder: (context) => ProductsScreen(settings.arguments as FoodCategory),
          settings: settings,
        );

      case '/mobileNo':
        return MaterialPageRoute<void>(
          builder: (context) => MobileNumberScreen(),
          settings: settings,
        );

      case '/otp':
        return MaterialPageRoute<void>(
          builder: (context) => OTPScreen(),
          settings: settings,
        );

      case '/notice':
        return MaterialPageRoute<void>(
          builder: (context) => NoticeScreen(),
          settings: settings,
        );

      case '/memorandum':
        return MaterialPageRoute<void>(
          builder: (context) => MemorandumScreen(),
          settings: settings,
        );

      case '/notice_details':
        return MaterialPageRoute<void>(
          builder: (context) => NoticeDetailsScreen(),
          settings: settings,
        );

      case '/building_directory':
        return MaterialPageRoute<void>(
          builder: (context) => BuildingDirectoryScreen(),
          settings: settings,
        );

      case '/club_facilities':
        return MaterialPageRoute<void>(
          builder: (context) => ClubFacilitiesScreen(),
          settings: settings,
        );
      case '/reservation':
        return MaterialPageRoute<void>(
          builder: (context) => ReservationScreen(),
          settings: settings,
        );
      case '/halls':
        return MaterialPageRoute<void>(
          builder: (context) => BanquetListScreen(),
          settings: settings,
        );

      case '/banquet_events':
        return MaterialPageRoute<void>(
          builder: (context) => BanquetEventListScreen(),
          settings: settings,
        );
      case '/book_schedule':
        return MaterialPageRoute<void>(
          builder: (context) => BookScheduleScreen(),
          settings: settings,
        );

      case '/edit_booking':
        return MaterialPageRoute<void>(
          builder: (context) => EditBookingScreen(selectedEvent: settings.arguments as BookingDatum,),
          settings: settings,
        );//
      case '/facility_details':
        return MaterialPageRoute<void>(
          builder: (context) => FacilityDetails(),
          settings: settings,
        );

      case '/board_directory':
        return MaterialPageRoute<void>(
          builder: (context) => BoardDirectoryScreen(),
          settings: settings,
        );
      case '/members_directory':
        return MaterialPageRoute<void>(
          builder: (context) => MembersDirectoryScreen(),
          settings: settings,
        );

      case '/member_details':
        return MaterialPageRoute<void>(
          builder: (context) => MemberDetailsScreen(),
          settings: settings,
        );

      case '/obituary':
        return MaterialPageRoute<void>(
          builder: (context) => ObituaryScreen(),
          settings: settings,
        );

      case '/members_ledger':
        return MaterialPageRoute<void>(
          builder: (context) => MembersLedgerScreen(),
          settings: settings,
        );

      case '/events':
        return MaterialPageRoute<void>(
          builder: (context) => EventsScreen(),
          settings: settings,
        );

      case '/event_details':
        return MaterialPageRoute<void>(
          builder: (context) => EventDetailsScreen(),
          settings: settings,
        );


      case '/notice':
        return MaterialPageRoute<void>(
          builder: (context) => NoticeScreen(),
          settings: settings,
        );

      case '/payment':
        return MaterialPageRoute<void>(
          builder: (context) => PaymentScreen(),
          settings: settings,
        );

      case '/pay_for_others':
        return MaterialPageRoute<void>(
          builder: (context) => PayForOtherScreen(),
          settings: settings,
        );


      case '/payment_methods':
        return MaterialPageRoute<void>(
          builder: (context) => PaymentMethodScreen(),
          settings: settings,
        );

        //BkashPaymentScreen

      case '/bkash_payment':
        return MaterialPageRoute<void>(
          builder: (context) => BkashPaymentScreen(),
          settings: settings,
        );
        //CityPaymentScreen

      case '/citybank_payment':
        return MaterialPageRoute<void>(
          builder: (context) => CityPaymentScreen(),
          settings: settings,
        );

      case '/statements':
        return MaterialPageRoute<void>(
          builder: (context) => StatementsScreen(),
          settings: settings,
        );

      case '/messages':
        return MaterialPageRoute<void>(
          builder: (context) => MessagesScreen(),
          settings: settings,
        );

      case '/chat':
        return MaterialPageRoute<void>(
          builder: (context) => SingleChatScreen(),
          settings: settings,
        );

      case '/employee_directory':
        return MaterialPageRoute<void>(
          builder: (context) => EmployeeDirectoryScreen(),
          settings: settings,
        );

      case '/profile':
        return MaterialPageRoute<void>(
          builder: (context) => ProfileScreen(),
          settings: settings,
        );

      case '/settings':
        return MaterialPageRoute<void>(
          builder: (context) => SettingsScreen(),
          settings: settings,
        );

      case '/contact':
        return MaterialPageRoute<void>(
          builder: (context) => ContactScreen(),
          settings: settings,
        );

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(child: Text('No route defined for ${settings.name}')),
            ));
    }
  }
}

