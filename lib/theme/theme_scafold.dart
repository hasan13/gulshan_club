import 'dart:convert';

import 'package:badges/badges.dart' as Badge;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gulshan_club_app/jsonModels/notification_count_response.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import './theme_drawer.dart';
import './theme_style.dart';
import 'package:http/http.dart' as http;

class ThemeScaffold extends StatefulWidget {
  const ThemeScaffold({
    required this.body,
    required this.pageTitle, this.parentNotificationCount,
    this.elevation = 0.0,
    Key? key,
    this.bottomNavigationBar,
  }) : super(key: key);

  final Widget body;
  final Widget? bottomNavigationBar;
  final String pageTitle;
  final double elevation;
  final int? parentNotificationCount;

  @override
  _ThemeScaffoldState createState() => _ThemeScaffoldState();
}

class _ThemeScaffoldState extends State<ThemeScaffold> {
  @override
  Widget build(BuildContext context) {
    return _ThemeScaffold(
        body: widget.body,
        pageTitle: widget.pageTitle,
        elevation: widget.elevation, parentNotificationCounter: widget.parentNotificationCount,
        bottomNavigationBar: widget.bottomNavigationBar == null ? null : widget.bottomNavigationBar );
  }
}

class _ThemeScaffold extends StatefulWidget {
  _ThemeScaffold(
      {Key? key,
      required this.body,
      required this.pageTitle,
      required this.elevation,this.parentNotificationCounter,
      this.bottomNavigationBar})
      : super(key: key);

  final Widget body;
  final String pageTitle;
  final double elevation ;
  final Widget? bottomNavigationBar;
  final int? parentNotificationCounter;

  @override
  _ThemeScaffoldStates createState() => _ThemeScaffoldStates();
}

class _ThemeScaffoldStates extends State<_ThemeScaffold> {
  final GlobalKey<ScaffoldMessengerState> _scaffoldKey = GlobalKey<ScaffoldMessengerState>();
  bool isExpanded = true;
  bool isLoginMenuExpanded = false;
  int notificationCount = 9;
  late int tabIndex;
  List<Widget> listScreens = [];

  int itemCount = 0;

  // void _openEndDrawer() {
  //   _scaffoldKey.currentState.openEndDrawer();
  // }

  @override
  void initState() {
    super.initState();

    print("parentNotificationCounter");
    print(widget.parentNotificationCounter);
    if (super.widget.pageTitle == "Home") {
      print("home");
      getNotificationBadgeCount();
    } else {
      print("not home");
    }
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    final bool displayMobileLayout = MediaQuery.of(context).size.width < 1000;
    return Row(
      children: [
        if (!displayMobileLayout && isExpanded == true)
           AppDrawer(Key(''),
              true,
          ),
        Expanded(
          child: WillPopScope(
            onWillPop: _onWillPop,
            child: new Scaffold(
              backgroundColor: themeBlackColor,
              key: _scaffoldKey,
              appBar: new AppBar(
                centerTitle: true,
                backgroundColor: themeBlackColor,
                iconTheme: new IconThemeData(color: themeGolderColor),
                automaticallyImplyLeading: displayMobileLayout,
                elevation: widget.elevation > 0 ? widget.elevation : 0.0,
                leading: (!displayMobileLayout)
                    ? new TextButton.icon(
                        label: Text(''),
                        icon: Icon(Icons.menu_outlined,
                            size: 20, color: themeGolderColor),
                        onPressed: () {
                          setState(() {
                            isExpanded = !isExpanded;
                            isLoginMenuExpanded = !isLoginMenuExpanded;
                          });
                        },
                      )
                    : null,
                title: new Text(widget.pageTitle, style: ScaffoldAppBarTitle),
                actions: widget.pageTitle != "Home"
                    ? [SizedBox()]
                    : [
                        ( Resources.notificationItemCount  == 0)
                            ? IconButton(
                                onPressed: () {
                                  print('clicked');
                                  // Navigator.pushNamed(
                                  //     context, "/notifications");
                                  Navigator.pushNamed(context, '/notifications').then((_) => setState(() {}));
                                },
                                icon: SvgPicture.asset(
                                  "assets/icons/Notification with bell.svg",
                                  fit: BoxFit.fitWidth,
                                ),

                                //Icon(Icons.notifications_active),//Notification with bell.svg
                                color: themeGolderColor,
                              )

                            : Badge.Badge(
                                badgeContent: Text(
                                  itemCount.toString(),
                                  style: TextStyle(color: Colors.white),
                                ),
                                // position: BadgePosition.topStart(),
                                position: Badge.BadgePosition.topEnd(top: 0, end: 4),
                                // animationDuration: Duration(milliseconds: 300),
                                // badgeStyle: Badge.BadgeStyle.,
                                child: IconButton(
                                  onPressed: () {
                                    print('clicked');
                                    Navigator.pushNamed(context, '/notifications').then((_) => setState(() {}));

                                  },
                                  icon: SvgPicture.asset(
                                    "assets/icons/Notification.svg",
                                    fit: BoxFit.fitWidth,
                                  ),
                                  color: themeGolderColor,
                                ),
                              ),
                        // PopupMenuButton<String>(
                        //
                        //   offset: Offset(0,40),
                        //   icon: Icon(Icons.notifications_active),
                        //   onSelected: choiceAction,color: themeGolderColor,
                        //   itemBuilder: (BuildContext context) {
                        //     return VehicleConstants.choices.map((String choice) {
                        //       return PopupMenuItem<String>(
                        //         value: choice,
                        //         child: Text(choice,style: gcNormalTextStyle,),
                        //       );
                        //     }).toList();
                        //   },
                        // )
                      ],
              ),
              drawer: displayMobileLayout
                  ? new AppDrawer(Key(''), false)
                  : null,
              body: widget.body,
              bottomNavigationBar: widget.bottomNavigationBar,
            ),
          ),
        )
      ],
    );
  }

  Future<void> choiceAction(String choice) async {
    SharedPref.remove('user');
    await _navigateTo(context, '/welcome');
  }

  Future<void> _navigateTo(BuildContext context, String routeName) async {
    await Navigator.pushNamed(context, routeName);
  }

  getNotificationBadgeCount() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl +
        NetworkManager.apiString +
        NetworkManager.versionString +
        "/messages/notifications/count";
    print('url---> $url');
    print('Bearer ${Resources.currentUser?.accessToken}');

    if (result == true) {
      try {
        http
            .get(Uri.parse(url), headers: {
              "Authorization": "Bearer ${Resources.currentUser?.accessToken}"
            })
            .timeout(Duration(seconds: 30))
            .then((value) {
              print("status code Token:${value.statusCode}");
              print(value.body);
              final data = json.decode(utf8.decode(value.bodyBytes));
              final noticeResponse = NotificationCountResponse.fromJson(
                  data); //noticeResponseFromJson(data);
              print('result....\n ${noticeResponse.data}');

              if (value.statusCode == 401) {
                SharedPref.remove('user');

                Navigator.pushNamedAndRemoveUntil(
                    context, '/welcome', (route) => false);
                return;
              }
              if (value.statusCode == 200) {
                setState(() {
                  itemCount = noticeResponse.data ?? 0;
                  Resources.notificationItemCount = itemCount;
                  // notificationCount = 9;
                });
              } else if (data['status'] == false) {
                setState(() {});
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                setState(() {});
                final snackBar = SnackBar(
                  backgroundColor: Colors.red,
                  content: Text(
                      'Error-${value.statusCode}, ${NetworkManager.errorMessage}'),
                );
                _scaffoldKey.currentState?.showSnackBar(snackBar);
                //ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            });
      } catch (e) {
        print(e);
        setState(() {});
      }
    } else {
      print('No internet :( Reason:');

      final snackBar = SnackBar(
        backgroundColor: Colors.red,
        content: Text('Internet is not available!'),
      );
      setState(() {});
      // Find the Scaffold in the widget tree and use
      // it to show a SnackBar.
      _scaffoldKey.currentState?.showSnackBar(snackBar);
    }
  }
}

class VehicleConstants {
  static const String FirstItem = 'Logout';
  static const String SecondItem = 'hold';
  static const String ThirdItem = 'Draft';
  static const String FourthItem = 'Sold';

  static const List<String> choices = <String>[
    FirstItem,
  ];
}
