import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_font_size.dart';


var v1 = FontSizeCalc();
final  defaultElevation = 1.50;
var kLarge = 22.0 + v1.fontSize;
var kTitle = 18.0 + v1.fontSize;
var kMedium = 16.0 + v1.fontSize;
var kSmall = 14.0 + v1.fontSize;


var poppins14Regular = GoogleFonts.poppins(
    fontWeight: FontWeight.w400,fontSize: 14,color: listDescriptionColor
);

var gcTitleTextStyle = TextStyle(
  fontSize: kTitle,fontFamily: 'Siri-Regular',
  color:themeWhiteColor,
);
var gcTitleTextStyleBlack = TextStyle(
  fontSize: kTitle,fontFamily: 'Siri-Regular',
  color:themeBlackColor,
);
var gcPaymentAlertStyle = TextStyle(
  fontSize: kSmall,fontFamily: 'Siri-Regular',
  color:paymentAlertColor,
);
// GoogleFonts.poppins(
//   fontSize: 18,color: themeWhiteColor,
// );

var gcSubTitleTextStyle =  TextStyle(
  fontSize: kMedium,color: themeWhiteColor,fontFamily: 'Siri-Regular',
);

var gcNormalTextStyle = TextStyle(
  fontSize: kSmall,fontFamily: 'Siri-Regular',
  color:themeWhiteColor,
);


// GoogleFonts.poppins(
//   fontSize: 14,color: themeWhiteColor,
// );

var gcWhatsAppTextStyle = GoogleFonts.poppins(
  fontSize: 18,color: Colors.green,
);


var gcKLargeListTitleTextStyle = TextStyle(
  fontSize: kLarge,fontFamily: 'Siri-Medium',
  color:listTileColor,
);
var gcKLargeTitleTextStyle = TextStyle(
  fontSize: kLarge,fontFamily: 'Siri-Medium',
  color:themeWhiteColor,
);

var gcListTitleTextStyle = TextStyle(
  fontSize: kTitle,fontFamily: 'Siri-Medium',
  color:listTileColor,
);
// GoogleFonts.poppins(
//   fontSize: 17,color: listTileColor,
// );

var listSubTitleTextStyle = TextStyle(
  fontSize: kMedium,fontFamily: 'Siri-Medium',
  color:listSubTitleColor,
);
// GoogleFonts.poppins(
//   fontSize: 14,color: listSubTitleColor,
//);


var listSubTitleBlackTextStyle = TextStyle(
  fontSize: kMedium,fontFamily: 'Siri-Medium',
  color:themeBlackColor,
);

var gcDescriptionTextStyle = TextStyle(
  fontSize: kSmall,fontFamily: 'Siri-Regular',
  color:listDescriptionColor,
);
// GoogleFonts.poppins(
//     fontSize: 13,color: listDescriptionColor
// );


var gcGreyDescriptionTextStyle = TextStyle(
  fontSize: kSmall,fontFamily: 'Siri-Regular',
  color:themeGreyColor,
);

var gcPaymentDescriptionTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: kSmall,
  color:listDescriptionColor,
);
// GoogleFonts.poppins(
//     fontSize: 10,color: listDescriptionColor
// );

var gcUnderlinedTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
    fontSize: kSmall,color: themeGreyColor,decoration: TextDecoration.underline
);

var gcPlaceholderTextStyle =  TextStyle(
  fontSize: kSmall,fontFamily: 'Siri-Regular',
  color:themeGreyColor,
);

var gcBkashPlaceholderTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
    fontSize: kMedium,color: Colors.pink
);

var gcInputFieldTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
    fontSize: kMedium,color:Colors.white
);



var gcPaymentPlaceholderTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: 8,color: listDescriptionColor,
);

var gcPaymentTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: kSmall,color: listTileColor,
);

// GoogleFonts.poppins(
//   fontSize: 14,color: listSubTitleColor,
//);


var gcPaymentListTitleTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: kSmall,color: listTileColor,
);
/// scaffold > title
var ScaffoldAppBarTitle = TextStyle(
  fontSize: kLarge,fontFamily: 'Siri-Medium',
  color:themeWhiteColor,
);

/// scaffold > icon text
var ScaffoldAppBarIconText =  TextStyle(fontFamily: 'Siri-Regular',
  color: Color.fromRGBO(105, 105, 105, 1),
);

/// scaffold > drawer > navigation text
var ScaffoldAppBarDrawer =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: kSmall,
  color: Colors.white,
);

var normalBodyTextWithGreyColor =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: kSmall,
  color: Colors.grey,
);

var greenTitleTextStyle =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: kLarge,
  fontWeight: FontWeight.bold,
  color: Colors.lightGreen,
);

var buttonTitleStyle =  TextStyle(fontFamily: 'Siri-Regular',
  fontSize: kMedium,
  fontWeight: FontWeight.w300,
  color: Colors.white,
);

/// Card > Title
const CardTitle =  TextStyle(fontFamily: 'Siri-Regular',color: Color.fromRGBO(105, 105, 105, 1), fontSize: 15.0, fontWeight: FontWeight.bold);

// Input > focusBorder
const FormFocusBorder = OutlineInputBorder(
    borderSide: BorderSide(
      color: Color.fromRGBO(44, 160, 28, 1),
    ));

final InputBorderUnderlined = UnderlineInputBorder(
  //borderRadius: BorderRadius.circular(50),
  borderSide: BorderSide(color: Colors.green.withOpacity(0.5), width: 1),
);
// Input > labelColor
const FormInputLabel = TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 12.0);

// Input > labelStyle
const FormInputLabelStyle = TextStyle(color: Color.fromRGBO(105, 105, 105, 1), fontSize: 14.0);

// PopupMenuButton > Style
const PopupMenuItemTextStyle = TextStyle(
  fontSize: 14.0,
  color: Color.fromRGBO(105, 105, 105, 1),
);

// PopupMenuButton > Style
const PopupMenuItemHeight = 25.0;

// SubNavigationButton > Style > Non-Active
const subNavNonActiveTextStyle = TextStyle(fontWeight: FontWeight.normal);

// AlertDialog > title
const AlertDialogTitleTextStyle = TextStyle(color: Color.fromRGBO(105, 105, 105, 1), fontWeight: FontWeight.w500, fontSize: 20.0);
