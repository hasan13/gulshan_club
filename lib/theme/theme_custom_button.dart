import 'package:flutter/material.dart';

class MyCustomButton extends StatelessWidget {

  final VoidCallback onPressed;
  final Widget child;
  final double? width;
  final double? height;
  final Color? color;

  MyCustomButton({
    required this.onPressed,
    required this.child,
    this.width,
    this.height,
    this.color
  });


  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      child: child,
      color: color,
      minWidth: width,
      height: height,
      elevation: 1.5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      colorBrightness: Brightness.light,
      disabledColor: Colors.black26,
    );
  }
}