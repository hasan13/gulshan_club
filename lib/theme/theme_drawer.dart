import 'dart:async';
import 'dart:io';
import 'dart:ui';

 import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gulshan_club_app/models/resource.dart';
import 'package:gulshan_club_app/networking/network_manager.dart';
import 'package:gulshan_club_app/theme/theme_colors.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:gulshan_club_app/utils/shared_preference.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class AppDrawer extends StatefulWidget {
   AppDrawer(  Key? key, this.permanentlyDisplay)
      : super(key: key);

  final bool permanentlyDisplay;

  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> with RouteAware {
  bool isFavouritesSelected = false;
  int selected = -1;


  late Timer timer;

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    timer = Timer(Duration(seconds: 5), () {
      Navigator.of(context).pop();
    });

    return
        // new Container(
        // width: 250,height: MediaQuery.of(context).size.height,
        // child:
      BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: new Drawer(
              elevation: 20.0,
              child: new ListTileTheme(
                selectedTileColor: themeGreyColor,
                selectedColor: themeGolderColor,
                tileColor: (Platform.isAndroid)? Colors.transparent: Colors.black.withOpacity(0.6),
                textColor: themeWhiteColor,
                minVerticalPadding: 0.0,
                iconColor: themeGolderColor,
                child: Column(
                  children: [
                    new Container(
                      height: 170,
                      color: themeBlackColor,
                      child: new DrawerHeader(
                        padding: EdgeInsets.zero,
                        margin: EdgeInsets.zero,
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                                child: SvgPicture.asset(
                              'assets/icons/ic_logo.svg',
                              width: 80.0,
                              height: 65,
                            )),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      print("fb tapped");
                                    },
                                    child: Container(
                                        height: 26,
                                        width: 26,
                                        child: Image.asset(
                                            'assets/icons/icons8-facebook-48.png')),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      print("insta tapped");
                                    },
                                    child: Container(
                                        height: 26,
                                        width: 26,
                                        child: Image.asset(
                                            'assets/icons/icons8-instagram-48.png')),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      print("ln tapped");
                                    },
                                    child: Container(
                                        height: 26,
                                        width: 26,
                                        child: Image.asset(
                                            'assets/icons/icons8-linkedin-48.png')),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () async {
                                      String url =
                                          "https://www.youtube.com/c/GulshanClubLtd";
                                      if (await canLaunch(url)) {
                                        await launch(url);
                                      } else {
                                        throw 'Could not call ${16717}';
                                      }
                                    },
                                    child: Container(
                                        height: 26,
                                        width: 26,
                                        child: Image.asset(
                                            'assets/icons/icons8-youtube-play-button-48.png')),
                                  ),
                                ),
                              ],
                            ),

                          ],
                        ),
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: themeGreyColor,
                    ),
                    Expanded(
                      // child:
                      // // BackdropFilter(
                      // //   filter: ImageFilter.blur(sigmaX: 10,sigmaY: 10,tileMode: TileMode.mirror),
                        child:
                        Container(
                          decoration: BoxDecoration(
                            // color: themeBlackColor.withOpacity(0.6),
                            gradient: LinearGradient(
                                colors: [themeMenuBackgroundColor, themeMenuBackgroundColor2],

                                begin: const FractionalOffset(0.0, 0.0),
                                end: const FractionalOffset(0.0, 0.0),
                                stops: [0.0, 1.0],
                                tileMode: TileMode.mirror
                            ),
                          ),
                          child: NotificationListener(
                            onNotification: (ScrollNotification scrollInfo) {
                              if (timer != null && timer.isActive) {
                                timer.cancel();
                              }
                              return true;
                            },
                            child: new ListView(
                              padding: EdgeInsets.zero,
                              children: ListTile.divideTiles(
                                //          <-- ListTile.divideTiles
                                context: context, color: themeGreyColor,
                                tiles: [
                                  new ListTile(
                                    dense: true,
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_notice_board.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Notice',style: gcNormalTextStyle,),
                                    selected: (selected == 0) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 0;
                                      });
                                      await _navigateTo(context, '/notice');
                                    },
                                  ),

                                  new ListTile(
                                    dense: true,
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/Reservation Icon.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Reservations',style: gcNormalTextStyle,),
                                    selected: (selected == 0) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 0;
                                      });
                                      await _navigateTo(context, '/reservation');
                                    },
                                  ),

                                  if(Resources.appUser?.banquetBookingView == true)new ListTile(
                                    dense: true,
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    // tileColor: themeGolderBackColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/banquest.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Banquet Booking',style: gcNormalTextStyle,),
                                    selected: (selected == 0) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 0;
                                      });
                                      await _navigateTo(context, '/halls');
                                    },
                                  ),

                                  new ListTile(
                                    dense: true,
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/Takeaway.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Take Away',style: gcNormalTextStyle,),
                                    selected: (selected == 0) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      // await _navigateTo(context, '/take_away');
                                      // if (widget.permanentlyDisplay) {
                                      //   Navigator.pop(context);
                                      // }
                                      //
                                      // if (timer != null && timer.isActive) {
                                      //   timer.cancel();
                                      // }
                                      Navigator.pop(context);
                                      timer.cancel();

                                      Navigator.pushNamed(context, '/take_away');

                                    },
                                  ),

                                  new ListTile(
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_obituary.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeWhiteColor,
                                        )),
                                    title: Text(
                                      'Obituary',
                                      style: TextStyle(color: Colors.white,fontSize: kMedium),
                                    ),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/obituary');
                                    },
                                  ),
                                  // Divider(
                                  //   color: Colors.grey,
                                  //   height: 1,
                                  // ),
                                  new ListTile(
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_board_of_directory.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Board of Director',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/board_directory');
                                    },
                                  ),
                                  // Divider(
                                  //   color: Colors.blueGrey,
                                  //   height: 1,
                                  // ),

                                  new ListTile(
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_facilities.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Facilities',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/club_facilities');
                                    },
                                  ),


                                  new ListTile(
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_memeber_director.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Members Directory',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/members_directory');
                                    },
                                  ),
                                  // Divider(
                                  //   color: Colors.white10,
                                  //   height: 1,
                                  // ),


                                  new ListTile(
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_profile.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('My Account',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/profile');
                                    },
                                  ),
                                  new ListTile(
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_club_staff.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Employee Directory',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/employee_directory');
                                    },
                                  ),
                                  new ListTile(
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_events.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Events',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/events');
                                    },
                                  ),
                                  new ListTile(
                                    dense: true,
                                    contentPadding: EdgeInsets.only(
                                        top: 0.0, bottom: 0.0, left: 16),
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/Memorandum and Articles.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Memorandum and Articles',style: gcNormalTextStyle,),
                                    selected: (selected == 0) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 0;
                                      });
                                      await _navigateTo(context, '/memorandum');
                                    },
                                  ),

                                  new ListTile(
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/ic_contact.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Contact Us',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/contact');
                                    },
                                  ),
                                  new ListTile(
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: SvgPicture.asset(
                                          'assets/icons/Setting.svg',
                                          color: (selected == 0)
                                              ? themeGolderColor
                                              : themeGolderColor,
                                        )),
                                    title: Text('Settings',style: gcNormalTextStyle,),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      await _navigateTo(context, '/settings');
                                    },
                                  ),


                                  new ListTile(
                                    dense: true,
                                    hoverColor: themeGreyColor,
                                    leading: Container(
                                        height: 20,
                                        width: 20,
                                        child: Icon(Icons.logout, color: Colors.red)),
                                    title: Text(
                                      'Log out',
                                      style: TextStyle(color: Colors.red,fontSize: kMedium),
                                    ),
                                    selected: (selected == 1) ? true : false,
                                    onTap: () async {
                                      setState(() {
                                        selected = 1;
                                      });
                                      removeCurrentUserInfo();
                                      SharedPref.remove('user');
                                      await _navigateTo(context, '/welcome');
                                    },
                                  ),
                                ],
                              ).toList(),
                            ),
                          ),
                        ),
                      // ),
                    ),
                   ],
                ),
              )),
        );
  }

  Future<void> _navigateTo(BuildContext context, String routeName) async {
    if (widget.permanentlyDisplay) {
      Navigator.pop(context);
    }

    if (timer != null && timer.isActive) {
      timer.cancel();
    }
    //await Navigator.pushNamed(context, routeName);
    await Navigator.pushNamedAndRemoveUntil(
        context, routeName, (route) => false);
  }

  removeCurrentUserInfo() async {
    bool result = await InternetConnectionChecker().hasConnection;
    var url = NetworkManager.baseUrl + NetworkManager.apiString + "/v1/token/blacklist?token=${Resources.currentUser?.accessToken}";
    print('url---> $url');
    if (result == true) {
      try {
        http.get(Uri.parse(url), ).timeout(Duration(seconds: 30)).then((value) {
          print("status code Token:${value.statusCode}");
          print(value.body);
          if (value.statusCode == 200 || value.statusCode == 202) {
            setState(() {
              isLoading = false;
            });
            //navigateToSignInPage(context);
            print('YAY! Free cute dog pics!');
          } else {
            setState(() {
              isLoading = false;
            });
          }
        });
      } catch (e) {
        print(e);
        setState(() {
          isLoading = false;
        });
      }
    } else {
      print('No internet :( Reason:');
 
    }
  }



}
