import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gulshan_club_app/theme/theme_style.dart';
import 'package:intl/intl.dart';

class MyInputField extends StatefulWidget {

  final TextEditingController controller;
  final String? labelText;
  final double? width;
  final EdgeInsets? padding;
  final VoidCallback? onComplete;
  final FocusNode? focusNode;
  final TextInputAction? inputAction;
  final IconData? icon;
  final String? hint;
  final bool? obscure;
  final bool? required;
  final int? maxLength;
  final int? maxLines;
  final int? minLines;
  final TextInputType? type;
  final FormFieldValidator? validator;
  final FormFieldSetter? onSaved;
  final bool? rounded;
  final VoidCallback? onTap;
  final bool? enabled;
  final bool? readOnly;


  MyInputField({
    required this.controller,
    required this.labelText,
    this.width,
    this.padding,
    this.onComplete,
    this.focusNode,
    this.inputAction,
    this.icon,
    this.hint,
    this.obscure,
    this.maxLength,
    this.type,
    this.validator,
    this.onSaved,
    this.rounded = true,
    this.onTap,
    this.enabled, this.readOnly, this.maxLines, this.minLines, this.required
  });


  @override
  _MyInputFieldState createState() => _MyInputFieldState();
}

class _MyInputFieldState extends State<MyInputField> {

  final InputBorder inputBorder = UnderlineInputBorder(
    //borderRadius: BorderRadius.circular(50),
    borderSide: BorderSide(color: Color(0xBFA465), width: 1),
  );

  final InputBorder inputBorderEdge = UnderlineInputBorder(
    //borderRadius: BorderRadius.circular(2),
    borderSide: BorderSide(color:  Color(0xBFA465), width: 0),
  );

  final InputBorder errorBorderEdge = UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.transparent, width: 0),
  );

  final TextStyle fieldStyle = gcPlaceholderTextStyle;
//   TextStyle(
// //      fontSize: 15,
//     color: Colors.grey.withOpacity(0.5),
// //      letterSpacing: 1.3
//   );

  final TextStyle focusedLabelStyle = TextStyle(
//      fontSize: 12,
    color: Colors.green,
//      letterSpacing: 1.0
  );

  final TextStyle normalLabelStyle = TextStyle(
//      fontSize: 12,
    color: Colors.black.withOpacity(0.5),
//      letterSpacing: 1.0
  );

  final TextStyle requiredLabelStyle = TextStyle(
//      fontSize: 12,
    color: Colors.red,
//      letterSpacing: 1.0
  );

 late  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    if ((widget.focusNode == null)) {
     } else {
      _focusNode = widget.focusNode!;
    }
    ;
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
    if (widget.onTap != null) {
      widget.onTap;
    }
  }

  Future<void> _setDate() async {
    setState(() {
      _focusNode.requestFocus();
      FocusScope.of(context).requestFocus(_focusNode);
    });

    if (widget.type == TextInputType.datetime) {

      DateFormat inputFormat;
      DateTime previouslySelectedDate = DateTime.now();
      if(widget.controller.text != ""){

        inputFormat = DateFormat("MM/dd/yyyy");
        previouslySelectedDate= inputFormat.parse(widget.controller.text);
        print(previouslySelectedDate);
      }

      final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: previouslySelectedDate  ,
        firstDate: DateTime(2000),
        lastDate: DateTime(2025),
        fieldLabelText: 'Booking date',
        fieldHintText: 'Month/Date/Year',
      );

      if (picked != null && picked != DateTime.now()) {
        setState(() {
          FocusScope.of(context).requestFocus(_focusNode);
          String dateSlug = "${picked.month.toString()}/${picked.day.toString()
              .padLeft(2, '0')}/${picked.year.toString().padLeft(2, '0')}";
          widget.controller.text = dateSlug
              .toString(); //toUtc().toString();//picked.toLocal().toString();
        });
      } else {
        setState(() {
          FocusScope.of(context).requestFocus(FocusNode());
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      padding: (widget.padding == null) ? const EdgeInsets.fromLTRB(
          0, 4, 16, 4) : widget.padding,
      decoration: widget.rounded == false?
      BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 0.8,
                offset: Offset(0.5, 1.0),
                spreadRadius: 1
            )
          ]
      ) : null,
      child:
      TextFormField(
        enabled: widget.enabled,
        controller: widget.controller,
        readOnly: (widget.readOnly == null) ? false:widget.readOnly! ,
        onEditingComplete: widget.onComplete,
        focusNode: _focusNode,
        inputFormatters: ( widget.type == TextInputType.number)?<TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly,
        ]:[],        //widget.focusNode,
        textInputAction: (widget.inputAction == null)
            ? TextInputAction.next
            : widget.inputAction,
        decoration: InputDecoration(
            prefixIcon: (widget.icon == null) ? null :
            Icon(
              widget.icon,
              color: (_focusNode.hasFocus) ? Colors.green : (widget.enabled == false)?Colors.grey :Colors.grey,
              size: 25,
            ),
//          ),
            enabledBorder: inputBorderEdge,
            //rounded ? inputBorderEdge : inputBorderEdge,
            border: widget.rounded == true ? inputBorder : inputBorderEdge,
            hintText: widget.hint ,
            labelText: (widget.required == true)?widget.labelText! + "\*": widget.labelText,//widget.labelText,
//            suffixText: (widget.isRequired == true)? "*": "",
//            suffixStyle: TextStyle(
//              color: Colors.red,
//            ),
            labelStyle: (_focusNode.hasFocus)
                ? focusedLabelStyle
                :gcPlaceholderTextStyle,// normalLabelStyle,
            hintStyle: fieldStyle,
            focusedBorder: inputBorder,
            //rounded ? inputBorder : inputBorderEdge,
            counterText: '',
            contentPadding: EdgeInsets.fromLTRB(5, 10, 5, 6),
            //vertical: 8, horizontal: 5
            errorStyle: TextStyle(
              color: Colors.red,
              fontSize: 10.0,
            ),
            errorBorder: widget.rounded  == true? null : errorBorderEdge
        ),
        style: (widget.enabled == false)?gcPlaceholderTextStyle:gcPlaceholderTextStyle,//fieldStyle.copyWith(color: Colors.black) :fieldStyle.copyWith(color: Colors.black),
        cursorColor: Colors.green,
        cursorWidth: 3,

        cursorRadius: Radius.circular(5),
        obscureText: widget.obscure ?? false,
        maxLength: widget.maxLength,
        minLines: widget.minLines ?? null,
        //(widget.obscure != true)?
        keyboardType: (widget.type == null) ? TextInputType.text : widget.type,
        maxLines: widget.maxLines ?? 1,
        //widget.maxLines,
        validator: widget.validator,
        onSaved: widget.onSaved,
        onTap: (widget.onTap == null)
            ? _requestFocus
            : _setDate, //widget.onTap,

      ),
    );
  }
}