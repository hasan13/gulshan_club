class MyCustomValidator{


  static String? validateIsEmpty(value, type, context){
    if (value.isEmpty && type == 'userID') {
      return 'Please type your user ID here';
    }

    return null;
  }

  static String? validateMultiLine(value, context){
    if (value.isEmpty) {
      return 'Please type something here';
    }

    return null;
  }

  static String? validateEmail(value, context){
    bool valid = RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$').hasMatch(value);
    if(!valid==true){
      return   'Please enter a valid Email';
    }
    return null;
  }

  static String? phoneNumberValidation(value, context){

    if (value.length != 14){
      return   'Please enter a valid number';
    }
    return null;
  }

}