import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


final themeWhiteColor =  Color(0xFFFFFFFF);
final themeGreyColor =  Color(0xFF8D8D8D);
final themeMenuBackgroundColor =  Color(0xE6252525);
final themeMenuBackgroundColor2 =  Color(0xA6333333);
final themeBlackColor = Color(0xFF272727);
final themeGolderColor =   Color(0xFFBFA465);//CABEA4
final themeGolderBackColor =   Color(0xFFCABEA4);//CABEA4
final themeGrayBackColor =   Color(0xFF444444);//CABEA4
final themeGrayIconBackColor =   Color(0xFF5E5E5E);//CABEA4

final listTileColor = Color(0xFFB18E4E);
final listDescriptionColor = Color(0xFFCABEA4);
final listSubTitleColor = Color(0xFFCABEA4);
final paymentAlertColor = Color(0xFF999999);
final detailsBackground = Color(0xFFC6BDAE);
final callBackground = Color(0xFF302D2D);
